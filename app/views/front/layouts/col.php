<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><?php echo CHtml::encode(Yii::app()->name . " / " . $this->pageData->name);?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="language" content="ru" />
        <meta name="description" content="<?php echo CHtml::encode($this->pageData->description); ?>">
        <meta name="keywords" content="<?php echo CHtml::encode($this->pageData->keywords); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/static/css/style.css" media="screen, projection" />
        <?php Yii::app()->bootstrap->register(); ?>
        <?php Yii::app()->clientScript->registerCoreScript('jquery');?>
        <style>
            body{
                background: #fff;
            }
        </style>
    </head>
    <body>
    	<div id="wrapper">
            <div id="header">
                <div class="navbar navbar-fixed-top">
                    <div class="navbar-inner">
                        <a class="brand" href="/">PlumCMF</a>
                        <?php $menu = Yii::app()->getModule('content')->getMainMenu();?>
                        <ul class="nav">
                            <?php foreach($menu as $key=>$item):?>
                            <li>
                                <a href="<?=$item['url_path'];?>" <?php if ($item['active']): ?>class="active"<?php endif; ?>><?=$item['name']?></a>
                            </li>
                            <?php endforeach;?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span8">
                    <div id="content">
                        <?php echo $content;?>
                    </div>
                </div>
                <div class="span4">
                    <div class="page-header">
                        <h3>Реклама на сайте</h3>
                    </div>
                    <?php echo Yii::app()->getModule('advbanners')->getBanner('adv_place_top', $this->pageData->adv_sections_id);?>
                    <?php $bottom_banner = Yii::app()->getModule('advbanners')->getBanner('adv_place_bottom', $this->pageData->adv_sections_id);?>
                    <?php if($bottom_banner):?>
                        <hr>
                        <?php echo $bottom_banner;?>
                    <?php endif;?>
                </div>
            </div>
            <div id="empty"></div>
        </div>
        <div id="footer">
            <div id="footer-content">
                <ul class="nav nav-footer pull-left">
                    <?php foreach($menu as $key=>$item):?>
                    <li>
                        <a href="<?=$item['url_path'];?>" <?php if ($item['active']): ?>class="active"<?php endif; ?>><?=$item['name']?></a>
                    </li>
                    <?php endforeach;?>
                </ul>
                <div class="pull-right copy">
                    <a href="http://www.astrikov.ru" target="_blank">Дмитрий Астриков</a> / <a href="mailto:info@plumcmf.com">info@plumcmf.com</a><br/>
                    <strong><a href="/">PlumCMF</a> &copy; 2012</strong>
                </div>
            </div>
        </div>
    </body>
</html>