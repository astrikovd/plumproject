<!DOCTYPE html>
<html>
<head>
  <title>
    simpleCRM / Страница не найдена
  </title>
  <style>
    body {
      background: #1F0046;
      color: #E8D6FF;
      font-family: Arial;
    }

    #error_wrapper {
      position: relative;
      width: 400px;
      height: 200px;
      margin: 200px auto 0px;
      font-size: 13px;
      text-align: center;
      top: 50%;
    }

    #error_wrapper h1 {
      font-size: 150px;
      margin: 20px 0px;
      font-weight: normal;
    }

    a {
      color: #E8D6FF;
    }

    a:hover {
      text-decoration: none;
    }
  </style>
</head>
<body>
<div id="error_wrapper">
  <h1>404</h1>

  <p>Страница с таким адресом не найдена. Возможно, когда то она была, но сейчас её точно нет.</p>

  <p>Проверьте правильность адреса страницы. Спасибо.</p>

  <p><a href="/">Вернуться на главную</a></p>
</div>
</body>
</html>