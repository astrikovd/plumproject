<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><?php echo CHtml::encode($this->pageTitle);?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="language" content="ru" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/static/css/style.css" media="screen, projection" />
        <?php Yii::app()->bootstrap->register(); ?>
    </head>
    <body>
    	<?php if ($this->breadcrumbs) :?>
                    <?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
                        'links'=>$this->breadcrumbs,
                    )); ?>
      <?php endif; ?>
      <?php
      $this->widget('bootstrap.widgets.TbAlert', array(
          'block'=>    true,      // display a larger alert block?
          'fade' =>    true,      // use transitions?
          'closeText'=>'&times;', // close link text - if set to false, no close link is displayed
        )
      );
      ?>
        <?php echo $content;?>
    </body>
</html>