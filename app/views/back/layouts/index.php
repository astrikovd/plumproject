<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title><?php echo CHtml::encode(Yii::app()->name . " / Панель администрирования сайта");?></title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="language" content="ru" />
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/static/css/style.css" media="screen, projection" />
  <?php Yii::app()->bootstrap->register(); ?>
</head>
<body>
<div id="id-main-menu">
  <?php $menu = Yii::app()->getModule('content')->getMainMenu(Yii::app()->user->id);?>
  <?php $this->widget('bootstrap.widgets.TbNavbar', array(
    'type'=>null,                                       // null or 'inverse'
    'brand'=>'PlumCMF',
    'brandUrl'=> Yii::app()->params['back_url'].'/',
    'collapse'=>true,                                   // requires bootstrap-responsive.css
    'items'=>array(
      array(
        'class'=>'bootstrap.widgets.TbMenu',
        'items'=> empty($menu) ? array() : $menu
      ),
      array(
        'class'=>'bootstrap.widgets.TbMenu',
        'htmlOptions'=>array('class'=>'pull-right'),
        'items'=>
        array(
          array(
            'label'=>'Выход',
            'url'=> Yii::app()->params['back_url'].'/auth/user/logout/'
          ),
        ),
      ),
    ),
  )); ?>
</div>
<div id="wrapper">
  <div id="content">
    <?php if ($this->breadcrumbs) :?>
      <?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
        'links'=>$this->breadcrumbs,
      )); ?>
    <?php endif; ?>
    <?php
    $this->widget('bootstrap.widgets.TbAlert', array(
        'block'=>    true,      // display a larger alert block?
        'fade' =>    true,      // use transitions?
        'closeText'=>'&times;', // close link text - if set to false, no close link is displayed
      )
    );
    ?>
    <?php echo $content;?>
  </div>
  <div id="empty"></div>
</div>
<div id="footer">
  <div id="copy">
    <strong><a href="http://www.plumcmf.com" target="_blank">PlumCMF</a> &copy; 2012</strong><br/>
    <a href="mailto:info@plumcmf.com">info@plumcmf.com</a>
  </div>
</div>
</body>
</html>