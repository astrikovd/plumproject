<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
  'id' => 'newsfeed-form',
  'enableAjaxValidation' => false,
)); ?>

<div class="alert alert-info">
  Поля, отмеченные <span class="red">*</span>, обязательны для заполнения.
</div>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->labelEx($model, 'newscat_id'); ?>
<?php echo $form->dropDownList($model, 'newscat_id', $newscats, array('class' => 'span5')); ?>

<?php echo $form->textFieldRow($model, 'name', array('class' => 'span5', 'maxlength' => 128)); ?>

<?php echo $form->textFieldRow($model, 'cnt', array('class' => 'span5')); ?>

<?php echo $form->checkBoxRow($model, 'vflag'); ?>

<div class="form-actions">
  <a class="btn btn-default" href="javascript:history.back();"><i class="icon icon-chevron-left"></i> Назад</a>
  <?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
    'type' => 'primary',
    'label' => $model->isNewRecord ? 'Добавить' : 'Сохранить',
  )); ?>
</div>

<?php $this->endWidget(); ?>
