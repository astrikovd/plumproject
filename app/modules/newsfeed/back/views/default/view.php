<?php
$this->breadcrumbs=array(
	'Новостные ленты'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'Список','url'=>array('index')),
	array('label'=>'Добавление','url'=>array('create')),
	array('label'=>'Изменение','url'=>array('update','id'=>$model->newsfeed_id)),
	array('label'=>'Удаление','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->newsfeed_id),'confirm'=>'Вы уверены? Это действие нельзя будет отменить')),
	array('label'=>'Управление','url'=>array('admin')),
);
?>

<div class="page-header">
	<h4>Новостная лента «<?php echo $model->name; ?>»</h4>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'name',
		'cnt',
		array('name' =>'Включена', 'value' => $model->vflag ? "Да" : "Нет"),
		array('name' => 'date0', 'value' => date('d.m.Y H:i:s', strtotime($model->date0))),
	),
)); ?>
