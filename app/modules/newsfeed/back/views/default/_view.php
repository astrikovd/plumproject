<div class="view">

  <b><?php echo CHtml::encode($data->getAttributeLabel('newsfeed_id')); ?>:</b>
  <?php echo CHtml::link(CHtml::encode($data->newsfeed_id), array('view', 'id' => $data->newsfeed_id)); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('newscat_id')); ?>:</b>
  <?php echo CHtml::encode($data->newscat_id); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
  <?php echo CHtml::encode($data->name); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('vflag')); ?>:</b>
  <?php echo CHtml::encode($data->vflag); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('date0')); ?>:</b>
  <?php echo CHtml::encode($data->date0); ?>
  <br/>


</div>