<?php
$this->breadcrumbs = array(
  'Новостные ленты' => array('index'),
  'Управление',
);

$this->menu = array(
  array('label' => 'Список', 'url' => array('index')),
  array('label' => 'Добавление', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('newsfeed-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="page-header">
  <h4>Управление новостными лентами</h4>
</div>

<div class="alert alert-info">
  <strong>Данный раздел предназначен для быстрого поиска и изменения новостных лент.</strong>
  Доступны функции добавления, удаления и редактирования.
</div>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
  'id' => 'newsfeed-grid',
  'dataProvider' => $model->search(),
  'filter' => $model,
  'columns' => array(
    'name',
    array(
      'class' => 'bootstrap.widgets.TbButtonColumn',
    ),
  ),
)); ?>
