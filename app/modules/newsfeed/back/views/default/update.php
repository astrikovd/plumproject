<?php
$this->breadcrumbs = array(
  'Новостные ленты' => array('index'),
  $model->name => array('view', 'id' => $model->newsfeed_id),
  'Изменение',
);

$this->menu = array(
  array('label' => 'Список', 'url' => array('index')),
  array('label' => 'Добавление', 'url' => array('create')),
  array('label' => 'Просмотр', 'url' => array('view', 'id' => $model->newsfeed_id)),
  array('label' => 'Управление', 'url' => array('admin')),
);
?>

  <div class="page-header">
    <h4>Изменение новостной ленты «<?php echo $model->name; ?>»</h4>
  </div>

<?php echo $this->renderPartial('_form', array('model' => $model, 'newscats' => $newscats)); ?>