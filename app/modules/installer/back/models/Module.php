<?php
/**
 * Copyright (c) 2012 by Dmitry Astrikov / www.astrikov.ru
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Module model for install/view and etc
 * @package modules/installer
 */
class Module extends AdmCmp
{
  /**
   * @var string $type
   */
  public $type;
  /**
   * @var string $category
   */
  public $category;
  /**
 * @var string $category_name
 */
  public $category_name;
  /**
   * @var string $install_mysql_file
   */
  public $install_mysql_file;
  /**
   * @var string $install_pgsql_file
   */
  public $install_pgsql_file;

  public function rules()
  {
    return CMap::mergeArray( parent::rules(), array(
      array('install_mysql_file,install_pgsql_file', 'safe'),
      array('category,category_name, type,description', 'length', 'allowEmpty'=> true , 'max' => 255 ),
    ));
  }

  protected function beforeSave()
  {
    if($this->getIsNewRecord())
    {
      $adm_cmp_id  = Yii::app()->db->createCommand( 'SELECT COALESCE(MAX(adm_cmp_id), 100)+1 FROM '.$this->tableName())->queryColumn();
      $adm_cmp_id  = array_pop($adm_cmp_id);
      if($this->type!='system')
      {
        if($adm_cmp_id<100)
        {
          $adm_cmp_id=101;
        }
      }
      $this->adm_cmp_id = $adm_cmp_id;
    }
    return parent::beforeSave();
  }
}
