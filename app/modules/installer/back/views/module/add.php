<?php
$this->breadcrumbs = array(
  'Модули системы'      => array('/installer/'),
  'Управление модулями' => array('/installer/module'),
  'Установить модуль'
);
CPlumViewHelper::requirePartial('_menu', $this);
?>

  <div class="page-header">
    <h4>Установить загруженый модуль</h4>
  </div>

  <div class="alert alert-info">
    <strong>Данный раздел предназначен для управления уже загруженными модулями системы.
    Доступны функции установки и настройки дополнительных модулей системы.</strong>
  </div>
<?php
$this->widget('bootstrap.widgets.TbGridView', array(
  'dataProvider' => $dataProvider,
  'id'=>'modules-add-grid',
  'template'=>'{summary} {items}<div class="pull-right">{pager}</div>',
  'itemsCssClass' => 'table table-striped table-condensed table-hover',
  'columns' => array(
      array(
        'name'            => 'char_id',
        'header'          => 'Системное наименование <small>(совпадает с директорией модуля)</small>.',
      ),
      array(
        'header'           => 'Действия',
        'class'            => 'bootstrap.widgets.TbButtonColumn',
        'template'         => "{alert} {view} {install} {delete}",
        'buttons'          => array(
          'alert' => array(
            'visible' => '$data["install_info"] == false',
            'label'   => 'Не обнаружено данных для автоматической установки. Устанавливая модуль вручную вы действуете на свой страх и риск!',
            'icon'    => 'warning-sign',
            'url'     =>  '',
          ),
          'view' => array(
            'visible' => '$data["install_info"] != false',
            'label'   => 'Просмотреть данные о модуле',
            'icon'    => 'eye-open',
            'url'     =>  '"view/module/".$data["char_id"]."/"',
          ),
          'install' => array(
            'label'   => 'Установить модуль',
            'icon'    => 'plus',
            'url'     =>  '"install/module/".$data["char_id"]."/"',
          ),
          'delete' => array(
            'label'   => 'Удалить загруженный модуль',
            'icon'    => 'remove',
            'url'     =>  '"delete/module/".$data["char_id"]."/"',
          ),
        ),
        'htmlOptions'          => array('style' => 'width: 60px'),
      )
    )
  )
);