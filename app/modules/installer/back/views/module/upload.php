<?php
$this->breadcrumbs = array(
  'Модули системы'      => array('/installer/'),
  'Управление модулями' => array('/installer/module'),
  'Загрузить модуль'
);
CPlumViewHelper::requirePartial('_menu', $this);
?>

  <div class="page-header">
    <h4>Загрузить модуль</h4>
  </div>

  <div class="alert alert-info">
    <strong>Папка с модулем должена быть запакована в ZIP архив.</strong> <br/>
    После успешной распаковки файлов будет предложено установить модуль.
  </div>
<?php echo $this->renderPartial('../_partials/_form_module_upload', array('model' => $model) ); ?>