<?php
$this->breadcrumbs = array(
  'Модули системы'      => array('/installer/'),
  'Управление модулями' => array('/installer/module'),
  'Установить модуль'
);
CPlumViewHelper::requirePartial('_menu', $this);
?>

  <div class="page-header">
    <h4>Установить модуль</h4>
  </div>

  <div class="alert alert-info">
    <strong>Данный раздел предназначен для управления уже загруженными модулями системы.
    Доступны функции установки и настройки дополнительных модулей системы.</strong>
  </div>
