<?php
$this->breadcrumbs = array(
  'Модули системы'      => array('/installer/'),
  'Управление модулями' => array('/installer/module'),
  'Просмотр данных модуля'
);
CPlumViewHelper::requirePartial('_menu', $this);
?>

  <div class="page-header">
    <h4>Установочные данные модуля</h4>
  </div>

  <div class="alert alert-info">
    <strong>Раздел позволяет просмотреть детальную информацию о модуле и подтвердить необходимости установки модуля.</strong>
  </div>
<?php
$this->widget('bootstrap.widgets.TbDetailView', array(
  'data' => $model,
  'attributes' => array(
    array(
      'label' => 'Тип модуля',
      'name'  => 'type',
      'value' => $model->type=='user' ? "Пользовательский модуль" : "Системный модуль"
    ),
    array(
      'label' => 'ID категории',
      'name'  => 'adm_cmp_grp_id',
    ),
    array(
      'label' => 'Код категории',
      'name'  => 'category',
    ),
    array(
      'label' => 'Категория модуля',
      'name'  => 'category_name'
    ),
    array(
      'label' => 'Системное имя модуля',
      'name'  => 'char_id'
    ),
    array(
      'label' => 'Название модуля',
      'name'  => 'name'
    ),
    array(
      'label' => 'Описание модуля',
      'name'  => 'description'
    ),
    array(
      'label' => 'Инсталлирующий файл для MySql',
      'name'  => 'install_mysql_file',
      'value' => !empty($model->install_mysql_file) ? "Присутствует" : "Нет",
    ),
    array(
      'label' => 'Инсталлирующий файл для Postgres',
      'name'  => 'install_pgsql_file',
      'value' => !empty($model->install_pgsql_file) ? "Присутствует" : "Нет",
    ),
  ),
));
?>
<div class="form-wrapper">
  <form class="form-vertical" id="redirect-form" type="get" action="/installer/module/install/module/<?php echo $model->char_id;?>/">
  <div class="form-actions">
    <a class="btn btn-default" href="javascript:history.back();">
      <i class="icon icon-chevron-left"></i> Назад
  </a>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
    'type' =>  'primary',
    'label' => 'Установить',
  )); ?>
  </div>
  </form>
</div>