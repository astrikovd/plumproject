<?php
$this->breadcrumbs = array(
  'Модули системы'      => array('/installer/'),
  'Управление модулями' => array('/installer/module'),
  'Список модулей системы'
);
CPlumViewHelper::requirePartial('_menu', $this);
?>

  <div class="page-header">
    <h4>Модули системы</h4>
  </div>

  <div class="alert alert-info">
    Данный раздел предназначен для управления модулями системы.
    Доступны функции установки, загрузки и удаления дополнительных модулей системы.
    <strong>Часть модулей являются системными и не поддаются изменению.</strong>
  </div>
<?php
$this->widget('bootstrap.widgets.TbGridView', array(
  'dataProvider' => $dataProvider,
  'id'=>'modules-grid',
  'template'=>'{summary} {items}<div class="pull-right">{pager}</div>',
  'itemsCssClass' => 'table table-striped table-condensed table-hover',
  'columns' => array(
      array(
        'name'            => 'adm_cmp_id',
        'header'          => 'ID',
        'htmlOptions'     => array('style' => 'width: 30px')
      ),
      array(
        'name'            => 'admCmpGrp.name',
        'sortable'        => true,
        'header'          => 'Раздел',
        'htmlOptions'     => array('style' => 'width: 80px')
      ),
      array(
        'name'            => 'char_id',
        'header'          => 'Директория',
        'htmlOptions'     => array('style' => 'width: 50px')
      ),
      array(
        'name'            => 'name',
        'header'          => 'Название',
        'htmlOptions'     => array('style' => 'width: 120px')
      ),
      array(
        'name'            => 'description',
        'header'          => 'Описание модуля',
        'htmlOptions'     => array('style' => 'width: 180px')
      ),
      array(
        'header'           => 'Действия',
        'class'            => 'bootstrap.widgets.TbButtonColumn',
        'template'         => "{alert} {edit} {delete}",
        'buttons'          => array(
          'alert' => array(
            'visible' => '$data->adm_cmp_id < 100',
            'icon'    => 'warning-sign',
            'label'   => 'Системный модуль, нельзя изменять',
            'url'     =>  '',
          ),
          'edit'=> array(
            'visible' => '$data->adm_cmp_id > 100',
            'icon'    => 'pencil',
            'label'   => 'Изменить модуль',
            'url'     =>  '',

          ),
          'delete'=> array(
            'visible' => '$data->adm_cmp_id > 100',
            'icon'    => 'remove',
            'label'   => 'Удалить установленный модуль',
            'url'     =>  '',
          ),
        ),
        'htmlOptions' => array('style' => 'width: 45px'),
      )
    )
  )
);