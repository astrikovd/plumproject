<?php
$this->breadcrumbs = array(
  'Модули системы'    => array('/installer'),
  'Категории модулей' => array('/installer/cat'),
  'Редактировать категорию'
);
CPlumViewHelper::requirePartial('_menu', $this);
?>

  <div class="page-header">
    <h4>Редактировать категорию</h4>
  </div>

  <div class="alert alert-info">
    <strong>Данный раздел позволяет изменить категорию модулей (отображается в верхнем меню).</strong>
  </div>
<?php echo $this->renderPartial('../_partials/_form_cat', array('model' => $model) ); ?>