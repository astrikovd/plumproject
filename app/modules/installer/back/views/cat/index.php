<?php
$this->breadcrumbs = array(
  'Модули системы'    => array('/installer'),
  'Категории модулей' => array('/installer/cat'),
  'Список категорий'
);
CPlumViewHelper::requirePartial('_menu', $this);
?>

<div class="page-header">
  <h4>Категории модулей и меню администрирования</h4>
</div>

<div class="alert alert-info">
  Данный раздел предназначен для управления категориями модулей системы, которые отображаются в главном меню административного интерфейса.
  <strong>Часть категорий модулей являются системными и не поддаются изменению.</strong>
</div>
<?php
$this->widget('bootstrap.widgets.TbGridView', array(
  'dataProvider' => $dataProvider,
  'id'=>'modules-cats-grid',
  'template'=>'{items}<div class="pull-right">{pager}</div>',
  'itemsCssClass' => 'table table-striped table-condensed table-hover',
  'columns' => array(
      array(
        'name'            => 'adm_cmp_grp_id',
        'header'          => 'ID',
        'htmlOptions'     => array('style' => 'width: 30px')
      ),
      array(
        'name'            => 'char_id',
        'sortable'        => true,
        'header'          => 'Символьный идентификатор',
        'htmlOptions'     => array('style' => 'width: 200px')
      ),
      array(
        'name'            => 'name',
        'header'          => 'Название категории (раздела административного меню)',
      ),
      array(
        'header'           => 'Действия',
        'class'            => 'bootstrap.widgets.TbButtonColumn',
        'template'         => "{update} {alert} {delete}",
        'buttons'          => array(
          'update'=> array(
            'url'     =>  '"edit/id/".$data->adm_cmp_grp_id',
            'icon'    => 'pencil'
          ),
          'alert' => array(
            'visible' => '$data->adm_cmp_grp_id<=8',
            'label'   => 'Системная категория, нельзя удалить',
            'url'     =>  '',
            'icon'    => 'warning-sign'
          ),
          'delete'=> array(
            'visible' => '$data->adm_cmp_grp_id>8',
            'icon'    => 'remove',
            'url'     => '"delete/id/".$data->adm_cmp_grp_id'
          )
        ),
        'htmlOptions'          => array('style' => 'width: 40px'),
      )
    )
  )
);