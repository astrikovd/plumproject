<?php
/** @var $form TbActiveForm */
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
  'id' => 'module-upload-form',
  'enableAjaxValidation' => false,
  'htmlOptions' => array('enctype' => 'multipart/form-data')
)); ?>

  <div class="alert alert-info">
    Поля, отмеченные <span class="red">*</span>, обязательны для заполнения.
  </div>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->labelEx($model,'archive'); ?>
<?php echo $form->fileField($model,'archive', array('class' => 'span5')); ?>

  <div class="form-actions">
    <a class="btn btn-default" href="javascript:history.back();">
      <i class="icon icon-chevron-left"></i> Назад
    </a>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
      'buttonType' => 'submit',
      'type' =>  'primary',
      'label' => 'Загрузить',
    )); ?>
  </div>

<?php $this->endWidget(); ?>