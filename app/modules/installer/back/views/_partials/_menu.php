<?php
if( $context instanceof CBaseController )
{
  $context->menu = array(
    array(
      'label'          => 'Категории модулей',
      'url'            => '#',
      'submenuOptions' => array(

      ),
      'items'          => array(
          array(
            'label'      => 'Список категорий модулей',
            'url'        =>  array('/installer/cat/index'),
          ),
          array(
            'label'      => 'Добавить категорию модулей',
            'url'        =>  array('/installer/cat/add'),
          ),
      ),
    ),
    array(
      'label'          => 'Модули системы',
      'url'            => "#",
      'submenuOptions' => array(

      ),
      'items'          => array(
          array(
            'label' => 'Список модулей',
            'url'   => array('/installer/module/index'),
          ),
          array(
            'label' => 'Установить модуль',
            'url'   => array('/installer/module/add'),
          ),
          array(
            'label' => 'Загрузить модуль',
            'url'   => array('/installer/module/upload'),
          ),
      ),
    ),
  );
}