<?php

/**
 * Copyright (c) 2012  by Dmitry Astrikov / www.astrikov.ru
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Installer for modules
 * @package modules/installer
 */
class CatController extends BackEndController
{

  /**
   * @var default layout
   */
  public $layout = '//layouts/column';

  /**
   * Index action
   * @return void
   */
  public function actionIndex()
  {
    $this->view->bind('dataProvider', new CActiveDataProvider( AdmCmpGrp::model() ) );
    $this->render('index');
  }

  /**
   * Add cat action
   * @return void
   */
  public function actionAdd()
  {
    $model = new AdmCmpGrp();

    $this->performAjaxValidation($model);

    if (!isset($_POST['ajax']) && isset($_POST['AdmCmpGrp']) ) {
      $model->attributes = $_POST['AdmCmpGrp'];
      if ($model->save()) {
        $this->redirect('/installer/cat/index');
      }
    }


    $this->view->bind('model',  $model );
    $this->render('add');
  }
  /**
   * Edit cat action
   * If update is successful, the browser will be redirected to the 'cats' page.
   * @param integer $id the ID of the model to be updated
   */
  public function actionEdit( $id )
  {
    $model = $this->loadModel( (int) $id );

    $this->performAjaxValidation($model);

    if (!isset($_POST['ajax']) && isset($_POST['AdmCmpGrp'])) {
      $model->attributes = $_POST['AdmCmpGrp'];
      if ($model->save()) {
        $this->redirect('/installer/cat/index');
      }
    }

    $this->view->bind('model',  $model );
    $this->render('edit');
  }
  /**
   * Deletes cat action.
   * If deletion is successful, the browser will be redirected to the 'admin' page.
   * @param integer $id the ID of the model to be deleted
   * @throws CHttpException
   * @return void
   */
  public function actionDelete( $id )
  {
    if (Yii::app()->request->isPostRequest) {
      // we only allow deletion via POST request
      $this->loadModel($id)->delete();

      // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
      if (!isset($_GET['ajax']))
        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('/installer/cat/index'));
    } else
      throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
  }

  /**
   * Returns the data model based on the primary key given in the GET variable.
   * If the data model is not found, an HTTP exception will be raised.
   * @param int $id
   * @throws CHttpException
   * @return \CActiveRecord
   */
  public function loadModel($id)
  {
    $model = AdmCmpGrp::model()->findByPk($id);
    if ($model === null)
      throw new CHttpException(404, 'The requested page does not exist.');
    return $model;
  }

}