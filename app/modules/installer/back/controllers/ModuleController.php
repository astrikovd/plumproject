<?php

/**
 * Copyright (c) 2012  by Dmitry Astrikov / www.astrikov.ru
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Installer for modules
 * @package modules/installer
 */
class ModuleController extends BackEndController
{

  /**
   * @var default layout
   */
  public $layout = '//layouts/column';

  /**
   * Index action
   * @return void
   */
  public function actionIndex()
  {
    $model = AdmCmp::model();

    if( isset($_POST['AdmCmp']) )
    {
      $model->attributes = $_POST['AdmCmp'];
    }

    $dataProvider = $model->search();

    $this->view->bind('model',  $model );
    $this->view->bind('dataProvider', $dataProvider );

    $this->render('index');
  }

  /**
   * Upload action
   * @return void
   */
  public function actionUpload()
  {
    $model =  new ModuleArchive();
    if( isset($_POST['ModuleArchive']) )
    {
      $model->attributes=$_POST['ModuleArchive'];
      $model->archive=CUploadedFile::getInstance( $model,'archive' );
      if( $model->validate() ){
        $tmp_name = Yii::app()->runtimePath. 'tmp.module.zip';
        if($model->archive->saveAs( $tmp_name ))
        {
          $dir     = YiiBase::getPathOfAlias('application.modules');
          $unziped = Yii::app()->zip->extractZip( $tmp_name, $dir );

          if( $unziped )
          {
            @unlink( $tmp_name );
            $this->redirect('/installer/module/add');
          }
        }
        else{
          Yii::app()->user->setFlash('error', 'Не удалось сохранить архив');
        }
      }
    }
    $this->view->bind('model', $model);
    $this->render('upload');
  }

  /**
   * Add action
   * @return void
   */
  public function actionAdd()
  {
    $filesystem_modules = array();
    $installed_modules  = array();

    $filesystem_modules = glob( YiiBase::getPathOfAlias('application.modules') . DIRECTORY_SEPARATOR. '*', GLOB_ONLYDIR);
    foreach ($filesystem_modules as $key => $module)
    {
      $filesystem_modules[$key] = basename($module);
    }

    $all_modules = AdmCmp::model()->findAll();
    foreach ($all_modules as $module)
    {
      $installed_modules[] = $module->char_id;
    }

    $modules = array_diff($filesystem_modules,$installed_modules);
    $data    = array();
    foreach( $modules AS $module )
    {
      $data[] = CMap::mergeArray(
        array( 'char_id'=> $module ),
        $this->getModuleInstallInfo( $module )
      );
    }
    $this->view->bind('dataProvider',
      new CArrayDataProvider(
        $data,
        array(
          'keyField'  => 'char_id')
      )
    );

    $this->render('add');
  }

  /**
   * View  action
   * @param $module
   * @throws CHttpException
   * @return void
   */
  public function actionView( $module )
  {
    $model = $this->getModuleModel($module);
    $this->view->bind('model', $model);
    $this->render('view');
  }


  /**
   * Install action
   * @param $module
   * @return void
   */
  public function actionInstall( $module )
  {
    $model = $this->getModuleModel($module);
    $installer_module = Yii::app()->getModule('installer');

    $installed_db = true;
    if($installer_module->prepareMigration($model))
    {
      $installed_db = $installer_module->applyLastMigration();
    }

    $info = array();
    if($installed_db)
    {
      $info[]='База даных для модуля '.$module.' успешно подготовлена!';
    }

    if($model->save())
    {
      Yii::app()->user->setFlash('success', 'Модуль '.$module.' успешно сохрен!');
    }
    else{
      Yii::app()->user->setFlash('error', 'Ошибка сохранения модуля '.$module.'!');
    }

    Yii::app()->user->setFlash('info', implode('<br/>',$info));

    $this->redirect('/installer/module');
  }

  /**
   * Delete action - delete module directory, don`t uninstall module.
   * @param $module
   * @return void
   */
  public function actionDelete( $module )
  {
    if (Yii::app()->request->isPostRequest) {
      $modules_dir = YiiBase::getPathOfAlias('application.modules');
      $full_path   = $modules_dir . DIRECTORY_SEPARATOR . $module;
      CPlumFileHelper::recursiveRemoveDirectory( $full_path );
      // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
      if (!isset($_GET['ajax']))
        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('/installer/cat/index'));
    }
    else
    {
      throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }
  }

  /**
   * @param $module
   * @return Module
   * @throws CHttpException
   */
  private function getModuleModel($module)
  {
    $installInfo = $this->getModuleInstallInfo($module);
    $data = $installInfo['paths']['install_file'];
    $data = file_get_contents($data);
    $data = CJSON::decode($data);

    $module_cat = $data['category'];
    $group = AdmCmpGrp::model()->findByAttributes(array('char_id' => $module_cat));
    if (!$group) {
      throw new CHttpException("500","В установочном файле модуля указана неизвестная категория '{$module_cat}' модуля! Создайте требуемую категорию или измените установочный файл '{$installInfo['paths']['install_file']}'.");
    } else {
      $data['adm_cmp_grp_id'] = $group->getAttribute('adm_cmp_grp_id');
      $data['category_name'] = $group->getAttribute('name');
    }

    $data['char_id'] = $module;
    $data['install_mysql_file'] = $installInfo['paths']["install_mysql"];
    $data['install_pgsql_file'] = $installInfo['paths']["install_pgsql"];
    $model = new Module();
    $model->attributes = $data;
    return $model;
  }

  private function getModuleInstallInfo( $module )
  {
    $install_info_file    = Yii::app()->basePath. DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . $module . DIRECTORY_SEPARATOR . '_install'. DIRECTORY_SEPARATOR . 'install.data.json';
    $install_mysql_file   = Yii::app()->basePath. DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . $module . DIRECTORY_SEPARATOR . '_install'. DIRECTORY_SEPARATOR . 'install.mysql.php';
    $install_pgsql_file   = Yii::app()->basePath. DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . $module . DIRECTORY_SEPARATOR . '_install'. DIRECTORY_SEPARATOR . 'install.pgsql.php';

    return array(
      'install_info'=> file_exists($install_info_file),
      'install_db'  => file_exists($install_mysql_file) && file_exists($install_pgsql_file),
      'paths'       => array(
        'install_file'    => $install_info_file,
        'install_mysql'   => $install_mysql_file,
        'install_pgsql'   => $install_pgsql_file,
      )
    );
  }


}