<?php
/**
 * Copyright (c) 2012  by Dmitry Astrikov / www.astrikov.ru
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Installer module
 * @package modules/installer
 */
class InstallerModule extends CPlumModule
{
  /**
   * Module initialization
   * @return void
   */
  public function init()
  {
    $endName = Yii::app()->endName;
    $this->setImport(array(
      "installer.{$endName}.interfaces.*",
      "installer.{$endName}.components.*",
      "installer.{$endName}.models.*",
      "user.{$endName}.models.*",
      "ext.zip.*",
    ));
    Yii::app()->setComponent('zip', array('class'=>'ext.zip.EZip'));
    Yii::app()->onModuleCreate(new CEvent($this));
  }

  public function prepareMigration( Module $module )
  {
    $db_driver = Yii::app()->db->driverName;
    $install_file = "install_{$db_driver}_file";
    $install_file = $module->$install_file;
    if(file_exists($install_file))
    {
      $migrations_path = $this->getMigrationPath();
      $migrations      = glob($migrations_path.'*.php');

      $first_migration = $last_migration = 0;

      if(count($migrations))
      {
        $last_migration = basename(array_pop($migrations));
        $last_migration = preg_replace("/.*(\d{6}\_\d{6}).*/","$1",$last_migration);
        $migrations     = explode('_', $last_migration);
        $last_migration = (int) array_pop($migrations);
        $first_migration = (int) array_pop($migrations);
      }

      $name = 'mFFFFFF_LLLLLL_module_'.$module->getAttribute('char_id');
      $name = str_replace( array('FFFFFF','LLLLLL'), array(
        $this->fullNum($first_migration),
        $this->fullNum($last_migration+1),
      ), $name);

      $newMigrationPath    = $migrations_path.$name.'.php';
      $newMigrationContent = file_get_contents($install_file);
      $newMigrationContent = str_replace('migration_name',$name,$newMigrationContent);

      return file_put_contents($newMigrationPath,$newMigrationContent);
    }
    else{
      return false;
    }
  }

  public function getMigrationPath()
  {
    $db_driver = Yii::app()->db->driverName;
    return Yii::app()->basePath.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'var'.DIRECTORY_SEPARATOR.'cli'.DIRECTORY_SEPARATOR.'dbmigrate'.DIRECTORY_SEPARATOR.'migrations'.DIRECTORY_SEPARATOR.$db_driver.DIRECTORY_SEPARATOR;
  }

  public function applyLastMigration()
  {
    $consolePath = realpath($this->getMigrationPath().DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..').DIRECTORY_SEPARATOR;
    $baseCmd = 'php '.$consolePath.'yiic.php contextmigrate ';
    exec( $baseCmd.' autoup '.Yii::app()->db->driverName.' ',$ret);
    return $ret;
  }

  private function fullNum($num, $cnt=6)
  {
    $str = '';
    for($i=strlen((string)$num);$i<$cnt;$i++)
    {
      $str.='0';
    }
    $str.=$num;

    return $str;
  }

}
