<?php

/**
 * Copyright (c) 2012 by Dmitry Astrikov / www.astrikov.ru
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Content model (DAO)
 * @package modules/content/back/models
 */
class Content
{
  /**
   * Get Main menu for backend
   * @param integer $user_id
   * @return array
   */
  public static function getMainMenu($user_id, $request_uri)
  {
    //select all non empty cmp groups for backend
    $pref = Yii::app()->db->tablePrefix;
    $q = "SELECT *, char_id AS grp_char_id, NAME AS grp_name
        FROM {$pref}adm_cmp_grp acg
        WHERE (SELECT COUNT(*) FROM {$pref}adm_cmp ac WHERE ac.adm_cmp_grp_id = acg.adm_cmp_grp_id) > 0
        ORDER BY adm_cmp_grp_id";
    $command = Yii::app()->db->createCommand($q);
    $adm_cmp_grps = $command->queryAll();

    //select all user components for backend
    $q = "SELECT
      `ac`.`adm_cmp_id` AS `adm_cmp_id`,
      `ac`.`char_id` AS `cmp_char_id`,
      `ac`.`name` AS `cmp_name`,
      `acg`.`adm_cmp_grp_id` AS `adm_cmp_grp_id`,
      `acg`.`char_id` AS `grp_char_id`,
      `acg`.`name` AS `grp_name`,
      `auac`.`adm_user_id` AS `adm_user_id`
    FROM ((`{$pref}adm_cmp` `ac` JOIN `{$pref}adm_cmp_grp` `acg`) JOIN `{$pref}adm_user_adm_cmp` `auac`)
    WHERE (
      (`ac`.`adm_cmp_grp_id` = `acg`.`adm_cmp_grp_id`)
        AND
      (`ac`.`adm_cmp_id` = `auac`.`adm_cmp_id`)
        AND
      (`auac`.`adm_user_id` = :user_id)
        AND
      (`acg`.`adm_cmp_grp_id` != 7)
    )
    ORDER BY `ac`.`adm_cmp_id`";

    $command = Yii::app()->db->createCommand($q);
    $command->bindParam(":user_id", $user_id);
    $user_cmps = $command->queryAll();

    foreach ($adm_cmp_grps as $grp) {
      $active = false;
      $items = array();
      foreach ($user_cmps as $cmp) {
        if ($cmp['grp_char_id'] == $grp['grp_char_id']) {
          if (!$active)
              $active = (strpos($request_uri, $cmp['cmp_char_id']) !== false) ? true : false;
              $items[] = array('label' => $cmp['cmp_name'],
                'url' => Yii::app()->params['back_url'] . "/{$cmp['cmp_char_id']}/");
        }
      }
      if (!empty($items)) {
        $result[] = array('label' => $grp['grp_name'],
          'url' => '#', 'items' => $items, 'active' => $active);
      }
    }

    //return result array
    return $result;
  }
}