<?php

/**
 * Copyright (c) 2012 by Dmitry Astrikov / www.astrikov.ru
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Content model (DAO)
 * @package modules/content/front/models
 */
class Content
{
  /**
   * Get Main menu for frontend
   * @param integer $user_id
   * @return array
   */
  public static function getMainMenu($user_id, $request_uri)
  {
    $pref = Yii::app()->db->tablePrefix;
    $q = "SELECT * FROM {$pref}page WHERE parent_id = 1 AND vflag AND eflag AND NOT aflag ORDER BY sort ASC";
    $command = Yii::app()->db->createCommand($q);
    $pages = $command->queryAll();

    $menu_items = array();

    foreach ($pages as $key => $page) {
      $active = false;
      if (strpos($request_uri, $page['url_path']) !== false && !$active) {
        $active = true;
      }
      $menu_items[] = array('url_path' => ($page['redirect_url'] != '') ? $page['redirect_url'] : $page['url_path'],
        'name' => $page['name'],
        'active' => $active);
    }
    return $menu_items;
  }
}