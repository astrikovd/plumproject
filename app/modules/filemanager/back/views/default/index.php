<?php
    $this->breadcrumbs=array(
        'Файловый менеджер',
    );
?>

<div class="page-header">
    <h4>Файловый менеджер</h4>
</div>

<div class="alert alert-info">
  <p>С помощью этого раздела Вы можете загружать файлы на сервер для их последующего использования на сайте.</p>
</div>

<div id="file-uploader"></div>

<?php

$filesPath = realpath(Yii::app()->basePath . "/../front/data/user/");
$filesUrl = Yii::app()->params['front_url'] . "/data/user/";

$this->widget("ext.ezzeelfinder.ElFinderWidget", array(
    'selector' => "div#file-uploader",
    'clientOptions' => array(
        'lang' => "ru",
        'resizable' => false
    ),
    'connectorRoute' => "/filemanager/default/fileUploaderConnector",
    'connectorOptions' => array(
        'roots' => array(
            array(
                'driver'  => "LocalFileSystem",
                'path' => $filesPath,
                'URL' => $filesUrl,
                'tmbPath' => $filesPath . DIRECTORY_SEPARATOR . ".thumbs",
                'mimeDetect' => "internal",
                'accessControl' => "access"
            )
        )
    )
));