<?php

/**
 * Copyright (c) 2012 by Dmitry Astrikov / www.astrikov.ru
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Default page module controller
 * @package modules/page
 */
class DefaultController extends FrontEndController
{

  /**
   * Home page
   * @throws CHttpException
   * @return void
   */
  public function actionHome()
  {
    if (empty($this->params))
      throw new CHttpException(404, 'The requested page does not exist.');
    $last_news = Newsitem::model()->findAll(array('condition' => 'vflag = true', 'order' => 'date0 desc', 'limit' => 3));
    $this->render("index", array('news' => $last_news, 'aspam' => CVerifyCode::create()));
  }

  /**
   * Static pages
   * @throws CHttpException
   * @return void
   */
  public function actionStatic()
  {
    $this->layout = '//layouts/col';

    $page = Page::model()->find(array('condition' => 'url_path = :url_path',
      'params' => array('url_path' => Yii::app()->request->requestUri)));

    if ($page === null)
      throw new CHttpException(404, 'The requested page does not exist.');

    $this->render("static", array('page' => $page));
  }
}
