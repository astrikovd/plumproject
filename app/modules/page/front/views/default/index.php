<?php Yii::app()->controller->module->registerResource('page', 'homepage.js') ?>
<?php Yii::app()->controller->module->registerResource('page', 'jquery.form.js') ?>

<div class="row-fluid">
  <div class="span8">
    <div class="page-header">
      <h3>PlumCMF - Демонстрационная версия</h3>
    </div>
    <div class="alert alert-info">
      Вы находитесь на сайте, предназначенном для демонстрации возможностей системы управления контентом PlumCMF.
      Для доступа к панели администрирования сайта перейдите по ссылке <a href="http://demo.plumcmf.com">http://demo.plumcmf.com</a>.
    </div>
    <?php if (!empty($news)): ?>
      <div class="page-header">
        <h3>Последние новости</h3>
      </div>
      <div>
        <?php foreach ($news as $key => $item): ?>
          <div>
            <h5><a href="/news/<?=$item['newsitem_id']?>"><?=$item['name'];?></a></h5>
            <?=$item['anons'];?>
          </div>
        <?php endforeach;?>
      </div>
    <?php endif;?>
  </div>
  <div class="span4">
    <div class="page-header">
      <h3>Реклама на сайте</h3>
    </div>
    <?php echo Yii::app()->getModule('advbanners')->getBanner('adv_place_top', $this->pageData->adv_sections_id);?>
    <?php $bottom_banner = Yii::app()->getModule('advbanners')->getBanner('adv_place_bottom', $this->pageData->adv_sections_id);?>
    <?php if ($bottom_banner): ?>
      <hr>
      <?php echo $bottom_banner; ?>
    <?php endif;?>
  </div>
</div>