<?php

/**
 * This is the model class for table "prj_feedback".
 *
 * The followings are the available columns in table 'prj_feedback':
 * @property integer $prj_feedback_id
 * @property string $email
 * @property integer $rate
 * @property string $body
 * @property string $date_send
 */
class PrjFeedback extends CPlumActiveRecord
{
  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return PrjFeedback the static model class
   */
  public static function model($className = __CLASS__)
  {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName()
  {
    return $this->getTablePrefix() . 'prj_feedback';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules()
  {
    // NOTE: you should only define rules for those attributes that
    // will receive user inputs.
    return array(
      array('rate, date_send', 'required'),
      array('rate', 'numerical', 'integerOnly' => true),
      array('email', 'length', 'max' => 255),
      array('body', 'safe'),
      // The following rule is used by search().
      // Please remove those attributes that should not be searched.
      array('prj_feedback_id, email, rate, body, date_send', 'safe', 'on' => 'search'),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations()
  {
    // NOTE: you may need to adjust the relation name and the related
    // class name for the relations automatically generated below.
    return array();
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels()
  {
    return array(
      'prj_feedback_id' => 'Prj Feedback',
      'email' => 'Email',
      'rate' => 'Rate',
      'body' => 'Body',
      'date_send' => 'Date Send',
    );
  }

  /**
   * Retrieves a list of models based on the current search/filter conditions.
   * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
   */
  public function search()
  {
    // Warning: Please modify the following code to remove attributes that
    // should not be searched.

    $criteria = new CDbCriteria;

    $criteria->compare('prj_feedback_id', $this->prj_feedback_id);
    $criteria->compare('email', $this->email, true);
    $criteria->compare('rate', $this->rate);
    $criteria->compare('body', $this->body, true);
    $criteria->compare('date_send', $this->date_send, true);

    return new CActiveDataProvider($this, array(
      'criteria' => $criteria,
    ));
  }
}