<?php

/**
 * Copyright (c) 2012 by Dmitry Astrikov / www.astrikov.ru
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * This is the model class for table "page".
 *
 * The followings are the available columns in table 'page':
 * @property integer $page_id
 * @property integer $parent_id
 * @property string $page_module_id
 * @property string $page_module_param
 * @property string $url_path
 * @property string $url_name
 * @property string $redirect_url
 * @property string $name
 * @property string $header
 * @property string $before_content
 * @property string $after_content
 * @property integer $sort
 * @property boolean $vflag
 * @property boolean $aflag
 * @property boolean $hflag
 * @property boolean $eflag
 * @property string $date0
 *
 * The followings are the available model relations:
 * @property Page $parent
 * @property Page[] $pages
 * @property PageModule $pageModule
 */
class Page extends CPlumActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Page the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return $this->getTablePrefix().'page';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('page_module_id, sort, adv_sections_id, name, header, url_name', 'required'),
			array('parent_id, sort', 'numerical', 'integerOnly'=>true),
			array('page_module_id', 'length', 'max'=>16),
			array('page_module_param', 'length', 'max'=>32),
			array('url_path, url_name, redirect_url', 'length', 'max'=>512),
			array('url_name', 'match', 'pattern' => '/^[a-zA-Z0-9_-]{1,}$/u'),
			array('name, header', 'length', 'max'=>128),
			array('before_content, after_content, vflag, aflag, hflag, eflag, date0, keywords, description', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('page_id, parent_id, page_module_id, page_module_param, url_path, url_name, redirect_url, name, header, before_content, after_content, sort, vflag, aflag, hflag, eflag, date0', 'safe', 'on'=>'search'),
		);
	}


	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'parent' => array(self::BELONGS_TO, 'Page', 'parent_id'),
			'pages' => array(self::HAS_MANY, 'Page', 'parent_id'),
			'pageModule' => array(self::BELONGS_TO, 'PageModule', 'page_module_id'),
		);
	}


	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'parent_id' => 'Родительская страница',
			'page_module_id' => 'Функциональный модуль',
			'url_name' => 'Адрес страницы',
			'url_path' => 'Полный URL страницы',
			'adv_sections_id' => 'Рекламный раздел',
			'redirect_url' => 'Перенаправление на страницу',
			'name' => 'Название страницы',
			'header' => 'Заголовок страницы',
			'before_content' => 'Содержимое до контента',
			'after_content' => 'Содержимое после контента',
			'sort' => 'Сортировка',
			'vflag' => 'Отображать в меню',
			'aflag' => 'Только для зарегистрированных пользователей',
			'hflag' => 'Отображать заголовок на странице',
			'eflag' => 'Включена',
			'keywords' => 'SEO - Ключевые слова',
			'description' => 'SEO - Описание страницы',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->condition='page_id!=1';

		$criteria->compare('page_id',$this->page_id);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('page_module_id',$this->page_module_id,true);
		$criteria->compare('page_module_param',$this->page_module_param,true);
		$criteria->compare('url_path',$this->url_path,true);
		$criteria->compare('url_name',$this->url_name,true);
		$criteria->compare('redirect_url',$this->redirect_url,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('header',$this->header,true);
		$criteria->compare('before_content',$this->before_content,true);
		$criteria->compare('after_content',$this->after_content,true);
		$criteria->compare('sort',$this->sort);
		$criteria->compare('vflag',$this->vflag);
		$criteria->compare('aflag',$this->aflag);
		$criteria->compare('hflag',$this->hflag);
		$criteria->compare('eflag',$this->eflag);
		$criteria->compare('date0',$this->date0,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

  public function behaviors()
  {
    return array(
      'ChangedActiveRecordAttributesBehavior'
    );
  }


  //TODO: move pgsql function page_modify to model logic and clear pgsql dont needly functions
  protected function beforeSave()
  {
    $is_new      = $this->getIsNewRecord();
    $parent_id  = $this->getAttribute('parent_id');
    $res        = Yii::app()->db->createCommand( 'SELECT * FROM '.$this->tableName().' WHERE page_id ='.(int)$parent_id );
    $parent_row = $res->queryAll();
    if(empty($parent_row)){
      throw new CException('Parent not exists - '.$parent_id);
    }
    $parent_path = (Yii::app()->db->createCommand( 'SELECT url_path FROM '.$this->tableName().' WHERE page_id = '.(int)$parent_id.' AND page_id IS NOT null')->queryColumn());
    $parent_path = array_pop($parent_path);
    $sort        = (Yii::app()->db->createCommand( 'SELECT COALESCE(MAX(sort), 0)+1 FROM '.$this->tableName().' WHERE parent_id = '.(int)$parent_id.'')->queryColumn());
    $sort        = array_pop($sort);

    $url_path    = $parent_path ? $parent_path : '/';
    $url_path_new    = $this->getAttribute('url_name') ?  $this->getAttribute('url_name') : $url_path;
    $url_path_new    = strpos($url_path_new,$url_path)!==0 ? '/'.$url_path_new : $url_path_new;
    $url_path    = $url_path.$url_path_new;

    if($is_new)
    {
      $this->sort =  $sort;
      $this->url_path = $url_path;
    }
    else{
      $oldAttrs = $this->getOldAttributes();
      $newAttrs = $this->getAttributes();
      if( $newAttrs['url_path']!=$oldAttrs['url_path']  && (
             strpos($newAttrs['url_path'], $oldAttrs['url_path'])!==false
                 ||
             strpos($newAttrs['url_path'], '%')!==false )
        ) {
          throw new CException('can\'t move node: '.$oldAttrs['url_path'].' => '.$newAttrs['url_path']);
      }
    }
    return parent::beforeSave();
  }

  protected function afterSave()
  {
    $is_new      = $this->getIsNewRecord();
    if(!$is_new)
    {
      $oldAttrs = $this->getOldAttributes();
      $changedAttrs = $this->getChangedAttributes();
      if(isset($changedAttrs['sort']))
      {
        $parent_id  = $this->getAttribute('parent_id');

        $criteria  = new CDbCriteria();
        $criteria->condition = "parent_id = :parent_id AND sort ";
        $criteria->params    = array(
          ':parent_id' => $parent_id
        );

        if( ($changedAttrs['sort']-$oldAttrs['sort']) == ($oldAttrs['sort']-1) )
        {
          $criteria->order     = 'url_path DESC';
          $criteria->condition .= " AND sort > :sort";
          $criteria->params[':sort'] = $oldAttrs['sort'];
          $ord = '-1';
        }
        else
        {
          $criteria->order     = 'url_path ASC';

          $criteria->condition .= " AND sort < :sort";
          $criteria->params[':sort'] = $oldAttrs['sort'];
          $ord = '+1';
        }

        $pages = Page::model()->count($criteria);

        echo 'all before '.$ord.' where $parent_id = '.$parent_id;
        var_dump($pages);
        if($pages>0)
        {
          Page::model()->updateAll(array(
            'sort' => new CDbExpression(' sort '. $ord)
          ), $criteria);
          return;
        }

        $criteria->condition = "parent_id = :parent_id AND sort ";

        if( ($changedAttrs['sort']-$oldAttrs['sort']) == ($oldAttrs['sort']-1) )
        {
          $criteria->order     = 'url_path DESC';
          $criteria->condition .= " AND sort < :sort";
          $criteria->params[':sort'] = $oldAttrs['sort'];
          $ord = '+1';
        }
        else
        {
          $criteria->order     = 'url_path ASC';

          $criteria->condition .= " AND sort > :sort";
          $criteria->params[':sort'] = $oldAttrs['sort'];
          $ord = '-1';
        }

        $pages = Page::model()->count($criteria);
        var_dump($pages);

        echo 'all after '.$ord.' where $parent_id = '.$parent_id;
        if($pages>0)
        {
          Page::model()->updateAll(array(
            'sort' => new CDbExpression(' sort '. $ord)
          ), $criteria);
        }
        die('die!');
      }
    }
    return parent::afterSave();
  }
  protected function beforeDelete()
  {
    return parent::beforeDelete();
  }
}