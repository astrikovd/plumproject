<?php
$this->breadcrumbs = array(
  'Структура сайта' => array('index'),
  $model->name,
);

$this->menu = array(
  array('label' => 'Список', 'url' => array('index')),
  array('label' => 'Добавление', 'url' => array('create')),
  array('label' => 'Изменение', 'url' => array('update', 'id' => $model->page_id)),
  array('label' => 'Удаление', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->page_id), 'confirm' => 'Вы уверены? Это действие нельзя будет отменить.')),
  array('label' => 'Управление', 'url' => array('admin')),
);
?>

<div class="page-header">
  <h4>Данные страницы «<?php echo $model->name; ?>»</h4>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
  'data' => $model,
  'attributes' => array(
    'page_module_id',
    'url_name',
    'url_path',
    'redirect_url',
    'name',
    'header',
    array('name' => 'before_content', 'type' => 'raw'),
    array('name' => 'after_content', 'type' => 'raw'),
    array('name' => 'vflag', 'value' => $model->vflag ? 'Да' : 'Нет'),
    array('name' => 'aflag', 'value' => $model->aflag ? 'Да' : 'Нет'),
    array('name' => 'hflag', 'value' => $model->hflag ? 'Да' : 'Нет'),
    array('name' => 'eflag', 'value' => $model->eflag ? 'Да' : 'Нет'),
  ),
)); ?>
