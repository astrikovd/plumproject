<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'page_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'parent_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'page_module_id',array('class'=>'span5','maxlength'=>16)); ?>

	<?php echo $form->textFieldRow($model,'page_module_param',array('class'=>'span5','maxlength'=>32)); ?>

	<?php echo $form->textFieldRow($model,'url',array('class'=>'span5','maxlength'=>512)); ?>

	<?php echo $form->textFieldRow($model,'redirect_url',array('class'=>'span5','maxlength'=>512)); ?>

	<?php echo $form->textFieldRow($model,'name',array('class'=>'span5','maxlength'=>128)); ?>

	<?php echo $form->textFieldRow($model,'header',array('class'=>'span5','maxlength'=>128)); ?>

	<?php echo $form->textAreaRow($model,'before_content',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textAreaRow($model,'after_content',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->checkBoxRow($model,'vflag'); ?>

	<?php echo $form->checkBoxRow($model,'aflag'); ?>

	<?php echo $form->checkBoxRow($model,'hflag'); ?>

	<?php echo $form->checkBoxRow($model,'eflag'); ?>

	<?php echo $form->textFieldRow($model,'date0',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
		    'buttonType'=>'submit'
			'type'=>'primary',
			'label'=>'Найти',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
