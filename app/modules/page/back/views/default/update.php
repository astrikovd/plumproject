<?php
$this->breadcrumbs=array(
	'Структура сайта'=>array('index'),
	$model->name=>array('view','id'=>$model->page_id),
	'Изменение',
);

$this->menu=array(
	array('label'=>'Список','url'=>array('index')),
	array('label'=>'Добавление','url'=>array('create')),
	array('label'=>'Просмотр','url'=>array('view','id'=>$model->page_id)),
	array('label'=>'Управление','url'=>array('admin')),
);
?>

<div class="page-header">
	<h4>Изменение страницы «<?php echo $model->name; ?>»</h4>
</div>
<?php echo $this->renderPartial('_form',array('model'=>$model,
											  'parents'=>$parents,
											  'modules'=>$modules,
                                              'adv_sections'=>$adv_sections)); ?>