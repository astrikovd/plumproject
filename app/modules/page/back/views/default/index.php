<?php
$this->breadcrumbs = array(
  'Структура сайта',
);

$this->menu = array(
  array('label' => 'Добавление', 'url' => array('create')),
  array('label' => 'Управление', 'url' => array('admin')),
);

Yii::app()->clientScript->registerScript('delete', "
    $(function(){
        $('.delete').click(function(){
            var id = $(this).attr('id');
            if(confirm('Вы уверены, что хотите удалить эту страницу?')){
                $.post('/page/default/delete/id/'+id, { id: id },function(result){
                    if(result.success){
                        location.reload(true);
                    }
                    else alert('Ошибка удаления страницы');
                },'json');
            }
        });

        $('.sort').click(function(){
            var url = $(this).attr('href');
            $.post(url,{},function(result){
                if(result.success){
                    location.reload(true);
                }
                else alert('Ошибка сортировки');
            },'json');
            return false;
        });
    });
");
?>

  <div class="page-header">
    <h4>Структура сайта</h4>
  </div>

  <div class="alert alert-info">
    <strong>Данный раздел предназначен для управления структурой сайта.</strong>
    Доступны функции добавления, удаления и редактирования страниц.
  </div>

<?php $this->widget('CTreeView', array('data' => $data,
  'collapsed' => false,
  'htmlOptions' => array('class' => 'treeview-gray')));?>