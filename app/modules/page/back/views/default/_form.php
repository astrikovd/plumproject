<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
  'id' => 'page-form',
  'enableAjaxValidation' => false,
)); ?>

<div class="alert alert-info">
  Поля, отмеченные <span class="red">*</span>, обязательны для заполнения.
</div>

<?php echo $form->errorSummary($model); ?>

<div class="page-header"><h5>Основные настройки</h5></div>

<?php echo $form->labelEx($model, 'parent_id'); ?>
<?php echo $form->dropDownList($model, 'parent_id', $parents, array('class' => 'span5')); ?>

<?php echo $form->textFieldRow($model, 'sort', array('class' => 'span5')); ?>
<a href="javascript:return void();" rel="tooltip"
   data-original-title="Позиция страницы относительно других страниц, находящихся на том же уровне"><i
      class="tooltip-icon icon icon-question-sign"></i></a>

<?php echo $form->labelEx($model, 'page_module_id'); ?>
<select name="Page[page_module_id]" id="Page_page_module_id" class="span5">
  <?php foreach ($modules as $key => $module): ?>
    <?php $module_id = ($model->page_module_param != "") ? $model->page_module_id . ":" . $model->page_module_param : $model->page_module_id; ?>
    <option value="<?=$module['page_module_id']?>"
            <?php if($module_id === $module['page_module_id']):?>selected<?php endif;?>><?=$module['name']?></option>
  <?php endforeach;?>
</select>
<a href="javascript:return void();" rel="tooltip"
   data-original-title="Функциональный модуль, запускаемый на странице"><i
      class="tooltip-icon icon icon-question-sign"></i></a>

<?php echo $form->textFieldRow($model, 'url_name', array('class' => 'span5', 'maxlength' => 512)); ?>
<a href="javascript:return void();" rel="tooltip"
   data-original-title="Может содержать буквы латинского алфавита, цифры, символы подчеркивания и тире"><i
      class="tooltip-icon icon icon-question-sign"></i></a>

<?php echo $form->textFieldRow($model, 'redirect_url', array('class' => 'span5', 'maxlength' => 512)); ?>
<a href="javascript:return void();" rel="tooltip"
   data-original-title="Для перенаправления на другую страницу, укажите её адрес в этом поле"><i
      class="tooltip-icon icon icon-question-sign"></i></a>

<?php echo $form->textFieldRow($model, 'name', array('class' => 'span5', 'maxlength' => 128)); ?>

<?php echo $form->textFieldRow($model, 'header', array('class' => 'span5', 'maxlength' => 128)); ?>

<div class="page-header"><h5>Контент</h5></div>

<div class="alert">Содержимое до контента выводится <strong>до</strong> содержимого функционального модуля.</div>
<?php echo $form->labelEx($model, 'before_content'); ?>
<?php $this->widget('application.lib.extensions.redactorjs.ImperaviRedactorWidget',
  array('model' => $model,
    'attribute' => 'before_content',
    'htmlOptions' => array('style' => 'width: 100%; height: 350px;'),
    'options' => array(
      'imageUpload' => Yii::app()->params['back_url'] . '/upload/',
    )
  ));?>

<br/>

<div class="alert">Содержимое после контента выводится <strong>после</strong> содержимого функционального модуля.</div>
<?php echo $form->labelEx($model, 'after_content'); ?>
<?php $this->widget('application.lib.extensions.redactorjs.ImperaviRedactorWidget',
  array('model' => $model,
    'attribute' => 'after_content',
    'htmlOptions' => array('style' => 'width: 100%; height: 350px;'),
    'options' => array(
      'imageUpload' => Yii::app()->params['back_url'] . '/upload/',
    )
  ));?>

<div class="page-header"><h5>Настройки доступа и отображения</h5></div>

<?php echo $form->checkBoxRow($model, 'vflag'); ?>

<?php echo $form->checkBoxRow($model, 'aflag'); ?>

<?php echo $form->checkBoxRow($model, 'hflag'); ?>

<?php echo $form->checkBoxRow($model, 'eflag'); ?>

<div class="page-header"><h5>Реклама на странице</h5></div>

<?php echo $form->labelEx($model, 'adv_sections_id'); ?>
<?php echo $form->dropDownList($model, 'adv_sections_id', $adv_sections, array('class' => 'span5')); ?>

<div class="page-header"><h5>Поисковая оптимизация</h5></div>

<?php echo $form->textAreaRow($model, 'keywords', array('class' => 'span12', 'maxlength' => 255)); ?>
<?php echo $form->textAreaRow($model, 'description', array('class' => 'span12', 'maxlength' => 1023)); ?>

<div class="form-actions">
  <a class="btn btn-default" href="javascript:history.back();"><i class="icon icon-chevron-left"></i> Назад</a>
  <?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
    'type' => 'primary',
    'label' => $model->isNewRecord ? 'Добавить' : 'Сохранить',
  )); ?>
</div>

<?php $this->endWidget(); ?>
