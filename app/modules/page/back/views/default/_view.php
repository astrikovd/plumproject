<div class="view">

  <b><?php echo CHtml::encode($data->getAttributeLabel('page_id')); ?>:</b>
  <?php echo CHtml::link(CHtml::encode($data->page_id), array('view', 'id' => $data->page_id)); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('parent_id')); ?>:</b>
  <?php echo CHtml::encode($data->parent_id); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('page_module_id')); ?>:</b>
  <?php echo CHtml::encode($data->page_module_id); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('page_module_param')); ?>:</b>
  <?php echo CHtml::encode($data->page_module_param); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('url_name')); ?>:</b>
  <?php echo CHtml::encode($data->url); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('redirect_url')); ?>:</b>
  <?php echo CHtml::encode($data->redirect_url); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
  <?php echo CHtml::encode($data->name); ?>
  <br/>

  <?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('header')); ?>:</b>
	<?php echo CHtml::encode($data->header); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('before_content')); ?>:</b>
	<?php echo CHtml::encode($data->before_content); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('after_content')); ?>:</b>
	<?php echo CHtml::encode($data->after_content); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vflag')); ?>:</b>
	<?php echo CHtml::encode($data->vflag); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('aflag')); ?>:</b>
	<?php echo CHtml::encode($data->aflag); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hflag')); ?>:</b>
	<?php echo CHtml::encode($data->hflag); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('eflag')); ?>:</b>
	<?php echo CHtml::encode($data->eflag); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date0')); ?>:</b>
	<?php echo CHtml::encode($data->date0); ?>
	<br />

	*/ ?>

</div>