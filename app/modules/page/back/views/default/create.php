<?php
$this->breadcrumbs = array(
  'Структура сайта' => array('index'),
  'Добавление',
);

$this->menu = array(
  array('label' => 'Список', 'url' => array('index')),
  array('label' => 'Управление', 'url' => array('admin')),
);
?>

  <div class="page-header">
    <h4>Добавление страницы</h4>
  </div>

<?php echo $this->renderPartial('_form', array('model' => $model,
  'parents' => $parents,
  'modules' => $modules,
  'adv_sections' => $adv_sections)); ?>