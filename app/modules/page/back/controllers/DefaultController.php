<?php

/**
 * Copyright (c) 2012 by Dmitry Astrikov / www.astrikov.ru
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Default page module controller
 * @package modules/page
 */
class DefaultController extends BackEndController
{

  /**
   * @var modules - functional modules
   */
  private $modules = array();

  /**
   * @var adv_sections - advertisement sections
   */
  private $adv_sections = array();

  /**
   * Displays a particular model.
   * @param integer $id the ID of the model to be displayed
   */
  public function actionView($id)
  {
    $this->render('view', array(
      'model' => $this->loadModel($id),
    ));
  }

  /**
   * Before action
   * @return boolean
   */
  protected function beforeAction($action)
  {
    /**
     * Preparing page modules
     */
    $modules = PageModuleT::model()->findAll(array('condition' => 'vflag = true'));

    //Newsfeeds
    $newsfeeds = Newsfeed::model()->findAll(array('condition' => 'vflag = true'));
    foreach ($newsfeeds as $key => $nf)
      $modules[] = array("page_module_id" => "news:{$nf->newsfeed_id}", "name" => "Новостная лента: {$nf->name}");

    //Photos
    $photogalleries = Photogallery::model()->findAll(array('condition' => 'vflag = true'));
    foreach ($photogalleries as $key => $pg)
      $modules[] = array("page_module_id" => "photo:{$pg->photogallery_id}", "name" => "Фотогалерея: {$pg->name}");

    //$this->modules = CHtml::listData($modules, 'page_module_id', 'name');
    $this->modules = $modules;

    /**
     * Preparing adv sections
     */
    $this->adv_sections = CHtml::listData(AdvSections::model()->findAll(array('condition' => 'vflag = true')), 'adv_sections_id', 'name');
    return true;
  }

  /**
   * Creates a new model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   */
  public function actionCreate()
  {
    $model = new Page;

    if (isset($_POST['Page'])) {
      $model->attributes = $_POST['Page'];
      $page_module = explode(':', $_POST['Page']['page_module_id']);
      $model->page_module_id = $page_module[0];
      $model->page_module_param = isset($page_module[1]) ? $page_module[1] : '';
      if ($model->save())
      {
        $this->redirect(array('view', 'id' => $model->page_id));
      }
    }

    //Родительские страницы
    $parents = Page::model()->findAll(array('order' => 'page_id'));
    $parents = CHtml::listData($parents, 'page_id', 'name');

    $this->render('create', array(
      'model' => $model,
      'parents' => $parents,
      'modules' => $this->modules,
      'adv_sections' => $this->adv_sections
    ));
  }

  /**
   * Updates a particular model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id the ID of the model to be updated
   */
  public function actionUpdate($id)
  {
    $model = $this->loadModel($id);

    if (isset($_POST['Page'])) {
      $model->attributes = $_POST['Page'];

      $page_module = explode(':', $_POST['Page']['page_module_id']);
      $model->page_module_id = $page_module[0];
      $model->page_module_param = isset($page_module[1]) ? $page_module[1] : '';

      if ($model->save())
        $this->redirect(array('view', 'id' => $model->page_id));
    }

    //Родительские страницы
    $parents = Page::model()->findAll(array('condition' => 'page_id != :page_id',
      'order' => 'page_id',
      'params' => array(':page_id' => $id)));
    $parents = CHtml::listData($parents, 'page_id', 'name');

    $this->render('update', array(
      'model' => $model,
      'parents' => $parents,
      'modules' => $this->modules,
      'adv_sections' => $this->adv_sections
    ));
  }

  /**
   * Deletes a particular model.
   * If deletion is successful, the browser will be redirected to the 'admin' page.
   * @param integer $id the ID of the model to be deleted
   */
  public function actionDelete($id)
  {
    if (Yii::app()->request->isPostRequest) {
      $this->loadModel($id)->delete();
      $result = array('success' => true);
    } else
      $result = array('success' => false);
    echo json_encode($result);
    die();
  }

  /**
   * Lists all models.
   */
  public function actionIndex()
  {
    $data = Page::model()->findAll(array('order' => 'sort'));

    $pages = $this->buildTree($data);

    $this->render('index', array(
      'data' => $pages,
    ));
  }

  /**
   * Building page tree
   * @param array $data
   * @return array
   */
  private function buildTree($data)
  {
    $pages = array();
    $children = $this->getChildren(1, $data);
    $pages[] = ($children) ? array('text' => 'Главная', 'children' => $children) :
        array('text' => 'Главная');
    return $pages;
  }

  /**
   * Get children for page with page_id == parent_id
   * @param integer $parent_id
   * @param array $pages
   * @return mixed
   */
  private function getChildren($parent_id, $pages)
  {
    $children = array();
    foreach ($pages as $page) {
      if ($page->parent_id == $parent_id) {
        $page_url = ($page->redirect_url != '') ? $page->redirect_url : $page->url_path;
        $labels = "";
        if (!$page->vflag)
          $labels .= '<span class="btn disabled btn-mini btn-primary">Скрыта</span>&nbsp;';
        if (!$page->eflag)
          $labels .= '<span class="btn disabled btn-mini btn-danger">Выключена</span>&nbsp;';
        $children[] = array('text' =>
        '<a style="display:inline-block;max-width:435px;" rel="tooltip" title="URL: ' . $page_url . ' | Сортировка: ' . $page->sort . '" href="/page/default/update/id/' . $page->page_id . '">' . $page->name . '</a>
                                    <div class="pull-right">
                                        ' . $labels . '
                                        <a class="sort" title="Поднять вверх" rel="tooltip" href="/page/default/sort/id/' . $page->page_id . '/dir/up/"><i class="icon-circle-arrow-up"></i></a>
                                        <a class="sort" title="Опустить вниз" rel="tooltip" href="/page/default/sort/id/' . $page->page_id . '/dir/down/"><i class="icon-circle-arrow-down"></i></a>
                                        &nbsp;|&nbsp;
                                        <a class="view" title="Просмотреть" rel="tooltip" href="/page/default/view/id/' . $page->page_id . '"><i class="icon-eye-open"></i></a>
                                        <a class="update" title="Редактировать" rel="tooltip" href="/page/default/update/id/' . $page->page_id . '"><i class="icon-pencil"></i></a>
                                        <a class="delete" title="Удалить" rel="tooltip" href="javascript: void(0);" id="' . $page->page_id . '"><i class="icon-trash"></i></a>
                                    </div>',
          'id' =>
          $page->page_id,
          'sort' => $page->sort);
      }
    }
    if (!empty($children)) {
      // sort alphabetically by sort
      usort($children, array('CArray', 'compare_sort'));
      foreach ($children as $k => $child) {
        $cchildren = $this->getChildren($child['id'], $pages);
        if ($cchildren) $children[$k]['children'] = $cchildren;
      }
      return $children;
    } else return false;
  }

  /**
   * Manages all models.
   */
  public function actionAdmin()
  {
    $model = new Page('search');
    $model->unsetAttributes(); // clear any default values
    if (isset($_GET['Page']))
      $model->attributes = $_GET['Page'];

    $this->render('admin', array(
      'model' => $model,
    ));
  }

  /**
   * Returns the data model based on the primary key given in the GET variable.
   * If the data model is not found, an HTTP exception will be raised.
   * @param integer the ID of the model to be loaded
   */
  public function loadModel($id)
  {
    $model = Page::model()->findByPk($id);
    if ($model === null)
      throw new CHttpException(404, 'The requested page does not exist.');
    return $model;
  }

  /**
   * Performs the AJAX validation.
   * @param CModel the model to be validated
   */
  protected function performAjaxValidation($model)
  {
    if (isset($_POST['ajax']) && $_POST['ajax'] === 'page-form') {
      echo CActiveForm::validate($model);
      Yii::app()->end();
    }
  }

  /**
   * Sort for page items
   * @param $id
   * @param $dir
   */
  public function actionSort($id, $dir)
  {
    if (Yii::app()->request->isAjaxRequest) {
      $page = Page::model()->findByPk($id);
      $page->sort = ($dir == "up") ? $page->sort - 1 : $page->sort + 1;
      $page->save();
      $result = array('success' => true);
      echo json_encode($result);
      die();
    }
  }
}
