<?php
/**
 * Copyright (c) 2012 by Dmitry Astrikov / www.astrikov.ru
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
require_once( realpath(dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'CPlumDbMigrationWithTablePrefix.php') );

class migration_name extends CPlumDbMigrationWithTablePrefix
{
  protected function upQueries()
  {
    $this->execute('SET statement_timeout = 0;');
    $this->execute('SET client_encoding = \'UTF8\';');
    $this->execute('SET check_function_bodies = false;');
    $this->execute('SET client_min_messages = warning;');
    $this->execute('SET escape_string_warning = off;');
    $this->execute('ALTER PROCEDURAL LANGUAGE plpgsql OWNER TO ##username##;');
    $this->execute('SET search_path = public, pg_catalog;');
    $this->createTable('##_example', array(
      'example_id' => 'integer NOT NULL',
      'name' => 'character varying(32) NOT NULL',
    ));
    $this->execute('ALTER TABLE public.##_example OWNER TO ##username##');
    $this->execute('CREATE SEQUENCE  ##_example_example_id_seq START WITH 1 INCREMENT BY 1 NO MAXVALUE NO MINVALUE CACHE 1;');
    $this->execute('ALTER TABLE public.##_example_example_id_seq OWNER TO ##username##;');
    $this->execute('ALTER SEQUENCE ##_example_example_id_seq OWNED BY ##_example.example_id;');
    $this->execute('SELECT pg_catalog.setval(\'##_example_example_id_seq\', 1, false);');


    $this->execute("REVOKE ALL ON SCHEMA public FROM PUBLIC;");
    $this->execute("REVOKE ALL ON SCHEMA public FROM postgres;");
    $this->execute("GRANT ALL ON SCHEMA public TO postgres;");
    $this->execute("GRANT ALL ON SCHEMA public TO PUBLIC;");
  }

  protected function downQueries()
  {
    $this->execute('DROP SEQUENCE ##_example_example_id_seq CASCADE;' );
    $this->execute('DROP TABLE ##_example CASCADE;' );
  }

}