<?php
$this->breadcrumbs = array(
  'Пример модуля'      => array('/example'),
);
?>
<div class="page-header">
  <h4>Пример модуля</h4>
</div>

<div class="alert alert-info">
  Простой пример структуры модуля. Никаких функций не выполняет, служит для примера создания расширений PlumCMF
</div>

<h1>Пример модуля</h1>
<p>Hello, world!</p>