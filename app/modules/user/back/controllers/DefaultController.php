<?php

/**
 * Copyright (c) 2012 by Dmitry Astrikov / www.astrikov.ru
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Default controller
 * @package modules/user/back/controllers
 */
class DefaultController extends BackEndController
{

  /**
   * @var components
   */
  private $components = array();

  /**
   * Before action
   * @return boolean
   */
  protected function beforeAction($action)
  {
    //All backend components
    $this->components = AdmCmp::model()->findAll(array('condition'=>'t.adm_cmp_grp_id NOT IN(7,8)'));
    return true;
  }

  /**
   * Displays a particular model.
   * @param integer $id the ID of the model to be displayed
   */
  public function actionView($id)
  {
    $this->render('view', array(
      'model' => $this->loadModel($id),
    ));
  }

  /**
   * Creates a new model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   */
  public function actionCreate()
  {
    $model = new AdmUser;

    if (isset($_POST['AdmUser'])) {
      $model->attributes = $_POST['AdmUser'];
      $model->passwordmd5 = md5($model->passwordmd5);
      if ($model->save()) {

        //Select only fields which corresponds to adm_user/adm_cmp relation
        $adm_cmp_ids = array();
        foreach ($_POST as $key => $value) {
          if (strpos($key, "uc_") === false) continue;
          $parts = explode("_", $key);
          $adm_cmp_ids[] = $parts[1];
        }

        AdmUserAdmCmp::model()->deleteAll('adm_user_id = :adm_user_id', array('adm_user_id' => $model->adm_user_id));

        foreach ($adm_cmp_ids as $id) {
          $new_row = new AdmUserAdmCmp;
          $new_row->adm_user_id = $model->adm_user_id;
          $new_row->adm_cmp_id = $id;
          $new_row->save();
        }

        $this->redirect(array('view', 'id' => $model->adm_user_id));
      }
    }

    $this->render('create', array(
      'model' => $model,
      'components' => $this->components
    ));
  }

  /**
   * Updates a particular model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id the ID of the model to be updated
   */
  public function actionUpdate($id)
  {
    $user_id = $id;
    $model = $this->loadModel($user_id);

    if (isset($_POST['AdmUser'])) {
      if ($_POST['AdmUser']['passwordmd5'] !== '') {
        $model->passwordmd5 = md5($_POST['AdmUser']['passwordmd5']);
      }
      $model->username = $_POST['AdmUser']['username'];
      $model->email = $_POST['AdmUser']['email'];

      if ($model->save()) {

        //Select only fields which corresponds to adm_user/adm_cmp relation
        $adm_cmp_ids = array();
        foreach ($_POST as $key => $value) {
          if (strpos($key, "uc_") === false) continue;
          $parts = explode("_", $key);
          $adm_cmp_ids[] = $parts[1];
        }

        AdmUserAdmCmp::model()->deleteAll('adm_user_id = :adm_user_id', array('adm_user_id' => $user_id));

        foreach ($adm_cmp_ids as $id) {
          $new_row = new AdmUserAdmCmp;
          $new_row->adm_user_id = $user_id;
          $new_row->adm_cmp_id = $id;
          $new_row->save();
        }

        $this->redirect(array('view', 'id' => $user_id));
      }
    }

    //Select components only for current user
    $uc = AdmUserAdmCmp::model()->findAll(array(
        'condition' => 'adm_user_id = :adm_user_id',
        'params' => array('adm_user_id' => $user_id))
    );
    $user_components = array();
    foreach ($uc as $cmp)
      $user_components[] = $cmp->adm_cmp_id;

    $this->render('update', array(
      'model' => $model,
      'components' => $this->components,
      'user_components' => $user_components
    ));
  }

  /**
   * Deletes a particular model.
   * If deletion is successful, the browser will be redirected to the 'admin' page.
   * @param integer $id the ID of the model to be deleted
   */
  public function actionDelete($id)
  {
    if (Yii::app()->request->isPostRequest) {
      // we only allow deletion via POST request
      $this->loadModel($id)->delete();

      // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
      if (!isset($_GET['ajax']))
        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    } else
      throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
  }

  /**
   * Lists all models.
   */
  public function actionIndex()
  {
    $dataProvider = new CActiveDataProvider('AdmUser', array(
      'criteria' => array('order' => 'username'),
      'pagination' => array('pageSize' => 10)
    ));
    $this->render('index', array(
      'dataProvider' => $dataProvider,
    ));
  }

  /**
   * Manages all models.
   */
  public function actionAdmin()
  {
    $model = new AdmUser('search');
    $model->unsetAttributes(); // clear any default values
    if (isset($_GET['AdmUser']))
      $model->attributes = $_GET['AdmUser'];

    $this->render('admin', array(
      'model' => $model,
    ));
  }

  /**
   * Returns the data model based on the primary key given in the GET variable.
   * If the data model is not found, an HTTP exception will be raised.
   * @param integer the ID of the model to be loaded
   */
  public function loadModel($id)
  {
    $model = AdmUser::model()->findByPk($id);
    if ($model === null)
      throw new CHttpException(404, 'The requested page does not exist.');
    return $model;
  }

  /**
   * Performs the AJAX validation.
   * @param CModel the model to be validated
   */
  protected function performAjaxValidation($model)
  {
    if (isset($_POST['ajax']) && $_POST['ajax'] === 'adm-user-form') {
      echo CActiveForm::validate($model);
      Yii::app()->end();
    }
  }
}
