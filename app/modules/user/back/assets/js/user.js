$(function () {
  $('#id-check-all').click(function () {
    var checked = $('.adm-cmp-cb').attr('checked');
    if (checked) {
      $('.adm-cmp-cb').attr('checked', false);
    }
    else {
      $('.adm-cmp-cb').attr('checked', true);
    }
    return false;
  });
})