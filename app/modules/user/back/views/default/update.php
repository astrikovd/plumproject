<?php
$this->breadcrumbs=array(
	'Учетные записи'=>array('index'),
	$model->username=>array('view','id'=>$model->adm_user_id),
	'Редактирование',
);

$this->menu=array(
	array('label'=>'Список','url'=>array('index')),
	array('label'=>'Добавление','url'=>array('create')),
	array('label'=>'Информация','url'=>array('view','id'=>$model->adm_user_id)),
	array('label'=>'Управление','url'=>array('admin')),
);
?>

<div class="page-header">
    <h4>Редактирование учетной записи <?php echo $model->username; ?></h4>
</div>
<?php echo $this->renderPartial('_form',array('model'=>$model,
											  'components'=>$components,
											  'user_components'=>$user_components)); ?>