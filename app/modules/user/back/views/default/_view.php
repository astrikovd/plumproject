<div class="view">

  <b><?php echo CHtml::encode($data->getAttributeLabel('adm_user_id')); ?>:</b>
  <?php echo CHtml::link(CHtml::encode($data->adm_user_id), array('view', 'id' => $data->adm_user_id)); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('username')); ?>:</b>
  <?php echo CHtml::encode($data->username); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('passwordmd5')); ?>:</b>
  <?php echo CHtml::encode($data->passwordmd5); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
  <?php echo CHtml::encode($data->email); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('last_login')); ?>:</b>
  <?php echo CHtml::encode($data->last_login); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('last_ip')); ?>:</b>
  <?php echo CHtml::encode($data->last_ip); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('date_create')); ?>:</b>
  <?php echo CHtml::encode($data->date_create); ?>
  <br/>


</div>