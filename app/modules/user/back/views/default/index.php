<?php
$this->breadcrumbs = array(
  'Учетные записи',
);

$this->menu = array(
  array('label' => 'Добавление', 'url' => array('create')),
  array('label' => 'Управление', 'url' => array('admin')),
);
?>

<div class="page-header">
  <h4>Пользователи системы управления</h4>
</div>

<div class="alert alert-info">
  <strong>Данный раздел предназначен для управления пользователями системы.</strong>
  Доступны функции добавления, удаления и редактирования учетных записей.
</div>

<? $this->widget('bootstrap.widgets.TbGridView', array(
  'dataProvider' => $dataProvider,
  'template' => '{items}<div style="text-align:center">{pager}</div>',
  'itemsCssClass' => 'table table-striped table-condensed',
  'columns' => array(
    array('name' => 'username', 'header' => 'Имя пользователя'),
    array('name' => 'email', 'header' => 'E-mail'),
    array(
      'class' => 'bootstrap.widgets.TbButtonColumn',
      'htmlOptions' => array('style' => 'width: 50px'),
    ),
  ),
)); ?>
