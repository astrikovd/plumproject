<?php
$this->breadcrumbs=array(
	'Учетные записи'=>array('index'),
	$model->username,
);

$this->menu=array(
	array('label'=>'Список','url'=>array('index')),
	array('label'=>'Добавление','url'=>array('create')),
	array('label'=>'Редактирование','url'=>array('update','id'=>$model->adm_user_id)),
	array('label'=>'Удаление','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->adm_user_id),'confirm'=>'Вы уверены? Это действие нельзя будет отменить.')),
	array('label'=>'Управление','url'=>array('admin')),
);
?>

<div class="page-header">
	<h4>Информация об учетной записи «<?php echo $model->username; ?>»</h4>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'username',
		'email',
		'last_login',
		'last_ip',
		array('name' => 'date_create', 'value' => date('d.m.Y H:i:s', strtotime($model->date_create))),
	),
)); ?>
