<?php Yii::app()->controller->module->registerResource('user', 'user.js') ?>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
  'id' => 'adm-user-form',
  'enableAjaxValidation' => false,
)); ?>

<div class="alert alert-info">
  Поля, отмеченные <span class="red">*</span>, обязательны для заполнения.
</div>

<div class="page-header">
  <h5>Основная информация</h5>
</div>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->textFieldRow($model, 'username', array('class' => 'span5', 'maxlength' => 32)); ?>
<a href="javascript:return void();" rel="tooltip"
   data-original-title="Имя пользователя может содержать буквы латинского алфавита и цифры, и должно состоять не менее, чем из 3 символов"><i
      class="tooltip-icon icon icon-question-sign"></i></a>

<?php echo $form->passwordFieldRow($model, 'passwordmd5', array('class' => 'span5', 'maxlength' => 32, 'value' => '')); ?>

<?php echo $form->textFieldRow($model, 'email', array('class' => 'span5', 'maxlength' => 32)); ?>

<div class="page-header">
  <h5>Доступ к модулям системы управления</h5>
</div>

<p><a href="#" class="dotted" id="id-check-all">Отметить/снять все</a></p>

<?php foreach ($components as $key => $cmp): ?>
  <label>
    <input type="checkbox" class="adm-cmp-cb" name="uc_<?=$cmp->adm_cmp_id;?>"
           <?if(!$model->isNewRecord && in_array($cmp->adm_cmp_id, $user_components)):?>checked<?endif;?>/>
    <a class="dotted" rel="tooltip" data-original-title="<?php echo $cmp->description;?>"><?php echo $cmp->name;?></a>
  </label>
<?php endforeach; ?>

<div class="form-actions">
  <a class="btn btn-default" href="javascript:history.back();"><i class="icon icon-chevron-left"></i> Назад</a>
  <?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
    'type' => 'primary',
    'label' => $model->isNewRecord ? 'Создать' : 'Сохранить',
  )); ?>
</div>

<?php $this->endWidget(); ?>
