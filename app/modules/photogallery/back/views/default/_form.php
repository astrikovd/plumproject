<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
  'id' => 'photogallery-form',
  'enableAjaxValidation' => false,
  'htmlOptions' => array('enctype' => 'multipart/form-data')
)); ?>

<div class="alert alert-info">
  Поля, отмеченные <span class="red">*</span>, обязательны для заполнения.
</div>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->textFieldRow($model, 'name', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php echo $form->textFieldRow($model, 'cnt', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php echo $form->fileFieldRow($model, 'cover'); ?>
<?php if ($model->cover && !$model->isNewRecord): ?>
  <div id="id-photo-controls" class="photo-controls">
    <a href="<?=Yii::app()->params->front_url . $model->cover?>" class="btn btn-mini" target="_blank" id="id-btn-view">Посмотреть
      <i class="icon-eye-open"></i></a>
    <a href="<?=Yii::app()->params->front_url . $model->cover?>" class="btn btn-warning btn-mini" id="id-btn-delete">Удалить
      <i class="icon-remove"></i></a>
  </div>
<?php endif; ?>

<hr>

<?php echo $form->checkBoxRow($model, 'vflag'); ?>

<div class="form-actions">
  <a class="btn btn-default" href="javascript:history.back();"><i class="icon icon-chevron-left"></i> Назад</a>
  <?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
    'type' => 'primary',
    'label' => $model->isNewRecord ? 'Добавить' : 'Сохранить',
  )); ?>
</div>

<?php $this->endWidget(); ?>
