<div class="view">

  <b><?php echo CHtml::encode($data->getAttributeLabel('photogallery_id')); ?>:</b>
  <?php echo CHtml::link(CHtml::encode($data->photogallery_id), array('view', 'id' => $data->photogallery_id)); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
  <?php echo CHtml::encode($data->name); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('cover')); ?>:</b>
  <?php echo CHtml::encode($data->cover); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('vflag')); ?>:</b>
  <?php echo CHtml::encode($data->vflag); ?>
  <br/>


</div>