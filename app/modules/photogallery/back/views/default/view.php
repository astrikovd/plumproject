<?php
$this->breadcrumbs=array(
	'Фотогалереи'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'Список','url'=>array('index')),
	array('label'=>'Добавление','url'=>array('create')),
	array('label'=>'Изменение','url'=>array('update','id'=>$model->photogallery_id)),
	array('label'=>'Удаление','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->photogallery_id),'confirm'=>'Вы уверены? Это действия нельзя будет отменить')),
	array('label'=>'Управление','url'=>array('admin')),
);
?>

<div class="page-header">
	<h4>Просмотр фотогалереи «<?php echo $model->name; ?>»</h4>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'name',
		'cnt',
		array('name' =>'Включена', 'value' => $model->vflag ? "Да" : "Нет"),
	),
)); ?>
