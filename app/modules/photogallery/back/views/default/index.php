<?php
$this->breadcrumbs=array(
	'Фотогалереи',
);

$this->menu=array(
	array('label'=>'Добавление','url'=>array('create')),
	array('label'=>'Управление','url'=>array('admin')),
);
?>

<div class="page-header">
	<h4>Фотогалереи</h4>
</div>

<div class="alert alert-info">
    <strong>Данный раздел предназначен для управления фотогалереями.</strong>
    Доступны функции добавления, удаления и редактирования галерей.
</div>

<? $this->widget('bootstrap.widgets.TbGridView', array(
    'dataProvider'=>$dataProvider,
    'template'=>'{items}<div style="text-align:center">{pager}</div>',
    'itemsCssClass'=>'table table-striped table-condensed',
    'columns'=>array(
        array('name'=>'name', 'header'=>'Название'),
        array('name'=>'vflag', 'header'=>'Включена', 'value' => '($data->vflag) ? "Да" : "Нет"'),
        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'htmlOptions'=>array('style'=>'width: 50px'),
        ),
    ),
)); ?>
