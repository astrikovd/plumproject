<?php

/**
 * Copyright (c) 2012 by Dmitry Astrikov / www.astrikov.ru
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Default site controller
 * @package modules/news/back/controllers
 */
class DefaultController extends BackEndController
{

  /**
   * @var newsfeeds - News feeds
   */
  private $newsfeeds;

  /**
   * Before action
   * @return boolean
   */
  protected function beforeAction($action)
  {
    $this->newsfeeds = CHtml::listData(Newsfeed::model()->findAll(array('condition' => 'vflag = true')), 'newsfeed_id', 'name');
    return true;
  }

  /**
   * Displays a particular model.
   * @param integer $id the ID of the model to be displayed
   */
  public function actionView($id)
  {
    $this->render('view', array(
      'model' => $this->loadModel($id),
    ));
  }

  /**
   * Creates a new model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   */
  public function actionCreate()
  {
    $model = new Newsitem;

    if (isset($_POST['Newsitem'])) {
      $model->attributes = $_POST['Newsitem'];

      $file = CUploadedFile::getInstance($model, 'picture');
      if (isset($file->name)) {
        $file_url = '/data/news/' . md5($file->name . microtime()) . "." . $file->getExtensionName();
        $model->picture = $file_url;
      }

      if ($model->save()) {
        if (isset($file->name)) {
          $file_path = Yii::app()->basePath . "/../front{$file_url}";
          $file->saveAs($file_path);
        }
        $this->redirect(array('view', 'id' => $model->newsitem_id));
      }
    }

    $this->render('create', array(
      'model' => $model,
      'newsfeeds' => $this->newsfeeds
    ));
  }

  /**
   * Updates a particular model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id the ID of the model to be updated
   */
  public function actionUpdate($id)
  {
    $model = $this->loadModel($id);

    //Picture delete (Ajax)
    if (isset($_POST['cp']) && Yii::app()->request->isAjaxRequest) {
      if ($_POST['cp'] == 'delete_pic') {
        $model->picture = '';
        $result = ($model->save()) ? array('result' => true) : array('result' => false);
        header('Content-type: application/json');
        echo CJavaScript::jsonEncode($result);
        Yii::app()->end();
      }
    }

    if (isset($_POST['Newsitem'])) {
      $ex_picture = $model->picture;
      $model->attributes = $_POST['Newsitem'];
      $file = CUploadedFile::getInstance($model, 'picture');
      if (isset($file->name)) {
        $file_url = '/data/news/' . md5($file->name . microtime()) . "." . $file->getExtensionName();
        $model->picture = $file_url;
      } else
        $model->picture = $ex_picture;

      if ($model->save()) {
        if (isset($file->name)) {
          $file_path = Yii::app()->basePath . "/../front{$file_url}";
          $file->saveAs($file_path);
        }
        $this->redirect(array('view', 'id' => $model->newsitem_id));
      }
    }

    $this->render('update', array(
      'model' => $model,
      'newsfeeds' => $this->newsfeeds
    ));
  }

  /**
   * Deletes a particular model.
   * If deletion is successful, the browser will be redirected to the 'admin' page.
   * @param integer $id the ID of the model to be deleted
   * @throws CHttpException
   * @return void
   */
  public function actionDelete($id)
  {
    if (Yii::app()->request->isPostRequest) {
      // we only allow deletion via POST request
      $this->loadModel($id)->delete();

      // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
      if (!isset($_GET['ajax']))
        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    } else
      throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
  }

  /**
   * Lists all models.
   */
  public function actionIndex()
  {
    $dataProvider = new CActiveDataProvider('Newsitem', array(
      'criteria' => array('order' => 'date0 DESC'),
      'pagination' => array('pageSize' => 10)
    ));
    $this->render('index', array(
      'dataProvider' => $dataProvider,
    ));
  }

  /**
   * Manages all models.
   */
  public function actionAdmin()
  {
    $model = new Newsitem('search');
    $model->unsetAttributes(); // clear any default values
    if (isset($_GET['Newsitem']))
      $model->attributes = $_GET['Newsitem'];

    $this->render('admin', array(
      'model' => $model,
    ));
  }

  /**
   * Returns the data model based on the primary key given in the GET variable.
   * If the data model is not found, an HTTP exception will be raised.
   * @param integer the ID of the model to be loaded
   */
  public function loadModel($id)
  {
    $model = Newsitem::model()->findByPk($id);
    if ($model === null)
      throw new CHttpException(404, 'The requested page does not exist.');
    return $model;
  }

  /**
   * Performs the AJAX validation.
   * @param CModel the model to be validated
   */
  protected function performAjaxValidation($model)
  {
    if (isset($_POST['ajax']) && $_POST['ajax'] === 'newsitem-form') {
      echo CActiveForm::validate($model);
      Yii::app()->end();
    }
  }
}