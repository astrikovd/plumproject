<?php Yii::app()->clientScript->registerScriptFile('/static/js/pic_delete.js');?>

<?php
$this->breadcrumbs=array(
	'Новости'=>array('index'),
	$model->name=>array('view','id'=>$model->newsitem_id),
	'Изменение',
);

$this->menu=array(
	array('label'=>'Список','url'=>array('index')),
	array('label'=>'Добавление','url'=>array('create')),
	array('label'=>'Просмотр','url'=>array('view','id'=>$model->newsitem_id)),
	array('label'=>'Управление','url'=>array('admin')),
);
?>

<div class="page-header">
	<h4>Изменение новости «<?php echo $model->name; ?>»</h4>
</div>

<?php echo $this->renderPartial('_form',array('model'=>$model, 'newsfeeds'=>$newsfeeds)); ?>