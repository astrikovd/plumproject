<?php
$this->breadcrumbs = array(
  'Новости' => array('index'),
  $model->name,
);

$this->menu = array(
  array('label' => 'Список', 'url' => array('index')),
  array('label' => 'Добавление', 'url' => array('create')),
  array('label' => 'Изменение', 'url' => array('update', 'id' => $model->newsitem_id)),
  array('label' => 'Удаление', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->newsitem_id), 'confirm' => 'Вы уверены? Это действие нельзя будет отменить')),
  array('label' => 'Управление', 'url' => array('admin')),
);
?>

<div class="page-header">
  <h4>Новость «<?php echo $model->name; ?>»</h4>
</div>


<?php $this->widget('bootstrap.widgets.TbDetailView', array(
  'data' => $model,
  'attributes' => array(
    'name',
    array('name' => 'anons', 'type' => 'raw'),
    array('name' => 'body', 'type' => 'raw'),
    array('name' => 'vflag', 'value' => $model->vflag ? "Да" : "Нет"),
    array('name' => 'date0', 'value' => date('d.m.Y H:i:s', strtotime($model->date0))),
    'keywords',
    'description',
  ),
)); ?>
