<div class="view">

  <b><?php echo CHtml::encode($data->getAttributeLabel('newsitem_id')); ?>:</b>
  <?php echo CHtml::link(CHtml::encode($data->newsitem_id), array('view', 'id' => $data->newsitem_id)); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('newsfeed_id')); ?>:</b>
  <?php echo CHtml::encode($data->newsfeed_id); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
  <?php echo CHtml::encode($data->name); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('anons')); ?>:</b>
  <?php echo CHtml::encode($data->anons); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('body')); ?>:</b>
  <?php echo CHtml::encode($data->body); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('picture')); ?>:</b>
  <?php echo CHtml::encode($data->picture); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('vflag')); ?>:</b>
  <?php echo CHtml::encode($data->vflag); ?>
  <br/>

  <?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('date0')); ?>:</b>
	<?php echo CHtml::encode($data->date0); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keywords')); ?>:</b>
	<?php echo CHtml::encode($data->keywords); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

	*/ ?>

</div>