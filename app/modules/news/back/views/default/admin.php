<?php
$this->breadcrumbs = array(
  'Новости' => array('index'),
  'Управление',
);

$this->menu = array(
  array('label' => 'Список', 'url' => array('index')),
  array('label' => 'Добавление', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('newsitem-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="page-header">
  <h4>Управление новостями</h4>
</div>

<div class="alert alert-info">
  <strong>Данный раздел предназначен для быстрого поиска и изменения новостей.</strong>
  Доступны функции добавления, удаления и редактирования.
</div>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
  'id' => 'newsitem-grid',
  'dataProvider' => $model->search(),
  'filter' => $model,
  'columns' => array(
    'name',
    array('name' => 'date0', 'value' => 'date("d.m.Y", strtotime($data->date0))'),
    array(
      'class' => 'bootstrap.widgets.TbButtonColumn',
    ),
  ),
)); ?>
