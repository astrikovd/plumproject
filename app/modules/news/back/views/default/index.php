<?php Yii::app()->clientScript->registerScriptFile('/static/js/newsitem.js'); ?>

<?php
$this->breadcrumbs = array(
  'Новости',
);

$this->menu = array(
  array('label' => 'Добавление', 'url' => array('create')),
  array('label' => 'Управление', 'url' => array('admin')),
);
?>

<div class="page-header">
  <h4>Новости</h4>
</div>

<div class="alert alert-info">
  <strong>Данный раздел предназначен для управления новостями.</strong>
  Доступны функции добавления, удаления и редактирования новостей.
</div>

<? $this->widget('bootstrap.widgets.TbGridView', array(
  'dataProvider' => $dataProvider,
  'id' => 'newsitem-grid',
  'template' => '{items}<div class="pull-right">{pager}</div>',
  'itemsCssClass' => 'table table-striped table-condensed',
  'columns' => array(
    array(
      'class' => 'CCheckBoxColumn',
      'id' => 'newsitem_cb',
      'selectableRows' => 2
    ),
    array('name' => 'name', 'header' => 'Название'),
    array('name' => 'newsfeed.name', 'header' => 'Лента'),
    array('name' => 'date0', 'header' => 'Дата добавления', 'value' => 'date("d.m.Y", strtotime($data->date0))'),
    array('name' => 'vflag', 'header' => 'Включена', 'value' => '($data->vflag) ? "Да" : "Нет"'),
    array(
      'class' => 'bootstrap.widgets.TbButtonColumn',
      'htmlOptions' => array('style' => 'width: 50px'),
    ),
  ),
)); ?>

<div class="btn-group pull-left">
  <a class="btn dropdown-toggle btn-mini" data-toggle="dropdown" href="#">Отмеченные <span class="caret"></span></a>
  <ul class="dropdown-menu">
    <li><a href="#" class="multiple-select-action" id="id-hide">Скрыть</a></li>
    <li><a href="#" class="multiple-select-action" id="id-show">Показать</a></li>
  </ul>
</div>

