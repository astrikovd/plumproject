<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'newsitem-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data')
)); ?>

<?php if(!empty($newsfeeds)): ?>
	<div class="alert alert-info">
	    Поля, отмеченные <span class="red">*</span>, обязательны для заполнения.
	</div>

	<?php echo $form->errorSummary($model); ?>

	<div class="page-header">
		<h5>Основные настройки</h5>
	</div>

	<?php echo $form->labelEx($model,'newsfeed_id'); ?>
	<?php echo $form->dropDownList($model,'newsfeed_id', $newsfeeds, array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'name',array('class'=>'span5','maxlength'=>255)); ?>

	<div class="page-header">
		<h5>Контент</h5>
	</div>

	<?php echo $form->labelEx($model,'anons'); ?>
	<?php $this->widget('application.lib.extensions.redactorjs.ImperaviRedactorWidget',
                        array('model' => $model,
                            'attribute' => 'anons',
                            'htmlOptions' => array('style' => 'width: 100%; height: 250px;'),
                            'options' => array(
                                'imageUpload' => Yii::app()->params['back_url'].'/upload/',
                            )
                            ));?>

    <hr>

	<?php echo $form->labelEx($model,'body'); ?>
	<?php $this->widget('application.lib.extensions.redactorjs.ImperaviRedactorWidget',
                        array('model' => $model,
                            'attribute' => 'body',
                            'htmlOptions' => array('style' => 'width: 100%; height: 350px;'),
                            'options' => array(
                                'imageUpload' => Yii::app()->params['back_url'].'/upload/',
                            )
                            ));?>

	<hr>

	<?php echo $form->fileFieldRow($model, 'picture'); ?>
    <?php if($model->picture && !$model->isNewRecord): ?>
        <div id="id-photo-controls" class="photo-controls">
            <a href="<?=Yii::app()->params->front_url . $model->picture?>" class="btn btn-mini" target="_blank" id="id-btn-view">Посмотреть <i class="icon-eye-open"></i></a>
            <a href="<?=Yii::app()->params->front_url . $model->picture?>" class="btn btn-warning btn-mini" id="id-btn-delete">Удалить <i class="icon-remove"></i></a>
        </div>
    <?php endif; ?>

    <hr>

	<div class="page-header">
		<h5>Настройки отображения</h5>
	</div>

	<?php echo $form->checkBoxRow($model,'vflag'); ?>
	<br/>

	<?php echo $form->labelEx($model,'date0'); ?>
	<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'name'=>'date0',
                    'model' => $model,
                    'attribute' => 'date0',
                    // additional javascript options for the date picker plugin
                    'options'=>array(
                        'showAnim'=>false,
                        'dateFormat'=>'dd.mm.yy',
                    ),
                    'language' => 'ru',
                    'htmlOptions'=>array(
                        'style'=>'height:20px;'
                    ),
                ));?>

	<div class="page-header">
		<h5>Поисковая оптимизация</h5>
	</div>

	<?php echo $form->textAreaRow($model,'keywords',array('class'=>'span12','maxlength'=>255)); ?>

	<?php echo $form->textAreaRow($model,'description',array('class'=>'span12','maxlength'=>1023)); ?>

	<div class="form-actions">
		<a class="btn btn-default" href="javascript:history.back();"><i class="icon icon-chevron-left"></i> Назад</a>
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Добавить' : 'Сохранить',
		)); ?>
	</div>
<?php else: ?>
	<div class="alert alert-error">
	    Для добавления новостей необходимо <a href="/newsfeed/default/create" target="_blank">добавить</a> хотя бы одну новостную ленту.
	</div>
<?php endif; ?>


<?php $this->endWidget(); ?>
