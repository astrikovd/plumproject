<?php

/**
 * Copyright (c) 2012 by Dmitry Astrikov / www.astrikov.ru
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Default controller for news module
 * @package modules/news/front/controllers/
 */
class DefaultController extends FrontEndController
{

    /**
     * @var default layout
     */
    public $layout='//layouts/col';

    /**
     * Route action
     * @return mixed
     */
    public function actionRoute()
    {
        if(empty($this->params)) throw new CHttpException(404);

        if(isset($this->params['newsitem_id']))
            return $this->actionView($this->params['newsitem_id']);
        else
            return $this->actionIndex();
    }

    /**
     * Index Action
     * @return void
     */
	private function actionIndex()
	{
        //Get newsfeed info
        $newsfeed_id = intval($this->pageData->page_module_param);
        $newsfeed = Newsfeed::model()->findByPk($newsfeed_id);
        $newsfeed->cnt = $newsfeed->cnt ? $newsfeed->cnt : 10;

        //Forming criteria
        $criteria = new CDbCriteria;
        $criteria->order = "date0 desc";
        $criteria->condition = "vflag = true AND newsfeed_id = {$newsfeed_id}";

        $active_page = isset($this->params['pg'])? $this->params['pg'] : 1;

        $news_count = Newsitem::model()->count($criteria);


        //Adding limit and offset to db_criteria
        $criteria->limit  = $newsfeed->cnt;
        $criteria->offset = $newsfeed->cnt*($active_page-1);

        //Get newsitems
        $news = Newsitem::model()->findAll($criteria);

        //Create pager
        $pager = CPager::create($this->pageData['url_path'], $news_count, $newsfeed->cnt, $active_page);

        $this->render('index',array('newsfeed' => $newsfeed,
                                    'pager' => $pager,
                                    'news' => $news));
	}

    /**
     * View action
     * @param integer $id
     * @return void
     */
    private function actionView($id)
    {
        $newsitem = Newsitem::model()->findByPk($id);
        if(!$newsitem) throw new CHttpException(404);

        $this->render('view', array('item' => $newsitem));
    }
}