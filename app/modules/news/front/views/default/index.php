<?php if ($this->pageData->hflag): ?>
  <div class="page-header">
    <h3><?=$this->pageData->header;?></h3>
  </div>
<?php endif; ?>

<?php if (!empty($news)): ?>
  <?php foreach ($news as $item): ?>
    <div class="row-fluid news-item">
      <div>
        <div class="news-item-name">
          <div class="page-header">
            <h5><a href="<?=$this->pageData['url_path'];?>/<?=$item['newsitem_id']?>/"><?=$item->name?></a></h5>
            <small><?=Yii::app()->dateFormatter->formatDateTime($item->date0, 'medium', null);?></small>
          </div>
        </div>
        <div><?=$item->anons?></div>
      </div>
    </div>
  <?php endforeach; ?>
<?php else: ?>
  <p>Новостей еще нет</p>
<?php endif; ?>
<?= $pager
; ?>