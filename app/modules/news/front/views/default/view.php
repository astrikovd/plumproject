<?php $this->pageTitle = Yii::app()->name . " / " . $item->name; ?>

<div class="page-header">
  <h3><?=$item->name?></h3>
</div>
<div class="row-fluid">
  <?=$item->body?>
</div>
<div class="row-fluid">
  <div class="pull-right">
    <a class="small" href="javascript:history.back();">Вернуться назад</a>
  </div>
</div>
