<?php

/**
 * Copyright (c) 2012 by Dmitry Astrikov / www.astrikov.ru
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Default controller for upload module
 * @package modules/upload
 */
class DefaultController extends BackEndController
{

  /**
   * Actions
   * @return array
   */
  public function actions()
  {
    return array(
      'index' => array(
        'class' => 'ext.EFileUploadAction.EFileUploadAction',
        'name' => 'file',
        'createDirectory' => true,
        'createDirectoryMode' => 0777,
        'createDirectoryRecursive' => true,
        'filenameRule' => 'md5($file->name).".".$file->extensionName',
        'path' =>
        realpath(Yii::app()->basePath . DIRECTORY_SEPARATOR . '..') . DIRECTORY_SEPARATOR .
            'front' . DIRECTORY_SEPARATOR .
            'data' . DIRECTORY_SEPARATOR .
            'user' . DIRECTORY_SEPARATOR . date('Y-m-d'),
        'onAfterUpload' => function ($event) {
          if ($event->sender->hasErrors()) {
            $result = CJSON::encode($event->sender->getErrors());
          } else {
            $path = Yii::app()->params->front_url . '/data/user/';
            $file = array(
              'filelink' => $path . date('Y-m-d') . '/' . $event->sender->filename,
              'filename' => $event->sender->filename,
            );
            if ($type = Yii::app()->request->getQuery('type', 'file') == 'image') {
            }
            $result = CJSON::encode($file);
          }
          echo stripcslashes($result);
          exit;
        }
      ),
    );
  }
}