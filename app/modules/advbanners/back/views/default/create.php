<?php
$this->breadcrumbs=array(
	'Рекламные баннеры'=>array('index'),
	'Добавление',
);

$this->menu=array(
	array('label'=>'Список','url'=>array('index')),
	array('label'=>'Управление','url'=>array('admin')),
);
?>

<div class="page-header">
    <h4>Добавление баннера</h4>
</div>

<?php echo $this->renderPartial('_form', array('model'=>$model, 'sections' => $sections, 'places' => $places)); ?>