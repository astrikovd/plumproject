<?php Yii::app()->clientScript->registerScriptFile('/static/js/pic_delete.js');?>

<?php
$this->breadcrumbs=array(
	'Рекламные баннеры'=>array('index'),
	$model->name=>array('view','id'=>$model->adv_banners_id),
	'Изменение',
);

$this->menu=array(
	array('label'=>'Список','url'=>array('index')),
	array('label'=>'Добавление','url'=>array('create')),
	array('label'=>'Просмотр','url'=>array('view','id'=>$model->adv_banners_id)),
	array('label'=>'Управление','url'=>array('admin')),
);
?>

<div class="page-header">
    <h4>Изменение рекламного баннера</h4>
</div>

<?php echo $this->renderPartial('_form',array('model'=>$model, 'sections' => $sections, 'places' => $places)); ?>