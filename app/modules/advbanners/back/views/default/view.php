<?php
$this->breadcrumbs=array(
	'Рекламные баннеры'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'Список','url'=>array('index')),
	array('label'=>'Добавление','url'=>array('create')),
	array('label'=>'Изменение','url'=>array('update','id'=>$model->adv_banners_id)),
	array('label'=>'Удаление','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->adv_banners_id),'confirm'=>'Вы уверены? Это действия нельзя будет отменить')),
	array('label'=>'Управление','url'=>array('admin')),
);
?>

<div class="page-header">
	<h4>Рекламный баннер «<?php echo $model->name; ?>»</h4>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'name',
		array('name'=>'picture', 'type'=>'raw','value'=>($model->picture) ? CHtml::image(Yii::app()->params->front_url . $model->picture) : "Не загружено"),
		'code',
        'link',
        'title',
		array('name' => 'date_start', 'value' => date('d.m.Y', strtotime($model->date_start))),
		array('name' => 'date_end', 'value' => date('d.m.Y', strtotime($model->date_end))),
		array('name' => 'vflag', 'value' => $model->vflag ? "Да" : "Нет"),
		array('name' => 'date0', 'value' => date('d.m.Y H:i:s', strtotime($model->date0))),
	),
)); ?>
