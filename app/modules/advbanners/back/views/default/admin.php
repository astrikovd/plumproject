<?php
$this->breadcrumbs = array(
  'Рекламные баннеры' => array('index'),
  'Управление',
);

$this->menu = array(
  array('label' => 'Список', 'url' => array('index')),
  array('label' => 'Добавление', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('adv-banners-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="page-header">
  <h4>Управление рекламными баннерами</h4>
</div>

<div class="alert alert-info">
  <strong>Данный раздел предназначен для быстрого поиска и изменения рекламных баннеров.</strong>
  Доступны функции добавления, удаления и редактирования.
</div>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
  'id' => 'adv-banners-grid',
  'dataProvider' => $model->search(),
  'filter' => $model,
  'columns' => array(
    'name',
    array(
      'class' => 'bootstrap.widgets.TbButtonColumn',
    ),
  ),
)); ?>
