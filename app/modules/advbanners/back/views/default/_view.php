<div class="view">

  <b><?php echo CHtml::encode($data->getAttributeLabel('adv_banners_id')); ?>:</b>
  <?php echo CHtml::link(CHtml::encode($data->adv_banners_id), array('view', 'id' => $data->adv_banners_id)); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('adv_places_id')); ?>:</b>
  <?php echo CHtml::encode($data->adv_places_id); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('adv_sections_id')); ?>:</b>
  <?php echo CHtml::encode($data->adv_sections_id); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
  <?php echo CHtml::encode($data->name); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('picture')); ?>:</b>
  <?php echo CHtml::encode($data->picture); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('code')); ?>:</b>
  <?php echo CHtml::encode($data->code); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('date_start')); ?>:</b>
  <?php echo CHtml::encode($data->date_start); ?>
  <br/>

  <?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('date_end')); ?>:</b>
	<?php echo CHtml::encode($data->date_end); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date0')); ?>:</b>
	<?php echo CHtml::encode($data->date0); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vflag')); ?>:</b>
	<?php echo CHtml::encode($data->vflag); ?>
	<br />

	*/ ?>

</div>