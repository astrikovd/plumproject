<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
  'id' => 'adv-banners-form',
  'enableAjaxValidation' => false,
  'htmlOptions' => array('enctype' => 'multipart/form-data')
)); ?>

<div class="alert alert-info">
  Поля, отмеченные <span class="red">*</span>, обязательны для заполнения.
</div>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->labelEx($model, 'adv_places_id'); ?>
<?php echo $form->dropDownList($model, 'adv_places_id', $places, array('class' => 'span5')); ?>

<?php echo $form->labelEx($model, 'adv_sections_id'); ?>
<?php echo $form->dropDownList($model, 'adv_sections_id', $sections, array('class' => 'span5')); ?>

<?php echo $form->textFieldRow($model, 'name', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php echo $form->textFieldRow($model, 'link', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php echo $form->textFieldRow($model, 'title', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php echo $form->fileFieldRow($model, 'picture'); ?>
<?php if ($model->picture && !$model->isNewRecord): ?>
  <div id="id-photo-controls" class="photo-controls">
    <a href="<?=Yii::app()->params->front_url . $model->picture?>" class="btn btn-mini" target="_blank"
       id="id-btn-view">Посмотреть <i class="icon-eye-open"></i></a>
    <a href="<?=Yii::app()->params->front_url . $model->picture?>" class="btn btn-warning btn-mini" id="id-btn-delete">Удалить
      <i class="icon-remove"></i></a>
  </div>
<?php endif; ?>

<br/><br/>
<div class="alert">
  Код баннера имеет более высокий приоритет, чем изображение.
</div>
<?php echo $form->textAreaRow($model, 'code', array('rows' => 6, 'cols' => 50, 'class' => 'span8')); ?>

<?php echo $form->labelEx($model, 'date_start'); ?>
<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
  'name' => 'date_start',
  'attribute' => 'date_start',
  'model' => $model,
  'value' => $model->isNewRecord ? "" : Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($model->date_start, 'dd.mm.YYYY'), 'medium', null),
  // additional javascript options for the date picker plugin
  'options' => array(
    'showAnim' => false,
    'dateFormat' => 'dd.mm.yy',
  ),
  'language' => 'ru',
  'htmlOptions' => array(
    'style' => 'height:20px;'
  ),
));?>

<?php echo $form->labelEx($model, 'date_end'); ?>
<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
  'name' => 'date_end',
  'attribute' => 'date_end',
  'model' => $model,
  'value' => $model->isNewRecord ? "" : Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($model->date_end, 'dd.mm.YYYY'), 'medium', null),
  // additional javascript options for the date picker plugin
  'options' => array(
    'showAnim' => false,
    'dateFormat' => 'dd.mm.yy',
  ),
  'language' => 'ru',
  'htmlOptions' => array(
    'style' => 'height:20px;'
  ),
));?>

<?php echo $form->checkBoxRow($model, 'vflag'); ?>

<div class="form-actions">
  <a class="btn btn-default" href="javascript:history.back();"><i class="icon icon-chevron-left"></i> Назад</a>
  <?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
    'type' => 'primary',
    'label' => $model->isNewRecord ? 'Добавить' : 'Сохранить',
  )); ?>
</div>

<?php $this->endWidget(); ?>
