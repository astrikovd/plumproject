<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
  'action' => Yii::app()->createUrl($this->route),
  'method' => 'get',
)); ?>

<?php echo $form->textFieldRow($model, 'adv_banners_id', array('class' => 'span5')); ?>

<?php echo $form->textFieldRow($model, 'adv_places_id', array('class' => 'span5')); ?>

<?php echo $form->textFieldRow($model, 'adv_sections_id', array('class' => 'span5')); ?>

<?php echo $form->textFieldRow($model, 'name', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php echo $form->textFieldRow($model, 'picture', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php echo $form->textAreaRow($model, 'code', array('rows' => 6, 'cols' => 50, 'class' => 'span8')); ?>

<?php echo $form->textFieldRow($model, 'date_start', array('class' => 'span5')); ?>

<?php echo $form->textFieldRow($model, 'date_end', array('class' => 'span5')); ?>

<?php echo $form->textFieldRow($model, 'date0', array('class' => 'span5')); ?>

<?php echo $form->checkBoxRow($model, 'vflag'); ?>

<div class="form-actions">
  <?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit'
  'type'=>'primary',
  'label'=>'Search',
  )); ?>
</div>

<?php $this->endWidget(); ?>
