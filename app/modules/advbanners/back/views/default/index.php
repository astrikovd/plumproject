<?php Yii::app()->clientScript->registerScriptFile('/static/js/advbanners.js'); ?>

<?php
$this->breadcrumbs = array(
  'Рекламные баннеры',
);

$this->menu = array(
  array('label' => 'Добавление', 'url' => array('create')),
  array('label' => 'Управление', 'url' => array('admin')),
);
?>

<div class="page-header">
  <h4>Рекламные баннеры</h4>
</div>

<div class="alert alert-info">
  <strong>Данный раздел предназначен для управления отображением рекламных баннеров.</strong>
</div>

<? $this->widget('bootstrap.widgets.TbGridView', array(
  'dataProvider' => $dataProvider,
  'id' => 'advbanners-grid',
  'template' => '{items}<div class="pull-right">{pager}</div>',
  'itemsCssClass' => 'table table-striped table-condensed',
  'columns' => array(
    array(
      'class' => 'CCheckBoxColumn',
      'id' => 'advbanners_cb',
      'selectableRows' => 2
    ),
    array('name' => 'name', 'header' => 'Название'),
    array('name' => 'vflag', 'header' => 'Включен', 'value' => '($data->vflag) ? "Да" : "Нет"'),
    array(
      'class' => 'bootstrap.widgets.TbButtonColumn',
      'htmlOptions' => array('style' => 'width: 50px'),
    ),
  ),
)); ?>

<div class="btn-group pull-left">
  <a class="btn dropdown-toggle btn-mini" data-toggle="dropdown" href="#">Отмеченные <span class="caret"></span></a>
  <ul class="dropdown-menu">
    <li><a href="#" class="multiple-select-action" id="id-hide">Выключить</a></li>
    <li><a href="#" class="multiple-select-action" id="id-show">Включить</a></li>
  </ul>
</div>
