<?php

/**
 * Copyright (c) 2012 by Dmitry Astrikov / www.astrikov.ru
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * This is the model class for table "adv_banners".
 *
 * The followings are the available columns in table 'adv_banners':
 * @property integer $adv_banners_id
 * @property integer $adv_places_id
 * @property integer $adv_sections_id
 * @property string $name
 * @property string $picture
 * @property string $code
 * @property string $link
 * @property string $title
 * @property string $date_start
 * @property string $date_end
 * @property string $date0
 * @property boolean $vflag
 *
 * The followings are the available model relations:
 * @property AdvPlaces $advPlaces
 * @property AdvSections $advSections
 */
class AdvBanners extends CPlumActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AdvBanners the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return $this->getTablePrefix().'adv_banners';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('adv_places_id, adv_sections_id', 'numerical', 'integerOnly'=>true),
			array('name, picture', 'length', 'max'=>255),
			array('code, vflag, date_start, date_end, link, title, picture', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('adv_banners_id, adv_places_id, adv_sections_id, name, picture, code, date_start, date_end, date0, vflag', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'advPlaces' => array(self::BELONGS_TO, 'AdvPlaces', 'adv_places_id'),
			'advSections' => array(self::BELONGS_TO, 'AdvSections', 'adv_sections_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'adv_banners_id' => 'Adv Banners',
			'adv_places_id' => 'Рекламное место',
			'adv_sections_id' => 'Рекламный раздел',
			'name' => 'Название',
			'picture' => 'Изображение',
            'title' => 'Подсказка при наведении',
            'link' => 'Ссылка',
			'code' => 'Код баннера',
			'date_start' => 'Дата начала показов',
			'date_end' => 'Дата окончания показов',
			'date0' => 'Дата создания',
			'vflag' => 'Включен',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('adv_banners_id',$this->adv_banners_id);
		$criteria->compare('adv_places_id',$this->adv_places_id);
		$criteria->compare('adv_sections_id',$this->adv_sections_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('picture',$this->picture,true);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('date_start',$this->date_start,true);
		$criteria->compare('date_end',$this->date_end,true);
		$criteria->compare('date0',$this->date0,true);
		$criteria->compare('vflag',$this->vflag);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


  protected function beforeSave()
  {
    $is_new = $this->getIsNewRecord();
    if($is_new)
    {
      $this->date_end = new CDbExpression('NOW()');
      $this->date0 = new CDbExpression('NOW()');
    }
    else
    {
      if($this->getAttribute('date_end')=='0000-00-00 00:00:00'){
        $this->date_end = new CDbExpression('NOW()');
      }
      if($this->getAttribute('date0')=='0000-00-00 00:00:00'){
        $this->date0 = new CDbExpression('NOW()');
      }
    }
    return parent::beforeSave();
  }
}