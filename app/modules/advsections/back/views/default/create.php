<?php
$this->breadcrumbs=array(
	'Рекламные разделы'=>array('index'),
	'Добавление',
);

$this->menu=array(
	array('label'=>'Список','url'=>array('index')),
	array('label'=>'Управление','url'=>array('admin')),
);
?>

<div class="page-header">
	<h4>Добавление рекламного раздела</h4>
</div>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>