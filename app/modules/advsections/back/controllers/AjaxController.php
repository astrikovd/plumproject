<?php

/**
 * Copyright (c) 2012 by Dmitry Astrikov / www.astrikov.ru
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Ajax controller
 * @package modules/advsections/back/controllers
 */
class AjaxController extends BackEndController
{

  /**
   * Router for ajax requests
   * @return void
   */
  public function actionRoute()
  {
    if (Yii::app()->request->isAjaxRequest) {
      if (isset($_POST['cp'])) {
        $checked = $_POST['checked'];
        switch ($_POST['cp']) {
          case "id-show":
            $res = $this->toggleChecked($checked, true);
            break;
          case "id-hide":
            $res = $this->toggleChecked($checked, false);
            break;
        }
        $result = array('result' => ($res) ? true : false);
        header('Content-type: application/json');
        echo CJavaScript::jsonEncode($result);
        Yii::app()->end();
      }
    } else
      throw new CHttpException(403);
  }

  /**
   * Toggle vflag field in adv sections
   * @param array $checked
   * @param boolean $show
   * @return boolean
   */
  private function toggleChecked($checked, $show)
  {
    $res = AdvSections::model()->updateByPk($checked, array('vflag' => $show));
    return ($res) ? true : false;
  }
}