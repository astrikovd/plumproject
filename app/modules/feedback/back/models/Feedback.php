<?php

/**
 * Copyright (c) 2012 by Dmitry Astrikov / www.astrikov.ru
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * This is the model class for table "feedback".
 *
 * The followings are the available columns in table 'feedback':
 * @property integer $feedback_id
 * @property string $fio
 * @property string $phone
 * @property string $question
 * @property string $answer
 * @property string $date_send
 * @property string $date_ans
 * @property boolean $vflag
 */
class Feedback extends CPlumActiveRecord
{
  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return Feedback the static model class
   */
  public static function model($className = __CLASS__)
  {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName()
  {
    return $this->getTablePrefix() . 'feedback';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules()
  {
    // NOTE: you should only define rules for those attributes that
    // will receive user inputs.
    return array(
      array('fio, phone', 'length', 'max' => 255),
      array('question, answer, date_send, date_ans, vflag', 'safe'),
      // The following rule is used by search().
      // Please remove those attributes that should not be searched.
      array('feedback_id, fio, phone, question, answer, date_send, date_ans, vflag', 'safe', 'on' => 'search'),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations()
  {
    // NOTE: you may need to adjust the relation name and the related
    // class name for the relations automatically generated below.
    return array();
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels()
  {
    return array(
      'fio' => 'Отправитель',
      'phone' => 'Контактный телефон',
      'question' => 'Текст вопроса',
      'answer' => 'Текст ответа',
      'date_send' => 'Дата отправки',
      'date_ans' => 'Дата ответа',
      'vflag' => 'Показывать на сайте',
    );
  }

  /**
   * Retrieves a list of models based on the current search/filter conditions.
   * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
   */
  public function search()
  {
    // Warning: Please modify the following code to remove attributes that
    // should not be searched.

    $criteria = new CDbCriteria;

    $criteria->compare('feedback_id', $this->feedback_id);
    $criteria->compare('fio', $this->fio, true);
    $criteria->compare('phone', $this->phone, true);
    $criteria->compare('question', $this->question, true);
    $criteria->compare('answer', $this->answer, true);
    $criteria->compare('date_send', $this->date_send, true);
    $criteria->compare('date_ans', $this->date_ans, true);
    $criteria->compare('vflag', $this->vflag);

    return new CActiveDataProvider($this, array(
      'criteria' => $criteria,
    ));
  }

  protected function beforeSave()
  {
    $is_new = $this->getIsNewRecord();
    if ($is_new) {
      $this->date_ans = new CDbExpression('NOW()');
    } else {
      if ($this->getAttribute('date_create') == '0000-00-00 00:00:00') {
        $this->date_ans = new CDbExpression('NOW()');
      }
    }
    return parent::beforeSave();
  }
}