<div class="view">

  <b><?php echo CHtml::encode($data->getAttributeLabel('feedback_id')); ?>:</b>
  <?php echo CHtml::link(CHtml::encode($data->feedback_id), array('view', 'id' => $data->feedback_id)); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('fio')); ?>:</b>
  <?php echo CHtml::encode($data->fio); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('phone')); ?>:</b>
  <?php echo CHtml::encode($data->phone); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('question')); ?>:</b>
  <?php echo CHtml::encode($data->question); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('answer')); ?>:</b>
  <?php echo CHtml::encode($data->answer); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('date_send')); ?>:</b>
  <?php echo CHtml::encode($data->date_send); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('date_ans')); ?>:</b>
  <?php echo CHtml::encode($data->date_ans); ?>
  <br/>

  <?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('vflag')); ?>:</b>
	<?php echo CHtml::encode($data->vflag); ?>
	<br />

	*/ ?>

</div>