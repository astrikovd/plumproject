<?php
$this->breadcrumbs=array(
	'Обратная связь'=>array('index'),
	'Управление',
);

$this->menu=array(
	array('label'=>'Список','url'=>array('index')),
	array('label'=>'Добавление','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('feedback-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="page-header">
	<h4>Управление сообщениями обратной связи</h4>
</div>

<div class="alert alert-info">
    <strong>Данный раздел предназначен для быстрого поиска и изменения сообщений, поступающих через раздел "Обратная связь" на сайте.</strong>
    Доступны функции добавления, удаления и редактирования.
</div>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'feedback-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'fio',
		array('name' =>'date_send', 'value' => 'date("d.m.Y H:i:s", strtotime($data->date_send))'),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
