<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
  'id' => 'feedback-form',
  'enableAjaxValidation' => false,
)); ?>

<div class="alert alert-info">
  Поля, отмеченные <span class="red">*</span>, обязательны для заполнения.
</div>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->textFieldRow($model, 'fio', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php echo $form->textFieldRow($model, 'phone', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php echo $form->labelEx($model, 'question'); ?>
<?php $this->widget('application.lib.extensions.redactorjs.ImperaviRedactorWidget',
  array('model' => $model,
    'attribute' => 'question',
    'htmlOptions' => array('style' => 'width: 100%; height: 150px;'),
    'options' => array(
      'imageUpload' => Yii::app()->params->back_url . '/upload/',
    )
  ));?>

<br/>

<?php echo $form->labelEx($model, 'answer'); ?>
<?php $this->widget('application.lib.extensions.redactorjs.ImperaviRedactorWidget',
  array('model' => $model,
    'attribute' => 'answer',
    'htmlOptions' => array('style' => 'width: 100%; height: 150px;'),
    'options' => array(
      'imageUpload' => Yii::app()->params->back_url . '/upload/',
    )
  ));?>

<hr>

<?php echo $form->labelEx($model, 'date_send'); ?>
<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
  'name' => 'date_send',
  'attribute' => 'date_send',
  'model' => $model,
  'value' => Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($model->date_send, 'dd.mm.YYYY'), 'medium', null),
  // additional javascript options for the date picker plugin
  'options' => array(
    'showAnim' => false,
    'dateFormat' => 'dd.mm.yy',
  ),
  'language' => 'ru',
  'htmlOptions' => array(
    'style' => 'height:20px;'
  ),
));?>

<?php echo $form->labelEx($model, 'date_ans'); ?>
<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
  'name' => 'date_ans',
  'attribute' => 'date_ans',
  'model' => $model,
  'value' => Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($model->date_ans, 'dd.mm.YYYY'), 'medium', null),
  // additional javascript options for the date picker plugin
  'options' => array(
    'showAnim' => false,
    'dateFormat' => 'dd.mm.yy',
  ),
  'language' => 'ru',
  'htmlOptions' => array(
    'style' => 'height:20px;'
  ),
));?>

<hr>

<?php echo $form->checkBoxRow($model, 'vflag'); ?>

<div class="form-actions">
  <a class="btn btn-default" href="javascript:history.back();"><i class="icon icon-chevron-left"></i> Назад</a>
  <?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
    'type' => 'primary',
    'label' => $model->isNewRecord ? 'Добавить' : 'Сохранить',
  )); ?>
</div>

<?php $this->endWidget(); ?>
