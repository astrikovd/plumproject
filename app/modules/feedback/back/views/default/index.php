<?php Yii::app()->clientScript->registerScriptFile('/static/js/feedback.js');?>

<?php
$this->breadcrumbs=array(
	'Обратная связь',
);

$this->menu=array(
	array('label'=>'Добавление','url'=>array('create')),
	array('label'=>'Управление','url'=>array('admin')),
);
?>

<div class="page-header">
	<h4>Обратная связь</h4>
</div>

<div class="alert alert-info">
    <strong>Данный раздел предназначен для управления сообщениями, поступающими через форму обратной связи.</strong>
    Доступны функции добавления, удаления и редактирования сообщений.
</div>

<? $this->widget('bootstrap.widgets.TbGridView', array(
    'dataProvider'=>$dataProvider,
    'id'=>'feedback-grid',
    'template'=>'{items}<div class="pull-right">{pager}</div>',
    'itemsCssClass'=>'table table-striped table-condensed',
    'columns'=>array(
        array(
           'class'=>'CCheckBoxColumn',
           'id'=>'feedback_cb',
           'selectableRows'=>2
        ),
        array('name'=>'fio', 'header'=>'Отправитель'),
        array('name'=>'date_send', 'header'=>'Дата отправки','value'=>'date("d.m.Y", strtotime($data->date_send))'),
        array('name'=>'vflag', 'header'=>'Показывать на сайте', 'value' => '($data->vflag) ? "Да" : "Нет"'),
        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'htmlOptions'=>array('style'=>'width: 50px'),
        ),
    ),
)); ?>

<div class="btn-group pull-left">
    <a class="btn dropdown-toggle btn-mini" data-toggle="dropdown" href="#">Отмеченные <span class="caret"></span></a>
    <ul class="dropdown-menu">
        <li><a href="#" class="multiple-select-action" id="id-hide">Скрыть</a></li>
        <li><a href="#" class="multiple-select-action" id="id-show">Показать</a></li>
    </ul>
</div>