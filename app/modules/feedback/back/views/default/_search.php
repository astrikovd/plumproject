<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
  'action' => Yii::app()->createUrl($this->route),
  'method' => 'get',
)); ?>

<?php echo $form->textFieldRow($model, 'feedback_id', array('class' => 'span5')); ?>

<?php echo $form->textFieldRow($model, 'fio', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php echo $form->textFieldRow($model, 'phone', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php echo $form->textAreaRow($model, 'question', array('rows' => 6, 'cols' => 50, 'class' => 'span8')); ?>

<?php echo $form->textAreaRow($model, 'answer', array('rows' => 6, 'cols' => 50, 'class' => 'span8')); ?>

<?php echo $form->textFieldRow($model, 'date_send', array('class' => 'span5')); ?>

<?php echo $form->textFieldRow($model, 'date_ans', array('class' => 'span5')); ?>

<?php echo $form->checkBoxRow($model, 'vflag'); ?>

<div class="form-actions">
  <?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit'
  'type'=>'primary',
  'label'=>'Search',
  )); ?>
</div>

<?php $this->endWidget(); ?>
