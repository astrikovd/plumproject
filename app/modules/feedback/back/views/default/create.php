<?php
$this->breadcrumbs = array(
  'Обратная связь' => array('index'),
  'Добавление',
);

$this->menu = array(
  array('label' => 'Список', 'url' => array('index')),
  array('label' => 'Управление', 'url' => array('admin')),
);
?>

  <div class="page-header">
    <h4>Добавление сообщения</h4>
  </div>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>