<?php
$this->breadcrumbs = array(
  'Обратная связь' => array('index'),
  "Сообщение от {$model->fio}",
);

$this->menu = array(
  array('label' => 'Список', 'url' => array('index')),
  array('label' => 'Добавление', 'url' => array('create')),
  array('label' => 'Изменение', 'url' => array('update', 'id' => $model->feedback_id)),
  array('label' => 'Удаление', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->feedback_id), 'confirm' => 'Are you sure you want to delete this item?')),
  array('label' => 'Управление', 'url' => array('admin')),
);
?>

<div class="page-header">
  <h4>Сообщение от <?=$model->fio?></h4>
</div>


<?php $this->widget('bootstrap.widgets.TbDetailView', array(
  'data' => $model,
  'attributes' => array(
    'fio',
    'phone',
    array('name' => 'question', 'type' => 'raw'),
    array('name' => 'answer', 'type' => 'raw'),
    array('name' => 'date_send', 'value' => date('d.m.Y H:i:s', strtotime($model->date_send))),
    array('name' => 'date_ans', 'value' => ($model->answer != "") ? date('d.m.Y H:i:s', strtotime($model->date_ans)) : "Не отвечено"),
    array('name' => 'vflag', 'value' => $model->vflag ? "Да" : "Нет"),
  ),
)); ?>
