<?php
$this->breadcrumbs=array(
	'Обратная связь'=>array('index'),
	"Сообщение от {$model->fio}"=>array('view','id'=>$model->feedback_id),
	'Изменение',
);

$this->menu=array(
	array('label'=>'Список','url'=>array('index')),
	array('label'=>'Добавление','url'=>array('create')),
	array('label'=>'Просмотр','url'=>array('view','id'=>$model->feedback_id)),
	array('label'=>'Управление','url'=>array('admin')),
);
?>

<div class="page-header">
	<h4>Изменение сообщения</h4>
</div>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>