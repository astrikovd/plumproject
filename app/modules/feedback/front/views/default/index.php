<?php Yii::app()->controller->module->registerResource('feedback','feedback.js')?>
<?php Yii::app()->controller->module->registerResource('feedback','jquery.form.js')?>

<?php if($this->pageData->hflag): ?>
    <div class="page-header">
    	<h3><?=$this->pageData->header;?></h3>
    </div>
<?php endif; ?>

<div id="id-form-result" class="hidden"></div>
<form method="POST" class="custom" id="id-feedback-form">
	<label for="id-fio">Представьтесь, пожалуйста: <span class="required">*</span></label>
	<input type="text" class="span5" name="fio" id="id-fio"/>
	<label for="id-question">Текст вопроса: <span class="required">*</span></label>
	<textarea name="question" id="id-question" style="width:605px; height:150px;"></textarea>
	<label for="id-captcha">Сколько будет <?=$aspam['num1'];?> + <?=$aspam['num2'];?>? <span class="required">*</span></label>
	<input type="text" class="span5" name="captcha" id="id-captcha"/>
	<input type="hidden" name="captcha_hash" value="<?=$aspam['answer'];?>"/>
	<p class="help-block">Поля, отмеченные <span class="required">*</span>, обязательны для заполнения.</p>
	<br/>
	<input type="submit" value="Отправить" name="send_form" class="btn"/>
</form>