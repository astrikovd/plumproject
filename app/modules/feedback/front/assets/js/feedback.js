$(function () {
  var options = {
    success: function (result) {
      if (result.success) {
        $('#id-form-result').removeClass('alert-error');
        $('#id-form-result').addClass('alert alert-success');
        $('#id-feedback-form').resetForm();
      }
      else {
        $('#id-form-result').removeClass('alert-success');
        $('#id-form-result').addClass('alert alert-error');
      }
      $('#id-form-result').html(result.msg);
      $('#id-form-result').show();
    },
    dataType: 'json'
  };
  $('#id-feedback-form').ajaxForm(options);
});