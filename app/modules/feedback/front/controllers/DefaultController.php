<?php

/**
 * Copyright (c) 2012 by Dmitry Astrikov / www.astrikov.ru
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Default controller for feedback module
 * @package modules/feedback/front/controllers/
 */
class DefaultController extends FrontEndController
{

	/**
	 * @var layout
	 */
	public $layout ="//layouts/col";

    public function actionIndex()
    {
    	if(isset($_POST['send_form'])){
			$errors = array();

			if(strlen($_POST['fio'])>255 || strlen(trim($_POST['fio']))==0)
				$errors[] = "Поле \"Представьтесь, пожалуйста\" заполнено неправильно";
			if(strlen(trim($_POST['question']))==0)
				$errors[] = "Поле \"Текст вопроса\" заполнено неправильно";
			if($_POST['captcha_hash']!=md5($_POST['captcha']))
				$errors[] = "Вы неверно ввели код проверки";

			if(empty($errors)){
				$fio = htmlspecialchars($_POST['fio'], ENT_QUOTES);
				$question = htmlspecialchars($_POST['question'], ENT_QUOTES);

				$model = new Feedback;
				$model->fio = $fio;
				$model->question = $question;

				$result = $model->save() ? array('success' => true, 'msg' => 'Ваше сообщение успешно отправлено.') :
										array('success' => false, 'msg' => 'Ошибка при отправке сообщения.');
			}
			else
				$result = array('success' => false, 'msg' => 'При отправке формы возникли следующие ошибки:<br/>'.implode('<br/>', $errors));
			echo json_encode($result);
			die();
		}
        $this->render('index', array('aspam' => CVerifyCode::create()));
    }
}