<?php
$this->breadcrumbs=array(
	"Изменить пароль",
);
?>

<div class="page-header">
    <h4>Изменение пароля</h4>
</div>
<? if (isset($errors)): ?>
    <div class="alert alert-error">
        <p><?=$errors;?></p>
    </div>
<? elseif(isset($res)): ?>
    <div class="alert alert-success">
        <p>Пароль успешно изменен.</p>
    </div>
<? endif; ?>
<form method="POST">
    <label for="oldpswd">Текущий пароль:</label>
    <input type="password" name="oldpswd" id="oldpswd"/>
    <label for="newpswd">Новый пароль:</label>
    <input type="password" name="newpswd" id="newpswd"/>
    <label for="newpswdre">Повторите пароль:</label>
    <input type="password" name="newpswdre" id="newpswdre"/>
    <div class="form-actions">
        <input type="submit" class="btn btn-primary" value="Изменить пароль" name="change_password">
    </div>
</form>
