<?php

/**
 * Copyright (c) 2012 by Dmitry Astrikov / www.astrikov.ru
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Default controller for chpasswd module
 * @package modules/chpasswd/back/controllers
 */
class DefaultController extends BackEndController
{

  /**
   * @var layout
   */
  public $layout = '//layouts/index';

  /**
   * Index action
   * @return void
   */
  public function actionIndex()
  {
    $errors = array();
    if (isset($_POST['change_password'])) {

      $old_password = $_POST['oldpswd'];
      $user = AdmUser::model()->findByPk(Yii::app()->user->user_id);
      if ($user->passwordmd5 != md5($old_password))
        $errors[] = "Текущий пароль введен неправильно";

      if ($_POST['newpswd'] !== $_POST['newpswdre'])
        $errors[] = "Введенные пароли не совпадают";

      if (!preg_match("/[a-zA-Z0-9_-]{6,}/", $_POST['newpswd']))
        $errors[] = "Длина пароля должна быть не менее 6 символов. Пароль может состоять
                            из английских букв, цифр, символов подчеркивания или тире";

      if (!empty($errors)) {
        return $this->render('index', array('errors' => implode('<br/>', $errors)));
      } else {
        $user->passwordmd5 = md5($_POST['newpswd']);
        $user->save();
        return $this->render('index', array('res' => true));
      }
    }
    $this->render('index');
  }
}