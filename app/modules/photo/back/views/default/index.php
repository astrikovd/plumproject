<?php Yii::app()->clientScript->registerScriptFile('/static/js/photo.js');?>

<?php
$this->breadcrumbs=array(
	'Фото',
);

$this->menu=array(
	array('label'=>'Добавление','url'=>array('create')),
	array('label'=>'Управление','url'=>array('admin')),
);
?>

<div class="page-header">
	<h4>Фотографии</h4>
</div>

<div class="alert alert-info">
    <strong>Данный раздел предназначен для управления фотографиями.</strong>
    Доступны функции добавления, удаления и редактирования фотографий.
</div>

<? $this->widget('bootstrap.widgets.TbGridView', array(
    'dataProvider'=>$dataProvider,
    'id'=>'photo-grid',
    'template'=>'{items}<div class="pull-right">{pager}</div>',
    'itemsCssClass'=>'table table-striped table-condensed',
    'columns'=>array(
        array(
           'class'=>'CCheckBoxColumn',
           'id'=>'photo_cb',
           'selectableRows'=>2
        ),
        array('name'=>'name', 'header'=>'Название'),
        array('name'=>'photogallery.name', 'header'=>'Галерея'),
        array('name'=>'vflag', 'header'=>'Показывать на сайте', 'value' => '($data->vflag) ? "Да" : "Нет"'),
        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'htmlOptions'=>array('style'=>'width: 50px'),
        ),
    ),
)); ?>

<div class="btn-group pull-left">
    <a class="btn dropdown-toggle btn-mini" data-toggle="dropdown" href="#">Отмеченные <span class="caret"></span></a>
    <ul class="dropdown-menu">
        <li><a href="#" class="multiple-select-action" id="id-hide">Скрыть</a></li>
        <li><a href="#" class="multiple-select-action" id="id-show">Показать</a></li>
    </ul>
</div>
