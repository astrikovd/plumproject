<div class="view">

  <b><?php echo CHtml::encode($data->getAttributeLabel('photo_id')); ?>:</b>
  <?php echo CHtml::link(CHtml::encode($data->photo_id), array('view', 'id' => $data->photo_id)); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('photogallery_id')); ?>:</b>
  <?php echo CHtml::encode($data->photogallery_id); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
  <?php echo CHtml::encode($data->name); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('author')); ?>:</b>
  <?php echo CHtml::encode($data->author); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
  <?php echo CHtml::encode($data->description); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('picture')); ?>:</b>
  <?php echo CHtml::encode($data->picture); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('date_create')); ?>:</b>
  <?php echo CHtml::encode($data->date_create); ?>
  <br/>

  <?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('vflag')); ?>:</b>
	<?php echo CHtml::encode($data->vflag); ?>
	<br />

	*/ ?>

</div>