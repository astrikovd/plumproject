<?php
$this->breadcrumbs = array(
  'Фото' => array('index'),
  $model->name,
);

$this->menu = array(
  array('label' => 'Список', 'url' => array('index')),
  array('label' => 'Добавление', 'url' => array('create')),
  array('label' => 'Изменение', 'url' => array('update', 'id' => $model->photo_id)),
  array('label' => 'Удаление', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->photo_id), 'confirm' => 'Вы уверены? Это действие нельзя будет отменить')),
  array('label' => 'Управление', 'url' => array('admin')),
);
?>

<div class="page-header">
  <h4>Фотография «<?php echo $model->name; ?>»</h4>
</div>

<?php $this->widget('application.lib.extensions.fancybox.EFancyBox', array(
    'target' => 'a[rel=gallery]',
    'config' => array(),
  )
);?>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
  'data' => $model,
  'attributes' => array(
    'photogallery.name',
    'name',
    'author',
    array('name' => 'description', 'type' => 'raw'),
    array('name' => 'picture', 'type' => 'raw', 'value' => '<a href="' . Yii::app()->params->front_url . $model->picture . '" rel="gallery">' . CHtml::image(Yii::app()->params->front_url . $model->picture) . '</a>'),
    array('name' => 'vflag', 'value' => $model->vflag ? "Да" : "Нет"),
    array('name' => 'date_create', 'value' => date('d.m.Y H:i:s', strtotime($model->date_create))),
  ),
)); ?>
