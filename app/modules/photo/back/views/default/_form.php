<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
  'id' => 'photo-form',
  'enableAjaxValidation' => false,
  'htmlOptions' => array('enctype' => 'multipart/form-data')
)); ?>

<?php if (!empty($photogalleries)): ?>

  <div class="alert alert-info">
    Поля, отмеченные <span class="red">*</span>, обязательны для заполнения.
  </div>

  <?php echo $form->errorSummary($model); ?>

  <div class="page-header">
    <h5>Основные настройки</h5>
  </div>

  <?php echo $form->labelEx($model, 'photogallery_id'); ?>
  <?php echo $form->dropDownList($model, 'photogallery_id', $photogalleries, array('class' => 'span5')); ?>


  <?php echo $form->textFieldRow($model, 'name', array('class' => 'span5', 'maxlength' => 255)); ?>

  <?php echo $form->textFieldRow($model, 'author', array('class' => 'span5', 'maxlength' => 255)); ?>

  <?php echo $form->labelEx($model, 'description'); ?>
  <?php $this->widget('application.lib.extensions.redactorjs.ImperaviRedactorWidget',
    array('model' => $model,
      'attribute' => 'description',
      'htmlOptions' => array('style' => 'width: 100%; height: 150px;'),
      'options' => array(
        'imageUpload' => Yii::app()->params->back_url . '/upload/',
      )
    ));?>

  <hr>

  <?php echo $form->fileFieldRow($model, 'picture'); ?>
  <?php if ($model->picture && !$model->isNewRecord): ?>
    <div id="id-photo-controls" class="photo-controls">
      <a href="<?=Yii::app()->params->front_url . $model->picture?>" class="btn btn-mini" target="_blank"
         id="id-btn-view">Посмотреть <i class="icon-eye-open"></i></a>
      <a href="<?=Yii::app()->params->front_url . $model->picture?>" class="btn btn-warning btn-mini"
         id="id-btn-delete">Удалить <i class="icon-remove"></i></a>
    </div>
  <?php endif; ?>

  <div class="page-header">
    <h5>Настройки отображения</h5>
  </div>

  <?php echo $form->labelEx($model, 'date_create'); ?>
  <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
    'name' => 'date_create',
    'model' => $model,
    'attribute' => 'date_create',
    // additional javascript options for the date picker plugin
    'options' => array(
      'showAnim' => false,
      'dateFormat' => 'dd.mm.yy',
    ),
    'language' => 'ru',
    'htmlOptions' => array(
      'style' => 'height:20px;'
    ),
  ));?>

  <?php echo $form->checkBoxRow($model, 'vflag'); ?>

  <div class="form-actions">
    <a class="btn btn-default" href="javascript:history.back();"><i class="icon icon-chevron-left"></i> Назад</a>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
      'buttonType' => 'submit',
      'type' => 'primary',
      'label' => $model->isNewRecord ? 'Добавить' : 'Сохранить',
    )); ?>
  </div>

<?php else: ?>
  <div class="alert alert-error">
    Для добавления новостей необходимо <a href="/photogallery/default/create" target="_blank">добавить</a> хотя бы одну
    фотогалерею.
  </div>
<?php endif; ?>

<?php $this->endWidget(); ?>
