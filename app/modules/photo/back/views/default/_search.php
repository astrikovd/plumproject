<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
  'action' => Yii::app()->createUrl($this->route),
  'method' => 'get',
)); ?>

<?php echo $form->textFieldRow($model, 'photo_id', array('class' => 'span5')); ?>

<?php echo $form->textFieldRow($model, 'photogallery_id', array('class' => 'span5')); ?>

<?php echo $form->textFieldRow($model, 'name', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php echo $form->textFieldRow($model, 'author', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php echo $form->textAreaRow($model, 'description', array('rows' => 6, 'cols' => 50, 'class' => 'span8')); ?>

<?php echo $form->textFieldRow($model, 'picture', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php echo $form->textFieldRow($model, 'date_create', array('class' => 'span5')); ?>

<?php echo $form->checkBoxRow($model, 'vflag'); ?>

<div class="form-actions">
  <?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit'
  'type'=>'primary',
  'label'=>'Search',
  )); ?>
</div>

<?php $this->endWidget(); ?>
