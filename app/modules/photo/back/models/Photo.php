<?php

/**
 * Copyright (c) 2012 by Dmitry Astrikov / www.astrikov.ru
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * This is the model class for table "photo".
 *
 * The followings are the available columns in table 'photo':
 * @property integer $photo_id
 * @property integer $photogallery_id
 * @property string $name
 * @property string $author
 * @property string $description
 * @property string $picture
 * @property string $date_create
 * @property boolean $vflag
 *
 * The followings are the available model relations:
 * @property Photogallery $photogallery
 */
class Photo extends CPlumActiveRecord
{
  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return Photo the static model class
   */
  public static function model($className = __CLASS__)
  {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName()
  {
    return $this->getTablePrefix() . 'photo';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules()
  {
    // NOTE: you should only define rules for those attributes that
    // will receive user inputs.
    return array(
      array('name', 'required'),
      array('photogallery_id', 'numerical', 'integerOnly' => true),
      array('name, author, picture', 'length', 'max' => 255),
      array('description, vflag', 'safe'),
      // The following rule is used by search().
      // Please remove those attributes that should not be searched.
      array('photo_id, photogallery_id, name, author, description, picture, date_create, vflag', 'safe', 'on' => 'search'),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations()
  {
    // NOTE: you may need to adjust the relation name and the related
    // class name for the relations automatically generated below.
    return array(
      'photogallery' => array(self::BELONGS_TO, 'Photogallery', 'photogallery_id'),
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels()
  {
    return array(
      'photo_id' => 'Photo',
      'photogallery_id' => 'Фотогалерея',
      'name' => 'Название',
      'author' => 'Автор',
      'description' => 'Описание',
      'picture' => 'Изображение',
      'date_create' => 'Дата добавления',
      'vflag' => 'Показывать на сайте',
    );
  }

  /**
   * Retrieves a list of models based on the current search/filter conditions.
   * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
   */
  public function search()
  {
    // Warning: Please modify the following code to remove attributes that
    // should not be searched.

    $criteria = new CDbCriteria;

    $criteria->compare('photo_id', $this->photo_id);
    $criteria->compare('photogallery_id', $this->photogallery_id);
    $criteria->compare('name', $this->name, true);
    $criteria->compare('author', $this->author, true);
    $criteria->compare('description', $this->description, true);
    $criteria->compare('picture', $this->picture, true);
    $criteria->compare('date_create', $this->date_create, true);
    $criteria->compare('vflag', $this->vflag);

    return new CActiveDataProvider($this, array(
      'criteria' => $criteria,
    ));
  }
}