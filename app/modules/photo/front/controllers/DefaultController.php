<?php

/**
 * Copyright (c) 2012 by Dmitry Astrikov / www.astrikov.ru
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Default controller for photo module
 * @package modules/photo/front/controllers/
 */
class DefaultController extends FrontEndController
{

  /**
   * @var default layout
   */
  public $layout = '//layouts/col';

  /**
   * Index Action
   * @return void
   */
  public function actionIndex()
  {
    //Get photogallery info
    $photogallery_id = $this->pageData->page_module_param;
    $photogallery = Photogallery::model()->findByPk($photogallery_id);

    //Forming criteria
    $criteria = new CDbCriteria;
    $criteria->order = "date_create desc";
    $criteria->condition = "vflag = true AND photogallery_id = {$photogallery_id}";

    $active_page = isset($this->params['pg']) ? $this->params['pg'] : 1;

    $photo_count = Photo::model()->count($criteria);

    //Adding limit and offset to db_criteria
    $criteria->limit = $photogallery->cnt;
    $criteria->offset = $photogallery->cnt * ($active_page - 1);

    //Get newsitems
    $photo = Photo::model()->findAll($criteria);

    //Create pager
    $pager = CPager::create($this->pageData['url_path'], $photo_count, $photogallery->cnt, $active_page);

    $this->render('index', array('photogallery' => $photogallery,
      'pager' => $pager,
      'photo' => $photo));
  }

}