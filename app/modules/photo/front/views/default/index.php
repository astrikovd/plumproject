<?php if ($this->pageData->hflag): ?>
  <div class="page-header">
    <h4><?php echo $this->pageData->header;?></h4>
  </div>
<?php endif; ?>
<?php if (!empty($photo)): ?>
  <?php $this->widget('application.lib.extensions.fancybox.EFancyBox', array(
      'target' => 'a[rel=gallery]',
      'config' => array(),
    )
  );?>

  <?php $this->beginWidget('application.lib.extensions.thumbnailer.Thumbnailer', array(
      'thumbsDir' => 'data/photo/thumbs',
      'thumbWidth' => 187
    )
  );?>
  <div class="row">
    <ul class="photos">
      <? foreach ($photo as $key => $p): ?>
        <li class="photo-item">
          <div class="inner-img">
            <a href="<?php echo $p->picture?>" rel="gallery"><?php echo CHtml::image($p->picture)?></a>
          </div>
        </li>
      <? endforeach; ?>
    </ul>
  </div>
  <? $this->endWidget(); ?>
<? endif; ?>
<?= $pager
; ?>