<?php

/**
 * Copyright (c) 2012 by Dmitry Astrikov / www.astrikov.ru
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Adv banners module
 * @package modules/advbanners
 */
class AdvbannersModule extends CPlumModule
{
	/**
	 * Module initialization
	 * @return void
	 */
	public function init()
	{
		$endName = Yii::app()->endName;
		$this->setImport(
			array(
				"advbanners.{$endName}.models.*",
				"advplaces.back.models.*",
				"advsections.back.models.*"
				)
		);
		Yii::app()->onModuleCreate(new CEvent($this));
	}

    /**
     * Get advertisement banner
     * @param $char_id
     * @param $adv_sections_id
     * @return string
     */
    public function getBanner($char_id, $adv_sections_id)
	{
        $banner = AdvBanners::model()->find(array('condition' =>
               'char_id = :char_id AND adv_sections_id = :adv_sections_id AND
                ap_vflag AND ab_vflag AND as_vflag AND date_start < now() AND
                date_end > NOW()',
            'order' => Yii::app()->db->driverName == 'mysql' ? 'rand()' : 'random()',
           'limit' => 1,
           'params' => array('char_id' => $char_id, 'adv_sections_id' => $adv_sections_id)));
        if($banner) {
            return ($banner->code) ? $banner->code : "<a href='{$banner->link}' title='{$banner->title}'><img src='{$banner->picture}'/></a>";
        }
	}
}
