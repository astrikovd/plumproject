<?php
$this->breadcrumbs = array(
  'Рекламные места' => array('index'),
  $model->name,
);

$this->menu = array(
  array('label' => 'Список', 'url' => array('index')),
  array('label' => 'Изменение', 'url' => array('update', 'id' => $model->adv_places_id)),
  array('label' => 'Управление', 'url' => array('admin')),
);
?>

<div class="page-header">
  <h4>Рекламное место «<?php echo $model->name; ?>»</h4>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
  'data' => $model,
  'attributes' => array(
    'name',
    array('name' => 'vflag', 'value' => $model->vflag ? "Да" : "Нет"),
    array('name' => 'date0', 'value' => date('d.m.Y H:i:s', strtotime($model->date0))),
  ),
)); ?>
