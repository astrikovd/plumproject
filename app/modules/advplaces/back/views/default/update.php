<?php
$this->breadcrumbs=array(
	'Рекламные места'=>array('index'),
	$model->name=>array('view','id'=>$model->adv_places_id),
	'Изменение',
);

$this->menu=array(
	array('label'=>'Список','url'=>array('index')),
	array('label'=>'Просмотр','url'=>array('view','id'=>$model->adv_places_id)),
	array('label'=>'Управление','url'=>array('admin')),
);
?>

<div class="page-header">
	<h4>Изменение рекламного места</h4>
</div>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>