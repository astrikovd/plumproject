<?php
$this->breadcrumbs = array(
  'Рекламные места' => array('index'),
  'Управление',
);

$this->menu = array(
  array('label' => 'Список', 'url' => array('index')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('adv-places-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="page-header">
  <h4>Управление рекламными местами</h4>
</div>

<div class="alert alert-info">
  <strong>Данный раздел предназначен для быстрого поиска и изменения рекламных мест.</strong>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
  'id' => 'adv-places-grid',
  'dataProvider' => $model->search(),
  'filter' => $model,
  'columns' => array(
    'name',
    array(
      'class' => 'bootstrap.widgets.TbButtonColumn',
      'template' => '{view} {update}'
    ),
  ),
)); ?>
