<div class="view">

  <b><?php echo CHtml::encode($data->getAttributeLabel('adv_places_id')); ?>:</b>
  <?php echo CHtml::link(CHtml::encode($data->adv_places_id), array('view', 'id' => $data->adv_places_id)); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
  <?php echo CHtml::encode($data->name); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('char_id')); ?>:</b>
  <?php echo CHtml::encode($data->char_id); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('date0')); ?>:</b>
  <?php echo CHtml::encode($data->date0); ?>
  <br/>

  <b><?php echo CHtml::encode($data->getAttributeLabel('vflag')); ?>:</b>
  <?php echo CHtml::encode($data->vflag); ?>
  <br/>


</div>