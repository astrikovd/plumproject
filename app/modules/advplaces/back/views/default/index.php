<?php Yii::app()->clientScript->registerScriptFile('/static/js/advplaces.js'); ?>

<?php
$this->breadcrumbs = array(
  'Рекламные места',
);

$this->menu = array(
  array('label' => 'Управление', 'url' => array('admin')),
);
?>

<div class="page-header">
  <h4>Рекламные места</h4>
</div>

<div class="alert alert-info">
  <strong>Данный раздел предназначен для управления отображением рекламных мест.</strong>
</div>

<? $this->widget('bootstrap.widgets.TbGridView', array(
  'dataProvider' => $dataProvider,
  'id' => 'advplaces-grid',
  'template' => '{items}<div class="pull-right">{pager}</div>',
  'itemsCssClass' => 'table table-striped table-condensed',
  'columns' => array(
    array(
      'class' => 'CCheckBoxColumn',
      'id' => 'advplaces_cb',
      'selectableRows' => 2
    ),
    array('name' => 'name', 'header' => 'Рекламное место'),
    array('name' => 'vflag', 'header' => 'Включено', 'value' => '($data->vflag) ? "Да" : "Нет"'),
    array(
      'class' => 'bootstrap.widgets.TbButtonColumn',
      'htmlOptions' => array('style' => 'width: 50px'),
      'template' => '{view} {update}'
    ),
  ),
)); ?>

<div class="btn-group pull-left">
  <a class="btn dropdown-toggle btn-mini" data-toggle="dropdown" href="#">Отмеченные <span class="caret"></span></a>
  <ul class="dropdown-menu">
    <li><a href="#" class="multiple-select-action" id="id-hide">Скрыть</a></li>
    <li><a href="#" class="multiple-select-action" id="id-show">Показать</a></li>
  </ul>
</div>
