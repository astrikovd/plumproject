<?php

/**
 * Copyright (c) 2012 by Dmitry Astrikov / www.astrikov.ru
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * This is the model class for table "adv_places".
 *
 * The followings are the available columns in table 'adv_places':
 * @property integer $adv_places_id
 * @property string $name
 * @property string $char_id
 * @property string $date0
 * @property boolean $vflag
 *
 * The followings are the available model relations:
 * @property AdvBanners[] $advBanners
 */
class AdvPlaces extends CPlumActiveRecord
{
  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return AdvPlaces the static model class
   */
  public static function model($className = __CLASS__)
  {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName()
  {
    return $this->getTablePrefix() . 'adv_places';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules()
  {
    // NOTE: you should only define rules for those attributes that
    // will receive user inputs.
    return array(
      array('char_id, date0', 'required'),
      array('name', 'length', 'max' => 255),
      array('char_id', 'length', 'max' => 64),
      array('vflag', 'safe'),
      // The following rule is used by search().
      // Please remove those attributes that should not be searched.
      array('adv_places_id, name, char_id, date0, vflag', 'safe', 'on' => 'search'),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations()
  {
    // NOTE: you may need to adjust the relation name and the related
    // class name for the relations automatically generated below.
    return array(
      'advBanners' => array(self::HAS_MANY, 'AdvBanners', 'adv_places_id'),
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels()
  {
    return array(
      'adv_places_id' => 'Adv Places',
      'name' => 'Название',
      'char_id' => 'Идентификатор',
      'date0' => 'Дата создания',
      'vflag' => 'Включено',
    );
  }

  /**
   * Retrieves a list of models based on the current search/filter conditions.
   * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
   */
  public function search()
  {
    // Warning: Please modify the following code to remove attributes that
    // should not be searched.

    $criteria = new CDbCriteria;

    $criteria->compare('adv_places_id', $this->adv_places_id);
    $criteria->compare('name', $this->name, true);
    $criteria->compare('char_id', $this->char_id, true);
    $criteria->compare('date0', $this->date0, true);
    $criteria->compare('vflag', $this->vflag);

    return new CActiveDataProvider($this, array(
      'criteria' => $criteria,
    ));
  }
}