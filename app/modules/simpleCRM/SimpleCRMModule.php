<?php
/**
 *
 * @author: sem
 * Date: 23.12.12
 * Time: 10:02
 */
class SimpleCRMModule extends CPlumModule
{
  public $model = null;

  /**
   * Module initialization
   * @return void
   */
  public function init()
  {

    $endName = Yii::app()->endName;
    $this->setImport(array(
      "orders.{$endName}.interfaces.*",
      "orders.{$endName}.models.*",
      "orders.{$endName}.components.*",
      "ext.jqSuite.*"
    ));
    $default = array(
      'components' => array(
        'jqSuite' => array(
          'class' => 'ext.jqSuite.jqSuite',
          'options' => array(
            'default_theme' => 'redmond'
          )
        )
      )
    );
    //dynamic replace of configuration
    $config = new CMap();
    if (file_exists($this->basePath . "/config/{$endName}.php")) {
      $currentConfig = $this->basePath . "/config/{$endName}.php";
      $config = new CConfiguration($currentConfig);
    }
    Yii::app()->configure(CMap::mergeArray($default, $config->toArray()));

    Yii::app()->onModuleCreate(new CEvent($this));
  }
}
