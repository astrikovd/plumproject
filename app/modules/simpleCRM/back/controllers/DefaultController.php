<?php

/**
 * Copyright (c) 2012 by Fukalov Semen <yapendalff@gmail,com> http://fsemlab.ru
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Orders
 * @package modules/orders
 */
class DefaultController extends BackEndController
{

  public $layout = '//layouts/index';

  /**
   * @var jqSuite $jqSuite
   */
  protected $jqSuite;

  public function init()
  {
    $this->jqSuite = Yii::app()->jqSuite;
  }

  /**
   * Index action
   * @return void
   */
  public function actionIndex()
  {
    $model = VirtueMartDataModel::model();
    var_dump($model, $model->getTablePrefix());
    die;
    $this->render('index');
  }

  /**
   * Index action
   * @return void
   */
  public function actionLoad()
  {
    $this->render('load');
  }

}
