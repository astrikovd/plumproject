<?php
/**
 * 
 * @author: sem
 * Date: 26.12.12
 * Time: 9:14
 */
interface IOrderDataModel {

  /**
   * @return IDataProvider|CArrayDataProvider|CActiveDataProvider|CSqlDataProvider
   */
  public function searchOrders();

  /**
   * @return IDataProvider|CArrayDataProvider|CActiveDataProvider|CSqlDataProvider
   */
  public function searchItems();

  /**
   * @return IDataProvider|CArrayDataProvider|CActiveDataProvider|CSqlDataProvider
   */
  public function searchUser();
}
