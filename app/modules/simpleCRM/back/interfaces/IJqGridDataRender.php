<?php
/**
 *
 * @author: sem
 * Date: 26.12.12
 * Time: 9:14
 */
interface IJqGridDataRender
{

  /**
   * @param CActiveRecord|string $modelClass
   * @param array $config
   * @param bool $echo
   */
  public function __construct($modelClass, $config = array(), $echo = false);
}
