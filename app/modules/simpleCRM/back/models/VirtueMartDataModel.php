<?php
/**
 *
 * @author: sem
 * Date: 24.12.12
 * Time: 5:30
 */
/**
 * This is the model class for table "adv_places".
 *
 * The followings are the available columns in table 'adv_places':
 * @property integer $adv_places_id
 * @property string $name
 * @property string $char_id
 * @property string $date0
 * @property boolean $vflag
 *
 * The followings are the available model relations:
 * @property AdvBanners[] $advBanners
 */
class VirtueMartDataModel extends CPlumActiveRecord implements IOrderDataModel
{

  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return VirtueMartDataModel the static model class
   */
  public static function model($className = __CLASS__)
  {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName()
  {

    return $this->getTablePrefix() . 'virtuemart_orders';
  }

  public function getDbConnection()
  {
    if (self::$db !== null)
      return self::$db;
    else {
      self::$db = (!Yii::app()->db_integration instanceof CDbConnection ? parent::getDbConnection() : Yii::app()->db_integration);
      if (self::$db instanceof CDbConnection)
        return self::$db;
      else
        throw new CDbException(Yii::t('yii', 'Active Record requires a "db" CDbConnection application component.'));
    }
  }

  /**
   * @return IDataProvider|CArrayDataProvider|CActiveDataProvider|CSqlDataProvider
   */
  public function searchOrders()
  {

  }

  /**
   * @return IDataProvider|CArrayDataProvider|CActiveDataProvider|CSqlDataProvider
   */
  public function searchItems()
  {

  }

  /**
   * @return IDataProvider|CArrayDataProvider|CActiveDataProvider|CSqlDataProvider
   */
  public function searchUser()
  {

  }
}
