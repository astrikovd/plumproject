<?php
/**
 * Config for module orders
 * @author: sem
 * Date: 24.12.12
 * Time: 08:54
 */
return array(
  'components' => array(
    'jqSuite' => array(
      'class' => 'ext.jqSuite.jqSuite',
      'options' => array(
        'default_theme' => 'redmond'
      )
    )
  ),
  'modules' =>
  array(
    'orders' => array(
      'model' => 'VirtuemartModel'
    )
  )
);