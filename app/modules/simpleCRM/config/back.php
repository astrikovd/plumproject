<?php
/**
 * Config for module orders
 * @author: sem
 * Date: 24.12.12
 * Time: 08:54
 */
return array(
  'components' => array(
    'db_integration' => array(
      'class' => 'CDbConnection',
      'connectionString' => 'mysql:host=localhost;dbname=joomla_susi',
      'username' => 'joomla',
      'password' => 'joomla',
      'tablePrefix' => 'd8j65_',
      'emulatePrepare' => true,
      'charset' => 'utf8',
    ),
    'jqSuite' => array(
      'class' => 'ext.jqSuite.jqSuite',
      'options' => array(
        'default_theme' => 'redmond'
      )
    )
  ),
  'modules' =>
  array(
    'orders' => array(
      'model' => 'VirtuemartModel'
    )
  )
);