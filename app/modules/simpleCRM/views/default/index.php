<?php
$this->breadcrumbs = array(
  'Просмотр заказов',
);
?>
<script type="text/javascript">
  function MakeGrid(t_opt, tab, colNames, colModel, listModel, url, editurl) {
//jQuery.extend( t_opt, { width: 900 } );
    jQuery.extend({ width: 900 }, t_opt);
    t_opt.width = t_opt.width || 900;
    var lastSel = t_opt.lastSel;
    jQuery('#' + t_opt.g_selector).jqGrid({
      url: url,
      datatype: 'json',
      mtype: 'POST',
      viewsortcols: true,
      colNames: colNames,
      colModel: colModel,
//options
      pager: jQuery('#' + t_opt.g_selector + '_pager'),
      rowNum: 20,
      rowList: [10, 20, 50, 100],
      sortname: 'id',
      sortorder: t_opt.sortorder,
      rownumbers: true,
      viewrecords: true,
      caption: t_opt.caption,
      multiselect: true,
      toolbar: t_opt.toolbar,
      editurl: editurl,
      width: t_opt.width,
      height: t_opt.height,//pagerpos:"right",recordpos:"center",
      loadComplete: function (xhr) {
        var no = eval("(" + xhr.responseText + ")");
//ошибка в данных
        if (no.errMess) {
          error_dialog(t_opt.g_selector, no.errMess, no.errCode);
        }
        return true;
      },
      loadError: function (status, error) {
        error_dialog(t_opt.g_selector, status, error);
      },
      onSelectRow: function (id) {
        if (id && id == lastSel) {
          jQuery('#' + t_opt.g_selector).restoreRow(lastSel);
          jQuery('#' + t_opt.g_selector).editRow(id, true, false, AfterSave);
          lastSel = id;
        }
        else {
          jQuery('#' + t_opt.g_selector).restoreRow(lastSel);
          lastSel = id;
        }
      }//Свойства навигации
    }).navGrid('#' + t_opt.g_selector + '_pager',
        {add: true, addtext: "Добавить ", edittext: "Изменить ", deltext: "Удалить ", refresh: true, searchtext: "Поиск ", view: false},
// default settings for edit
        {bottominfo: "Поля помеченные (*) - обязательны", width: 400, closeAfterEdit: true, closeOnEscape: true, beforeInitData: function () {
          jQuery('#' + t_opt.g_selector).restoreRow(lastSel);
        }, afterSubmit: processAddEdit},
// default settings for add
        {bottominfo: "Поля помеченные (*) - обязательны", closeAfterAdd: true, closeOnEscape: true, width: 400, beforeInitData: function () {
          jQuery('#' + t_opt.g_selector).restoreRow(lastSel);
        }, afterSubmit: processAddEdit},
// delete instead that del:false we need this
        {closeOnEscape: true, beforeInitData: function () {
          jQuery('#' + t_opt.g_selector).restoreRow(lastSel);
        }, afterSubmit: processAddEdit},
//search parameters
        {onInitializeSearch: Before,
          closeOnEscape: true, multipleSearch: t_opt.multipleSearch, closeAfterSearch: true //afterShowSearch: resetform, onClose: resetform2,
        },
        {} // view parameters
//Кнопка "Показать столбцы"
    ).navButtonAdd('#' + t_opt.g_selector + '_pager', { caption: "", buttonicon: " ui-icon-pin-w", onClickButton: function () {
          jQuery('#' + t_opt.g_selector).setColumns()
        }, position: "last"});
    if (listModel != false) {
      for (var i = 0; i < listModel.length; i++) {
        var n = listModel[i];
        load_list_opt(t_opt.g_selector, url + "ajax.php", n.colname, n.listname);
      }
    }
    jQuery('#del_' + t_opt.g_selector).click(function () {
      jQuery('#' + t_opt.g_selector).restoreRow(lastSel)
    });
  }
  //диалог - ошибка
  function error_dialog(grid, mess, code) {
    var wi = (jQuery('#gbox_' + grid).width() - 200) / 2;
    var pos = jQuery('#gbox_' + grid).position();
    info_dialog("Ошибка", "&nbsp;<b>Содержимое ошибки:</b>&nbsp;&nbsp;" + mess + "<br>&nbsp;<b>Статус ошибки:</b>&nbsp;&nbsp;" + code + ".<br><br>&nbsp;Попробуйте перезагрузить страницу или обратитесь к разработчику скопировав информацию об ошибке.<br>", "Закрыть", {top: pos.top + 60, left: wi, align: 'left', closeOnEscape: true});
    setTimeout(function () {
      info_dialog("Выполненно!", "&nbsp;<b>Выполненно:</b>&nbsp;&nbsp;" + mess + "<br>&nbsp;<b>Затронуты ключи:</b>&nbsp;&nbsp;" + code + "", "Закрыть", {top: pos.top + 60, left: wi, align: 'left', closeOnEscape: true});
      jQuery("#info_dialog").focus();
    }, 1000);
    return true;
  }
  //диалог - Выполненно
  function done_dialog(grid, mess, code) {
    var pos = jQuery('#gbox_' + grid).position();
    var wi = (jQuery('#gbox_' + grid).width() - 200) / 2;
    renewTables();
//console.info(jQuery('#gbox_'+grid));
    setTimeout(function () {
      info_dialog("Выполненно!", "&nbsp;<b>Выполненно:</b>&nbsp;&nbsp;" + mess + "<br>&nbsp;<b>Затронуты ключи:</b>&nbsp;&nbsp;" + code + "", "Закрыть", {top: pos.top + 60, left: wi, align: 'left', closeOnEscape: true});
      jQuery("#info_dialog").focus();
    }, 1000);
    return true;
  }
  //загрузка списка
  function load_list_opt(grid, url, colname, list) {
    jQuery.post(url,
        { task: "getopt", mode: false, list: list},
        function (data) {
          var oc = eval("(" + data + ")");
          if (oc.errMess) {
            message = oc.errMess + '<br/>';
            error_dialog(grid, oc.errMess, oc.errCode);
            return false;
          }
          jQuery("#" + grid).setColProp(colname, {editoptions: oc});
          return false;
        });
  }
  //сообщения после form edit
  function processAddEdit(response, postdata) {
    var success = true;
    var message = ""
    var json = eval('(' + response.responseText + ')');
    if (json.doneMess) {
      message = json.doneMess + '<br/>';
      done_dialog(json.selector, json.doneMess, json.doneCode);
    }
    if (json.errMess) {
      success = false;
      message = json.errMess + '<br/>';
//error_dialog('list',json.errMess, json.errCode);
    }

    var new_id = 1;
//     return false;
    return [success, message, new_id];
  }
  //сообщения после inline edit
  function AfterSave(res) {
    var no = eval("(" + res.responseText + ")");
//ошибка
    if (no.errCode) {
      error_dialog(no.selector, no.errMess, no.errCode);
    }
//успех
    if (no.doneCode) {
      done_dialog(no.selector, no.doneMess, no.doneCode);
    }
    jQuery('#' + no.selector).trigger('reloadGrid');
  }
  //сбрасывает события КЛИК для кнопки ресета поиска
  function Before(g_selector) {
    var r = jQuery.find(".ui-reset");
    var nm = jQuery;
    for (var i = 0; i < r.length; i++) {
      nm(r[i]).unbind("click").click(function () {
        return false;
      });
    }
  }
</script>
<div class="page-header">
  <h4>Просмотр заказов</h4>
</div>
<div class="alert alert-info">
  <p>С помощью этого раздела Вы можете быстро просматривать данные о заказах магазина.</p>
</div>
{name: 'myac', width:80, fixed:true, sortable:false, resize:false, formatter:'actions',
formatoptions:{keys:true}},
<div id="order-grid" class="grid-view">
  <table id="list_orders" class="scroll"></table>
  <div id="list_orders_pager" class="scroll" style="text-align:center;"></div>
  <!--  <p>Для редактирования сделайте двойной клик на строке</p>-->
  <!--  <p>Enter - сохраняет новое значение</p>-->
  <p>Esc - отмена (+закрытие всплывающих окошек)</p>
</div>
<?php define('BASEURL', 'tes'); ?>
<script type="text/javascript">
  jQuery(document).ready(function ($) {

    var lastSel;
    /****** ORDERS START ****/
//заголовки
    var cNamesOrders = ['#', 'Название страны', 'Количество регионов'];
//модель данных
    var cModelOrders = [
      { name: 'id',
        index: 'id',
        width: 30,
        search: false
      },
      {
        name: 'name',
        index: 'name',
        width: 250,
        align: 'left',
        editable: true,
        edittype: "text",
        editrules: {required: true},
        formoptions: { elmprefix: '(*)', rowpos: 1, colpos: 1},
        searchoptions: {sopt: ['eq', 'ne', 'bw', 'cn', 'nc']}
      },
      {
        name: 'regions_count',
        index: 'regions_count',
        width: 80,
        search: false
      }
    ];
//опции
    var t_Opt = {
      lastSel: lastSel,
      g_selector: 'list_orders',
      caption: 'Заказы',
      height: 500,
      multipleSearch: false,
      sortorder: "ASC"
    };
    MakeGrid(t_Opt, 'countries', cNamesOrders, cModelOrders, false, '<?=BASEURL;?>');
  });
</script>