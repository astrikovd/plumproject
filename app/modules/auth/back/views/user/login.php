<?php $this->pageTitle = Yii::app()->name . " / Авторизация" ?>
<div class="well login">
  <h4>Авторизация</h4>
  <hr>
  <div class="form">
    <?php if (!empty($errors)): ?>
      <div class="alert alert-error">
        <?php echo implode("<br/>", $errors['login']);?>
      </div>
    <?php endif; ?>

    <?php $form = $this->beginWidget('CActiveForm', array(
      'id' => 'id-login-form',
      'enableClientValidation' => true,
      'focus' => array($model, 'username')
    )); ?>

    <p>
      <?php echo $form->labelEx($model, 'username', array('label' => 'Имя пользователя:')); ?>
      <?php echo $form->textField($model, 'username'); ?>
    </p>

    <p>
      <?php echo $form->labelEx($model, 'password', array('label' => 'Пароль:')); ?>
      <?php echo $form->passwordField($model, 'password'); ?>
    </p>

    <p>
      <?php echo CHtml::submitButton('Войти', array('class' => 'btn btn-primary btn-medium')); ?>
    </p>

    <?php $this->endWidget(); ?>
  </div>
  <hr>
  <div style="font-size:11px;text-align:left;">
    <strong><a href="http://www.plumcmf.com" target="_blank">PlumCMF</a> © 2012</strong>
  </div>
</div>