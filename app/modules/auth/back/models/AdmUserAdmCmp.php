<?php

/**
 * Copyright (c) 2012 by Dmitry Astrikov / www.astrikov.ru
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * This is the model class for table "adm_user_adm_cmp".
 *
 * The followings are the available columns in table 'adm_user_adm_cmp':
 * @property integer $adm_user_adm_cmp_id
 * @property integer $adm_user_id
 * @property integer $adm_cmp_id
 *
 * The followings are the available model relations:
 * @property AdmUser $admUser
 * @property AdmCmp $admCmp
 */
class AdmUserAdmCmp extends CPlumActiveRecord
{
  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return AdmUserAdmCmp the static model class
   */
  public static function model($className = __CLASS__)
  {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName()
  {
    return $this->getTablePrefix() . 'adm_user_adm_cmp';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules()
  {
    // NOTE: you should only define rules for those attributes that
    // will receive user inputs.
    return array(
      array('adm_user_id, adm_cmp_id', 'required'),
      array('adm_user_id, adm_cmp_id', 'numerical', 'integerOnly' => true),
      // The following rule is used by search().
      // Please remove those attributes that should not be searched.
      array('adm_user_adm_cmp_id, adm_user_id, adm_cmp_id', 'safe', 'on' => 'search'),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations()
  {
    // NOTE: you may need to adjust the relation name and the related
    // class name for the relations automatically generated below.
    return array(
      'admUser' => array(self::BELONGS_TO, 'AdmUser', 'adm_user_id'),
      'admCmp' => array(self::BELONGS_TO, 'AdmCmp', 'adm_cmp_id'),
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels()
  {
    return array(
      'adm_user_adm_cmp_id' => 'Adm User Adm Cmp',
      'adm_user_id' => 'Adm User',
      'adm_cmp_id' => 'Adm Cmp',
    );
  }

  /**
   * Retrieves a list of models based on the current search/filter conditions.
   * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
   */
  public function search()
  {
    // Warning: Please modify the following code to remove attributes that
    // should not be searched.

    $criteria = new CDbCriteria;

    $criteria->compare('adm_user_adm_cmp_id', $this->adm_user_adm_cmp_id);
    $criteria->compare('adm_user_id', $this->adm_user_id);
    $criteria->compare('adm_cmp_id', $this->adm_cmp_id);

    return new CActiveDataProvider($this, array(
      'criteria' => $criteria,
    ));
  }
}