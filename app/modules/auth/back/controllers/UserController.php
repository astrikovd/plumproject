<?php

/**
 * Copyright (c) 2012 by Dmitry Astrikov / www.astrikov.ru
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Default auth controller
 * @package modules/auth/back/controllers
 */
class UserController extends BackEndController
{

  /**
   * @var layout
   */
  public $layout = "//layouts/blank";

  public function filters()
  {
    return array(
      'accessControl'
    );
  }

  protected function beforeAction($action)
  {
    return true;
  }

  /**
   * Login action
   * @return mixed
   */
  public function actionLogin()
  {
    //if user already authorized, redirect him to the root url
    if (!Yii::app()->user->isGuest) $this->redirect("/");

    $model = new LoginForm;

    if (isset($_POST['LoginForm'])) {
      $model->attributes = $_POST['LoginForm'];
      //Try to authenticate user
      if ($model->authenticate()) {
        //if authentication was success, redirect user to previous url
        $this->redirect(Yii::app()->user->returnUrl);
      } else {
        //else output authentication errors
        return $this->render('login', array('model' => $model, 'errors' => $model->getErrors()));
      }
    }
    return $this->render('login', array('model' => $model));
  }

  /**
   * Logout action
   * @return void
   */
  public function actionLogout()
  {
    Yii::app()->user->logout();
    $this->redirect("/");
  }
}