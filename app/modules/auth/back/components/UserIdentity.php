<?php

/**
 * Copyright (c) 2012 by Dmitry Astrikov / www.astrikov.ru
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * User Identity class for authentication
 *
 * Works with AdmUser model
 * @package modules/auth/back/components
 */
class UserIdentity extends CUserIdentity
{

    /**
     * @var $id - holds user id
     */
    private $id;

    /**
     * Authentication
     * @return integer
     */
    public function authenticate()
    {
        $record = AdmUser::model()->findByAttributes(array('username'=>$this->username));
        if ($record === null)
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        elseif ($record->passwordmd5 !== md5($this->password))
            $this->errorCode= self::ERROR_PASSWORD_INVALID;
        else
        {
            $this->id = $record->adm_user_id;
            $this->setState('username', $record->username);
            $this->setState('user_id', $record->adm_user_id);

            //Store user access to plum modules
            $components = AdmUserAdmCmp::model()->with('admCmp')->findAll(array(
                                            'condition'=>'adm_user_id = :adm_user_id',
                                            'params'=>array(
                                                'adm_user_id' => $record->adm_user_id
                                                )
                                        ));
            $user_modules = array();
            foreach($components as $cmp) {
                $user_modules[] = $cmp->admCmp->char_id;
            }
            $this->setState('user_modules', $user_modules);

            //Save login time and user IP address
            $record->last_ip = Yii::app()->request->userHostAddress;
            $record->last_login = date('Y-m-d H:i:s');
            $record->save();

            $this->errorCode=self::ERROR_NONE;
        }
        return !$this->errorCode;
    }

    /**
     * Getter for user id
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}