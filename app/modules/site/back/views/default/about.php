<div class="page-header">
  <h4>О системе</h4>
</div>
<p>simpleCRM работает на основе PlumCMF</p>
<p>Сайт проекта - <a href="http://www.plumcmf.com">www.plumcmf.com</a>.</p>
<p>Вопросы и предложения по поводу работы системы управления присылайте на почту <a href="mailto:info@plumcmf.com">info@plumcmf.com</a>.
</p>
<p>Работает на <a href="http://www.yiiframework.com" target="_blank">Yii Framework</a>.</p>
