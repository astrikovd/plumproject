<div class="page-header">
  <h2>PlumCMF
    <small>эффективное управление Вашим сайтом</small>
  </h2>
</div>
<div class="alert alert-info">
  <strong>Здравствуйте, <?echo Yii::app()->user->username;?></strong>! Для того чтобы начать работу, воспользуйтесь
  основным меню.
</div>
<hr>
<p>Вы используете:</p>
<ul>
  <li><strong><a href="http://www.yiiframework.com" target="_blank">Yii Framework</a>
      версии <?php echo Yii::getVersion();?></strong></li>
  <li><strong><a href="http://www.php.net" target="_blank">PHP</a> версии <?php echo phpversion();?></strong></li>
</ul>

