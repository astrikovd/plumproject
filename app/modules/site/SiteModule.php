<?php

/**
 * Copyright (c) 2012 by Dmitry Astrikov / www.astrikov.ru
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Site module
 *
 * For main routing (frontend)
 *
 * @package modules/site
 */
class SiteModule extends CPlumModule
{
	/**
	 * Module initialization
	 * @return void
	 */
	public function init()
	{
		$endName = Yii::app()->endName;

        $this->setImport(array(
        	"site.{$endName}.models.*",
        	"page.{$endName}.models.*"
        ));

        if($endName == 'front') {
            $siteinfo = Yii::app()->getModule('siteinfo')->getSiteinfo();
            if($siteinfo->mtflag) {
                //Run maintenance action if needed
                $url = Yii::app()->urlManager->createUrl("siteinfo/default/maintenance");
                Yii::app()->runController($url);
            } else {
                //Start requested Plum module and terminate application
                $this->startModule();
            }
            Yii::app()->end();
    	}
        else {
        	//Continue app running (for backend)
        	Yii::app()->onModuleCreate(new CEvent($this));
        }
	}

    /**
     * Starts requested module
     * @throws CHttpException
     * @return void
     */
	private function startModule()
	{
		list($module, $controller, $action) = array("site", "default", "Index");

    $condition = (Yii::app()->db->driverName == 'mysql' ?
        'INSTR(:url_path, url_path) = 1 AND eflag'
        :
        'strpos(:url_path, url_path) = 1 AND eflag');
    //var_dump(Yii::app()->request->requestUri);die;
		$requested_page = Page::model()->find(
                    array('condition' => $condition,
													'order' => 'length(url_path) DESC',
													'params' => array('url_path' => Yii::app()->request->requestUri)));

        if (!$requested_page->eflag) {
            throw new CHttpException(404);
        }

		switch($requested_page->page_module_id) {
			case "homepage": //Homepage
				$module = "page";
				$action = "home";
		  break;
			case "news": //News
				$module = "news";
        $action = "route";
		  break;
      case "feedback": //Feedback
        $module = "feedback";
      break;
      case "photo": //Photo
        $module = "photo";
      break;
			default: //Static pages
				$module = "page";
				$action = "static";
				break;
		}

		$url = Yii::app()->urlManager->createUrl("$module/$controller/$action");
		Yii::app()->runController($url);
	}
}
