<?php

/**
 * Copyright (c) 2012 by Dmitry Astrikov / www.astrikov.ru
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * This is the model class for table "siteinfo".
 *
 * The followings are the available columns in table 'siteinfo':
 * @property integer $siteinfo_id
 * @property integer $site_id
 * @property string $title
 * @property string $admin_email
 * @property boolean $mtflag
 * @property string $mt_text
 */
class Siteinfo extends CPlumActiveRecord
{
  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return Siteinfo the static model class
   */
  public static function model($className = __CLASS__)
  {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName()
  {
    return $this->getTablePrefix() . 'siteinfo';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules()
  {
    // NOTE: you should only define rules for those attributes that
    // will receive user inputs.
    return array(
      array('site_id', 'numerical', 'integerOnly' => true),
      array('title, admin_email', 'length', 'max' => 255),
      array('mtflag, mt_text', 'safe'),
      // The following rule is used by search().
      // Please remove those attributes that should not be searched.
      array('siteinfo_id, site_id, title, admin_email, mtflag, mt_text', 'safe', 'on' => 'search'),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations()
  {
    // NOTE: you may need to adjust the relation name and the related
    // class name for the relations automatically generated below.
    return array();
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels()
  {
    return array(
      'title' => 'Название',
      'admin_email' => 'E-mail администратора',
      'mtflag' => 'Сайт на техобслуживании',
      'mt_text' => 'Текст объявления о техобслуживании',
    );
  }

  /**
   * Retrieves a list of models based on the current search/filter conditions.
   * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
   */
  public function search()
  {
    // Warning: Please modify the following code to remove attributes that
    // should not be searched.

    $criteria = new CDbCriteria;

    $criteria->compare('siteinfo_id', $this->siteinfo_id);
    $criteria->compare('site_id', $this->site_id);
    $criteria->compare('title', $this->title, true);
    $criteria->compare('admin_email', $this->admin_email, true);
    $criteria->compare('mtflag', $this->mtflag);
    $criteria->compare('mt_text', $this->mt_text, true);

    return new CActiveDataProvider($this, array(
      'criteria' => $criteria,
    ));
  }
}