<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('siteinfo_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->siteinfo_id),array('view','id'=>$data->siteinfo_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('site_id')); ?>:</b>
	<?php echo CHtml::encode($data->site_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('admin_email')); ?>:</b>
	<?php echo CHtml::encode($data->admin_email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mtflag')); ?>:</b>
	<?php echo CHtml::encode($data->mtflag); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mt_text')); ?>:</b>
	<?php echo CHtml::encode($data->mt_text); ?>
	<br />


</div>