<?php
$this->breadcrumbs=array(
	'Информация о сайте'=>array('index'),
	'Изменение',
);

$this->menu=array(
	array('label'=>'Просмотр','url'=>array('view','id'=>$model->siteinfo_id)),
);
?>

<div class="page-header">
	<h4>Изменение информации о сайте</h4>
</div>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>