<?php
$this->breadcrumbs = array(
  'Информация о сайте'
);

$this->menu = array(
  array('label' => 'Изменение', 'url' => array('update', 'id' => $model->siteinfo_id)),
);
?>

<div class="page-header">
  <h4>Информация о сайте</h4>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
  'data' => $model,
  'attributes' => array(
    'title',
    'admin_email',
    array('name' => 'mtflag', 'value' => $model->mtflag ? "Да" : "Нет"),
    array('name' => 'mt_text', 'type' => 'raw')
  ),
)); ?>
