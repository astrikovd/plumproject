<?php Yii::app()->controller->module->registerResource('siteinfo', 'siteinfo.js') ?>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
  'id' => 'siteinfo-form',
  'enableAjaxValidation' => false,
)); ?>

<div class="alert alert-info">
  Поля, отмеченные <span class="red">*</span>, обязательны для заполнения.
</div>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->textFieldRow($model, 'title', array('class' => 'span5', 'maxlength' => 255)); ?>
<a href="javascript:return void();" rel="tooltip"
   data-original-title="Выводится внутри тега title, отображается на вкладке браузера, влияет на индексацию в поисковых системах"><i
      class="tooltip-icon icon icon-question-sign"></i></a>

<?php echo $form->textFieldRow($model, 'admin_email', array('class' => 'span5', 'maxlength' => 255)); ?>

<hr>

<div class="alert">Если сайт находится в режиме <strong>техобслуживания</strong>, пользователи, заходящие на него будут
  видеть специальное сообщение, текст которого вы можете указать ниже
</div>
<?php echo $form->checkBoxRow($model, 'mtflag'); ?>


<?php echo $form->textAreaRow($model, 'mt_text', array('rows' => 6, 'cols' => 50, 'class' => 'span12')); ?>

<div class="form-actions">
  <a class="btn btn-default" href="javascript:history.back();"><i class="icon icon-chevron-left"></i> Назад</a>
  <?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
    'type' => 'primary',
    'label' => $model->isNewRecord ? 'Добавить' : 'Сохранить',
  )); ?>
</div>

<?php $this->endWidget(); ?>
