<?php
/**
 * Set YII_DEBUG to false on production, and it use yiilite verion (is more faster, if server dont need opcode-cacher)
 * @author: sem
 * Date: 24.12.12
 * Time: 3:47
 */
// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG', true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

// change the following paths if necessary
if (YII_DEBUG) {
  $yii = realpath(dirname(__FILE__) . '/../../../yii/framework/yii.php');
} else {
  $yii = realpath(dirname(__FILE__) . '/../../../yii/framework/yiilite.php');
}
