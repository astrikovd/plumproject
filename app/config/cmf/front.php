<?php

/**
 * Copyright (c) 2012 by Dmitry Astrikov / www.astrikov.ru
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Frontend configuration
 * @package config/cmf
 */
return CMap::mergeArray(
    require(realpath(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..'. DIRECTORY_SEPARATOR. 'main.php')),
    array(
        'name'=>'PlumCMF',
        'components'=>array(
            'assetManager'=>array(
                'basePath'=>realpath(dirname(__FILE__) .
                            DIRECTORY_SEPARATOR . ".." .
                            DIRECTORY_SEPARATOR . ".." .
                            DIRECTORY_SEPARATOR . ".." .
                            DIRECTORY_SEPARATOR . "front" .
                            DIRECTORY_SEPARATOR . "static" .
                            DIRECTORY_SEPARATOR . "assets" .
                            DIRECTORY_SEPARATOR),
                'baseUrl'=> '/static/assets/',
            ),
            'par'=>array(
                'class'=>'application.lib.extensions.par.par',
            ),
            'urlManager'=>array(
                'urlFormat'=>'path',
                'showScriptName'=>false,
                'rules'=>array(
                    '[a-zA-Z_0-9\./-]+' => 'site/default'
                ),
            ),
            'bootstrap' => array(
              'class' => 'bootstrap.components.Bootstrap',
            ),
        )
    )
);