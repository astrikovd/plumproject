<?php

/**
 * Copyright (c) 2012 by Dmitry Astrikov / www.astrikov.ru
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
Yii::setPathOfAlias('bootstrap',
  realpath(
    dirname(__FILE__) .
        DIRECTORY_SEPARATOR . ".." .
        DIRECTORY_SEPARATOR . "lib" .
        DIRECTORY_SEPARATOR . "extensions".
        DIRECTORY_SEPARATOR . "yiibootstrap"
  )
);
/**
 * Main configuration
 * @package config
 */
return CMap::mergeArray(
  require(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'etc' . DIRECTORY_SEPARATOR . 'sys.php'),
  array(
    'components' => array(
      'user' => array(
        'allowAutoLogin' => true,
        'loginUrl' => array('auth/user/login'),
      ),
      'db' => array(
        'connectionString' => 'mysql:host=localhost;dbname=plumcmf',
        'emulatePrepare' => true,
        'username' => 'plumcmf',
        'password' => 'plumcmf',
        'charset' => 'utf8',
        'tablePrefix'=>'plum_'
      ),/*
      'db'=>array(
        'connectionString' => 'pgsql:host=localhost;dbname=plumcmf',
        'emulatePrepare' => true,
        'username' => 'plumcmf',
        'password' => 'plumcmf',
        'charset' => 'utf8',
        'tablePrefix'=>'plum_'
      ), */
    ),
    'params' => array(
      'back_url'  => 'http://' . $_SERVER['HTTP_HOST'],
      'front_url' => 'http://' . $_SERVER['HTTP_HOST'],
    ),
    'modules' => array(
      'site',
      'auth' => array(
        'defaultController' => 'user'
      ),
      'content',
      'user',
      'page',
      'newsfeed',
      'news',
      'feedback',
      'photo',
      'photogallery',
      'advsections',
      'advplaces',
      'advbanners',
      'siteinfo',
      'installer'
    ),
  )
);