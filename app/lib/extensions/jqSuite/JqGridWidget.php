<?php
class jqGridWidget extends \CWidget
{
  /**
   * Assets package ID.
   */
  const PACKAGE_ID = 'jqSuite';
  /**
   * @var
   */
  public $options = array();
  /**
   * @var string|null The selector.
   */
  public $selector = 'list';

  /**
   * Init widget.
   */
  public function init()
  {
    parent::init();

    $this->registerClientScript();
  }

  /**
   * Register CSS and Script.
   */
  protected function registerClientScript()
  {
    /** @var $cs \CClientScript */
    $cs = Yii::app()->getClientScript();
    if (!isset($cs->packages[self::PACKAGE_ID])) {
      /** @var $am \CAssetManager */
      $am = Yii::app()->GetAssetManager();
      $cs->packages[self::PACKAGE_ID] = array(
        'basePath' => dirname(__FILE__) . '/assets',
        'baseUrl' => $am->publish(dirname(__FILE__) . '/assets', false, -1, YII_DEBUG),
        'js' => array('jquery-ui-custom.min.js', 'grid.common.js', 'jqModal.js', 'jquery.form.js', 'jquery.cookie.js', 'i18n/grid.locale-ru.js', 'jquery.jqGrid.min',),
        'css' => array('css/redmond/jquery-ui-custom', 'css/ui.jqgrid.css',),
        'depends' => array('jquery',),
      );
    }
    $cs->registerPackage(self::PACKAGE_ID);

    if (!isset($this->options['path'])) {
      $this->options['path'] = $cs->packages[self::PACKAGE_ID]['baseUrl'];
    }
  }
}
