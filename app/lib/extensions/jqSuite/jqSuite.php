<?php
/**
 * jqSuite register all need assets files and provide widgets/renders of jqgrid suite components
 * @author: sem
 * Date: 23.12.12
 * Time: 21:15
 */ 
class jqSuite extends CApplicationComponent
{
  /**
   * Assets package ID.
   */
  public static $PACKAGE_ID = "jqSuite";

  /**
   * @var array
   */
  public $options = array(
    "default_theme" => "redmond",
    "use_min_versions" => YII_DEBUG
  );


  /**
   * @var array
   */
  protected $available_themes = array();

  /**
   * Init widget.
   */
  public function init()
  {
    parent::init();
    $this->getAvailableThemes();
    $this->registerClientScript();
  }

  /**
   * @param $option
   * @param $value
   */
  public function setOption($option, $value)
  {
    $this->options[$option] = $value;
  }

  /**
   * @param $option
   * @param null $default
   * @return null
   */
  public function getOption($option, $default = null)
  {
    return isset($this->options[$option]) ? $this->options[$option] : $default;
  }
  /**
   * Register CSS and Script.
   */
  protected function registerClientScript()
  {
    self::$PACKAGE_ID.=$this->getCurrentCssTheme().(int)$this->getOption("use_min_versions", false);
    $end = $this->getOption("use_min_version") ? ".min" : "";
    /** @var $cs \CClientScript */
    $cs = Yii::app()->getClientScript();
    if(!isset($cs->packages[self::$PACKAGE_ID])) {
      /** @var $am \CAssetManager */
      $am = Yii::app()->GetAssetManager();
      $assets = dirname(__FILE__)."/assets";
      $baseUrl = Yii::app()->assetManager->publish($assets);
      $cs->packages[self::$PACKAGE_ID] = array(
        "basePath" => dirname(__FILE__) . "/assets",
        "baseUrl" => $am->publish(dirname(__FILE__) . "/assets", false, -1, false),
        "js" => array(
          "js/i18n/grid.locale-ru.js",
          "js/jquery{$end}.js",
          "js/jquery.ui.custom{$end}.js",
          "js/ui.multiselect{$end}.js",
          "js/jquery.cookie{$end}.js",
          "js/jquery.form{$end}.js",
          "js/modernizr.2.0.6{$end}.js",
          "js/jqModal{$end}.js",
          "js/jqDnR{$end}.js",
          "js/jquery.jqChart{$end}.js",
          "css/gray.js",
          "js/jquery.jqScheduler.min.js",
          "js/jquery.jqGrid.min.js",
        ),
        "css" => array(
          "css/themes/".$this->getCurrentCssTheme()."/jquery-ui-custom.css",
          "css/jquery.ui.tooltip.css",
          "css/ui.jqform.css",
          "css/ui.jqscheduler.css",
          "css/ui.multiselect.css",
          "css/ui.jqgrid.css",
        ),
        "depends" => array(),
      );
    }
    $cs->registerPackage(self::$PACKAGE_ID);

  }

  protected function getCurrentCssTheme()
  {
    $jqtheme = Yii::app()->request->cookies["jqtheme"]->value;
    if(!$jqtheme){
      return $this->getOption("default_theme", "redmond");
    }
    elseif( in_array( $jqtheme, $this->available_themes ) ){
      return $jqtheme;
    }
  }

  protected function getAvailableThemes()
  {
    $themes_path = dirname(__FILE__) . "/assets/css/themes";
    $directoryIterator = new DirectoryIterator( $themes_path );
    foreach( $directoryIterator AS $theme )
    {
      /** @var $theme DirectoryIterator */
      if( $theme->isDir() && !in_array($theme->getFilename(),array(".","..")) )
      {
         $this->available_themes[]=$theme->getFilename();
      }
    }
  }
}
