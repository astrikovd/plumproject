<?php

/**
 * Wrapper for Dklab_Realplexor
 * User: fsem
 * Date: 19.12.11
 * Time: 12:50
 */

Yii::import('ext.realplexor.dklab.dklab_realplexor', true);

class Realplexor extends CApplicationComponent{

  public $basehost    = null;
  public $rplhost     = null;
	public $host				= null;
	public $port				= null;

	public $namespace		= null;
	public $timeout 		= 5;
	public $identifier	= null;

	public $login				= null;
	public $password		= null;

  /**
   * @var Dklab_Realplexor $_rpl
   */
	protected $_rpl     = null;

	public function init(){

			$this->_rpl = new Dklab_Realplexor($this->host,$this->port,$this->namespace,$this->identifier);

			parent::init();
	}

	/**
	 * @see Dklab_Realplexor::logon
	 * @param $login
	 * @param $password
	 * @return void
	 */
	public function logon($login, $password){

		$this->_rpl->logon($login, $password);

	}

	/**
	 * @see Dklab_Realplexor::send
	 * @param $idsAndCursors
	 * @param $data
	 * @param null $showOnlyForIds
	 * @return void
	 */
	public function send($idsAndCursors, $data, $showOnlyForIds = null){

		$this->_rpl->send( $idsAndCursors, $data, $showOnlyForIds);

	}
	/**
	 * @see Dklab_Realplexor::cmdOnlineWithCounters
	 * @param null $idPrefixes
	 * @return array List of matched online IDs with counters
	 */
	public function cmdOnlineWithCounters($idPrefixes = null){
		$this->_rpl->cmdOnlineWithCounters( $idPrefixes );
	}

	/**
	 * @see 	Dklab_Realplexor::cmdOnline
	 * @param 	null $idPrefixes
	 * @return array List of matched online IDs.
	 */
	public function cmdOnline($idPrefixes = null){
		$this->_rpl->cmdOnline( $idPrefixes );
	}

	/**
	 * @see 	Dklab_Realplexor::cmdWatch
	 * @param 						$fromPos
	 * @param null 				$idPrefixes
	 * @return array			List of array("event" => ..., "cursor" => ..., "id" => ...).
	 */
	public function cmdWatch($fromPos, $idPrefixes = null){
		$this->_rpl->cmdWatch( $fromPos, $idPrefixes );
	}


}