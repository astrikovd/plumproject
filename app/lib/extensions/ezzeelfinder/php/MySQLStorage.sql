DROP TABLE IF EXISTS `elfinder_file`;
CREATE TABLE IF NOT EXISTS `elfinder_file` (
  `id`        INT(7) unsigned NOT NULL auto_increment,
  `parent_id` INT(7) unsigned NOT NULL,
  `NAME`      VARCHAR(256) NOT NULL,
  `content`   longblob NOT NULL,
  `SIZE`      INT(10) unsigned NOT NULL DEFAULT '0',
  `mtime`     INT(10) unsigned NOT NULL,
  `mime`      VARCHAR(256) NOT NULL DEFAULT 'unknown',
  `READ`      enum('1', '0') NOT NULL DEFAULT '1',
  `WRITE`     enum('1', '0') NOT NULL DEFAULT '1',
  `locked`    enum('1', '0') NOT NULL DEFAULT '0',
  `hidden`    enum('1', '0') NOT NULL DEFAULT '0',
  `width`     INT(5) NOT NULL,
  `height`    INT(5) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY  `parent_name` (`parent_id`, `NAME`),
  KEY         `parent_id`   (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

INSERT INTO `elfinder_file`
(`id`, `parent_id`, `NAME`,     `content`, `SIZE`, `mtime`, `mime`,      `READ`, `WRITE`, `locked`, `hidden`, `width`, `height`) VALUES
('1',  '0',         'DATABASE', '',        '0',    '0',     'directory', '1',    '1',     '0',      '0',      '0',     '0');
