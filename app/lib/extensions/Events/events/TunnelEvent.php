<?php
/**
 * Class for events for tunnel of PS40
 * User: sem
 * Date: 29.05.12
 * Time: 19:10
 */
class TunnelEvent extends CEvent
{
  /**
   * @var null|OrderModel|SessionModel|SessionCallModel
   */
  private $targetObject = null;

  /**
   * Constructor.
   * @param mixed $sender sender of the event
   * @param mixed $params additional parameters for the event
   */
  public function __construct($sender=null,$params=null, $targetObject=null )
  {
    $this->sender=$sender;
    $this->params=(object)$params;
    $this->targetObject = $targetObject;
  }

  /**
   * @param null|\OrderModel|\SessionCallModel|\SessionModel $targetObject
   */
  public function setTargetObject($targetObject)
  {
    $this->targetObject = $targetObject;
  }

  /**
   * @return null|\OrderModel|\SessionCallModel|\SessionModel
   */
  public function getTargetObject()
  {
    return $this->targetObject;
  }


}
