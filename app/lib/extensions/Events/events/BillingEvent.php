<?php
/**
 * Class for events of billing
 * User: sem
 * Date: 29.05.12
 * Time: 19:10
 */
class BillingEvent extends CEvent
{
  /**
   * @var null|BillingTransactionsModel
   */
  private $targetObject = null;

  /**
   * Constructor.
   * @param mixed $sender sender of the event
   * @param mixed $params additional parameters for the event
   */
  public function __construct( $sender=null, $params=null, BillingTransactionsModel $targetObject=null )
  {
    $this->sender=$sender;
    $this->params=$params;
    $this->targetObject = $targetObject;
  }

  /**
   * @param null|BillingTransactionsModel
   */
  public function setTargetObject( BillingTransactionsModel $targetObject = null )
  {
    $this->targetObject = $targetObject;
  }

  /**
   * @return null|BillingTransactionsModel
   */
  public function getTargetObject()
  {
    return $this->targetObject;
  }


}
