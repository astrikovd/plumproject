<?php
Yii::import('core.extensions.Events.events.*');
Yii::import('core.extensions.Events.handlers.*');
class EventsDispatcher extends CComponent
{
    private $_e;
    /**
     * @var
     */
    private static $_instance = null;

    private static $_handlersAttached = false;

    private function __construct(){}

    private function __clone(){}

    /**
     * Override parent for magic raiseEvent calls
     */
    public function __get($name)
    {
      if(strncasecmp($name,'on',2)===0)
      {
        $this->hasConfigListners( $name );
        // duplicating getEventHandlers() here for performance
        $name=strtolower($name);
        if (!isset($this->_e[$name]))
            {
              $this->_e[$name] = new CList;
            }
        return $this->_e[$name];
      }
      return parent::__get( $name );
    }

    /**
     * Override parent for magic raiseEvent calls
     */
    public function __isset($name)
    {
      if( strncasecmp($name,'on',2)===0)
      {
        $name=strtolower($name);
        return isset($this->_e[$name]) && $this->_e[$name]->getCount();
      }
      return parent::__isset( $name );
    }

    /**
     * Override parent for magic raiseEvent calls
     */
    public function __unset($name)
    {
      if( strncasecmp($name,'on',2)===0){
        unset($this->_e[strtolower($name)]); return;
      }
      parent::__unset( $name );
    }
    /**
     * Override parent for magic raiseEvent calls
     */
    public function __set($name,$value)
    {
      if( strncasecmp($name,'on',2)===0)
      {
        $this->hasConfigListners( $name );
        // duplicating getEventHandlers() here for performance
        $name=strtolower($name);
        if (!isset($this->_e[$name]))
            {
              $this->_e[$name] = new CList;
            }
        return $this->_e[$name]->add($value);
      }

      return parent::__set($name,$value);
    }
  /**
   * Overrive CComponent::__call
   * @param string $name
   * @param array $parameters
   * @throws CException
   * @return mixed
   */
    public function __call($name,$parameters)
    {
      if( strncasecmp($name,'on',2)===0 && isset($parameters[0]) && $parameters[0] instanceof CEvent)
      {
        $this->hasConfigListners( $name );
        return $this->raiseEvent( $name, $parameters[0] );
      }

      return parent::__call( $name, $parameters );
    }

    /**
     * Determines whether an event is defined.
     * An event is defined if the class has a method named like 'onXXX'.
     * Note, event name is case-insensitive.
     * @param string $name the event name
     * @return boolean whether an event is defined
     */
    public function hasEvent($name)
    {
      return !strncasecmp($name,'on',2) && is_callable(array(&$this,$name));
    }

    /**
     * Returns the list of attached event handlers for an event.
     * @param string $name the event name
     * @return CList list of attached event handlers for the event
     * @throws CException if the event is not defined
     */
    public function getEventHandlers($name)
    {
      if($this->hasEvent($name))
      {
        $name=strtolower($name);
        if (!isset($this->_e[$name]))
        {
          $this->_e[$name] = new CList;
        }
        return $this->_e[$name];
      } else
      {
        throw new CException(Yii::t('yii', 'Event "{class}.{event}" is not defined.',
          array('{class}' => get_class($this), '{event}' => $name)));
      }
    }
  /**
   *
   */
  public function attachEventHandler($name,$handler)
  {
    $this->getEventHandlers($name)->add($handler);
  }

  /**
   * Detaches an existing event handler.
   * This method is the opposite of {@link attachEventHandler}.
   * @param string $name event name
   * @param callback $handler the event handler to be removed
   * @return boolean if the detachment process is successful
   * @see attachEventHandler
   */
  public function detachEventHandler($name,$handler)
  {
    if ($this->hasEventHandler($name))
        {
          return $this->getEventHandlers($name)->remove($handler) !== false;
        }
    else {
      return false;
    }
  }
  /**
   * Raises an event.
   * This method represents the happening of an event. It invokes
   * all attached handlers for the event.
   * @param string $name the event name
   * @param CEvent $event the event parameter
   * @throws CException if the event is undefined or an event handler is invalid.
   */
  public function raiseEvent($name,$event)
  {
    $name=strtolower($name);

    if(isset($this->_e[$name]))
    {
      foreach($this->_e[$name] as $handler)
      {
        if (is_string($handler))
            {
              call_user_func($handler, $event);
            }
        else if (is_callable($handler, true)) {
          if (is_array($handler)) {
            // an array: 0 - object, 1 - method name
            list($object, $method) = $handler;
            if (is_string($object)) // static method call
            {
              call_user_func($handler, $event);
            } else if (method_exists($object, $method)) {
              $object->$method($event);
            } else {
              throw new CException(Yii::t('yii', 'Event "{class}.{event}" is attached with an invalid handler "{handler}".',
                array('{class}' => get_class($this), '{event}' => $name, '{handler}' => $handler[1])));
            }
          } else // PHP 5.3: anonymous function
          {
            call_user_func($handler, $event);
          }
        } else {
          throw new CException(Yii::t('yii', 'Event "{class}.{event}" is attached with an invalid handler "{handler}".',
            array('{class}' => get_class($this), '{event}' => $name, '{handler}' => gettype($handler))));
        }
        // stop further handling if param.handled is set true
        if (($event instanceof CEvent) && $event->handled)
            {
              return;
            }
      }
    }
    else if (YII_DEBUG && !$this->hasEvent($name))
        {
          throw new CException(Yii::t('yii', 'Event "{class}.{event}" is not defined.',
            array('{class}' => get_class($this), '{event}' => $name)));
        }
  }

  /**
   * @param $name
   * @throws CException
   */
  public function hasConfigListners( $name ){
    $attachedHandlers = self::getEventsConfig();
    if( !isset( $attachedHandlers[$name]) ){
      throw new CException(Yii::t('yii','Event "{class}.{event}" is not defined in LISTNERS configs.',
        array('{class}'=>get_class($this), '{event}'=>$name)));
    }
  }
  /**
   * Singleton
   * @return EventDispatcher
   */
  public static function getInstance()
  {
   if(!self::$_instance){
     self::$_instance = new self();
     if( !self::isInited() )
     {
       self::markAsInited();
       self::attacheHandlers( null );
     }
   }
   return self::$_instance;
  }

  public static function isInited()
  {
    return self::$_handlersAttached;
  }

  public static function markAsInited()
  {
    self::$_handlersAttached = true;
  }

  /**
   * @static
   * @param $path
   * @return array
   */
  public static function loadConfig( $path )
  {
    if( file_exists( $path ) ){
      return require( $path );
    }

    return array();
  }

  /**
   * @static
   * @return mixed
   */
  public static function getEventsConfig()
  {
    static $config;
    if(!$config){
      $defaultConfig = self::loadConfig(
        dirname(__FILE__).DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'listners.php'
      );
      $coreConfig    = self::loadConfig(
        Yii::getPathOfAlias('core.config').DIRECTORY_SEPARATOR.'listners.php'
      );
      $appConfig     = self::loadConfig(
        Yii::getPathOfAlias('application.config').DIRECTORY_SEPARATOR.'listners.php'
      );
      $config = CMap::mergeArray( $defaultConfig, $coreConfig, $appConfig );
    }
    return $config;
  }

  /**
   * Навещивает обработчики
   * @static
   * @param CComponent $object
   * @return object
   */
  public static function attacheHandlers(CComponent $object = null) {
      $dispatcher = self::getInstance();
      $config = self::getEventsConfig();
      foreach($config as $event=>$handlers) {
        if(
          ( $object instanceof CComponent
            &&
            !is_callable(array($object, $event))
          ) &&
          ( !is_callable(array($dispatcher, $event)) )
        ){
            continue;
        }

        foreach($handlers as $handler){
          if( $object instanceof CComponent && is_callable(array($object, $event)))
          {
            $object->attachEventHandler( $event, $handler);
          }

          if(is_callable(array($dispatcher, $event)))
          {
            $dispatcher->attachEventHandler( $event, $handler);
          }
        }
      }
      self::markAsInited();
  }
}