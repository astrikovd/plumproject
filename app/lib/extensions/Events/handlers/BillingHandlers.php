<?php
/**
 * Created by JetBrains PhpStorm.
 * User: sem
 * Date: 30.05.12
 * Time: 16:49
 * To change this template use File | Settings | File Templates.
 */
class BillingHandlers
{
  public static function changeBalance( BillingEvent $event )
  {
    if($event->getTargetObject() !== null ){
      $user = $event->getTargetObject()->getUser();
      if( $user instanceof UserModel ){
        Yii::app()->customer->updateUserOnPS40( $user );
      }
    }
  }
}
