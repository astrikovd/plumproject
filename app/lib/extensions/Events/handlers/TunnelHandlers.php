<?php
/**
 * Handlers for Tunnel events
 * User: sem
 * Date: 29.05.12
 * Time: 19:51
 */
class TunnelHandlers extends CComponent
{
  protected $_ps40Response = null;

  /**
   * @var
   */
  private static $_instance = null;

  private function __construct(){}

  private function __clone(){}

  /**
   * Singleton
   * @return TunnelHandlers
   */
  public static function getInstance()
  {
    if(!self::$_instance){
      self::$_instance = new self();
    }
    return self::$_instance;
  }


  /* event handlers */

  /**
   * Event tunnel push order handler
   * @static
   * @param TunnelEvent $event
   */
  public static function orderHandlerForLoadDataPS( TunnelEvent $event )
  {

    $that = self::getInstance();
    $data  = $that->_ps40Response = $event->params;
    $order = $event->getTargetObject();

    //убрали проверку так как теперь веб решает сколько денег и минут с нять ориентируясь по длине height
    //if (($that->getPSValue('status') == 'EXECUTED' && $that->getPSValue('length')  && !$that->getPSValue('cost'))) {

      $orderData['uid'] = $that->getPSValue('id');
      $orderData['advUId'] = $that->getPSValue('adviser');
      $orderData['duration'] =
          ($that->getPSValue('length') > $that->getPSValue('maxduration') )
              ?
              $that->getPSValue('maxduration') : intval($that->getPSValue('length'));


      $orderData['startTime'] = $that->getPSValue('moment');
      $orderData['status'] = $that->getPSValue('status');
      if ($order instanceof OrderModel) {

        $order->loadDataFromPS($orderData);

        $event->setTargetObject( $order );
        $event->params->isPS40Loaded = true;
      }
    //}
  }

  /**
   * Event tunnel push order handler
   * @static
   * @param TunnelEvent $event
   * @todo - AMN need this for orders form PS40 and not registered on DB
   */
  public static function orderHandlerForNewFromPS40( TunnelEvent $event )
  {
    $that = self::getInstance();
    $data  = $that->_ps40Response = $event->params;
    $order = $event->getTargetObject();
    //$that->debugLog('Input data from event: ', $data );
    //$that->debugLog('Input orderModel from event: ', $order );

    $phoneProcessor = yii::app()->phoneOrderProcessor;

    $checkPSData = array(
      'adviser' => $that->getPSValue('adviser'),
      'client' => $that->getPSValue('client'),
      'client_phone' => $that->getPSValue('client_phone'),
      'maxduration' => $that->getPSValue('maxduration'),
     );

    if($phoneProcessor->isAutoCreatedPsOrder( $data, $order )
        && !empty( $checkPSData['adviser'] )
        && !empty( $checkPSData['client'] )
        && !empty( $checkPSData['client_phone'] )
        && !empty( $checkPSData['maxduration'] )
    ){

      $phoneProcessor->setParam( IPhoneOrderProcessCommand::PS40ResponseKey, $data );
      $phoneProcessor->setParam( IPhoneOrderProcessCommand::SelfOrderedKey, 0 );

      $adviser = AdviserModel::findByUid($that->getPSValue('adviser'));
      if( !($adviser instanceof AdviserModel) )
      {
        throw new CException('Tunnel order "fulllang":"ru-op-auto" - incorrect ADVISER uid, not create order DB row');
      }

      $user = UserModel::model()->findByUid( $that->getPSValue('client') );
      if( !($user instanceof UserModel) )
      {
        throw new CException('Tunnel order "fulllang":"ru-op-auto" - incorrect USER uid, not create order DB row');
      }

      $userPhoneNumber = $that->getPSValue('client_phone');

      $country = Yii::app()->phoneNumber->getCountry( $userPhoneNumber );
      if( $country instanceof CountryModel ){
        UserModel::model()->updatePhoneNumberCommon( $user, $country, $userPhoneNumber );
      }

      $phoneOrder = $phoneProcessor->createPhoneOrder( 'auto',
        $adviser,
        $user,
        $userPhoneNumber,
        $that->getPSValue('moment'),
        $that->secToMin($that->getPSValue('maxduration'))
      );

    if(($error = Yii::app()->phoneOrderProcessor->validate($phoneOrder)) === false){
      try
      {
          Yii::app()->phoneOrderProcessor->processOrder($phoneOrder);
          if( $phoneOrder instanceof IPhoneOrderDatabase && $phoneOrder->getOrderModel() instanceof OrderModel )
          {
            $event->setTargetObject( $phoneOrder->getOrderModel() );
          }
      }
      catch( CException $e )
      {
        $that->debugLog('Error create cn from ps', $e );
      }
      catch( Exception $e )
      {
        $that->debugLog('Error validate cn from ps', $e );
      }
    }

    }
/*order={
"stealleg":"incoming"      с какой ноги украли звонок
"stealsid":117     с какой сессии украли звонок
"status":"ORDERED"
"fulllang":"ru-op-auto"  тег который говорит о том что PS автоматом создал заказ
"maxduration":599
"rate":0
"moment":1337601205   старт-тайм заказа
"adviser":1
"client_phone":"1003"
"client":7
"type":"CN"
"id":9      для нас uid
"ts":"20120521155326"   когда запись последний раз изменялась у PS40
}*/
  }

  /**
   * Event tunnel push order handler
   * @static
   * @param TunnelEvent $event
   */
  public static function orderHandlerForEndBilling( TunnelEvent $event )
  {
    $that = self::getInstance();
    $data  = $that->_ps40Response = $event->params;
    $order = $event->getTargetObject();
    if( $order instanceof OrderModel ){
      //отправить уведомлялку на мыло
      if( in_array( $order->getStatus(), array(OrderModel::STATUS_SYSFAIL) ) )
      {
        Yii::log(
          'PhoneOrder with SYSFAIL status. Datacomboy, you time now! DB data:'.
              PHP_EOL.var_export( $order->getAttributes(), true ),
          CLogger::LEVEL_INFO,
          'application.emailNotifers');
        Yii::getLogger()->flush(true);
      }
      else
      {
        try
        {
          //IPhoneOrderClosable
          $phoneOrder = Yii::app()->phoneOrderProcessor->createPhoneOrderFromModel( $order );
          if( $phoneOrder instanceof IPhoneOrderClosable ){
            Yii::app()->phoneOrderProcessor->processEndOrder( $phoneOrder );
          }
          $event->setTargetObject( $event );
        }
        catch( CException $e )
        {
         $that->debugLog('Error end of phone order billing ', $e );
        }
        catch( Exception $e )
        {
         $that->debugLog('Error end of phone order billing ', $e );
        }
      }
    }
  }


  /**
   * GetValue from PS40Response
   * @param $key
   * @param null $default
   * @return null
   */
  public function getPSValue( $key, $default = null)
  {
    return (isset($this->_ps40Response->$key)) ? $this->_ps40Response->$key : $default;
  }


  protected function secToMin( $seconds ){
    return ceil( $seconds / 60 );
  }

  private function debugLog( $text = 'debug:', $varible = -1111)
  {
    $line = 'debug:'.$text.PHP_EOL;
    if ($varible != -1111) {
      $line.=CVarDumper::dumpAsString($varible);
    }
    Yii::log($line ,  CLogger::LEVEL_ERROR);//'application.emailNotifers'
    Yii::getLogger()->flush(true);
  }
}
