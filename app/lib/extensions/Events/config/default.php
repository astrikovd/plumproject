<?php
/**
 * Date: 01.02.12
 * Time: 18:00
 * @author: fsem
 */

return array(
  /* user area */
  /*
  'onUserCreate'=>array(
      array('Helper', 'onUserCreate'),
  ),
  'onUserUpdate'=>array(
      array('Helper', 'onUserUpdate'),
  ),
  */
  /* billing area */
  'onChangeBalance'=>array(
    array('BillingHandlers', 'changeBalance'),
  ),

  /* tunnel area */

  'onTunnelPushedOrder' => array(
    array('TunnelHandlers', 'orderHandlerForLoadDataPS'),
    array('TunnelHandlers', 'orderHandlerForNewFromPS40'),
    array('TunnelHandlers', 'orderHandlerForEndBilling')
  )

);