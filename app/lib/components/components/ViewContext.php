<?php
/**
 * Copyright (c) 2012 by Dmitry Astrikov / www.astrikov.ru
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Simple ViewContext object
 * @package lib/components
 */
class ViewContext implements IViewContext
{
  protected $_context = null;

  protected $_data = array();

  /**
   * Sets the initial view local data.
   * @param CBaseController $context
   * @param   array   array of values
   * @return \ViewContext|\IViewContext
   */
  public function __construct(CBaseController $context, array $data = null)
  {
    $this->_context = $context;

    if ($data !== null) {
      // Add the values to the current data
      $this->_data = $data + $this->_data;
    }
  }

  /**
   * Assigns a variable by name. Assigned values will be available as a
   * variable within the view file:
   *
   *     // This value can be accessed as $foo within the view
   *     $view->set('foo', 'my value');
   *
   * You can also use an array to set several values at once:
   *
   *     // Create the values $food and $beverage in the view
   *     $view->set(array('food' => 'bread', 'beverage' => 'water'));
   *
   * @param   string   variable name or an array of variables
   * @param   mixed    value
   * @return  View
   */
  public function set($key, $value = null)
  {
    if (is_array($key)) {
      foreach ($key as $name => $value) {
        $this->_data[$name] = $value;
      }
    } else {
      CArray::set_path($this->_data, $key, $value);
    }

    return $this;
  }

  /**
   * Assigns a value by reference. The benefit of binding is that values can
   * be altered without re-setting them. It is also possible to bind variables
   * before they have values. Assigned values will be available as a
   * variable within the view file:
   *
   *     // This reference can be accessed as $ref within the view
   *     $view->bind('ref', $bar);
   *
   * @param   string   variable name
   * @param   mixed    referenced variable
   * @return  View
   */
  public function bind($key, & $value)
  {
    CArray::bind_path($this->_data, $key, $value);
    return $this;
  }

  /**
   * Get all view data
   * @return array|null
   */
  public function getData()
  {
    return $this->_data;
  }

  /**
   * Magic method, searches for the given variable and returns its value.
   * Local variables will be returned before global variables.
   *
   * @param   string  variable name
   * @return  mixed
   */
  public function & __get($key)
  {
    if (isset($this->_data[$key])) {
      return $this->_data[$key];
    } else {
      throw new CException('View variable is not set: :var',
        array(':var' => $key));
    }
  }

  /**
   * Magic method, calls set() with the same parameters.
   *
   * @param   string  variable name
   * @param   mixed   value
   * @return  void
   */
  public function __set($key, $value)
  {
    $this->set($key, $value);
  }

  /**
   * Magic method, determines if a variable is set and is not null.
   *
   * @param   string  variable name
   * @return  boolean
   */
  public function __isset($key)
  {
    return (isset($this->_data[$key]));
  }

  /**
   * Magic method, unset a given variable.
   *
   * @param   string  variable name
   * @return  void
   */
  public function __unset($key)
  {
    unset($this->_data[$key]);
  }
}
