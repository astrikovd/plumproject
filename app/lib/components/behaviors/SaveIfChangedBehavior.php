<?php
/**
 *
 * Поведение, проверяющее свойства на изменение и разрешающее выполнить UPDATE только, если свойства модели были изменены
 */
class SaveIfChangedBehavior extends CActiveRecordBehavior
{
  private $_oldattributes = array();

  public function beforeSave($event)
  {
    if (!$this->Owner->isNewRecord) {
      // new attributes
      $newattributes = $this->Owner->getAttributes();
      $oldattributes = $this->getOldAttributes();

      // compare old and new
      foreach ($newattributes as $name => $value) {
        if (!empty($oldattributes)) {
          $old = $oldattributes[$name];
        } else {
          $old = '';
        }
        if ($value != $old) {
          return true;
        }
      }
      return false;
    }
    return true;
  }


  public function afterFind($event)
  {
    $this->setOldAttributes($this->Owner->getAttributes());
  }

  public function getOldAttributes()
  {
    return $this->_oldattributes;
  }

  public function setOldAttributes($value)
  {
    $this->_oldattributes=$value;
  }
}