<?php

/**
 * Поведение позволяющее получить изменения атрибутов в ActiveRecord моделей
 */
class ChangedActiveRecordAttributesBehavior extends CActiveRecordBehavior
{
  private $updateAttributesOnAfterConstruct=true;
  private $updateAttributesOnAfterFind=true;
  private $updateAttributesOnAfterSave=true;
  private $oldAttributes;

  public function __construct(array $oldAttributes=array())
  {
    $this->setOldAttributes($oldAttributes);
  }

  public function getOldAttribute($name)
  {
    return $this->oldAttributes[$name] ?  $this->oldAttributes[$name] : null;
  }

  public function getOldAttributes()
  {
    return $this->oldAttributes;
  }

  private function setOldAttributes($value)
  {
    $this->oldAttributes = $value;
  }

  public function getChangedAttributes()
  {
    return array_diff_assoc($this->getOwner()->getAttributes(), $this->getOldAttributes());
  }

  public function hasChangedAttributes()
  {
    return count($this->getChangedAttributes()) > 0;
  }

  public function afterConstruct($event)
  {
    // Update old values
    if($this->updateAttributesOnAfterConstruct){
      $this->setOldAttributes($this->getOwner()->getAttributes());
    }
  }

  public function afterFind($event)
  {
    // Update old values
    if($this->updateAttributesOnAfterFind){
      $this->setOldAttributes($this->getOwner()->getAttributes());
    }
  }

  public function afterSave($event)
  {
    // Update old values
    if($this->updateAttributesOnAfterSave){
      $this->setOldAttributes($this->getOwner()->getAttributes());
    }
  }

  public function setUpdateAttributesOnAfterConstruct($updateAttributesOnConstruct)
  {
    $this->updateAttributesOnAfterConstruct = (bool)$updateAttributesOnConstruct;
    return $this;
  }

  public function setUpdateAttributesOnAfterFind($updateAttributesOnFind)
  {
    $this->updateAttributesOnAfterFind = (bool)$updateAttributesOnFind;
    return $this;
  }

  public function setUpdateAttributesOnAfterSave($updateAttributesOnSave)
  {
    $this->updateAttributesOnAfterSave = (bool)$updateAttributesOnSave;
    return $this;
  }
}
