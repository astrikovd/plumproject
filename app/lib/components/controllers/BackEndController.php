<?php

/**
 * Copyright (c) 2012 by Dmitry Astrikov / www.astrikov.ru
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Base backend controller class
 * @package lib/components/controllers
 */
class BackEndController extends CController implements IViewContextController
{
  /**
   * @var default layout
   */
  public $layout = '//layouts/column';

  /**
   * @var array breadcrumbs
   */
  public $breadcrumbs;

  /**
   * @var array menu
   */
  public $menu;
  /**
   * @var ViewContext $view
   */
  protected $view = null;

  public function __construct($id, $module = null)
  {
    parent::__construct($id, $module);
    $this->view = new ViewContext($this);
  }

  /**
   * FrontEndController initialization
   * set all need data,config,params and set \IViewContext
   * @return void
   */
  public function init()
  {
    $this->setView( new ViewContext($this) );
  }

  /**
   * Filters
   * @return array
   */
  public function filters()
  {
    return array(
      //filters user access to backend
      'accessControl',
      //filters user access to plum modules
      'moduleControl'
    );
  }

  /**
   * Access rules
   * @return array
   */
  public function accessRules()
  {
    return array(
      array('allow',
        'users' => array('*'),
        'actions' => array('login'),
      ),
      array('allow',
        'users' => array('@'),
      ),
      array('deny',
        'users' => array('*'),
      ),
    );
  }

  /**
   * Filters user access for selected Plum module
   * @param CFilterChain $filterChain
   * @return void
   */
  public function filterModuleControl($filterChain)
  {
    //If user tries to access restricted module - show him a 404 page
    if (!in_array($this->module->id, Yii::app()->user->user_modules) &&
        $this->module->id !== 'site' &&
        $this->module->id !== 'upload'
    ) {
      throw new CHttpException(404, 'The requested page does not exist.');
    }
    $filterChain->run();
  }

  protected function beforeAction($action)
  {
    return true;
  }

  /**
   * Method use CMap::mergeArray for merging input $data and data from \IViewContext|\ViewContext
   * @param $view
   * @param null $data
   * @param bool $return
   * @return string
   */
  public function render($view, $data = null, $return = false)
  {
    $data = $data ? (array) $data : array();
    $data = CMap::mergeArray( $this->getView()->getData(), $data);
    return parent::render($view, $data, $return);
  }


  /**
   * @param ViewContext $view
   */
  public function setView( ViewContext $view)
  {
    $this->view = $view;
  }

  /**
   * @return ViewContext
   */
  public function getView()
  {
    return $this->view;
  }


  /**
   * Performs the AJAX validation.
   * @param CModel the model to be validated
   */
  protected function performAjaxValidation($model)
  {
    if(isset($_POST['ajax']) && Yii::app()->request->isAjaxRequest) {
      echo CActiveForm::validate($model);
      Yii::app()->end();
    }
  }
}