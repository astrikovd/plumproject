<?php

/**
 * Copyright (c) 2012 by Dmitry Astrikov / www.astrikov.ru
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Base controller class
 * @package lib/components/controllers
 */
class FrontEndController extends CController implements IViewContextController
{
  /**
   * @var default layout
   */
  public $layout = '//layouts/index';

  /**
   * @var FrontEndController params
   */
  protected $params = array();

  /**
   * @var pageData
   */
  protected $pageData = array();

  /**
   * @var config
   */
  protected $config = array();

  /**
   * @var ViewContext $view
   */
  protected $view = null;

  public function __construct($id, $module = null)
  {
    parent::__construct($id, $module);
  }

  /**
   * FrontEndController initialization
   * set pageData
   * set config
   * set params
   * set \IViewContext
   * @return void
   */
  public function init()
  {
    //Store page data
    $this->pageData = Yii::app()->getModule('page')->getPageData(Yii::app()->request->requestUri);

    //Load configuration
    $this->config = $this->loadConfig();

    //If there are some routing rules - we must parse them and put in params variable
    if (isset($this->config['routing']) && is_array($this->config['routing'])) {
      $this->params = $this->processRoute($this->pageData->url_path, Yii::app()->request->requestUri, $this->config['routing']);
    }

    $this->setView( new ViewContext($this) );
  }

  /**
   * Load params
   * @return array
   */
  private function loadConfig()
  {
    Yii::app()->par->loadParams();
    $app_params = Yii::app()->getParams();
    return isset($app_params[$this->module->id]) ? $app_params[$this->module->id] : array();
  }

  /**
   * Process routing rules
   * @param string $url_path
   * @param string $requestUri
   * @param array $routing_rules
   * @return array
   */
  private function processRoute($url_path, $requestUri, $routing_rules)
  {
    $url_path = str_replace("/", "\/", $url_path);
    $pattern = "/^" . $url_path . "(.*)$/";
    preg_match($pattern, $requestUri, $matches);
    $params_str = trim($matches[1], '/');
    $params = array();
    foreach ($routing_rules as $regexp => $settings) {
      if (preg_match("#$regexp#", $params_str, $matches)) {
        array_shift($matches);
        $exploded = explode(",", $settings);
        $matches = empty($matches) ? array(true) : $matches;
        $params = array_combine($exploded, $matches);
        break;
      }
    }
    return $params;
  }

  /**
   * Method use CMap::mergeArray for merging input $data and data from \IViewContext|\ViewContext
   * @param $view
   * @param null $data
   * @param bool $return
   * @return string
   */
  public function render($view, $data = null, $return = false)
  {
    $data = $data ? (array) $data : array();
    $data = CMap::mergeArray( $this->getView()->getData(), $data);
    return parent::render($view, $data, $return);
  }


  /**
   * @param ViewContext $view
   */
  public function setView( ViewContext $view)
  {
    $this->view = $view;
  }

  /**
   * @return ViewContext
   */
  public function getView()
  {
    return $this->view;
  }
}