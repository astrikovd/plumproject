<?php

/**
 * Copyright (c) 2012 by Dmitry Astrikov / www.astrikov.ru
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * CPlumFileHelper
 * Plum extend standart CFileHelper
 * Date:    04.01.13
 * Time:    05:10
 * @author  sem
 * @package lib/components/helpers
 */
class CPlumFileHelper  extends CFileHelper
{

  /**
   * recursive remove a directory
   * @param $directory   directory to delete
   * @param bool $empty  optional true / false to empty
   * @return bool
   */
  public static function recursiveRemoveDirectory($directory, $empty = false)
  {
    if (substr($directory, -1) == '/') {
      $directory = substr($directory, 0, -1);
    }
    if (!file_exists($directory) || !is_dir($directory)) {
      return false;
    } elseif (is_readable($directory)) {
      $handle = opendir($directory);
      while (false !== ($item = readdir($handle))) {
        if ($item != '.' && $item != '..') {
          $path = $directory . '/' . $item;
          if (is_dir($path)) {
            self::recursiveRemoveDirectory($path);
          } else {
            unlink($path);
          }
        }
      }
      closedir($handle);
      if ($empty == false) {
        if (!rmdir($directory)) {
          return false;
        }
      }
    }
    return true;
  }
}
