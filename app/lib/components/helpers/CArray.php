<?php

/**
 * Copyright (c) 2012 by Dmitry Astrikov / www.astrikov.ru
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Array helper.
 * @source     package kohana.core
 * @version    from Kohana 3.3.0
 * @category   Helpers
 * @author     Kohana Team
 * @copyright  (c) 2007-2012 Kohana Team
 * @license    http://kohanaframework.org/license
 * @package    lib/components/helpers
 * @changes    f-sem
 */
class CArray
{

  /**
   * @var  string  default delimiter for path()
   */
  public static $delimiter = '.';

  /**
   * Tests if an array is associative or not.
   *
   *     // Returns true
   *     CArray::is_assoc(array('username' => 'john.doe'));
   *
   *     // Returns false
   *     CArray::is_assoc('foo', 'bar');
   *
   * @param   array   $array  array to check
   * @return  boolean
   */
  public static function is_assoc(array $array)
  {
    // Keys of the array
    $keys = array_keys($array);

    // If the array keys of the keys match the keys, then the array must
    // not be associative (e.g. the keys array looked like {0:0, 1:1...}).
    return array_keys($keys) !== $keys;
  }

  /**
   * Test if a value is an array with an additional check for array-like objects.
   *
   *     // Returns true
   *     CArray::is_array(array());
   *     CArray::is_array(new ArrayObject);
   *
   *     // Returns false
   *     CArray::is_array(false);
   *     CArray::is_array('not an array!');
   *     CArray::is_array(Database::instance());
   *
   * @param   mixed   $value  value to check
   * @return  boolean
   */
  public static function is_array($value)
  {
    if (is_array($value)) {
      // Definitely an array
      return true;
    } else {
      // Possibly a Traversable object, functionally the same as an array
      return (is_object($value) AND $value instanceof Traversable);
    }
  }

  /**
   * Gets a value from an array using a dot separated path.
   *
   *     // Get the value of $array['foo']['bar']
   *     $value = CArray::path($array, 'foo.bar');
   *
   * Using a wildcard "*" will search intermediate arrays and return an array.
   *
   *     // Get the values of "color" in theme
   *     $colors = CArray::path($array, 'theme.*.color');
   *
   *     // Using an array of keys
   *     $colors = CArray::path($array, array('theme', '*', 'color'));
   *
   * @param   array   $array      array to search
   * @param   mixed   $path       key path string (delimiter separated) or array of keys
   * @param   mixed   $default    default value if the path is not set
   * @param   string  $delimiter  key path delimiter
   * @return  mixed
   */
  public static function path($array, $path, $default = null, $delimiter = null)
  {
    if (!CArray::is_array($array)) {
      // This is not an array!
      return $default;
    }

    if (is_array($path)) {
      // The path has already been separated into keys
      $keys = $path;
    } else {
      if (array_key_exists($path, $array)) {
        // No need to do extra processing
        return $array[$path];
      }

      if ($delimiter === null) {
        // Use the default delimiter
        $delimiter = CArray::$delimiter;
      }

      // Remove starting delimiters and spaces
      $path = ltrim($path, "{$delimiter} ");

      // Remove ending delimiters, spaces, and wildcards
      $path = rtrim($path, "{$delimiter} *");

      // Split the keys by delimiter
      $keys = explode($delimiter, $path);
    }

    do {
      $key = array_shift($keys);

      if (ctype_digit($key)) {
        // Make the key an integer
        $key = (int)$key;
      }

      if (isset($array[$key])) {
        if ($keys) {
          if (CArray::is_array($array[$key])) {
            // Dig down into the next part of the path
            $array = $array[$key];
          } else {
            // Unable to dig deeper
            break;
          }
        } else {
          // Found the path requested
          return $array[$key];
        }
      } elseif ($key === '*') {
        // Handle wildcards

        $values = array();
        foreach ($array as $arr) {
          if ($value = CArray::path($arr, implode('.', $keys))) {
            $values[] = $value;
          }
        }

        if ($values) {
          // Found the values requested
          return $values;
        } else {
          // Unable to dig deeper
          break;
        }
      } else {
        // Unable to dig deeper
        break;
      }
    } while ($keys);

    // Unable to find the value requested
    return $default;
  }

  /**
   * Set a value on an array by path.
   *
   * @see Arr::path()
   * @param array   $array     Array to update
   * @param string  $path      Path
   * @param mixed   $value     Value to set
   * @param string  $delimiter Path delimiter
   */
  public static function set_path(& $array, $path, $value, $delimiter = null)
  {
    if (!$delimiter) {
      // Use the default delimiter
      $delimiter = CArray::$delimiter;
    }

    // Split the keys by delimiter
    $keys = explode($delimiter, $path);

    // Set current $array to inner-most array path
    while (count($keys) > 1) {
      $key = array_shift($keys);

      if (ctype_digit($key)) {
        // Make the key an integer
        $key = (int)$key;
      }

      if (!isset($array[$key])) {
        $array[$key] = array();
      }

      $array = & $array[$key];
    }

    // Set key on inner-most array
    $array[array_shift($keys)] = $value;
  }

  /**
   * Set a value on an array by path.
   *
   * @see Arr::path()
   * @param array   $array     Array to update
   * @param string  $path      Path
   * @param mixed   $value     Value to set
   * @param string  $delimiter Path delimiter
   */
  public static function bind_path(& $array, $path, & $value, $delimiter = null)
  {
    if (!$delimiter) {
      // Use the default delimiter
      $delimiter = CArray::$delimiter;
    }

    // Split the keys by delimiter
    $keys = explode($delimiter, $path);

    // Set current $array to inner-most array path
    while (count($keys) > 1) {
      $key = array_shift($keys);

      if (ctype_digit($key)) {
        // Make the key an integer
        $key = (int)$key;
      }

      if (!isset($array[$key])) {
        $array[$key] = array();
      }

      $array = & $array[$key];
    }

    // Set key on inner-most array
    $array[array_shift($keys)] = & $value;
  }

  /**
   * Fill an array with a range of numbers.
   *
   *     // Fill an array with values 5, 10, 15, 20
   *     $values = CArray::range(5, 20);
   *
   * @param   integer $step   stepping
   * @param   integer $max    ending number
   * @return  array
   */
  public static function range($step = 10, $max = 100)
  {
    if ($step < 1)
      return array();

    $array = array();
    for ($i = $step; $i <= $max; $i += $step)
    {
      $array[$i] = $i;
    }

    return $array;
  }

  /**
   * Retrieve a single key from an array. If the key does not exist in the
   * array, the default value will be returned instead.
   *
   *     // Get the value "username" from $_POST, if it exists
   *     $username = CArray::get($_POST, 'username');
   *
   *     // Get the value "sorting" from $_GET, if it exists
   *     $sorting = CArray::get($_GET, 'sorting');
   *
   * @param   array   $array      array to extract from
   * @param   string  $key        key name
   * @param   mixed   $default    default value
   * @return  mixed
   */
  public static function get($array, $key, $default = null)
  {
    return isset($array[$key]) ? $array[$key] : $default;
  }

  /**
   * Retrieves multiple paths from an array. If the path does not exist in the
   * array, the default value will be added instead.
   *
   *     // Get the values "username", "password" from $_POST
   *     $auth = CArray::extract($_POST, array('username', 'password'));
   *
   *     // Get the value "level1.level2a" from $data
   *     $data = array('level1' => array('level2a' => 'value 1', 'level2b' => 'value 2'));
   *     CArray::extract($data, array('level1.level2a', 'password'));
   *
   * @param   array  $array    array to extract paths from
   * @param   array  $paths    list of path
   * @param   mixed  $default  default value
   * @return  array
   */
  public static function extract($array, array $paths, $default = null)
  {
    $found = array();
    foreach ($paths as $path)
    {
      CArray::set_path($found, $path, CArray::path($array, $path, $default));
    }

    return $found;
  }

  /**
   * Retrieves muliple single-key values from a list of arrays.
   *
   *     // Get all of the "id" values from a result
   *     $ids = CArray::pluck($result, 'id');
   *
   * [!!] A list of arrays is an array that contains arrays, eg: array(array $a, array $b, array $c, ...)
   *
   * @param   array   $array  list of arrays to check
   * @param   string  $key    key to pluck
   * @return  array
   */
  public static function pluck($array, $key)
  {
    $values = array();

    foreach ($array as $row) {
      if (isset($row[$key])) {
        // Found a value in this row
        $values[] = $row[$key];
      }
    }

    return $values;
  }

  /**
   * Adds a value to the beginning of an associative array.
   *
   *     // Add an empty value to the start of a select list
   *     CArray::unshift($array, 'none', 'Select a value');
   *
   * @param   array   $array  array to modify
   * @param   string  $key    array key name
   * @param   mixed   $val    array value
   * @return  array
   */
  public static function unshift(array & $array, $key, $val)
  {
    $array = array_reverse($array, true);
    $array[$key] = $val;
    $array = array_reverse($array, true);

    return $array;
  }

  /**
   * Recursive version of [array_map](http://php.net/array_map), applies one or more
   * callbacks to all elements in an array, including sub-arrays.
   *
   *     // Apply "strip_tags" to every element in the array
   *     $array = CArray::map('strip_tags', $array);
   *
   *     // Apply $this->filter to every element in the array
   *     $array = CArray::map(array(array($this,'filter')), $array);
   *
   *     // Apply strip_tags and $this->filter to every element
   *     $array = CArray::map(array('strip_tags',array($this,'filter')), $array);
   *
   * [!!] Because you can pass an array of callbacks, if you wish to use an array-form callback
   * you must nest it in an additional array as above. Calling CArray::map(array($this,'filter'), $array)
   * will cause an error.
   * [!!] Unlike `array_map`, this method requires a callback and will only map
   * a single array.
   *
   * @param   mixed   $callbacks  array of callbacks to apply to every element in the array
   * @param   array   $array      array to map
   * @param   array   $keys       array of keys to apply to
   * @return  array
   */
  public static function map($callbacks, $array, $keys = null)
  {
    foreach ($array as $key => $val)
    {
      if (is_array($val))
      {
        $array[$key] = CArray::map($callbacks, $array[$key]);
      }
      elseif ( ! is_array($keys) OR in_array($key, $keys))
      {
        if (is_array($callbacks))
        {
          foreach ($callbacks as $cb)
          {
            $array[$key] = call_user_func($cb, $array[$key]);
          }
        }
        else
        {
          $array[$key] = call_user_func($callbacks, $array[$key]);
        }
      }
    }

    return $array;
  }

  /**
   * Recursively merge two or more arrays. Values in an associative array
   * overwrite previous values with the same key. Values in an indexed array
   * are appended, but only when they do not already exist in the result.
   *
   * Note that this does not work the same as [array_merge_recursive](http://php.net/array_merge_recursive)!
   *
   *     $john = array('name' => 'john', 'children' => array('fred', 'paul', 'sally', 'jane'));
   *     $mary = array('name' => 'mary', 'children' => array('jane'));
   *
   *     // John and Mary are married, merge them together
   *     $john = CArray::merge($john, $mary);
   *
   *     // The output of $john will now be:
   *     array('name' => 'mary', 'children' => array('fred', 'paul', 'sally', 'jane'))
   *
   * @param   array  $array1      initial array
   * @param   array  $array2,...  array to merge
   * @param   array  ...
   * @return  array
   */
  public static function merge(array $array1, array $array2)
  {
    if (CArray::is_assoc($array2))
    {
      foreach ($array2 as $key => $value)
      {
        if (is_array($value)
            AND isset($array1[$key])
                AND is_array($array1[$key])
        )
        {
          $array1[$key] = CArray::merge($array1[$key], $value);
        }
        else
        {
          $array1[$key] = $value;
        }
      }
    }
    else
    {
      foreach ($array2 as $value)
      {
        if ( ! in_array($value, $array1, true))
        {
          $array1[] = $value;
        }
      }
    }

    if (func_num_args() > 2)
    {
      foreach (array_slice(func_get_args(), 2) as $array2)
      {
        if (CArray::is_assoc($array2))
        {
          foreach ($array2 as $key => $value)
          {
            if (is_array($value)
                AND isset($array1[$key])
                    AND is_array($array1[$key])
            )
            {
              $array1[$key] = CArray::merge($array1[$key], $value);
            }
            else
            {
              $array1[$key] = $value;
            }
          }
        }
        else
        {
          foreach ($array2 as $value)
          {
            if ( ! in_array($value, $array1, true))
            {
              $array1[] = $value;
            }
          }
        }
      }
    }

    return $array1;
  }

  /**
   * Overwrites an array with values from input arrays.
   * Keys that do not exist in the first array will not be added!
   *
   *     $a1 = array('name' => 'john', 'mood' => 'happy', 'food' => 'bacon');
   *     $a2 = array('name' => 'jack', 'food' => 'tacos', 'drink' => 'beer');
   *
   *     // Overwrite the values of $a1 with $a2
   *     $array = CArray::overwrite($a1, $a2);
   *
   *     // The output of $array will now be:
   *     array('name' => 'jack', 'mood' => 'happy', 'food' => 'tacos')
   *
   * @param   array   $array1 master array
   * @param   array   $array2 input arrays that will overwrite existing values
   * @return  array
   */
  public static function overwrite($array1, $array2)
  {
    foreach (array_intersect_key($array2, $array1) as $key => $value) {
      $array1[$key] = $value;
    }

    if (func_num_args() > 2) {
      foreach (array_slice(func_get_args(), 2) as $array2) {
        foreach (array_intersect_key($array2, $array1) as $key => $value) {
          $array1[$key] = $value;
        }
      }
    }

    return $array1;
  }

  /**
   * Creates a callable function and parameter list from a string representation.
   * Note that this function does not validate the callback string.
   *
   *     // Get the callback function and parameters
   *     list($func, $params) = CArray::callback('Foo::bar(apple,orange)');
   *
   *     // Get the result of the callback
   *     $result = call_user_func_array($func, $params);
   *
   * @param   string  $str    callback string
   * @return  array   function, params
   */
  public static function callback($str)
  {
    // Overloaded as parts are found
    $command = $params = null;

    // command[param,param]
    if (preg_match('/^([^\(]*+)\((.*)\)$/', $str, $match)) {
      // command
      $command = $match[1];

      if ($match[2] !== '') {
        // param,param
        $params = preg_split('/(?<!\\\\),/', $match[2]);
        $params = str_replace('\,', ',', $params);
      }
    } else {
      // command
      $command = $str;
    }

    if (strpos($command, '::') !== false) {
      // Create a static method callable command
      $command = explode('::', $command, 2);
    }

    return array($command, $params);
  }

  /**
   * Convert a multi-dimensional array into a single-dimensional array.
   *
   *     $array = array('set' => array('one' => 'something'), 'two' => 'other');
   *
   *     // Flatten the array
   *     $array = CArray::flatten($array);
   *
   *     // The array will now be
   *     array('one' => 'something', 'two' => 'other');
   *
   * [!!] The keys of array values will be discarded.
   *
   * @param   array   $array  array to flatten
   * @return  array
   * @since   3.0.6
   */
  public static function flatten($array)
  {
    $is_assoc = CArray::is_assoc($array);

    $flat = array();
    foreach ($array as $key => $value)
    {
      if (is_array($value))
      {
        $flat = array_merge($flat, CArray::flatten($value));
      }
      else
      {
        if ($is_assoc)
        {
          $flat[$key] = $value;
        }
        else
        {
          $flat[] = $value;
        }
      }
    }
    return $flat;
  }
  /**
   * Return a callback array from a string, eg: limit[10,20] would become
   * array('limit', array('10', '20'))
   *
   * @param   string  callback string
   * @return  array
   */
  public static function callback_string($str)
  {
    // command[param,param]
    if (preg_match('/([^\[]*+)\[(.+)\]/', (string) $str, $match))
    {
      // command
      $command = $match[1];

      // param,param
      $params = preg_split('/(?<!\\\\),/', $match[2]);
      $params = str_replace('\,', ',', $params);
    }
    else
    {
      // command
      $command = $str;

      // No params
      $params = null;
    }

    return array($command, $params);
  }

  /**
   * Rotates a 2D array clockwise.
   * Example, turns a 2x3 array into a 3x2 array.
   *
   * @param   array    array to rotate
   * @param   boolean  keep the keys in the final rotated array. the sub arrays of the source array need to have the same key values.
   *                   if your subkeys might not match, you need to pass false here!
   * @return  array
   */
  public static function rotate($source_array, $keep_keys = true)
  {
    $new_array = array();
    foreach ($source_array as $key => $value)
    {
      $value = ($keep_keys === true) ? $value : array_values($value);
      foreach ($value as $k => $v)
      {
        $new_array[$k][$key] = $v;
      }
    }

    return $new_array;
  }

  /**
   * Removes a key from an array and returns the value.
   *
   * @param   string  key to return
   * @param   array   array to work on
   * @return  mixed   value of the requested array key
   */
  public static function remove($key, & $array)
  {
    if ( ! array_key_exists($key, $array))
      return null;

    $val = $array[$key];
    unset($array[$key]);

    return $val;
  }


  /**
   * Because PHP does not have this function.
   *
   * @param   array   array to unshift
   * @param   string  key to unshift
   * @param   mixed   value to unshift
   * @return  array
   */
  public static function unshift_assoc( array & $array, $key, $val)
  {
    $array = array_reverse($array, true);
    $array[$key] = $val;
    $array = array_reverse($array, true);

    return $array;
  }

  /**
   * Because PHP does not have this function, and array_walk_recursive creates
   * references in arrays and is not truly recursive.
   *
   * @param   mixed  callback to apply to each member of the array
   * @param   array  array to map to
   * @return  array
   */
  public static function map_recursive($callback, array $array)
  {
    foreach ($array as $key => $val)
    {
      // Map the callback to the key
      $array[$key] = is_array($val) ? self::map_recursive($callback, $val) : call_user_func($callback, $val);
    }

    return $array;
  }

  /**
   * Binary search algorithm.
   *
   * @param   mixed    the value to search for
   * @param   array    an array of values to search in
   * @param   boolean  return false, or the nearest value
   * @param   mixed    sort the array before searching it
   * @return  integer
   */
  public static function binary_search($needle, $haystack, $nearest = false, $sort = false)
  {
    if ($sort === true)
    {
      sort($haystack);
    }

    $high = count($haystack);
    $low = 0;

    while ($high - $low > 1)
    {
      $probe = ($high + $low) / 2;
      if ($haystack[$probe] < $needle)
      {
        $low = $probe;
      }
      else
      {
        $high = $probe;
      }
    }

    if ($high == count($haystack) OR $haystack[$high] != $needle)
    {
      if ($nearest === false)
        return false;

      // return the nearest value
      $high_distance = $haystack[ceil($low)] - $needle;
      $low_distance = $needle - $haystack[floor($low)];

      return ($high_distance >= $low_distance) ? $haystack[ceil($low)] : $haystack[floor($low)];
    }

    return $high;
  }


  /**
   * Recursively convert an array to an object.
   *
   * @param   array   array to convert
   * @return  object
   */
  public static function to_object(array $array, $class = 'stdClass')
  {
    $object = new $class;

    foreach ($array as $key => $value)
    {
      if (is_array($value))
      {
        // Convert the array to an object
        $value = self::to_object($value, $class);
      }

      // Add the value to the object
      $object->{$key} = $value;
    }

    return $object;
  }

  /**
   * Compares 2 elemets with sort keys
   * @param array $a
   * @param array $b
   * @return boolean
   */
  public static function compare_sort($a, $b){
    return strnatcmp($a['sort'], $b['sort']);
  }

}