<?php

/**
 * Copyright (c) 2012 by Dmitry Astrikov / www.astrikov.ru
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * CPager
 * @package lib/components/helpers
 */
class CPager
{
	public static function create($url="?1=1", $cnt=100, $limit=5, $pg=1, $num=7, $class="pager")
	{
	    if($cnt < $limit) return;

		$cnt = ceil($cnt/$limit);
	         if($cnt == 1) return;

		$pg = abs(intval($pg));
		if($pg > $cnt) $pg = $cnt;
		if($pg < 1) $pg = 1;

		$prev_pg = $pg - 1;
		$next_pg = $pg + 1;

		$part_prev = "";
		$part_next = "";
		$part_prev_pages = "";
		$part_next_pages = "";
		$part_sel = ' <span class="selected">'.$pg.'</span> ';

		#if(!ereg("\?",$url)) $url=$url."?1=1";

		if ($prev_pg) $part_prev = '<a href="'.$url.'pg'.$prev_pg.'/">&larr;</a> ';
		if ($next_pg <= $cnt) $part_next = ' <a href="'.$url.'pg'.$next_pg.'/">&rarr;</a>';

		$part_first = '<a href="'.$url.'pg1/">1</a> ';
		$part_last = '<a href="'.$url.'pg'.$cnt.'/">'.$cnt.'</a> ';

		if ($pg == 1) $part_first = "";
	    if ($pg == $cnt) $part_last = "";

		$par_cnt = floor($num/2);
		$fp = $pg - $par_cnt;

		if($pg > $par_cnt && $fp > 2) {
			$part_prev_pages = " ... ";
			for($x = $fp; $x < $pg; $x++) {
				 $part_prev_pages.= '<a href="'.$url.'pg'.$x.'/">'.$x.'</a> ';
			}
		} else {
			for($x = 2; $x < $pg; $x++) {
				 $part_prev_pages.= '<a href="'.$url.'pg'.$x.'/">'.$x.'</a> ';
			}
		}

		if ($pg < $cnt-$par_cnt) {
			for($x = $pg+1; $x < $pg+$par_cnt+1; $x++) {
				$part_next_pages.= ' <a href="'.$url.'pg'.$x.'/">'.$x.'</a> ';
			}
			$part_next_pages.=" ... ";
		} else {
			for($x = $pg+1; $x < $cnt; $x++) {
				$part_next_pages.= ' <a href="'.$url.'pg'.$x.'/">'.$x.'</a> ';
			}
		}

		$pager = '<div class="'.$class.'">'.$part_prev.$part_first.$part_prev_pages.$part_sel.$part_next_pages.$part_last.$part_next.'</div>';
		return $pager;
	}

}


