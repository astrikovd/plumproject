<?php

/**
 * Copyright (c) 2012 by Dmitry Astrikov / www.astrikov.ru
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Object helper (in future is full analog CArray helper
 * @package lib/components/helpers
 */
class CObject
{
  /**
   * Analog CArray::path for objects.
   * @static
   * @param $object
   * @param $path
   * @param null $default
   * @param string $delimiter
   * @param bool $allowArray
   * @return array|mixed|null
   */
  public static function path($object, $path, $default = null, $delimiter = '.', $allowArray = true)
  {
    if ($allowArray && is_array($object)) {
      return CArray::path($object, $path, $default, $delimiter);
    }

    if (!is_object($object)) {
      return $default; // This is not an object!
    }

    if (is_array($path)) {
      $keys = $path; // The path has already been separated into keys
    } else {

      if (isset($object->$path)) {
        return $object->$path; // No need to do extra processing
      }

      if ($delimiter === null) {
        // Use the default delimiter
        $delimiter = '.';
      }

      // Remove starting delimiters and spaces
      $path = ltrim($path, "{$delimiter} ");

      // Remove ending delimiters, spaces, and wildcards
      $path = rtrim($path, "{$delimiter} *");

      // Split the keys by delimiter
      $keys = explode($delimiter, $path);
    }

    do {
      $key = array_shift($keys);
      //$key = preg_replace('/[^a-z\d]/i', '', $key);

      if (isset($object->$key)) {

        if ($keys) {
          if (isset($object->$key) && is_object($object->$key)) {
            // Dig down into the next part of the path
            $object = $object->$key;
          } else {
            // Unable to dig deeper
            break;
          }
        } else {

          return $object->$key; // Found the path requested
        }
      } elseif ($key === '*') {
        // Handle wildcards
        $values = array();
        foreach (get_object_vars($object) as $arr) {
          if ($value = CArray::path($arr, implode('.', $keys))) {
            $values[] = $value;
          }
        }

        if ($values) {
          // Found the values requested
          return $values;
        } else {
          // Unable to dig deeper
          break;
        }
      } else {
        // Unable to dig deeper
        break;
      }
    } while ($keys);

    return $default; // Unable to find the value requested
  }
}
