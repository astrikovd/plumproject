<?php

/**
 * Copyright (c) 2012 by Dmitry Astrikov / www.astrikov.ru
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * CPlumViewHelper
 * View helper for front/back views
 * Date:    02.01.13
 * Time:    18:45
 * @author  sem
 * @package lib/components/helpers
 */
class CPlumViewHelper
{

  /**
   * @param CBaseController $controller
   * @return string
   * @throws CException
   */
  public static function getPartialsPath( CBaseController $controller )
  {
    $partials_path =  realpath( $controller->getViewPath(). DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '_partials' ). DIRECTORY_SEPARATOR;
    if( $partials_path === DIRECTORY_SEPARATOR )
    {
      throw new CException('Partials components paths for module '. $controller->getModule()->getId(). ' not found!' );
    }

    return $partials_path;
  }
  /**
   * @return string
   * @throws CException
   */
  public static function getCurrentPartialsPath()
  {
    $app = Yii::app();

    $partials_path =  realpath( $app->getController()->getViewPath(). DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '_partials' ). DIRECTORY_SEPARATOR;
    if( $partials_path === DIRECTORY_SEPARATOR )
    {
      throw new CException('Partials components paths for module '. $app->getController()->getModule()->getId(). ' not found!' );
    }

    return $partials_path;
  }

  /**
   * Require partial file to view file.
   * @param $file_name
   * @param CBaseController $controller
   * @throws CException
   */
  public static function requirePartial( $file_name, CBaseController $controller = null )
  {
    $path = self::getPartialFullPath( $file_name, $controller );
    $context = $controller;
    require_once( $path );
  }

  /**
   * Include partial file to view file.
   * @param $file_name
   * @param CBaseController $controller
   * @throws CException
   */
  public static function includePartial( $file_name, CBaseController $controller = null )
  {
    $path = self::getPartialFullPath( $file_name, $controller );
    $context = $controller;
    include_once( $path );
  }
  /**
   * @param $file_name
   * @param CBaseController $controller
   * @return string
   * @throws CException
   */
  private static function getPartialFullPath( $file_name, CBaseController & $controller = null )
  {
    $path = self::getCurrentPartialsPath();

    if( $controller !== null )
    {
      $path = self::getPartialsPath( $controller );
    }
    else{
      $controller = Yii::app()->getController();
    }

    if( !file_exists( $path. $file_name . '.php') )
    {
      throw new CException('Partial file '.$file_name.'.php not found!');
    }

    return $path. $file_name . '.php';
  }

}
