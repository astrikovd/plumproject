<?php
/**
 *
 * @author: sem
 * Date: 26.12.12
 * Time: 21:21
 */
class CPlumActiveRecord extends CActiveRecord
{
  public function getTablePrefix()
  {
    return $this->getDbConnection()->tablePrefix;
  }
}
