SET NAMES utf8;

# SET foreign_key_checks = 0;
# ALTER TABLE `adm_cmp` DROP FOREIGN KEY adm_cmp_ibfk_1;
# ALTER TABLE `adm_user_adm_cmp` DROP FOREIGN KEY adm_user_adm_cmp_ibfk_1;
# ALTER TABLE `adm_user_adm_cmp` DROP FOREIGN KEY adm_user_adm_cmp_ibfk_2;
# ALTER TABLE `page` DROP FOREIGN KEY page_ibfk_1;
# ALTER TABLE `page` DROP FOREIGN KEY page_ibfk_2;
# ALTER TABLE `newsfeed` DROP FOREIGN KEY newsfeed_ibfk_1;
# ALTER TABLE `newsitem` DROP FOREIGN KEY newsitem_ibfk_1;
# ALTER TABLE `photo` DROP FOREIGN KEY photo_ibfk_1;
# ALTER TABLE `adv_banners` DROP FOREIGN KEY adv_banners_ibfk_1;
# ALTER TABLE `adv_banners` DROP FOREIGN KEY adv_banners_ibfk_2;
# SET foreign_key_checks = 1;

SET foreign_key_checks = 0;
--
-- Ограничения внешнего ключа таблицы `adm_cmp`
--
 ALTER TABLE `adm_cmp` ADD CONSTRAINT `adm_cmp_ibfk_1`
FOREIGN KEY (`adm_cmp_grp_id`)
REFERENCES `adm_cmp_grp` (`adm_cmp_grp_id`)
  ON DELETE RESTRICT;
--
-- Ограничения внешнего ключа таблицы `adm_user_adm_cmp`
--
 ALTER TABLE `adm_user_adm_cmp` ADD CONSTRAINT `adm_user_adm_cmp_ibfk_1`
  FOREIGN KEY (`adm_user_id`)
    REFERENCES `adm_user` (`adm_user_id`)
      ON DELETE RESTRICT;

ALTER TABLE `adm_user_adm_cmp` ADD CONSTRAINT `adm_user_adm_cmp_ibfk_2`
  FOREIGN KEY (`adm_cmp_id`)
    REFERENCES `adm_cmp` (`adm_cmp_id`)
      ON DELETE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `page`
--
 ALTER TABLE `page` ADD CONSTRAINT `page_ibfk_1`
  FOREIGN KEY (`page_module_id`)
    REFERENCES `page_module` (`page_module_id`)
      ON DELETE RESTRICT;

ALTER TABLE `page` ADD CONSTRAINT `page_ibfk_2`
  FOREIGN KEY (`adv_sections_id`)
    REFERENCES `adv_sections` (`adv_sections_id`)
      ON DELETE RESTRICT;

ALTER TABLE `page` ADD CONSTRAINT `page_ibfk_3`
    FOREIGN KEY (`parent_id`)
      REFERENCES `page` (`page_id`)
        ON DELETE RESTRICT;
--
-- Ограничения внешнего ключа таблицы `newsfeed`
--
 ALTER TABLE `newsfeed` ADD CONSTRAINT `newsfeed_ibfk_1`
  FOREIGN KEY (`newscat_id`)
    REFERENCES `newscat` (`newscat_id`)
      ON DELETE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `newsitem`
--
 ALTER TABLE `newsitem` ADD CONSTRAINT `newsitem_ibfk_1`
  FOREIGN KEY (`newsfeed_id`)
    REFERENCES `newsfeed` (`newsfeed_id`)
      ON DELETE RESTRICT;
--
-- Ограничения внешнего ключа таблицы `photo`
--
 ALTER TABLE `photo` ADD CONSTRAINT `photo_ibfk_1`
  FOREIGN KEY (`photogallery_id`)
    REFERENCES `photogallery` (`photogallery_id`)
      ON DELETE RESTRICT;
--
-- Ограничения внешнего ключа таблицы `photo`
--
 ALTER TABLE `adv_banners` ADD CONSTRAINT `adv_banners_ibfk_1`
  FOREIGN KEY (`adv_places_id`)
    REFERENCES `adv_places` (`adv_places_id`)
      ON DELETE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `adv_banners`
--
 ALTER TABLE `adv_banners` ADD CONSTRAINT `adv_banners_ibfk_2`
  FOREIGN KEY (`adv_sections_id`)
    REFERENCES `adv_sections` (`adv_sections_id`)
      ON DELETE RESTRICT;

SET foreign_key_checks = 1;

