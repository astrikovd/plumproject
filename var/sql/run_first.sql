--Backend users
CREATE TABLE adm_user (
    adm_user_id SERIAL NOT NULL PRIMARY KEY,
    username VARCHAR(32) NOT NULL,
    passwordmd5 VARCHAR(32) NOT NULL,
    email VARCHAR(32) NOT NULL,
    last_login TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
    last_ip VARCHAR(32) NOT NULL DEFAULT '0.0.0.0',
    date_create TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW()
);

INSERT INTO adm_user(adm_user_id, username, passwordmd5, email, last_login, last_ip, date_create)
VALUES (1, 'admin', 'e10adc3949ba59abbe56e057f20f883e', 'astrikov.d@gmail.com', now(), '127.0.0.1', now());

--Backend component groups
CREATE TABLE adm_cmp_grp (
    adm_cmp_grp_id SERIAL NOT NULL PRIMARY KEY,
    char_id VARCHAR(32) NOT NULL,
    name VARCHAR(32) NOT NULL
);

INSERT INTO adm_cmp_grp(adm_cmp_grp_id, char_id, name)
VALUES (1, 'adm', 'Администрирование');
INSERT INTO adm_cmp_grp(adm_cmp_grp_id, char_id, name)
VALUES (2, 'info', 'Информация');
INSERT INTO adm_cmp_grp(adm_cmp_grp_id, char_id, name)
VALUES (3, 'serv', 'Сервисы');
INSERT INTO adm_cmp_grp(adm_cmp_grp_id, char_id, name)
VALUES (4, 'shop', 'Интернет-магазин');
INSERT INTO adm_cmp_grp(adm_cmp_grp_id, char_id, name)
VALUES (5, 'adv', 'Реклама');
INSERT INTO adm_cmp_grp(adm_cmp_grp_id, char_id, name)
VALUES (6, 'other', 'Разное');

--Backend components
CREATE TABLE adm_cmp (
    adm_cmp_id SERIAL NOT NULL PRIMARY KEY,
    char_id VARCHAR(32) NOT NULL,
    name VARCHAR(32) NOT NULL,
    adm_cmp_grp_id INTEGER NOT NULL REFERENCES adm_cmp_grp,
    description VARCHAR(255) NOT NULL DEFAULT ''
);
--Users
INSERT INTO adm_cmp (adm_cmp_id, char_id, name, adm_cmp_grp_id, description)
VALUES (1, 'user', 'Пользователи Plum', 1, 'Управление учетными записями и разграничением прав');
--Site structure
INSERT INTO adm_cmp (adm_cmp_id, char_id, name, adm_cmp_grp_id, description)
VALUES (2, 'page', 'Структура сайта', 2, 'Управление структурой сайта, добавление новых страниц, управление их содержимым');
--File manager
INSERT INTO adm_cmp (adm_cmp_id, char_id, name, adm_cmp_grp_id, description)
VALUES (3, 'filemanager', 'Файловый менеджер', 1, 'Загрузка файлов на сервер для их последующего использования на сайте');
--News
INSERT INTO adm_cmp (adm_cmp_id, char_id, name, adm_cmp_grp_id, description)
VALUES (4, 'newsfeed', 'Новостные ленты', 2, 'Управление новостными лентами');
INSERT INTO adm_cmp (adm_cmp_id, char_id, name, adm_cmp_grp_id, description)
VALUES (5, 'news', 'Новости', 2, 'Управление новостями, статьями и другой информацией, которая может быть представлена в виде списка элементов');
--Photo
INSERT INTO adm_cmp (adm_cmp_id, char_id, name, adm_cmp_grp_id, description)
VALUES (6, 'photogallery', 'Фотогалереи', 2, 'Управление фотогалереями');
INSERT INTO adm_cmp (adm_cmp_id, char_id, name, adm_cmp_grp_id, description)
VALUES (7, 'photo', 'Фотографии', 2, 'Управление фотографиями');
--Siteinfo
INSERT INTO adm_cmp (adm_cmp_id, char_id, name, adm_cmp_grp_id, description)
VALUES (8, 'siteinfo', 'Информация о сайте', 1, 'Изменение информации о сайте, такой как название сайта, e-mail администратора. Управление режимом техобслуживания');
--Feedback
INSERT INTO adm_cmp (adm_cmp_id, char_id, name, adm_cmp_grp_id, description)
VALUES (9, 'feedback', 'Обратная связь', 3, 'Управление сообщениями, поступающими через форму обратной связи на сайте');
--Change password
INSERT INTO adm_cmp (adm_cmp_id, char_id, name, adm_cmp_grp_id, description)
VALUES (10, 'chpasswd', 'Изменить пароль', 6, 'Изменение пароля текущего пользователя');
--Advertisement
INSERT INTO adm_cmp (adm_cmp_id, char_id, name, adm_cmp_grp_id, description)
VALUES (11, 'advplaces', 'Рекламные места', 5, 'Управление рекламными местами для размещения баннеров');
INSERT INTO adm_cmp (adm_cmp_id, char_id, name, adm_cmp_grp_id, description)
VALUES (12, 'advsections', 'Рекламные разделы', 5, 'Управление разделами, предназначенными для размещения рекламы');
INSERT INTO adm_cmp (adm_cmp_id, char_id, name, adm_cmp_grp_id, description)
VALUES (13, 'advbanners', 'Рекламные баннеры', 5, 'Управление рекламными баннерами - загрузка изображений, изменение кодов и т.д.');

--Backend users and components relation
CREATE TABLE adm_user_adm_cmp (
    adm_user_adm_cmp_id SERIAL NOT NULL PRIMARY KEY,
    adm_user_id INTEGER NOT NULL REFERENCES adm_user,
    adm_cmp_id INTEGER NOT NULL REFERENCES adm_cmp
);

INSERT INTO adm_user_adm_cmp (adm_user_adm_cmp_id, adm_user_id, adm_cmp_id)
VALUES (1, 1, 1);
INSERT INTO adm_user_adm_cmp (adm_user_adm_cmp_id, adm_user_id, adm_cmp_id)
VALUES (2, 1, 2);
INSERT INTO adm_user_adm_cmp (adm_user_adm_cmp_id, adm_user_id, adm_cmp_id)
VALUES (3, 1, 3);

--View
/*
CREATE VIEW adm_cmp_v AS
SELECT ac.adm_cmp_id, ac.char_id AS cmp_char_id, ac.name as cmp_name,
acg.adm_cmp_grp_id, acg.char_id AS grp_char_id, acg.name as grp_name, auac.adm_user_id
FROM adm_cmp ac, adm_cmp_grp acg, adm_user_adm_cmp auac
WHERE ac.adm_cmp_grp_id = acg.adm_cmp_grp_id AND ac.adm_cmp_id = auac.adm_cmp_id
ORDER BY ac.adm_cmp_grp_id;
*/
--Page Module
CREATE TABLE page_module (
    page_module_id VARCHAR(16) NOT NULL PRIMARY KEY,
    name VARCHAR(32) NOT NULL,
    description TEXT NOT NULL DEFAULT '',
    vflag BOOLEAN NOT NULL DEFAULT true
);

--Page
CREATE TABLE page (
    page_id SERIAL NOT NULL PRIMARY KEY,
    parent_id INTEGER REFERENCES page,
    page_module_id VARCHAR(16) NOT NULL REFERENCES page_module,
    page_module_param VARCHAR(32) NOT NULL DEFAULT '',
    url_path VARCHAR(512) NOT NULL DEFAULT '',
    url_name VARCHAR(512) NOT NULL DEFAULT '',
    redirect_url VARCHAR(512) NOT NULL DEFAULT '',
    name VARCHAR(128) NOT NULL DEFAULT '',
    header VARCHAR(128) NOT NULL DEFAULT '',
    before_content TEXT NOT NULL DEFAULT '',
    after_content TEXT NOT NULL DEFAULT '',
    sort INTEGER NOT NULL,
    vflag BOOLEAN NOT NULL DEFAULT true,
    aflag BOOLEAN NOT NULL DEFAULT false,
    hflag BOOLEAN NOT NULL DEFAULT true,
    eflag BOOLEAN NOT NULL DEFAULT true,
    date0 TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now()
);

INSERT INTO page_module (page_module_id, name, description, vflag)
VALUES ('homepage', 'Главная страница', '', false);
INSERT INTO page_module (page_module_id, name, description, vflag)
VALUES ('static', 'Статическая страница', '', true);
INSERT INTO page_module (page_module_id, name, description, vflag)
VALUES ('news', 'Новости', '', false);
INSERT INTO page_module (page_module_id, name, description, vflag)
VALUES ('photo', 'Фотографии', '', false);
INSERT INTO page_module (page_module_id, name, description, vflag)
VALUES ('feedback', 'Обратная связь', '', true);

alter table page add keywords varchar(255) not null default '';
alter table page add description varchar(1023) not null default '';

--News
CREATE TABLE newscat (
    newscat_id VARCHAR(32) NOT NULL PRIMARY KEY,
    vflag BOOLEAN NOT NULL DEFAULT true,
    date0 TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW()
);

alter table newscat add name varchar(32) NOT NULL DEFAULT '';
INSERT INTO newscat (newscat_id, name, vflag, date0) VALUES ('news', 'Новости', true, now());
INSERT INTO newscat (newscat_id, name, vflag, date0) VALUES ('articles', 'Статьи', true, now());

CREATE TABLE newsfeed (
    newsfeed_id SERIAL NOT NULL PRIMARY KEY,
    newscat_id VARCHAR(32) REFERENCES newscat,
    name VARCHAR(128) NOT NULL DEFAULT '',
    cnt INTEGER NOT NULL DEFAULT 10,
    vflag BOOLEAN NOT NULL DEFAULT true,
    date0 TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW()
);

CREATE TABLE newsitem (
    newsitem_id SERIAL NOT NULL PRIMARY KEY,
    newsfeed_id INTEGER REFERENCES newsfeed,
    name VARCHAR(255) NOT NULL DEFAULT '',
    anons TEXT NOT NULL DEFAULT '',
    body TEXT NOT NULL DEFAULT '',
    picture VARCHAR(255) NOT NULL DEFAULT '',
    vflag BOOLEAN NOT NULL DEFAULT true,
    date0 TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW()
);

alter table newsitem add keywords varchar(255) not null default '';
alter table newsitem add description varchar(1023) not null default '';

--Siteinfo
CREATE TABLE siteinfo (
    siteinfo_id SERIAL NOT NULL PRIMARY KEY,
    site_id INTEGER NOT NULL DEFAULT 1,
    title VARCHAR(255) NOT NULL DEFAULT '',
    admin_email VARCHAR(255) NOT NULL DEFAULT '',
    mtflag BOOLEAN NOT NULL DEFAULT false,
    mt_text TEXT NOT NULL DEFAULT ''
);

INSERT INTO siteinfo (title, admin_email) VALUES ('Plum CMF', 'admin@plumcmf.com');

--Feedback
CREATE TABLE feedback (
    feedback_id SERIAL NOT NULL PRIMARY KEY,
    fio VARCHAR(255) NOT NULL DEFAULT '',
    phone VARCHAR(255) NOT NULL DEFAULT '',
    question TEXT NOT NULL DEFAULT '',
    answer TEXT NOT NULL DEFAULT '',
    date_send TIMESTAMP WITHOUT TIME ZONE DEFAULT now(),
    date_ans TIMESTAMP WITHOUT TIME ZONE DEFAULT now(),
    vflag BOOLEAN NOT NULL DEFAULT false
);

-- Photogallery
CREATE TABLE photogallery(
    photogallery_id SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    cover VARCHAR(255) NOT NULL DEFAULT '',
    cnt INTEGER NOT NULL DEFAULT 9,
    vflag BOOLEAN NOT NULL DEFAULT TRUE
);

-- Photo
CREATE TABLE photo(
    photo_id SERIAL PRIMARY KEY,
    photogallery_id INTEGER REFERENCES photogallery,
    name VARCHAR(255) NOT NULL,
    author VARCHAR(255) NOT NULL DEFAULT '',
    description TEXT NOT NULL DEFAULT '',
    picture VARCHAR(255) NOT NULL DEFAULT '',
    date_create TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
    vflag BOOLEAN NOT NULL DEFAULT TRUE
);

--Advertisement
CREATE TABLE adv_sections(
    adv_sections_id SERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(255) NOT NULL DEFAULT '',
    vflag BOOLEAN NOT NULL DEFAULT true,
    date0 TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now()
);

CREATE TABLE adv_places(
    adv_places_id SERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(255) NOT NULL DEFAULT '',
    char_id VARCHAR(64) NOT NULL,
    date0 TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now(),
    vflag BOOLEAN NOT NULL DEFAULT true
);

CREATE TABLE adv_banners(
    adv_banners_id SERIAL NOT NULL PRIMARY KEY,
    adv_places_id INTEGER REFERENCES adv_places,
    adv_sections_id INTEGER REFERENCES adv_sections,
    name VARCHAR(255) NOT NULL DEFAULT '',
    picture VARCHAR(255) NOT NULL DEFAULT '',
    code TEXT NOT NULL DEFAULT '',
    date_start TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now(),
    date_end TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now(),
    date0 TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now(),
    title VARCHAR(255) NOT NULL DEFAULT '',
    link VARCHAR(255) NOT NULL DEFAULT '',
    vflag BOOLEAN NOT NULL DEFAULT true
);
/*
CREATE view adv_banners_v AS (
    SELECT ab.*, ap.char_id , ab.vflag as ab_vflag, ast.vflag as as_vflag, ap.vflag as ap_vflag
    FROM adv_banners ab, adv_sections ast, adv_places ap
    WHERE ab.adv_places_id = ap.adv_places_id AND ab.adv_sections_id = ast.adv_sections_id
    ORDER BY ab.adv_banners_id
);
*/
insert into adv_places(name, char_id) values ('Рекламное место вверху страницы', 'adv_place_top');
insert into adv_places(name, char_id) values ('Рекламное место внизу страницы', 'adv_place_bottom');
insert into adv_sections(name) values('Не показывать баннеры');
insert into adv_sections(name) values('Главная страница');

alter table page add adv_sections_id integer references adv_sections default null;
INSERT INTO page(page_id, parent_id, page_module_id, url_path, name, sort, adv_sections_id)
VALUES(1, NULL, 'homepage', '/', 'Главная', 1, 2);

CREATE TABLE prj_feedback(
    prj_feedback_id SERIAL NOT NULL PRIMARY KEY,
    email VARCHAR(255) NOT NULL DEFAULT '',
    rate INTEGER NOT NULL,
    body TEXT NOT NULL DEFAULT '',
    date_send TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW()
);