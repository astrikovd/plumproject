CREATE LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION page_modify ()
RETURNS trigger AS
$body$
DECLARE
    prnt            record;
    srt             integer;
    ppath           varchar(300) := '';
	  f				        varchar(3);
BEGIN

    IF tg_op <> 'DELETE' THEN
        IF NEW.parent_id IS NOT NULL THEN
            SELECT * FROM page WHERE page_id = NEW.parent_id INTO prnt;
            IF prnt.page_id IS NULL THEN
                RAISE EXCEPTION 'parent is not exists: %', NEW.parent_id;
            END IF;
            ppath := prnt.url_path;
        END IF;

		-- menu_sort
		IF tg_op = 'INSERT' THEN
			SELECT COALESCE(MAX(sort), 0)+1 FROM page WHERE parent_id = NEW.parent_id INTO srt;
			NEW.sort := srt;
		END IF;

        NEW.url_path := ppath || NEW.url_name || '/';
    END IF;

    IF tg_op = 'UPDATE' THEN

        IF NEW.url_path <> OLD.url_path AND NEW.url_path LIKE OLD.url_path || '%' THEN
            RAISE EXCEPTION 'can''t move node: % => %', OLD.url_path, NEW.url_path;
        END IF;

		SELECT get_var('f') INTO f;

		IF (NEW.parent_id <> OLD.parent_id) AND f IS NULL THEN

			PERFORM set_var('f', 'yes');

			SELECT COALESCE(MAX(sort), 0)+1 AS sort FROM page WHERE parent_id = NEW.parent_id INTO srt;

			NEW.sort := srt;

		    UPDATE page SET sort = sort-1 WHERE parent_id = OLD.parent_id AND sort > OLD.sort;
		ELSE

		    SELECT get_var('f') INTO f;

			IF (NEW.sort <> OLD.sort) AND (NEW.parent_id = OLD.parent_id) AND f IS NULL THEN
	            PERFORM set_var('f', 'yes');

				IF NEW.sort < OLD.sort THEN

			        UPDATE page SET sort = sort+1 WHERE parent_id = NEW.parent_id
						AND sort >= NEW.sort AND sort < OLD.sort;

				ELSE

			        UPDATE page SET sort = sort-1 WHERE parent_id = NEW.parent_id
						AND sort <= NEW.sort AND sort > OLD.sort;

				END IF;
			END IF;
		END IF;
    END IF;

    IF tg_op <> 'DELETE' THEN RETURN NEW; ELSE RETURN OLD; END IF;
END;
$body$
LANGUAGE 'plpgsql';

CREATE TRIGGER "page_tr" BEFORE INSERT OR UPDATE OR DELETE
ON page FOR EACH ROW EXECUTE PROCEDURE page_modify();

CREATE OR REPLACE FUNCTION set_var(p_var_name varchar, p_var_value varchar) RETURNS void AS
$$
DECLARE
  v_cnt integer;
BEGIN
  SELECT Count(pc.relname) into v_cnt
  FROM pg_catalog.pg_class pc, pg_namespace pn
  WHERE pc.relname = 'session_var_tbl'
    AND pc.relnamespace = pn.oid
    AND pn.oid = pg_my_temp_schema();
  IF v_cnt = 0 THEN
    EXECUTE 'CREATE GLOBAL TEMPORARY TABLE session_var_tbl (var_name varchar(100) not null, var_value varchar(100)) ON COMMIT preserve ROWS';
  END IF;
  UPDATE session_var_tbl
  SET var_value = p_var_value
  WHERE var_name = p_var_name;
  IF NOT FOUND THEN
    INSERT INTO session_var_tbl(var_name, var_value)
    VALUES (p_var_name, p_var_value);
  END IF;
END;
$$
LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION get_var(p_var_name varchar) RETURNS varchar AS
$$
DECLARE
  v_cnt integer;
  v_result varchar(100);
BEGIN
  SELECT Count(pc.relname)
  INTO v_cnt
  FROM pg_catalog.pg_class pc, pg_namespace pn
  WHERE pc.relname='session_var_tbl'
    AND pc.relnamespace=pn.oid
    AND pn.oid=pg_my_temp_schema();
  IF v_cnt = 0 THEN
    v_result := null;
  ELSE
    SELECT var_value
    INTO v_result
    FROM session_var_tbl
    WHERE var_name = p_var_name;
    IF NOT FOUND THEN
      v_result := null;
    END IF;
  END IF;
  RETURN v_result;
END;
$$
LANGUAGE 'plpgsql';