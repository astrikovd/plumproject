#!/bin/bash
#************************************************#
#            Обновление субмодулей GIT           #
#************************************************#

# Дефолтные настройки
DEFAULT_WORK_DIR=`pwd`

# Устанавливаем название и каталог сайта
echo -e "Укажите относительный путь до репозитария с GIT относительно текущей директории $DEFAULT_WORK_DIR :";
  read WORK_DIR
  if [ -z "$WORK_DIR" ];
  then
    WORK_DIR=$DEFAULT_WORK_DIR
  fi
cd $WORK_DIR;
echo -e "Текущая директория `pwd`";
echo -e "Обновляем все submodules репозитария";
git submodule foreach 'git checkout master && git pull origin master';