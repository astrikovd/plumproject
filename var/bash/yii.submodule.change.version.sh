#!/bin/bash
#********************************************************#
#  Выбор активной для проекта версии yii (субмодуль же)  #
#********************************************************#

# Дефолтные настройки
DEFAULT_WORK_DIR=`pwd`
DEFAULT_MODE='branch'
DEFAULT_VERSION='1.1.12'
# Устанавливаем название и каталог сайта
echo -e "Укажите относительный путь до репозитария подмодуля YII относительно текущей директории $DEFAULT_WORK_DIR :";
  read WORK_DIR
  if [ -z "$WORK_DIR" ];
  then
    WORK_DIR=$DEFAULT_WORK_DIR
  fi
cd $WORK_DIR;
echo -e "Текущая директория `pwd`";

echo -e "Укажите использовать tag или branch для выбора версии YII:";
  read MODE
  if [ -z "$MODE" ];
  then
    MODE=$DEFAULT_MODE
  fi

  if [ $MODE != "tag" ] && [ $MODE != "branch" ];
  then
     echo -e "Укажите корректный режим выбора версии - tag или branch. Введен режим $MODE. Аварийный выход.";
     exit;
  fi
echo -e "Режим $MODE";

echo -e "Укажите версию для tag (пример 1.1.12) режима или название ветки для режима branch (пример master)";
  read VERSION
  if [ -z "$VERSION" ];
  then
    VERSION=$DEFAULT_VERSION
  fi

echo -e "Будут использована версия $MODE - $VERSION";

echo -e "Обновляем YII из репозитария";
git fetch origin
git checkout master && git pull origin master;

echo -e "Меняем текущую версию YII в подмодуле";
git checkout $VERSION;

