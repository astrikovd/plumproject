<?php

$bootstrapPath = realpath(dirname(__FILE__). DIRECTORY_SEPARATOR .'..'. DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'. DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR. 'bootstrap.php');

require_once( $bootstrapPath );

$config = __DIR__ . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'console.php';

$yiic = realpath(YII_PATH . DIRECTORY_SEPARATOR . 'yiic.php');

require_once($yiic);