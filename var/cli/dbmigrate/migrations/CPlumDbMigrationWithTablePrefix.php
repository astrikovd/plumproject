<?php
/**
 * Copyright (c) 2012 by Dmitry Astrikov / www.astrikov.ru
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
/**
 * 
 * @author    Fukalov Semen <yapendalff@gmail,com> http://f-sem.ru
 * @created   15.02.13 5:40
 * @package   cli/dbmigrate
 */

class CPlumDbMigrationWithTablePrefix extends CDbMigration
{
  public function up()
  {
    echo get_class($this)." up.\n";
    try
    {
      $this->upQueries();
      return true;
    }
    catch( CDbException $e )
    {
      return true;
    }
    catch( CException $e )
    {
      return true;
    }
    catch( Exception $e )
    {
      return true;
    }
  }

  public function down()
  {
    echo get_class($this)." down.\n";

    try
    {
      $this->downQueries();
      return true;
    }
    catch( CDbException $e )
    {
      return true;
    }
    catch( CException $e )
    {
      return true;
    }
    catch( Exception $e )
    {
      return true;
    }
  }

  protected function upQueries()
  {

  }

  protected function downQueries()
  {

  }

  protected function dropAllTables()
  {
    $tables = $this->getDbConnection()->getSchema()->getTables();
    foreach( $tables AS $table )
    {
      if( strpos($table->name, $this->getDbConnection()->tablePrefix)!==0 ) continue;
      /** @var $table CDbTableSchema */
      if(!preg_match('/^.*_v$/',$table->name)){

        if(!empty($table->foreignKeys) && $table instanceof CMysqlTableSchema)
        {
          for( $i=1; $i<count($table->foreignKeys)+1; $i++ )
          {
            $this->dropForeignKey($table->name.'_ibfk_'.$i, $table->name);
          }
        }
        if($table->sequenceName)
        {
          $this->execute('DROP SEQUENCE '. $table->sequenceName.' CASCADE;' );
        }
        $this->execute('DROP TABLE '. $table->name.' CASCADE;' );
      }
      else{
        $this->execute('DROP VIEW '.$table->name);
      }
    }
  }
  protected function insertInitData()
  {
    $this->insert('##_adm_cmp_grp',array(
      'adm_cmp_grp_id' => 1,
      'char_id' => 'adm',
      'name' => 'Администрирование'
    ));
    $this->insert('##_adm_cmp_grp',array(
      'adm_cmp_grp_id' => 2,
      'char_id' => 'info',
      'name' => 'Информация'
    ));
    $this->insert('##_adm_cmp_grp',array(
      'adm_cmp_grp_id' => 3,
      'char_id' => 'serv',
      'name' => 'Сервисы'
    ));
    $this->insert('##_adm_cmp_grp',array(
      'adm_cmp_grp_id' => 4,
      'char_id' => 'shop',
      'name' => 'Интернет-магазин'
    ));
    $this->insert('##_adm_cmp_grp',array(
      'adm_cmp_grp_id' => 5,
      'char_id' => 'adv',
      'name' => 'Реклама'
    ));
    $this->insert('##_adm_cmp_grp',array(
      'adm_cmp_grp_id' => 6,
      'char_id' => 'other',
      'name' => 'Разное'
    ));
    $this->insert('##_adm_cmp_grp',array(
      'adm_cmp_grp_id' => 7,
      'char_id' => 'core',
      'name' => 'Ядро'
    ));
    $this->insert('##_adm_cmp_grp',array(
      'adm_cmp_grp_id' => 8,
      'char_id' => 'front',
      'name' => 'Фронтэнд'
    ));
    $this->insert('##_adm_cmp',array(
      'adm_cmp_id' => 1,
      'char_id' => 'user',
      'name' => 'Пользователи Plum',
      'adm_cmp_grp_id' => 1,
      'description' => 'Управление учетными записями и разграничением прав.'
    ));
    $this->insert('##_adm_cmp',array(
      'adm_cmp_id' => 2,
      'char_id' => 'installer',
      'name' => 'Модули системы',
      'adm_cmp_grp_id' => 1,
      'description' => 'Управление модулями системы- загрузка, установка и удаление дополнительных модулей.'
    ));
    $this->insert('##_adm_cmp',array(
      'adm_cmp_id' => 3,
      'char_id' => 'siteinfo',
      'name' => 'Информация о сайте',
      'adm_cmp_grp_id' => 1,
      'description' => 'Изменение информации о сайте, такой как название сайта, e-mail администратора. Управление режимом техобслуживания.'
    ));
    $this->insert('##_adm_cmp',array(
      'adm_cmp_id' => 4,
      'char_id' => 'filemanager',
      'name' => 'Файловый менеджер',
      'adm_cmp_grp_id' => 1,
      'description' => 'Загрузка файлов на сервер для их последующего использования на сайте.'
    ));
    $this->insert('##_adm_cmp',array(
      'adm_cmp_id' => 5,
      'char_id' => 'page',
      'name' => 'Структура сайта',
      'adm_cmp_grp_id' => 2,
      'description' => 'Управление структурой сайта, добавление новых страниц, управление их содержимым.'
    ));
    $this->insert('##_adm_cmp',array(
      'adm_cmp_id' => 6,
      'char_id' => 'newsfeed',
      'name' => 'Новостные ленты',
      'adm_cmp_grp_id' => 2,
      'description' => 'Управление новостными лентами.'
    ));
    $this->insert('##_adm_cmp',array(
      'adm_cmp_id' => 7,
      'char_id' => 'news',
      'name' => 'Новости',
      'adm_cmp_grp_id' => 2,
      'description' => 'Управление новостями, статьями и другой информацией, которая может быть представлена в виде списка элементов'
    ));
    $this->insert('##_adm_cmp',array(
      'adm_cmp_id' => 8,
      'char_id' => 'photogallery',
      'name' => 'Фотогалереи',
      'adm_cmp_grp_id' => 2,
      'description' => 'Управление фотогалереями.'
    ));
    $this->insert('##_adm_cmp',array(
      'adm_cmp_id' => 9,
      'char_id' => 'photo',
      'name' => 'Фотографии',
      'adm_cmp_grp_id' => 2,
      'description' => 'Управление фотографиями.'
    ));
    $this->insert('##_adm_cmp',array(
      'adm_cmp_id' => 10,
      'char_id' => 'feedback',
      'name' => 'Обратная связь',
      'adm_cmp_grp_id' => 3,
      'description' => 'Управление сообщениями, поступающими через форму обратной связи на сайте.'
    ));
    $this->insert('##_adm_cmp',array(
      'adm_cmp_id' => 11,
      'char_id' => 'advplaces',
      'name' => 'Рекламные места',
      'adm_cmp_grp_id' => 5,
      'description' => 'Управление рекламными местами для размещения баннеров.'
    ));
    $this->insert('##_adm_cmp',array(
      'adm_cmp_id' => 12,
      'char_id' => 'advsections',
      'name' => 'Рекламные разделы',
      'adm_cmp_grp_id' =>  5,
      'description' => 'Управление разделами, предназначенными для размещения рекламы.'
    ));
    $this->insert('##_adm_cmp',array(
      'adm_cmp_id' => 13,
      'char_id' => 'advbanners',
      'name' => 'Рекламные баннеры',
      'adm_cmp_grp_id' => 5,
      'description' => 'Управление рекламными баннерами - загрузка изображений, изменение кодов и т.д.'
    ));
    $this->insert('##_adm_cmp',array(
      'adm_cmp_id' => 14,
      'char_id' => 'chpasswd',
      'name' => 'Изменить пароль',
      'adm_cmp_grp_id' =>  6,
      'description' => 'Изменение пароля текущего пользователя.'
    ));
    $this->insert('##_adm_cmp',array(
      'adm_cmp_id' => 15,
      'char_id' => 'auth',
      'name' => 'Авторизация',
      'adm_cmp_grp_id' => 7,
      'description' => 'Авторизация текущего пользователя. Модуль ядра.'
    ));
    $this->insert('##_adm_cmp',array(
      'adm_cmp_id' => 16,
      'char_id' => 'content',
      'name' => 'Контент',
      'adm_cmp_grp_id' => 7,
      'description' => 'Контент. Модуль ядра.'
    ));
    $this->insert('##_adm_cmp',array(
      'adm_cmp_id' => 17,
      'char_id' => 'site',
      'name' => 'Сайт',
      'adm_cmp_grp_id' => 7,
      'description' => 'Сайт. Модуль ядра.'
    ));
    $this->insert('##_adm_cmp',array(
      'adm_cmp_id' => 18,
      'char_id' => 'upload',
      'name' => 'Загрузка файлов',
      'adm_cmp_grp_id' => 7,
      'description' => 'Загрузка файлов. Модуль ядра.'
    ));

    $this->insert('##_adm_user',array(
      'adm_user_id' => 1,
      'username' => 'admin',
      'passwordmd5' => new CDbExpression("MD5('admin')"),
      'email' => 'astrikov.d@gmail.com',
      'last_login' => new CDbExpression("NOW()"),
      'last_ip' => '127.0.0.1',
      'date_create' => new CDbExpression("NOW()")
    ));

    for($i=1;$i<=18;$i++)
    {
      $this->insert('##_adm_user_adm_cmp',array(
        'adm_user_adm_cmp_id' => $i,
        'adm_user_id' => 1,
        'adm_cmp_id'  => $i
      ));
    }

    $this->insert('##_adv_places',array(
      'adv_places_id' => 1,
      'name' => 'Рекламное место вверху страницы',
      'char_id' => 'adv_place_top',
      'date0' => new CDbExpression("NOW()"),
      'vflag' => 1
    ));

    $this->insert('##_adv_places',array(
      'adv_places_id' => 2,
      'name' => 'Рекламное место внизу страницы',
      'char_id' => 'adv_place_bottom',
      'date0' => new CDbExpression("NOW()"),
      'vflag' => 1
    ));


    $this->insert('##_adv_sections',array(
      'adv_sections_id' => 1,
      'name' => 'Не показывать баннеры',
      'vflag' => 1,
      'date0' => new CDbExpression("NOW()"),
    ));
    $this->insert('##_adv_sections',array(
      'adv_sections_id' => 2,
      'name' => 'Главная страница',
      'vflag' => 1,
      'date0' => new CDbExpression("NOW()"),
    ));


    $this->insert('##_newscat',array(
      'newscat_id' => 'articles',
      'vflag' => 1,
      'date0' => new CDbExpression("NOW()"),
      'name' => 'Статьи'
    ));
    $this->insert('##_newscat',array(
      'newscat_id' => 'news',
      'vflag' => 1,
      'date0' => new CDbExpression("NOW()"),
      'name' => 'Новости'
    ));

    $this->insert('##_page_module',array(
      'page_module_id'  => 'feedback',
      'name'  => 'Обратная связь',
      'description'  => '',
      'vflag'  => 1
    ));
    $this->insert('##_page_module',array(
      'page_module_id'  => 'homepage',
      'name'  => 'Главная страница',
      'description'  => '',
      'vflag'  => 1
    ));
    $this->insert('##_page_module',array(
      'page_module_id'  => 'news',
      'name'  => 'Новости',
      'description'  => '',
      'vflag'  => 1
    ));
    $this->insert('##_page_module',array(
      'page_module_id'  => 'photo',
      'name'  => 'Фотографии',
      'description'  => '',
      'vflag'  => 1
    ));
    $this->insert('##_page_module',array(
      'page_module_id'  => 'static',
      'name'  => 'Статическая страница',
      'description'  => '',
      'vflag'  => 1
    ));
    $this->insert('##_page',array(
      'page_id' => 1,
      'parent_id' => null,
      'page_module_id' => 'homepage',
      'page_module_param' => '',
      'url_path' => '/',
      'url_name' => '',
      'redirect_url' => '',
      'name' => 'Главная',
      'header' => '',
      'before_content' => '',
      'after_content' => '',
      'sort' => 1,
      'vflag' => 1,
      'aflag' => 0,
      'hflag' => 1,
      'eflag' => 1,
      'date0' => new CDbExpression("NOW()"),
      'keywords' => '',
      'description' => '',
      'adv_sections_id' => 2
    ));

    $this->insert('##_siteinfo',array(
      'siteinfo_id'  => 1,
      'site_id'  => 1,
      'title'  => 'Plum CMF',
      'admin_email'  => 'admin@plumcmf.com',
      'mtflag'  => 0,
      'mt_text'  => ''
    ));
  }
  protected function replacePrefix( $string, $prefix = '##_')
  {
    return str_replace($prefix, $this->getDbConnection()->tablePrefix, $string);
  }

  public function execute($sql, $params=array())
  {
    $sql = $this->replacePrefix($sql);
    $sql = str_replace('##username##',$this->getDbConnection()->username,$sql);
    return parent::execute( $sql, $params );
  }

  public function insert($table, $columns)
  {
    $table = $this->replacePrefix($table);
    return parent::insert( $table, $columns );
  }

  public function update($table, $columns, $conditions='', $params=array())
  {
    $table = $this->replacePrefix($table);
    return parent::update( $table, $columns, $conditions, $params );
  }

  public function delete($table, $conditions='', $params=array())
  {
    $table = $this->replacePrefix($table);
    return parent::delete( $table, $conditions, $params );
  }

  public function createTable($table, $columns, $options=null)
  {
    $table = $this->replacePrefix($table);
    return parent::createTable( $table, $columns, $options );
  }

  public function renameTable($table, $newName)
  {
    $table = $this->replacePrefix($table);
    $newName = $this->replacePrefix($newName);
    return parent::renameTable( $table, $newName );
  }

  public function dropTable($table)
  {
    $table = $this->replacePrefix($table);
    return parent::dropTable( $table );
  }

  public function truncateTable($table)
  {
    $table = $this->replacePrefix($table);
    return parent::truncateTable( $table );
  }

  public function addColumn($table, $column, $type)
  {
    $table = $this->replacePrefix($table);
    $column = $this->replacePrefix($column);
    return parent::addColumn( $table, $column, $type );
  }

  public function dropColumn($table, $column)
  {
    $table = $this->replacePrefix($table);
    $column = $this->replacePrefix($column);
    return parent::dropColumn( $table, $column );
  }

  public function renameColumn($table, $name, $newName)
  {
    $table = $this->replacePrefix($table);
    $name = $this->replacePrefix($name);
    return parent::renameColumn( $table, $name, $newName );
  }

  public function alterColumn($table, $column, $type)
  {
    $table = $this->replacePrefix($table);
    $column = $this->replacePrefix($column);
    return parent::alterColumn( $table, $column, $type );
  }

  public function addForeignKey($name, $table, $columns, $refTable, $refColumns, $delete=null, $update=null)
  {

    $name = $this->replacePrefix($name);
    $table    = $this->replacePrefix($table);
    $refTable = $this->replacePrefix($refTable);
    return parent::addForeignKey( $name, $table, $columns, $refTable, $refColumns, $delete, $update );
  }

  public function dropForeignKey($name, $table)
  {
    $table = $this->replacePrefix($table);
    $name = $this->replacePrefix($name);
    return parent::dropForeignKey( $name, $table );
  }

  public function createIndex($name, $table, $column, $unique=false)
  {
    $table = $this->replacePrefix($table);
    $name = $this->replacePrefix($name);
    $column = $this->replacePrefix($column);
    return parent::createIndex( $name, $table, $column, $unique );
  }

  public function dropIndex($name, $table)
  {
    $table = $this->replacePrefix($table);
    $name = $this->replacePrefix($name);
    return parent::dropIndex( $table, $table );
  }

  public function refreshTableSchema($table)
  {
    $table = $this->replacePrefix($table);
    return parent::refreshTableSchema( $table );
  }

  public function addPrimaryKey($name,$table,$columns)
  {
    $table = $this->replacePrefix($table);
    $name = $this->replacePrefix($name);
    return parent::addPrimaryKey( $name,$table,$columns );
  }

  public function dropPrimaryKey($name,$table)
  {
    $table = $this->replacePrefix($table);
    $name = $this->replacePrefix($name);
    return parent::dropPrimaryKey( $name,$table );
  }
}