<?php
/**
 * Copyright (c) 2012 by Dmitry Astrikov / www.astrikov.ru
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
require_once( realpath(dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'CPlumDbMigrationWithTablePrefix.php') );

class m000000_000001_init extends CPlumDbMigrationWithTablePrefix
{
	public function up()
	{
    echo "m0000000_0000001_init up.\n";
    try
    {
      $this->upQueries();
      return true;
    }
    catch( CDbException $e )
    {
      return true;
    }
    catch( CException $e )
    {
      return true;
    }
    catch( Exception $e )
    {
      return true;
    }
	}

	public function down()
	{
		echo "m000000_000001_init down.\n";

    try
    {
      $this->downQueries();
      return true;
    }
    catch( CDbException $e )
    {
      return true;
    }
    catch( CException $e )
    {
      return true;
    }
    catch( Exception $e )
    {
      return true;
    }
	}

  protected function upQueries()
  {
    $this->execute('SET statement_timeout = 0;');
    $this->execute('SET client_encoding = \'UTF8\';');
    $this->execute('SET check_function_bodies = false;');
    $this->execute('SET client_min_messages = warning;');
    $this->execute('SET escape_string_warning = off;');
    $this->execute('ALTER PROCEDURAL LANGUAGE plpgsql OWNER TO ##username##;');
    $this->execute('SET search_path = public, pg_catalog;');

    $this->createTable('##_adm_cmp', array(
      'adm_cmp_id' => 'integer NOT NULL',
      'char_id' => 'character varying(32) NOT NULL',
      'name' => 'character varying(32) NOT NULL',
      'adm_cmp_grp_id' => 'integer NOT NULL',
      'description' => 'character varying(255) DEFAULT \'\'::character varying NOT NULL',
    ));
    $this->execute('ALTER TABLE public.##_adm_cmp OWNER TO ##username##');
    $this->execute('CREATE SEQUENCE  ##_adm_cmp_adm_cmp_id_seq START WITH 1 INCREMENT BY 1 NO MAXVALUE NO MINVALUE CACHE 1;');
    $this->execute('ALTER TABLE public.##_adm_cmp_adm_cmp_id_seq OWNER TO ##username##;');
    $this->execute('ALTER SEQUENCE ##_adm_cmp_adm_cmp_id_seq OWNED BY ##_adm_cmp.adm_cmp_id;');
    $this->execute('SELECT pg_catalog.setval(\'##_adm_cmp_adm_cmp_id_seq\', 1, false);');


    $this->createTable('##_adm_cmp_grp', array(
      'adm_cmp_grp_id' => 'integer NOT NULL',
      'char_id' => 'character varying(32) NOT NULL',
      'name' => 'character varying(32) NOT NULL',
    ));
    $this->execute('ALTER TABLE public.##_adm_cmp_grp OWNER TO ##username##');
    $this->execute('CREATE SEQUENCE  ##_adm_cmp_grp_adm_cmp_grp_id_seq START WITH 1 INCREMENT BY 1 NO MAXVALUE NO MINVALUE CACHE 1;');
    $this->execute('ALTER TABLE public.##_adm_cmp_grp_adm_cmp_grp_id_seq OWNER TO ##username##;');
    $this->execute('ALTER SEQUENCE ##_adm_cmp_grp_adm_cmp_grp_id_seq OWNED BY ##_adm_cmp_grp.adm_cmp_grp_id;');
    $this->execute('SELECT pg_catalog.setval(\'##_adm_cmp_grp_adm_cmp_grp_id_seq\', 1, false);');


    $this->createTable('##_adm_user_adm_cmp', array(
      'adm_user_adm_cmp_id' => 'integer NOT NULL',
      'adm_user_id' => 'integer NOT NULL',
      'adm_cmp_id' => 'integer NOT NULL'
    ));
    $this->execute('ALTER TABLE public.##_adm_user_adm_cmp OWNER TO ##username##');
    $this->execute('CREATE SEQUENCE  ##_adm_user_adm_cmp_adm_user_adm_cmp_id_seq START WITH 1 INCREMENT BY 1 NO MAXVALUE NO MINVALUE CACHE 1;');
    $this->execute('ALTER TABLE public.##_adm_user_adm_cmp_adm_user_adm_cmp_id_seq OWNER TO ##username##;');
    $this->execute('ALTER SEQUENCE ##_adm_user_adm_cmp_adm_user_adm_cmp_id_seq OWNED BY ##_adm_user_adm_cmp.adm_user_adm_cmp_id;');
    $this->execute('SELECT pg_catalog.setval(\'##_adm_user_adm_cmp_adm_user_adm_cmp_id_seq\', 1, false);');

    $this->createTable('##_adm_user', array(
      'adm_user_id' => 'integer NOT NULL',
      'username' => 'character varying(32) NOT NULL',
      'passwordmd5' => 'character varying(32) NOT NULL',
      'email' => 'character varying(64) NOT NULL',
      'last_login' => 'timestamp without time zone DEFAULT now() NOT NULL',
      'last_ip' => 'character varying(32) DEFAULT \'0.0.0.0\'::character varying NOT NULL',
      'date_create' => 'timestamp without time zone DEFAULT now() NOT NULL',
    ));
    $this->execute('ALTER TABLE public.##_adm_user OWNER TO ##username##');
    $this->execute('CREATE SEQUENCE  ##_adm_user_adm_user_id_seq START WITH 1 INCREMENT BY 1 NO MAXVALUE NO MINVALUE CACHE 1;');
    $this->execute('ALTER TABLE public.##_adm_user_adm_user_id_seq OWNER TO ##username##;');
    $this->execute('ALTER SEQUENCE ##_adm_user_adm_user_id_seq OWNED BY ##_adm_user.adm_user_id;');
    $this->execute('SELECT pg_catalog.setval(\'##_adm_user_adm_user_id_seq\', 1, false);');

    $this->createTable('##_adv_banners', array(
      'adv_banners_id' => 'integer NOT NULL',
      'adv_places_id' => 'integer',
      'adv_sections_id' => 'integer',
      'name' => 'character varying(255) DEFAULT \'\'::character varying NOT NULL',
      'picture' => 'character varying(255) DEFAULT \'\'::character varying NOT NULL',
      'code' => 'text DEFAULT \'\'::text NOT NULL',
      'date_start' => 'timestamp without time zone DEFAULT now() NOT NULL',
      'date_end' => 'timestamp without time zone DEFAULT now() NOT NULL',
      'date0' => 'timestamp without time zone DEFAULT now() NOT NULL',
      'title' => 'character varying(255) DEFAULT \'\'::character varying NOT NULL',
      'link' => 'character varying(255) DEFAULT \'\'::character varying NOT NULL',
      'vflag' => 'boolean DEFAULT true NOT NULL',
    ));
    $this->execute('ALTER TABLE public.##_adv_banners OWNER TO ##username##');
    $this->execute('CREATE SEQUENCE  ##_adv_banners_adv_banners_id_seq START WITH 1 INCREMENT BY 1 NO MAXVALUE NO MINVALUE CACHE 1;');
    $this->execute('ALTER TABLE public.##_adv_banners_adv_banners_id_seq OWNER TO ##username##;');
    $this->execute('ALTER SEQUENCE ##_adv_banners_adv_banners_id_seq OWNED BY ##_adv_banners.adv_banners_id;');
    $this->execute('SELECT pg_catalog.setval(\'##_adv_banners_adv_banners_id_seq\', 1, false);');

    $this->createTable('##_adv_places', array(
      'adv_places_id' => 'integer NOT NULL',
      'name' => 'character varying(255) DEFAULT \'\'::character varying NOT NULL',
      'char_id' => 'character varying(64) NOT NULL',
      'date0' => 'timestamp without time zone DEFAULT now() NOT NULL',
      'vflag' => 'boolean DEFAULT true NOT NULL',
    ));
    $this->execute('ALTER TABLE public.##_adv_places OWNER TO ##username##');
    $this->execute('CREATE SEQUENCE  ##_adv_places_adv_places_id_seq START WITH 1 INCREMENT BY 1 NO MAXVALUE NO MINVALUE CACHE 1;');
    $this->execute('ALTER TABLE public.##_adv_places_adv_places_id_seq OWNER TO ##username##;');
    $this->execute('ALTER SEQUENCE ##_adv_places_adv_places_id_seq OWNED BY ##_adv_places.adv_places_id;');
    $this->execute('SELECT pg_catalog.setval(\'##_adv_places_adv_places_id_seq\', 2, true);');

    $this->createTable('##_adv_sections', array(
      'adv_sections_id' => 'integer NOT NULL',
      'name' => 'character varying(255) DEFAULT \'\'::character varying NOT NULL',
      'vflag' => 'boolean DEFAULT true NOT NULL',
      'date0' => 'timestamp without time zone DEFAULT now() NOT NULL',
    ));
    $this->execute('ALTER TABLE public.##_adv_sections OWNER TO ##username##');
    $this->execute('CREATE SEQUENCE  ##_adv_sections_adv_sections_id_seq START WITH 1 INCREMENT BY 1 NO MAXVALUE NO MINVALUE CACHE 1;');
    $this->execute('ALTER TABLE public.##_adv_sections_adv_sections_id_seq OWNER TO ##username##;');
    $this->execute('ALTER SEQUENCE ##_adv_sections_adv_sections_id_seq OWNED BY ##_adv_sections.adv_sections_id;');
    $this->execute('SELECT pg_catalog.setval(\'##_adv_sections_adv_sections_id_seq\', 2, true);');


    $this->createTable('##_feedback', array(
      'feedback_id' => 'integer NOT NULL',
      'fio' => 'character varying(255) DEFAULT \'\'::character varying NOT NULL',
      'phone' => 'character varying(255) DEFAULT \'\'::character varying NOT NULL',
      'question' => 'text DEFAULT \'\'::text NOT NULL',
      'answer' => 'text DEFAULT \'\'::text NOT NULL',
      'date_send' => 'timestamp without time zone DEFAULT now() NOT NULL',
      'date_ans' => 'timestamp without time zone DEFAULT now()',
      'vflag' => 'boolean DEFAULT false NOT NULL',
    ));
    $this->execute('ALTER TABLE public.##_feedback OWNER TO ##username##');
    $this->execute('CREATE SEQUENCE  ##_feedback_feedback_id_seq START WITH 1 INCREMENT BY 1 NO MAXVALUE NO MINVALUE CACHE 1;');
    $this->execute('ALTER TABLE public.##_feedback_feedback_id_seq OWNER TO ##username##;');
    $this->execute('ALTER SEQUENCE ##_feedback_feedback_id_seq OWNED BY ##_feedback.feedback_id;');
    $this->execute('SELECT pg_catalog.setval(\'##_feedback_feedback_id_seq\', 1, false);');

    $this->createTable('##_newscat', array(
      'newscat_id' => 'character varying(32) NOT NULL',
      'vflag' => 'boolean DEFAULT true NOT NULL',
      'date0' => 'timestamp without time zone DEFAULT now() NOT NULL',
      'name' => 'character varying(64) DEFAULT \'\'::character varying NOT NULL',
    ));
    $this->execute('ALTER TABLE public.##_newscat OWNER TO ##username##');

    $this->createTable('##_newsfeed', array(
      'newsfeed_id' => 'integer NOT NULL',
      'newscat_id' => 'character varying(32)',
      'name' => 'character varying(128) DEFAULT \'\'::character varying NOT NULL',
      'cnt' => 'integer DEFAULT 10 NOT NULL',
      'vflag' => 'boolean DEFAULT true NOT NULL',
      'date0' => 'timestamp without time zone DEFAULT now() NOT NULL',
    ));
    $this->execute('ALTER TABLE public.##_newsfeed OWNER TO ##username##');
    $this->execute('CREATE SEQUENCE  ##_newsfeed_newsfeed_id_seq START WITH 1 INCREMENT BY 1 NO MAXVALUE NO MINVALUE CACHE 1;');
    $this->execute('ALTER TABLE public.##_newsfeed_newsfeed_id_seq OWNER TO ##username##;');
    $this->execute('ALTER SEQUENCE ##_newsfeed_newsfeed_id_seq OWNED BY ##_newsfeed.newsfeed_id;');
    $this->execute('SELECT pg_catalog.setval(\'##_newsfeed_newsfeed_id_seq\', 1, false);');

    $this->createTable('##_newsitem', array(
      'newsitem_id' => 'integer NOT NULL',
      'newsfeed_id' => 'integer',
      'name' => 'character varying(255) DEFAULT \'\'::character varying NOT NULL',
      'anons' => 'text DEFAULT \'\'::text NOT NULL',
      'body' => 'text DEFAULT \'\'::text NOT NULL',
      'picture' => 'character varying(255) DEFAULT \'\'::character varying NOT NULL',
      'vflag' => 'boolean DEFAULT true NOT NULL',
      'date0' => 'timestamp without time zone DEFAULT now() NOT NULL',
      'keywords' => 'character varying(255) DEFAULT \'\'::character varying NOT NULL',
      'description' => 'character varying(1023) DEFAULT \'\'::character varying NOT NULL'
    ));
    $this->execute('ALTER TABLE public.##_newsitem OWNER TO ##username##');
    $this->execute('CREATE SEQUENCE  ##_newsitem_newsitem_id_seq START WITH 1 INCREMENT BY 1 NO MAXVALUE NO MINVALUE CACHE 1;');
    $this->execute('ALTER TABLE public.##_newsitem_newsitem_id_seq OWNER TO ##username##;');
    $this->execute('ALTER SEQUENCE ##_newsitem_newsitem_id_seq OWNED BY ##_newsitem.newsitem_id;');
    $this->execute('SELECT pg_catalog.setval(\'##_newsitem_newsitem_id_seq\', 1, false);');


    $this->createTable('##_page', array(
      'page_id' => 'integer NOT NULL',
      'parent_id' => 'integer',
      'page_module_id' => 'character varying(16) NOT NULL',
      'page_module_param' => 'character varying(32) DEFAULT \'\'::character varying NOT NULL',
      'url_path' => 'character varying(512) DEFAULT \'\'::character varying NOT NULL',
      'url_name' => 'character varying(512) DEFAULT \'\'::character varying NOT NULL',
      'redirect_url' => 'character varying(512) DEFAULT \'\'::character varying NOT NULL',
      'name' => 'character varying(128) DEFAULT \'\'::character varying NOT NULL',
      'header' => 'character varying(128) DEFAULT \'\'::character varying NOT NULL',
      'before_content' => 'text DEFAULT \'\'::text NOT NULL',
      'after_content' => 'text DEFAULT \'\'::text NOT NULL',
      'sort' => 'integer NOT NULL',
      'vflag' => 'boolean DEFAULT true NOT NULL',
      'aflag' => 'boolean DEFAULT false NOT NULL',
      'hflag' => 'boolean DEFAULT true NOT NULL',
      'eflag' => 'boolean DEFAULT true NOT NULL',
      'date0' => 'timestamp without time zone DEFAULT now() NOT NULL',
      'keywords' => 'character varying(255) DEFAULT \'\'::character varying NOT NULL',
      'description' => 'character varying(1023) DEFAULT \'\'::character varying NOT NULL',
      'adv_sections_id' => 'integer',
    ));
    $this->execute('ALTER TABLE public.##_page OWNER TO ##username##');
    $this->execute('CREATE SEQUENCE  ##_page_page_id_seq START WITH 1 INCREMENT BY 1 NO MAXVALUE NO MINVALUE CACHE 1;');
    $this->execute('ALTER TABLE public.##_page_page_id_seq OWNER TO ##username##;');
    $this->execute('ALTER SEQUENCE ##_page_page_id_seq OWNED BY ##_page.page_id;');
    $this->execute('SELECT pg_catalog.setval(\'##_page_page_id_seq\', 1, false);');

    $this->createTable('##_page_module', array(
      'page_module_id' => 'character varying(16) NOT NULL',
      'name' => 'character varying(32) NOT NULL',
      'description' => 'text DEFAULT \'\'::text NOT NULL',
      'vflag' => 'boolean DEFAULT true NOT NULL'
    ));
    $this->execute('ALTER TABLE public.##_page_module OWNER TO ##username##');

    $this->createTable('##_photo', array(
      'photo_id' => 'integer NOT NULL',
      'photogallery_id' => 'integer NOT NULL',
      'name' => 'character varying(255) NOT NULL',
      'author' => 'character varying(255) DEFAULT \'\'::character varying NOT NULL',
      'description' => 'text DEFAULT \'\'::text NOT NULL',
      'picture' => 'character varying(255) DEFAULT \'\'::character varying NOT NULL',
      'date_create' => 'timestamp without time zone DEFAULT now() NOT NULL',
      'vflag' => 'boolean DEFAULT true NOT NULL',
    ));
    $this->execute('ALTER TABLE public.##_photo OWNER TO ##username##');
    $this->execute('CREATE SEQUENCE  ##_photo_photo_id_seq START WITH 1 INCREMENT BY 1 NO MAXVALUE NO MINVALUE CACHE 1;');
    $this->execute('ALTER TABLE public.##_photo_photo_id_seq OWNER TO ##username##;');
    $this->execute('ALTER SEQUENCE ##_photo_photo_id_seq OWNED BY ##_photo.photo_id;');
    $this->execute('SELECT pg_catalog.setval(\'##_photo_photo_id_seq\', 1, false);');

    $this->createTable('##_photogallery', array(
      'photogallery_id' => 'integer NOT NULL',
      'name' => 'character varying(255) NOT NULL',
      'cover' => 'character varying(255) DEFAULT \'\'::character varying NOT NULL',
      'cnt' => 'integer DEFAULT 9 NOT NULL',
      'vflag' => 'boolean DEFAULT true NOT NULL',
    ));
    $this->execute('ALTER TABLE public.##_photogallery OWNER TO ##username##');
    $this->execute('CREATE SEQUENCE  ##_photogallery_photogallery_id_seq START WITH 1 INCREMENT BY 1 NO MAXVALUE NO MINVALUE CACHE 1;');
    $this->execute('ALTER TABLE public.##_photogallery_photogallery_id_seq OWNER TO ##username##;');
    $this->execute('ALTER SEQUENCE ##_photogallery_photogallery_id_seq OWNED BY ##_photogallery.photogallery_id;');
    $this->execute('SELECT pg_catalog.setval(\'##_photogallery_photogallery_id_seq\', 1, false);');

    $this->createTable('##_siteinfo', array(
      'siteinfo_id' => 'integer NOT NULL',
      'site_id' => 'integer DEFAULT 1 NOT NULL',
      'title' => 'character varying(255) DEFAULT \'\'::character varying NOT NULL',
      'admin_email' => 'character varying(255) DEFAULT \'\'::character varying NOT NULL',
      'mtflag' => 'boolean DEFAULT false NOT NULL',
      'mt_text' => 'text DEFAULT \'\'::text NOT NULL',
    ));
    $this->execute('ALTER TABLE public.##_siteinfo OWNER TO ##username##');
    $this->execute('CREATE SEQUENCE  ##_siteinfo_siteinfo_id_seq START WITH 1 INCREMENT BY 1 NO MAXVALUE NO MINVALUE CACHE 1;');
    $this->execute('ALTER TABLE public.##_siteinfo_siteinfo_id_seq OWNER TO ##username##;');
    $this->execute('ALTER SEQUENCE ##_siteinfo_siteinfo_id_seq OWNED BY ##_siteinfo.siteinfo_id;');
    $this->execute('SELECT pg_catalog.setval(\'##_siteinfo_siteinfo_id_seq\', 1, false);');

    $this->createTable('##_prj_feedback', array(
      'prj_feedback_id' => 'integer NOT NULL',
      'email' => 'character varying(255) DEFAULT \'\'::character varying NOT NULL',
      'rate' => 'integer NOT NULL',
      'body' => 'text DEFAULT \'\'::text NOT NULL',
      'date_send' => 'timestamp without time zone DEFAULT now() NOT NULL',
    ));
    $this->execute('ALTER TABLE public.##_prj_feedback OWNER TO ##username##');
    $this->execute('CREATE SEQUENCE  ##_prj_feedback_prj_feedback_id_seq START WITH 1 INCREMENT BY 1 NO MAXVALUE NO MINVALUE CACHE 1;');
    $this->execute('ALTER TABLE public.##_prj_feedback_prj_feedback_id_seq OWNER TO ##username##;');
    $this->execute('ALTER SEQUENCE ##_prj_feedback_prj_feedback_id_seq OWNED BY ##_prj_feedback.prj_feedback_id;');
    $this->execute('SELECT pg_catalog.setval(\'##_prj_feedback_prj_feedback_id_seq\', 1, false);');

    $this->execute("ALTER TABLE ##_adm_cmp ALTER COLUMN adm_cmp_id SET DEFAULT nextval('##_adm_cmp_adm_cmp_id_seq'::regclass);");
    $this->execute("ALTER TABLE ##_adm_cmp_grp ALTER COLUMN adm_cmp_grp_id SET DEFAULT nextval('##_adm_cmp_grp_adm_cmp_grp_id_seq'::regclass);");
    $this->execute("ALTER TABLE ##_adm_user ALTER COLUMN adm_user_id SET DEFAULT nextval('##_adm_user_adm_user_id_seq'::regclass);");
    $this->execute("ALTER TABLE ##_adm_user_adm_cmp ALTER COLUMN adm_user_adm_cmp_id SET DEFAULT nextval('##_adm_user_adm_cmp_adm_user_adm_cmp_id_seq'::regclass);");
    $this->execute("ALTER TABLE ##_adv_banners ALTER COLUMN adv_banners_id SET DEFAULT nextval('##_adv_banners_adv_banners_id_seq'::regclass);");
    $this->execute("ALTER TABLE ##_adv_places ALTER COLUMN adv_places_id SET DEFAULT nextval('##_adv_places_adv_places_id_seq'::regclass);");
    $this->execute("ALTER TABLE ##_adv_sections ALTER COLUMN adv_sections_id SET DEFAULT nextval('##_adv_sections_adv_sections_id_seq'::regclass);");
    $this->execute("ALTER TABLE ##_feedback ALTER COLUMN feedback_id SET DEFAULT nextval('##_feedback_feedback_id_seq'::regclass);");
    $this->execute("ALTER TABLE ##_newsfeed ALTER COLUMN newsfeed_id SET DEFAULT nextval('##_newsfeed_newsfeed_id_seq'::regclass);");
    $this->execute("ALTER TABLE ##_newsitem ALTER COLUMN newsitem_id SET DEFAULT nextval('##_newsitem_newsitem_id_seq'::regclass);");
    $this->execute("ALTER TABLE ##_page ALTER COLUMN page_id SET DEFAULT nextval('##_page_page_id_seq'::regclass);");
    $this->execute("ALTER TABLE ##_photo ALTER COLUMN photo_id SET DEFAULT nextval('##_photo_photo_id_seq'::regclass);");
    $this->execute("ALTER TABLE ##_photogallery ALTER COLUMN photogallery_id SET DEFAULT nextval('##_photogallery_photogallery_id_seq'::regclass);");
    $this->execute("ALTER TABLE ##_prj_feedback ALTER COLUMN prj_feedback_id SET DEFAULT nextval('##_prj_feedback_prj_feedback_id_seq'::regclass);");
    $this->execute("ALTER TABLE ##_siteinfo ALTER COLUMN siteinfo_id SET DEFAULT nextval('##_siteinfo_siteinfo_id_seq'::regclass);");
/*
    $this->execute("CREATE VIEW  ##_adm_cmp_v AS SELECT ac.adm_cmp_id, ac.char_id AS cmp_char_id, ac.name AS cmp_name, acg.adm_cmp_grp_id, acg.char_id AS grp_char_id, acg.name AS grp_name, auac.adm_user_id FROM ##_adm_cmp ac, ##_adm_cmp_grp acg, ##_adm_user_adm_cmp auac WHERE ((ac.adm_cmp_grp_id = acg.adm_cmp_grp_id) AND (ac.adm_cmp_id = auac.adm_cmp_id)) ORDER BY ac.adm_cmp_grp_id;");
    $this->execute("ALTER TABLE public.##_adm_cmp_v OWNER TO plumcmf;");

    $this->execute("CREATE VIEW  ##_adv_banners_v AS SELECT ab.adv_banners_id, ab.adv_places_id, ab.adv_sections_id, ab.name, ab.picture, ab.code, ab.date_start, ab.date_end, ab.date0, ab.title, ab.link, ab.vflag, ap.char_id, ab.vflag AS ab_vflag, ast.vflag AS as_vflag, ap.vflag AS ap_vflag FROM ##_adv_banners ab, ##_adv_sections ast, ##_adv_places ap WHERE ((ab.adv_places_id = ap.adv_places_id) AND (ab.adv_sections_id = ast.adv_sections_id)) ORDER BY ab.adv_banners_id;");
    $this->execute("ALTER TABLE public.##_adv_banners_v OWNER TO plumcmf;");
*/
    $this->execute("ALTER TABLE ONLY ##_adm_cmp_grp ADD CONSTRAINT ##_adm_cmp_grp_pkey PRIMARY KEY (adm_cmp_grp_id);");
    $this->execute("ALTER TABLE ONLY ##_adm_cmp ADD CONSTRAINT ##_adm_cmp_pkey PRIMARY KEY (adm_cmp_id);");
    $this->execute("ALTER TABLE ONLY ##_adm_user_adm_cmp ADD CONSTRAINT ##_adm_user_adm_cmp_pkey PRIMARY KEY (adm_user_adm_cmp_id);");
    $this->execute("ALTER TABLE ONLY ##_adm_user ADD CONSTRAINT ##_adm_user_pkey PRIMARY KEY (adm_user_id);");
    $this->execute("ALTER TABLE ONLY ##_adv_banners ADD CONSTRAINT ##_adv_banners_pkey PRIMARY KEY (adv_banners_id);");
    $this->execute("ALTER TABLE ONLY ##_adv_places ADD CONSTRAINT ##_adv_places_pkey PRIMARY KEY (adv_places_id);");
    $this->execute("ALTER TABLE ONLY ##_adv_sections ADD CONSTRAINT ##_adv_sections_pkey PRIMARY KEY (adv_sections_id);");
    $this->execute("ALTER TABLE ONLY ##_feedback ADD CONSTRAINT ##_feedback_pkey PRIMARY KEY (feedback_id);");
    $this->execute("ALTER TABLE ONLY ##_newscat ADD CONSTRAINT ##_newscat_pkey PRIMARY KEY (newscat_id);");
    $this->execute("ALTER TABLE ONLY ##_newsfeed ADD CONSTRAINT ##_newsfeed_pkey PRIMARY KEY (newsfeed_id);");
    $this->execute("ALTER TABLE ONLY ##_newsitem ADD CONSTRAINT ##_newsitem_pkey PRIMARY KEY (newsitem_id);");
    $this->execute("ALTER TABLE ONLY ##_page_module ADD CONSTRAINT ##_page_module_pkey PRIMARY KEY (page_module_id);");
    $this->execute("ALTER TABLE ONLY ##_page  ADD CONSTRAINT ##_page_pkey PRIMARY KEY (page_id);");
    $this->execute("ALTER TABLE ONLY ##_photo ADD CONSTRAINT ##_photo_pkey PRIMARY KEY (photo_id);");
    $this->execute("ALTER TABLE ONLY ##_photogallery ADD CONSTRAINT ##_photogallery_pkey PRIMARY KEY (photogallery_id);");
    $this->execute("ALTER TABLE ONLY ##_prj_feedback ADD CONSTRAINT ##_prj_feedback_pkey PRIMARY KEY (prj_feedback_id);");
    $this->execute("ALTER TABLE ONLY ##_siteinfo ADD CONSTRAINT ##_siteinfo_pkey PRIMARY KEY (siteinfo_id);");

    $this->execute("ALTER TABLE ONLY ##_adm_cmp ADD CONSTRAINT ##_adm_cmp_ibfk_1 FOREIGN KEY (adm_cmp_grp_id) REFERENCES ##_adm_cmp_grp(adm_cmp_grp_id);");
    $this->execute("ALTER TABLE ONLY ##_adm_user_adm_cmp ADD CONSTRAINT ##_adm_user_adm_cmp_ibfk_1 FOREIGN KEY (adm_cmp_id) REFERENCES ##_adm_cmp(adm_cmp_id);");
    $this->execute("ALTER TABLE ONLY ##_adm_user_adm_cmp ADD CONSTRAINT ##_adm_user_adm_cmp_ibfk_2 FOREIGN KEY (adm_user_id) REFERENCES ##_adm_user(adm_user_id);");
    $this->execute("ALTER TABLE ONLY ##_adv_banners ADD CONSTRAINT ##_adv_banners_ibfk_1 FOREIGN KEY (adv_places_id) REFERENCES ##_adv_places(adv_places_id);");
    $this->execute("ALTER TABLE ONLY ##_adv_banners  ADD CONSTRAINT ##_adv_banners_ibfk_2 FOREIGN KEY (adv_sections_id) REFERENCES ##_adv_sections(adv_sections_id);");
    $this->execute("ALTER TABLE ONLY ##_newsfeed ADD CONSTRAINT ##_newsfeed_ibfk_1 FOREIGN KEY (newscat_id) REFERENCES ##_newscat(newscat_id);");
    $this->execute("ALTER TABLE ONLY ##_newsitem ADD CONSTRAINT ##_newsitem_ibfk_1 FOREIGN KEY (newsfeed_id) REFERENCES ##_newsfeed(newsfeed_id);");
    $this->execute("ALTER TABLE ONLY ##_page ADD CONSTRAINT ##_page_ibfk_1 FOREIGN KEY (adv_sections_id) REFERENCES ##_adv_sections(adv_sections_id);");
    $this->execute("ALTER TABLE ONLY ##_page ADD CONSTRAINT ##_page_ibfk_2 FOREIGN KEY (page_module_id) REFERENCES ##_page_module(page_module_id);");
    $this->execute("ALTER TABLE ONLY ##_page ADD CONSTRAINT ##_page_ibfk_3 FOREIGN KEY (parent_id) REFERENCES ##_page(page_id);");
    $this->execute("ALTER TABLE ONLY ##_photo ADD CONSTRAINT ##_photo_ibfk_1 FOREIGN KEY (photogallery_id) REFERENCES ##_photogallery(photogallery_id);");

    $this->insertInitData();

    $this->execute("REVOKE ALL ON SCHEMA public FROM PUBLIC;");
    $this->execute("REVOKE ALL ON SCHEMA public FROM postgres;");
    $this->execute("GRANT ALL ON SCHEMA public TO postgres;");
    $this->execute("GRANT ALL ON SCHEMA public TO PUBLIC;");
  }

  protected function downQueries()
  {
    $this->dropAllTables();
  }

}