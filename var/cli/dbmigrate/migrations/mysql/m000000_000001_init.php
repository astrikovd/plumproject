<?php
/**
 * Copyright (c) 2012 by Dmitry Astrikov / www.astrikov.ru
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
require_once( realpath(dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'CPlumDbMigrationWithTablePrefix.php') );

class m000000_000001_init extends CPlumDbMigrationWithTablePrefix
{
	public function up()
	{
    echo "m000000_000001_init up.\n";
    try
    {
      $this->upQueries();
      return true;
    }
    catch( CDbException $e )
    {
      return true;
    }
    catch( CException $e )
    {
      return true;
    }
    catch( Exception $e )
    {
      return true;
    }
	}

	public function down()
	{
		echo "m000000_000001_init down.\n";

    try
    {
      $this->downQueries();
      return true;
    }
    catch( CDbException $e )
    {
      return true;
    }
    catch( CException $e )
    {
      return true;
    }
    catch( Exception $e )
    {
      return true;
    }
	}

  protected function upQueries()
  {
    $this->execute('SET foreign_key_checks = 0;');

    $this->createTable('##_adm_cmp', array(
      'adm_cmp_id' => 'INT(11) unsigned NOT NULL AUTO_INCREMENT',
      'char_id' => 'VARCHAR(32) NOT NULL',
      'name' => 'VARCHAR(32) NOT NULL',
      'adm_cmp_grp_id' => 'INT(11) unsigned NOT NULL',
      'description' => 'VARCHAR(255) NOT NULL DEFAULT \'\'',
      'PRIMARY KEY (`adm_cmp_id`)',
      'KEY(`adm_cmp_grp_id`)'
    ), 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1');

    $this->createTable('##_adm_cmp_grp', array(
      'adm_cmp_grp_id' => 'INT(11) unsigned NOT NULL AUTO_INCREMENT',
      'char_id' => 'VARCHAR(32) NOT NULL',
      'name' => 'VARCHAR(32) NOT NULL',
      'PRIMARY KEY (`adm_cmp_grp_id`)',
    ), 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1');

    $this->createTable('##_adm_user_adm_cmp', array(
      'adm_user_adm_cmp_id' => 'INT(11) unsigned NOT NULL AUTO_INCREMENT',
      'adm_user_id' => 'INT(11) unsigned NOT NULL',
      'adm_cmp_id' => 'INT(11) unsigned NOT NULL',
      'PRIMARY KEY (`adm_user_adm_cmp_id`)',
      'KEY (`adm_user_id`)',
      'KEY (`adm_cmp_id`)',
    ), 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1');

    $this->createTable('##_adm_user', array(
      'adm_user_id' => 'INT(11) unsigned NOT NULL AUTO_INCREMENT',
      'username' => 'VARCHAR(32) NOT NULL',
      'passwordmd5' => 'VARCHAR(32) NOT NULL',
      'email' => 'VARCHAR(64) NOT NULL',
      'last_login' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
      'last_ip' => 'VARCHAR(32) NOT NULL DEFAULT \'0.0.0.0\'',
      'date_create' => 'TIMESTAMP NOT NULL DEFAULT \'0000-00-00 00:00:00\'',
      'PRIMARY KEY (`adm_user_id`)'
    ), 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1');

    $this->createTable('##_adv_banners', array(
      'adv_banners_id' => 'INT(11) unsigned NOT NULL AUTO_INCREMENT',
      'adv_places_id' => 'INT(11) unsigned NOT NULL',
      'adv_sections_id' => 'INT(11) unsigned NOT NULL',
      'name' => 'VARCHAR(255) NOT NULL DEFAULT \'\'',
      'picture' => 'VARCHAR(255) NOT NULL DEFAULT \'\'',
      'code' => 'text NOT NULL',
      'date_start' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
      'date_end' => 'TIMESTAMP NOT NULL DEFAULT \'0000-00-00 00:00:00\'',
      'date0' => 'TIMESTAMP NOT NULL DEFAULT \'0000-00-00 00:00:00\'',
      'title' => 'VARCHAR(255) NOT NULL DEFAULT \'\'',
      'link' => 'VARCHAR(255) NOT NULL DEFAULT \'\'',
      'vflag' => 'tinyint(1) NOT NULL DEFAULT \'1\'',
      'PRIMARY KEY (`adv_banners_id`)',
    ), 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1');

    $this->createTable('##_adv_places', array(
      'adv_places_id' => 'INT(11) unsigned NOT NULL AUTO_INCREMENT',
      'name' => 'VARCHAR(255) NOT NULL DEFAULT \'\'',
      'char_id' => 'VARCHAR(64) NOT NULL',
      'date0' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
      'vflag' => 'tinyint(1) NOT NULL DEFAULT \'1\'',
      'PRIMARY KEY (`adv_places_id`)',
    ), 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1');

    $this->createTable('##_adv_sections', array(
      'adv_sections_id' => 'INT(11) unsigned NOT NULL AUTO_INCREMENT',
      'name' => 'VARCHAR(255) NOT NULL DEFAULT \'\'',
      'vflag' => 'tinyint(1) NOT NULL DEFAULT \'1\'',
      'date0' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
      'PRIMARY KEY (`adv_sections_id`)',
    ), 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1');

    $this->createTable('##_feedback', array(
      'feedback_id' => 'INT(11) unsigned NOT NULL AUTO_INCREMENT',
      'fio' => 'VARCHAR(255) NOT NULL DEFAULT \'\'',
      'phone' => 'VARCHAR(255) NOT NULL DEFAULT \'\'',
      'question' => 'text NOT NULL',
      'answer' => 'text NOT NULL',
      'date_send' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
      'date_ans' => 'TIMESTAMP NOT NULL DEFAULT \'0000-00-00 00:00:00\'',
      'vflag' => 'tinyint(1) NOT NULL DEFAULT \'0\'',
      'PRIMARY KEY (`feedback_id`)',
    ), 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1');

    $this->createTable('##_newscat', array(
      'newscat_id' => 'VARCHAR(32) NOT NULL',
      'vflag' => 'tinyint(1) NOT NULL DEFAULT \'1\'',
      'date0' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
      'name' => 'VARCHAR(32) NOT NULL DEFAULT \'\'',
      'PRIMARY KEY (`newscat_id`)',
    ), 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1');

    $this->createTable('##_newsfeed', array(
      'newsfeed_id' => 'INT(11) unsigned NOT NULL AUTO_INCREMENT',
      'newscat_id' => 'VARCHAR(32) DEFAULT NULL',
      'name' => 'VARCHAR(128) NOT NULL DEFAULT \'\'',
      'cnt' => 'INT(11) NOT NULL DEFAULT \'10\'',
      'vflag' => 'tinyint(1) NOT NULL DEFAULT \'1\'',
      'date0' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
      'PRIMARY KEY (`newsfeed_id`)',
      'KEY (`newscat_id`)',
    ), 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1');

    $this->createTable('##_newsitem', array(
      'newsitem_id' => 'INT(11) unsigned NOT NULL AUTO_INCREMENT',
      'newsfeed_id' => 'INT(11) unsigned NOT NULL',
      'name' => 'VARCHAR(255) NOT NULL DEFAULT \'\'',
      'anons' => 'text NOT NULL',
      'body' => 'text NOT NULL',
      'picture' => 'VARCHAR(255) NOT NULL DEFAULT \'\'',
      'vflag' => 'tinyint(1) NOT NULL DEFAULT \'1\'',
      'date0' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
      'keywords' => 'VARCHAR(255) NOT NULL DEFAULT \'\'',
      'description' => 'VARCHAR(1023) NOT NULL DEFAULT \'\'',
      'PRIMARY KEY (`newsitem_id`)',
      'KEY (`newsfeed_id`)',
    ), 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1');

    $this->createTable('##_page', array(
      'page_id' => 'INT(11) unsigned NOT NULL AUTO_INCREMENT',
      'parent_id' => 'INT(11) unsigned DEFAULT NULL',
      'page_module_id' => 'VARCHAR(16) NOT NULL',
      'page_module_param' => 'VARCHAR(32) NOT NULL DEFAULT \'\'',
      'url_path' => 'VARCHAR(512) NOT NULL DEFAULT \'\'',
      'url_name' => 'VARCHAR(512) NOT NULL DEFAULT \'\'',
      'redirect_url' => 'VARCHAR(512) NOT NULL DEFAULT \'\'',
      'name' => 'VARCHAR(128) NOT NULL DEFAULT \'\'',
      'header' => 'VARCHAR(128) NOT NULL DEFAULT \'\'',
      'before_content' => 'text NOT NULL',
      'after_content' => 'text NOT NULL',
      'sort' => 'INT(11) NOT NULL',
      'vflag' => 'tinyint(1) NOT NULL DEFAULT \'1\'',
      'aflag' => 'tinyint(1) NOT NULL DEFAULT \'0\'',
      'hflag' => 'tinyint(1) NOT NULL DEFAULT \'1\'',
      'eflag' => 'tinyint(1) NOT NULL DEFAULT \'1\'',
      'date0' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
      'keywords' => 'VARCHAR(255) NOT NULL DEFAULT \'\'',
      'description' => 'VARCHAR(1023) NOT NULL DEFAULT \'\'',
      'adv_sections_id' => 'INT(11) unsigned DEFAULT NULL',
      'PRIMARY KEY (`page_id`)',
      'KEY (`parent_id`)',
      'KEY (`adv_sections_id`)'
    ), 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1');

    $this->createTable('##_page_module', array(
      'page_module_id' => 'VARCHAR(16) NOT NULL',
      'name' => 'VARCHAR(32) NOT NULL',
      'description' => 'text NOT NULL',
      'vflag' => 'tinyint(1) NOT NULL DEFAULT \'1\'',
      'PRIMARY KEY (`page_module_id`)'
    ), 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1');

    $this->createTable('##_photo', array(
      'photo_id' => 'INT(11) unsigned NOT NULL AUTO_INCREMENT',
      'photogallery_id' => 'INT(11) unsigned NOT NULL',
      'name' => 'VARCHAR(255) NOT NULL',
      'author' => 'VARCHAR(255) NOT NULL DEFAULT \'\'',
      'description' => 'text NOT NULL',
      'picture' => 'VARCHAR(255) NOT NULL DEFAULT \'\'',
      'date_create' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
      'vflag' => 'tinyint(1) NOT NULL DEFAULT \'1\'',
      'PRIMARY KEY (`photo_id`)',
      'KEY (`photogallery_id`)',
    ), 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1');

    $this->createTable('##_photogallery', array(
      'photogallery_id' => 'INT(11) unsigned NOT NULL AUTO_INCREMENT',
      'name' => 'VARCHAR(255) NOT NULL',
      'cover' => 'VARCHAR(255) NOT NULL DEFAULT \'\'',
      'cnt' => 'INT(11) NOT NULL DEFAULT \'9\'',
      'vflag' => 'tinyint(1) NOT NULL DEFAULT \'1\'',
      'PRIMARY KEY (`photogallery_id`)',
    ), 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1');

    $this->createTable('##_siteinfo', array(
      'siteinfo_id' => 'INT(11) unsigned NOT NULL AUTO_INCREMENT',
      'site_id' => 'INT(11) NOT NULL DEFAULT \'1\'',
      'title' => 'VARCHAR(255) NOT NULL DEFAULT \'\'',
      'admin_email' => 'VARCHAR(255) NOT NULL DEFAULT \'\'',
      'mtflag' => 'tinyint(1) NOT NULL DEFAULT \'0\'',
      'mt_text' => 'text NOT NULL',
      'PRIMARY KEY (`siteinfo_id`)',
    ), 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1');

    $this->createTable('##_prj_feedback', array(
      'prj_feedback_id' => 'INT(11) unsigned NOT NULL AUTO_INCREMENT',
      'email' => 'VARCHAR(255) NOT NULL DEFAULT \'\'',
      'rate' => 'INT(11) NOT NULL',
      'body' => 'text NOT NULL',
      'date_send' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
      'PRIMARY KEY (`prj_feedback_id`)',
    ), 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1');

/*
    $this->execute('CREATE ALGORITHM=UNDEFINED DEFINER=CURRENT_USER SQL SECURITY DEFINER VIEW `##_adm_cmp_v` AS SELECT `ac`.`adm_cmp_id` AS `adm_cmp_id`,`ac`.`char_id` AS `cmp_char_id`,`ac`.`name` AS `cmp_name`,`acg`.`adm_cmp_grp_id` AS `adm_cmp_grp_id`,`acg`.`char_id` AS `grp_char_id`,`acg`.`name` AS `grp_name`,`auac`.`adm_user_id` AS `adm_user_id` FROM ((`##_adm_cmp` `ac` JOIN `##_adm_cmp_grp` `acg`) JOIN `##_adm_user_adm_cmp` `auac`) WHERE ((`ac`.`adm_cmp_grp_id` = `acg`.`adm_cmp_grp_id`) AND (`ac`.`adm_cmp_id` = `auac`.`adm_cmp_id`)) ORDER BY `ac`.`adm_cmp_grp_id`;');

    $this->execute('CREATE ALGORITHM=UNDEFINED DEFINER=CURRENT_USER SQL SECURITY DEFINER VIEW `##_adv_banners_v` AS (SELECT `ab`.`adv_banners_id` AS `adv_banners_id`,`ab`.`adv_places_id` AS `adv_places_id`,`ab`.`adv_sections_id` AS `adv_sections_id`,`ab`.`name` AS `name`,`ab`.`picture` AS `picture`,`ab`.`code` AS `code`,`ab`.`date_start` AS `date_start`,`ab`.`date_end` AS `date_end`,`ab`.`date0` AS `date0`,`ab`.`title` AS `title`,`ab`.`link` AS `link`,`ab`.`vflag` AS `vflag`,`ap`.`char_id` AS `char_id`,`ab`.`vflag` AS `ab_vflag`,`ast`.`vflag` AS `as_vflag`,`ap`.`vflag` AS `ap_vflag` FROM ((`##_adv_banners` `ab` JOIN `##_adv_sections` `ast`) JOIN `##_adv_places` `ap`) WHERE ((`ab`.`adv_places_id` = `ap`.`adv_places_id`) AND (`ab`.`adv_sections_id` = `ast`.`adv_sections_id`)) ORDER BY `ab`.`adv_banners_id`);');
*/
    $this->addForeignKey('##_adm_cmp_ibfk_1','##_adm_cmp',('adm_cmp_grp_id'),'##_adm_cmp_grp',('adm_cmp_grp_id'),'RESTRICT');

    $this->addForeignKey('##_adm_user_adm_cmp_ibfk_1','##_adm_user_adm_cmp',('adm_user_id'),'##_adm_user',('adm_user_id'),'RESTRICT');

    $this->addForeignKey('##_adm_user_adm_cmp_ibfk_2','##_adm_user_adm_cmp',('adm_cmp_id'),'##_adm_cmp',('adm_cmp_id'),'RESTRICT');

    $this->addForeignKey('##_page_ibfk_1','##_page',('page_module_id'),'##_page_module',('page_module_id'),'RESTRICT');

    $this->addForeignKey('##_page_ibfk_2','##_page',('adv_sections_id'),'##_adv_sections',('adv_sections_id'),'RESTRICT');

    $this->addForeignKey('##_page_ibfk_3','##_page',('parent_id'),'##_page',('page_id'),'RESTRICT');

    $this->addForeignKey('##_newsfeed_ibfk_1','##_newsfeed',('newscat_id'),'##_newscat',('newscat_id'),'RESTRICT');

    $this->addForeignKey('##_newsitem_ibfk_1','##_newsitem',('newsfeed_id'),'##_newsfeed',('newsfeed_id'),'RESTRICT');

    $this->addForeignKey('##_photo_ibfk_1','##_photo',('photogallery_id'),'##_photogallery',('photogallery_id'),'RESTRICT');

    $this->addForeignKey('##_adv_banners_ibfk_1','##_adv_banners',('adv_places_id'),'##_adv_places',('adv_places_id'),'RESTRICT');

    $this->addForeignKey('##_adv_banners_ibfk_2','##_adv_banners',('adv_sections_id'),'##_adv_sections',('adv_sections_id'),'RESTRICT');

    $this->insertInitData();

    $this->execute('SET foreign_key_checks = 1;');
  }

  protected function downQueries()
  {
    $this->execute('SET foreign_key_checks = 0;');

    $this->dropAllTables();

    $this->execute('SET foreign_key_checks = 1;');
  }

}