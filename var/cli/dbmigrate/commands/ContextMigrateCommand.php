<?php

    class ContextMigrateCommand extends MigrateCommand
    {
      private $_params = array();
      private $_db     = null;
      /**
       * Return context parameter.
       * @param string $key Parameter key
       * @param mixed $default Default value if parameter key not reconized
       * @return mixed
       */
      protected function getParam($key, $default = null)
      {
        return key_exists($key, $this->_params) ? $this->_params[$key] : $default;
      }

      protected function getDbConnection()
      {
        if($this->_db!==null)
          return $this->_db;
        else if(($this->_db=Yii::app()->getComponent($this->connectionID)) instanceof CDbConnection)
          return $this->_db;
        else
          die("Error: CMigrationCommand.connectionID '{$this->connectionID}' is invalid. Please make sure it refers to the ID of a CDbConnection application component.\n");
      }

      /**
       * Return migration folder alias.
       * @param string $context
       * @return string
       */
      protected function getMigrationPath($context)
      {
        $path = $this->getParam('path', 'application.migrations.' . $context);

        if ($fullpath = realpath($path)) {
          $alias = 'migration.' . $context;
          Yii::setPathOfAlias($alias, $fullpath);
          return $alias;
        } elseif (!Yii::getPathOfAlias($path)) {
          echo "Error: Migration path not resolved. Overrided migration path must contains valid alias or exists file path.\n";
          exit();
          //throw new RuntimeException('Migration path not resolved. Overrided migration path must contains valid alias or exists file path.');
        }

        return $path;
      }

      /**
       * Sets the database context.
       * @param string $context
       * @return boolean
       */
      protected function setContext($context)
      {
        $params = null;
        if (Yii::app()->params->contains('context_migrations')) {
          $params = Yii::app()->params['context_migrations'];
          $this->_params = isset($params[$context]) ? $params[$context] : array();
        }

        $path = $this->getMigrationPath($context);
        try {
          $this->connectionID = $context;
          $this->migrationPath = $path;
          return null !== Yii::app()->$context;
        } catch (CDbException $e) {
          echo $e->getMessage(), PHP_EOL;
          return false;
        } catch (CException $e) {
          return false;
        }
      }


      /**
       * Processes parameters, identifies the context ID.
       * @see MigrateCommand::beforeAction()
       */
      public function beforeAction($action,$params)
      {
        $context = isset($params[0]) ? array_shift($params[0]) : null;
        if (!$context) {
          echo "Not set migration context.\n";
          return;
        }
        if (!$this->setContext($context)) {
          echo "Context $context not found.\n";
          return;
        }

        return parent::beforeAction($action, $params);
      }

      /**
       * (non-PHPdoc)
       * @see MigrateCommand::actionUp()
       */
      public function actionUp($args)
      {
        array_shift($args);
        parent::actionUp($args);
      }

      /**
       * (non-PHPdoc)
       * @see MigrateCommand::actionDown()
       */
      public function actionDown($args)
      {
        array_shift($args);
        parent::actionDown($args);
      }

      /**
       * (non-PHPdoc)
       * @see MigrateCommand::actionRedo()
       */
      public function actionRedo($args)
      {
        array_shift($args);
        parent::actionRedo($args);
      }

      /**
       * (non-PHPdoc)
       * @see MigrateCommand::actionTo()
       */
      public function actionTo($args)
      {
        array_shift($args);
        parent::actionTo($args);
      }

      /**
       * (non-PHPdoc)
       * @see MigrateCommand::actionMark()
       */
      public function actionMark($args)
      {
        array_shift($args);
        parent::actionMark($args);
      }

      /**
       * (non-PHPdoc)
       * @see MigrateCommand::actionHistory()
       */
      public function actionHistory($args)
      {
        array_shift($args);
        parent::actionHistory($args);
      }

      /**
       * (non-PHPdoc)
       * @see MigrateCommand::actionNew()
       */
      public function actionNew($args)
      {
        array_shift($args);
        parent::actionNew($args);
      }

      /**
       * (non-PHPdoc)
       * @see MigrateCommand::actionCreate()
       */
      public function actionCreate($args)
      {
        array_shift($args);
        parent::actionCreate($args);
      }

      public function actionAutoUp($args)
      {
        array_shift($args);
        if(($migrations=$this->getNewMigrations())===array())
        {
          echo "No new migration found. Your system is up-to-date.\n";
          return 0;
        }

        $total=count($migrations);
        $step=isset($args[0]) ? (int)$args[0] : 0;
        if($step>0)
          $migrations=array_slice($migrations,0,$step);

        $n=count($migrations);
        if($n===$total)
          echo "Total $n new auto ".($n===1 ? 'migration':'migrations')." to be applied:\n";
        else
          echo "Total $n out of $total new auto ".($total===1 ? 'migration':'migrations')." to be applied:\n";

        foreach($migrations as $migration)
          echo "    $migration\n";
        echo "\n";

        foreach($migrations as $migration)
        {
          if($this->migrateUp($migration)===false)
          {
            echo "\nMigration auto failed. All later migrations are canceled.\n";
            return 2;
          }
        }
        echo "\nMigrated auto up successfully.\n";
      }

      public function actionAutoDown($args)
      {
        array_shift($args);
        $step=isset($args[0]) ? (int)$args[0] : 1;
        if($step<1)
        {
          echo "Error: The step parameter must be greater than 0.\n";
          return 1;
        }

        if(($migrations=$this->getMigrationHistory($step))===array())
        {
          echo "No migration has been done before.\n";
          return 0;
        }
        $migrations=array_keys($migrations);

        $n=count($migrations);
        echo "Total $n ".($n===1 ? 'migration':'migrations')." to be reverted:\n";
        foreach($migrations as $migration)
          echo "    $migration\n";
        echo "\n";

        foreach($migrations as $migration)
        {
          if($this->migrateDown($migration)===false)
          {
            echo "\nMigration auto failed. All later migrations are canceled.\n";
            return 2;
          }
        }
        echo "\nMigrated auto down successfully.\n";
      }
      /**
       * Reversive create
       * @see self::actionCreate()
       */
      public function actionRevers($args)
      {
        array_shift($args);
        //destination DB
        if(isset($args[0])){
          $destination = $args[0];
        }else{
          $this->usageError("Error: The  destination_database parameter must be valid DB resource name.\n");
        }
        array_shift($args);
        //Migration name
        if(isset($args[0])){
          $migration_name = $args[0];
        }else{
          $this->usageError("Error: Need migration name.\n");
        }

        //allowed tables
        $allowed_tables = array();
        if(isset($args[1])){
          $allowed_tables = explode(" ", $args[1]);
        }

        //filter source list
        $source_tables = $this->getDbConnection()->getSchema()->getTables();
        if(count($allowed_tables)){
          $tables_new = array();
          foreach($source_tables AS $tbl){
            if( in_array( $tbl->name, $allowed_tables)) {
              $tables_new[$tbl->name] = $tbl;
            }
          }
          $source_tables = $tables_new;
        }

        $this->setContext($destination);
        $this->_db = Yii::app()->getComponent($destination);
        $destination_tables = $this->getDbConnection()->getSchema()->getTables();

        $no_existing_tables = array_diff( array_keys( $source_tables ),array_keys( $destination_tables ));
        $exsisting_tables   = array_intersect(array_keys( $source_tables ),array_keys( $destination_tables ));

        $up = array();
        $down = array();
        $up_foreigns = array();
        $down_foreigns = array();
        foreach ($source_tables as $def) {
          $source_foreigns = $def->foreignKeys;
          $source_columns = $def->getColumnNames();

          if(in_array($def->name, $no_existing_tables)){

            $up[] =  "\$this->createTable('{$def->name}', array(";
            foreach ($def->columns as $col) {
              $up[] = '    "' . $col->name . '"=>"' . $this->getColType($col) . '",';
            }

            $up[] = '),   "ENGINE=InnoDB DEFAULT CHARSET=utf8" );'.PHP_EOL;
            $down[] = "\$this->dropTable('{$def->name}');";

            //foreign keys
            if( count($source_foreigns) ){
              $i = 1;
              foreach ($source_foreigns as $key => $foreign) {
                $name   = $def->name."_fk".$i++;
                $table = $def->name;
                $columns = $key;
                $refTable = $foreign[0];
                $refColumns = $foreign[1];
                $foreign_str    =  "\$this->addForeignKey('{$name}','{$table}','{$columns}','{$refTable}','{$refColumns}','CASCADE','CASCADE');";
                $up_foreigns[]  = $foreign_str;
                $down_foreigns[]= "\$this->dropForeignKey('{$name}','{$table}');";
              }
            }
          }
          //exsisting compare
          if(in_array($def->name, $exsisting_tables)){
            $dest_table = $destination_tables[$def->name];
            $dest_columns = $dest_table->getColumnNames();

            $exist_columns = array_intersect( $source_columns, $dest_columns );
            $noexist_columns = array_diff( $source_columns, $dest_columns );

            foreach($noexist_columns AS $key){
              $col    = $def->getColumn($key);
              $up[]   =  "\$this->addColumn('{$def->name}','{$col->name}','{$this->getColType($col)}');";
              $down[] =  "\$this->dropColumn('{$def->name}','{$col->name}');";
            }

            foreach($exist_columns AS $key){
              $source_col = $def->getColumn($key);
              $dest_col   = $dest_table->getColumn($key);
              if($this->getColType($source_col)!=$this->getColType($dest_col)){
                $up[] = "\$this->alterColumn('{$def->name}','{$source_col->name}','{$this->getColType($source_col)}');";
                $down[] = "\$this->alterColumn('{$def->name}','{$source_col->name}','{$this->getColType($dest_col)}');";
              }
            }

          }
        }

        $up_text  = implode(PHP_EOL, $up).PHP_EOL;

        $up_text .= implode(PHP_EOL, $up_foreigns).PHP_EOL;

        $down_text = implode(PHP_EOL, $down_foreigns).PHP_EOL;   //remove foreign before drop/rename/etc,,.
        $down_text.= implode(PHP_EOL, $down).PHP_EOL;

        $content = <<<EOT
<?php
class {ClassName} extends CDbMigration
 {
   public function up(){

{UP}

   }
   public function down(){

{DOWN}

   }

/*  public function safeUp(){}    // Use safeUp/safeDown to do migration with transaction

	  public function safeDown(){}	*/
}
EOT;


      $name='m'.gmdate('ymd_His').'_'.$args[0];
      $content = strtr($content,array('{ClassName}'=>$name,'{UP}'=>$up_text,'{DOWN}'=>$down_text));
      $file=Yii::getPathOfAlias($this->migrationPath).DIRECTORY_SEPARATOR.$name.'.php';

      if($this->confirm("Create new migration '$name'?"))
      {
        file_put_contents($file, $content);
        echo "New migration created successfully.\n";
      }
     }


     public function getColType($col) {
       if ($col->isPrimaryKey) {
          return "pk";
        }
        $result = $col->dbType;
        if (!$col->allowNull) {
          $result .= ' NOT null';
        }
        if ($col->defaultValue != null) {
          $result .= " DEFAULT '{$col->defaultValue}'";
        }
        return $result;
      }

    /**
    * (non-PHPdoc)
     * @see MigrateCommand::getHelp()
      */
      public function getHelp()
      {
        return <<<EOD
USAGE
  yiic contextmigrate [action] [context] [parameter]

DESCRIPTION
  This command provides support for database migrations. The optional
  'action' parameter specifies which specific migration task to perform.
  It can take these values: up, down, to, create, history, new, mark.
  If the 'action' parameter is not given, it defaults to 'up'.
  Each action takes different parameters. Their usage can be found in
  the following examples.

EXAMPLES
 * yiic contextmigrate db
   Applies ALL new migrations. This is equivalent to 'yiic migrate to'.

 * yiic contextmigrate create db create_user_table
   Creates a new migration named 'create_user_table'.

 * yiic contextmigrate up db 3
   Applies the next 3 new migrations.

 * yiic contextmigrate down db
   Reverts the last applied migration.

 * yiic contextmigrate down db 3
   Reverts the last 3 applied migrations.

 * yiic contextmigrate to db 101129_185401
   Migrates up or down to version 101129_185401.

 * yiic contextmigrate mark db 101129_185401
   Modifies the migration history up or down to version 101129_185401.
   No actual migration will be performed.

 * yiic contextmigrate history db
   Shows all previously applied migration information.

 * yiic contextmigrate history db 10
   Shows the last 10 applied migrations.

 * yiic contextmigrate new db
   Shows all new migrations.

 * yiic contextmigrate new db 10
   Shows the next 10 migrations that have not been applied.

 * yiic contextmigrate revers source_db target_db migration_name "table1"
    Create migration with reverse engineering from source database to target database, with migration_name optional argument - list of tables (space separated)
    Note - currently NOT supported: composite primary keys, simple indexes - you need add its with you hands

 * yiic contextmigrate autoup db 3
   Auto applies the next 3 new migrations.

 * yiic contextmigrate autodown db 3
   Auto reverts the last 3 applied migrations.
EOD;
      }
}
