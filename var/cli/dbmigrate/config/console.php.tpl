<?php

return array(
  'basePath'   => __DIR__ . DIRECTORY_SEPARATOR . '..',
  'name'       => 'DB Migration Console Application',
  'import' => array(
    'system.cli.commands.*'
  ),
  'components' => array(
    '##type##' => array(
      'connectionString' => '##type##:host=##host##;dbname=##database##',
      'username'         => '##username##',
      'password'         => '##password##',
      'tablePrefix'      => '##tablePrefix##',
      'emulatePrepare'   => true,
      'charset'          => 'utf8',
      'class'            => 'CDbConnection'
    ),
  ),
  'params' => array(
    'context_migrations' => array(
      // Переопределенный путь для сохранения миграций в контексте
      // По умолчанию миграции сохраняются в application.migrations.id_миграции
      //   'onlinejournal' => array(
      //       'path' => '/home/dev/projects/onlinejournal/migrations'
      //   ),
    ),
  ),
);