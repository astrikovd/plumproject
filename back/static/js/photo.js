$(function () {
  $('.multiple-select-action').click(function () {
    var checked = [];
    $(".grid-view tbody input:checkbox[name=photo_cb[]]:checked").each(function () {
      checked.push($(this).val());
    });

    var cp = $(this).attr('id');

    $.post('/photo/ajax/route/', {cp: cp, checked: checked}, function (result) {
          if (result.result) {
            $.fn.yiiGridView.update('photo-grid');
          }
        },
        'json');
    return false;
  });
});