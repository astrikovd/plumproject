$(function(){
	$('.multiple-select-action').click(function(){
		var checked = [];
		$(".grid-view tbody input:checkbox[name=advsections_cb[]]:checked").each(function()
		{
		    checked.push($(this).val());
		});

		var cp = $(this).attr('id');

		$.post('/advsections/ajax/route/', {cp:cp, checked: checked}, function(result) {
			if(result.result){
				$.fn.yiiGridView.update('advsections-grid');
			}
		},
		'json');
		return false;
	});
});