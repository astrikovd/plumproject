$(function () {
  $('.multiple-select-action').click(function () {
    var checked = [];
    $(".grid-view tbody input:checkbox[name=newsitem_cb[]]:checked").each(function () {
      checked.push($(this).val());
    });

    var cp = $(this).attr('id');

    $.post('/news/ajax/route/', {cp: cp, checked: checked}, function (result) {
          if (result.result) {
            $.fn.yiiGridView.update('newsitem-grid');
          }
        },
        'json');
    return false;
  });
});