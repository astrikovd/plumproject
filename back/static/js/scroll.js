$(function () {
  $(window).scroll(function () {
    var scrollTop = $(window).scrollTop();
    if (scrollTop >= 75) {
      $('#id-submenu').addClass('submenu-fixed');
      $('#id-content-wrapper').addClass('pull-right');
    }
    else {
      $('#id-submenu').removeClass('submenu-fixed');
      $('#id-content-wrapper').removeClass('pull-right');
    }
  });
});