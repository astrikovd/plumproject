$(function () {
  $('.multiple-select-action').click(function () {
    var checked = [];
    $(".grid-view tbody input:checkbox[name=advbanners_cb[]]:checked").each(function () {
      checked.push($(this).val());
    });

    var cp = $(this).attr('id');

    $.post('/advbanners/ajax/route/', {cp: cp, checked: checked}, function (result) {
          if (result.result) {
            $.fn.yiiGridView.update('advbanners-grid');
          }
        },
        'json');
    return false;
  });
});