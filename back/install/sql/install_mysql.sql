SET NAMES utf8;
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+06:00";
SET foreign_key_checks = 0;

-- --------------------------------------------------------
-- <REINSTALL_BLOCK>
-- SET foreign_key_checks = 0;
-- ALTER TABLE ##_adm_cmp           DROP FOREIGN KEY ##_adm_cmp_ibfk_1;
-- ALTER TABLE ##_adm_user_adm_cmp  DROP FOREIGN KEY ##_adm_user_adm_cmp_ibfk_1;
-- ALTER TABLE ##_adm_user_adm_cmp  DROP FOREIGN KEY ##_adm_user_adm_cmp_ibfk_2;
-- ALTER TABLE ##_page              DROP FOREIGN KEY ##_page_ibfk_1;
-- ALTER TABLE ##_page              DROP FOREIGN KEY ##_page_ibfk_2;
-- ALTER TABLE ##_page              DROP FOREIGN KEY ##_page_ibfk_3;
-- ALTER TABLE ##_newsfeed          DROP FOREIGN KEY ##_newsfeed_ibfk_1;
-- ALTER TABLE ##_newsitem          DROP FOREIGN KEY ##_newsitem_ibfk_1;
-- ALTER TABLE ##_photo             DROP FOREIGN KEY ##_photo_ibfk_1;
-- ALTER TABLE ##_adv_banners       DROP FOREIGN KEY ##_adv_banners_ibfk_1;
-- ALTER TABLE ##_adv_banners       DROP FOREIGN KEY ##_adv_banners_ibfk_2;
-- DROP TABLE IF EXISTS ##_adm_cmp          CASCADE;
-- DROP TABLE IF EXISTS ##_adm_cmp_grp      CASCADE;
-- DROP TABLE IF EXISTS ##_adm_user_adm_cmp CASCADE;
-- DROP TABLE IF EXISTS ##_adm_user         CASCADE;
-- DROP TABLE IF EXISTS ##_adv_banners      CASCADE;
-- DROP TABLE IF EXISTS ##_adv_places       CASCADE;
-- DROP TABLE IF EXISTS ##_adv_sections     CASCADE;
-- DROP TABLE IF EXISTS ##_feedback         CASCADE;
-- DROP TABLE IF EXISTS ##_newscat          CASCADE;
-- DROP TABLE IF EXISTS ##_newsfeed         CASCADE;
-- DROP TABLE IF EXISTS ##_newsitem         CASCADE;
-- DROP TABLE IF EXISTS ##_page             CASCADE;
-- DROP TABLE IF EXISTS ##_page_module      CASCADE;
-- DROP TABLE IF EXISTS ##_photo            CASCADE;
-- DROP TABLE IF EXISTS ##_photogallery    CASCADE;
-- DROP TABLE IF EXISTS ##_siteinfo         CASCADE;
-- DROP TABLE IF EXISTS ##_prj_feedback     CASCADE;
-- DROP VIEW IF EXISTS  ##_adv_banners_v;
-- DROP VIEW IF EXISTS  ##_adm_cmp_v;
-- SET foreign_key_checks = 1;
-- </REINSTALL_BLOCK>
-- --------------------------------------------------------

-- --------------------------------------------------------
--
--  Структура таблицы `adm_cmp`
--
CREATE TABLE IF NOT EXISTS `##_adm_cmp` (
  `adm_cmp_id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
  `char_id` VARCHAR(32) NOT NULL,
  `name` VARCHAR(32) NOT NULL,
  `adm_cmp_grp_id` INT(11) unsigned NOT NULL,                 -- need +++REFERENCES adm_cmp_grp,
  `description` VARCHAR(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`adm_cmp_id`),
  KEY(`adm_cmp_grp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19;

--
-- Дамп данных таблицы `adm_cmp`
--

INSERT INTO `##_adm_cmp` (`adm_cmp_id`, `char_id`, `name`, `adm_cmp_grp_id`, `description`) VALUES
(1, 'user', 'Пользователи Plum', 1, 'Управление учетными записями и разграничением прав.'),
(2, 'installer', 'Модули системы',1, 'Управление модулями системы- загрузка, установка и удаление дополнительных модулей.'),
(3, 'siteinfo', 'Информация о сайте', 1, 'Изменение информации о сайте, такой как название сайта, e-mail администратора. Управление режимом техобслуживания.'),
(4, 'filemanager', 'Файловый менеджер', 1, 'Загрузка файлов на сервер для их последующего использования на сайте.'),
(5, 'page', 'Структура сайта', 2, 'Управление структурой сайта, добавление новых страниц, управление их содержимым.'),
(6, 'newsfeed', 'Новостные ленты', 2, 'Управление новостными лентами.'),
(7, 'news', 'Новости', 2, 'Управление новостями, статьями и другой информацией, которая может быть представлена в виде списка элементов'),
(8, 'photogallery', 'Фотогалереи', 2, 'Управление фотогалереями.'),
(9, 'photo', 'Фотографии', 2, 'Управление фотографиями.'),
(10,'feedback', 'Обратная связь', 3, 'Управление сообщениями, поступающими через форму обратной связи на сайте.'),
(11, 'advplaces', 'Рекламные места', 5, 'Управление рекламными местами для размещения баннеров.'),
(12, 'advsections', 'Рекламные разделы', 5, 'Управление разделами, предназначенными для размещения рекламы.'),
(13, 'advbanners', 'Рекламные баннеры', 5, 'Управление рекламными баннерами - загрузка изображений, изменение кодов и т.д.'),
(14, 'chpasswd', 'Изменить пароль', 6, 'Изменение пароля текущего пользователя.'),
(15, 'auth', 'Авторизация', 7, 'Авторизация текущего пользователя. Модуль ядра.'),
(16, 'content', 'Контент', 7, 'Контент. Модуль ядра.'),
(17, 'site', 'Сайт',7, 'Сайт. Модуль ядра.'),
(18, 'upload', 'Загрузка файлов', 7, 'Загрузка файлов. Модуль ядра.');

-- --------------------------------------------------------
--
-- Структура таблицы `adm_cmp_grp`
--
CREATE TABLE IF NOT EXISTS `##_adm_cmp_grp` (
  `adm_cmp_grp_id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
  `char_id` VARCHAR(32) NOT NULL,
  `name` VARCHAR(32) NOT NULL,
  PRIMARY KEY (`adm_cmp_grp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;


--
-- Дамп данных таблицы `adm_cmp_grp`
--
INSERT INTO `##_adm_cmp_grp` (`adm_cmp_grp_id`, `char_id`, `name`) VALUES
(1, 'adm', 'Администрирование'),
(2, 'info', 'Информация'),
(3, 'serv', 'Сервисы'),
(4, 'shop', 'Интернет-магазин'),
(5, 'adv', 'Реклама'),
(6, 'other', 'Разное'),
(7, 'core', 'Ядро'),
(8, 'front', 'Фронтэнд');

-- --------------------------------------------------------
--
-- Структура таблицы `adm_user_adm_cmp`
--
CREATE TABLE IF NOT EXISTS `##_adm_user_adm_cmp` (
  `adm_user_adm_cmp_id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
  `adm_user_id` INT(11) unsigned NOT NULL,                        -- +++need REFERENCES adm_user
  `adm_cmp_id` INT(11) unsigned NOT NULL,                         -- +++need REFERENCES adm_cmp
  PRIMARY KEY (`adm_user_adm_cmp_id`),
  KEY(`adm_user_id`),
  KEY(`adm_cmp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4;

--
-- Дамп данных таблицы `adm_user_adm_cmp`
--

INSERT INTO `##_adm_user_adm_cmp` (`adm_user_adm_cmp_id`, `adm_user_id`, `adm_cmp_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3);
-- --------------------------------------------------------
--
-- Структура таблицы `adm_user`
--
CREATE TABLE IF NOT EXISTS `##_adm_user` (
  `adm_user_id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(32) NOT NULL,
  `passwordmd5` VARCHAR(32) NOT NULL,
  `email` VARCHAR(32) NOT NULL,
  `last_login` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_ip` VARCHAR(32) NOT NULL DEFAULT '0.0.0.0',
  `date_create` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',   -- +++need set NOW() from model
  PRIMARY KEY (`adm_user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `adm_user`
--

INSERT INTO `##_adm_user` (`adm_user_id`, `username`, `passwordmd5`, `email`, `last_login`, `last_ip`, `date_create`) VALUES
(1, 'admin', MD5('admin'), 'astrikov.d@gmail.com', NOW(), '127.0.0.1', NOW());
-- --------------------------------------------------------
--
-- Структура таблицы `adv_banners`
--
CREATE TABLE IF NOT EXISTS `##_adv_banners` (
  `adv_banners_id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
  `adv_places_id` INT(11) unsigned NOT NULL,                  -- +++need REFERENCES adv_places
  `adv_sections_id` INT(11) unsigned NOT NULL,                -- +++need REFERENCES adv_sections
  `name` VARCHAR(255) NOT NULL DEFAULT '',
  `picture` VARCHAR(255) NOT NULL DEFAULT '',
  `code` text NOT NULL,
  `date_start` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_end` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00', -- +++need set NOW() from model
  `date0` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',    -- +++need set NOW() from model
  `title` VARCHAR(255) NOT NULL DEFAULT '',
  `link` VARCHAR(255) NOT NULL DEFAULT '',
  `vflag` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`adv_banners_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
-- --------------------------------------------------------
--
-- Структура таблицы `adv_places`
--
CREATE TABLE IF NOT EXISTS `##_adv_places` (
  `adv_places_id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL DEFAULT '',
  `char_id` VARCHAR(64) NOT NULL,
  `date0` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `vflag` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`adv_places_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `adv_places`
--

INSERT INTO `##_adv_places` (`adv_places_id`, `name`, `char_id`, `date0`, `vflag`) VALUES
(1, 'Рекламное место вверху страницы', 'adv_place_top', '2012-12-22 18:26:33', 1),
(2, 'Рекламное место внизу страницы', 'adv_place_bottom', '2012-12-22 18:26:33', 1);
-- --------------------------------------------------------
--
-- Структура таблицы `adv_sections`
--
CREATE TABLE IF NOT EXISTS `##_adv_sections` (
  `adv_sections_id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL DEFAULT '',
  `vflag` tinyint(1) NOT NULL DEFAULT '1',
  `date0` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`adv_sections_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `adv_sections`
--

INSERT INTO `##_adv_sections` (`adv_sections_id`, `name`, `vflag`, `date0`) VALUES
(1, 'Не показывать баннеры', 1, '2012-12-22 18:26:33'),
(2, 'Главная страница', 1, '2012-12-22 18:26:33');
-- --------------------------------------------------------
--
-- Структура таблицы `feedback`
--
CREATE TABLE IF NOT EXISTS `##_feedback` (
  `feedback_id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
  `fio` VARCHAR(255) NOT NULL DEFAULT '',
  `phone` VARCHAR(255) NOT NULL DEFAULT '',
  `question` text NOT NULL,
  `answer` text NOT NULL,
  `date_send` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_ans` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00', -- +++need set NOW() from model
  `vflag` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`feedback_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
-- --------------------------------------------------------
--
-- Структура таблицы `newscat`
--
CREATE TABLE IF NOT EXISTS `##_newscat` (
  `newscat_id` VARCHAR(32) NOT NULL,
  `vflag` tinyint(1) NOT NULL DEFAULT '1',
  `date0` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `name` VARCHAR(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`newscat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `newscat`
--

INSERT INTO `##_newscat` (`newscat_id`, `vflag`, `date0`, `name`) VALUES
('articles', 1, '2012-12-22 18:15:03', 'Статьи'),
('news', 1, '2012-12-22 18:15:03', 'Новости');
-- --------------------------------------------------------
--
-- Структура таблицы `newsfeed`
--
CREATE TABLE IF NOT EXISTS `##_newsfeed` (
  `newsfeed_id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
  `newscat_id` VARCHAR(32) DEFAULT NULL,                  -- need +++REFERENCES newscat
  `name` VARCHAR(128) NOT NULL DEFAULT '',
  `cnt` INT(11) NOT NULL DEFAULT '10',
  `vflag` tinyint(1) NOT NULL DEFAULT '1',
  `date0` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`newsfeed_id`),
  KEY(`newscat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
-- --------------------------------------------------------
--
-- Структура таблицы `newsitem`
--
CREATE TABLE IF NOT EXISTS `##_newsitem` (
  `newsitem_id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
  `newsfeed_id` INT(11) unsigned NOT NULL,                -- need +++REFERENCES newsfeed
  `name` VARCHAR(255) NOT NULL DEFAULT '',
  `anons` text NOT NULL,
  `body` text NOT NULL,
  `picture` VARCHAR(255) NOT NULL DEFAULT '',
  `vflag` tinyint(1) NOT NULL DEFAULT '1',
  `date0` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `keywords` VARCHAR(255) NOT NULL DEFAULT '',
  `description` VARCHAR(1023) NOT NULL DEFAULT '',
  PRIMARY KEY (`newsitem_id`),
  KEY(`newsfeed_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
-- --------------------------------------------------------
--
-- Структура таблицы `page`
--
CREATE TABLE IF NOT EXISTS `##_page` (
  `page_id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` INT(11) unsigned DEFAULT NULL,
  `page_module_id` VARCHAR(16) NOT NULL,              -- need +++REFERENCES page_module
  `page_module_param` VARCHAR(32) NOT NULL DEFAULT '',
  `url_path` VARCHAR(512) NOT NULL DEFAULT '',
  `url_name` VARCHAR(512) NOT NULL DEFAULT '',
  `redirect_url` VARCHAR(512) NOT NULL DEFAULT '',
  `name` VARCHAR(128) NOT NULL DEFAULT '',
  `header` VARCHAR(128) NOT NULL DEFAULT '',
  `before_content` text NOT NULL,
  `after_content` text NOT NULL,
  `sort` INT(11) NOT NULL,
  `vflag` tinyint(1) NOT NULL DEFAULT '1',
  `aflag` tinyint(1) NOT NULL DEFAULT '0',
  `hflag` tinyint(1) NOT NULL DEFAULT '1',
  `eflag` tinyint(1) NOT NULL DEFAULT '1',
  `date0` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `keywords` VARCHAR(255) NOT NULL DEFAULT '',
  `description` VARCHAR(1023) NOT NULL DEFAULT '',
  `adv_sections_id` INT(11) unsigned DEFAULT NULL,      -- need +++REFERENCES adv_sections
  PRIMARY KEY (`page_id`),
  KEY(`parent_id`),
  KEY(`adv_sections_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `page`
--

INSERT INTO `##_page` (`page_id`, `parent_id`, `page_module_id`, `page_module_param`, `url_path`, `url_name`, `redirect_url`, `name`, `header`, `before_content`, `after_content`, `sort`, `vflag`, `aflag`, `hflag`, `eflag`, `date0`, `keywords`, `description`, `adv_sections_id`) VALUES
(1, NULL, 'homepage', '', '/', '', '', 'Главная', '', '', '', 1, 1, 0, 1, 1, '2012-12-22 18:27:23', '', '', 2);
-- --------------------------------------------------------
--
-- Структура таблицы `page_module`
--
CREATE TABLE IF NOT EXISTS `##_page_module` (
  `page_module_id` VARCHAR(16) NOT NULL,
  `name` VARCHAR(32) NOT NULL,
  `description` text NOT NULL,
  `vflag` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`page_module_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `page_module`
--

INSERT INTO `##_page_module` (`page_module_id`, `name`, `description`, `vflag`) VALUES
('feedback', 'Обратная связь', '', 1),
('homepage', 'Главная страница', '', 0),
('news', 'Новости', '', 0),
('photo', 'Фотографии', '', 0),
('static', 'Статическая страница', '', 1);

-- --------------------------------------------------------
--
-- Структура таблицы `photo`
--
CREATE TABLE IF NOT EXISTS `##_photo` (
  `photo_id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
  `photogallery_id` INT(11) unsigned NOT NULL,         -- need +++REFERENCES photogallery
  `name` VARCHAR(255) NOT NULL,
  `author` VARCHAR(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `picture` VARCHAR(255) NOT NULL DEFAULT '',
  `date_create` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `vflag` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`photo_id`),
  KEY(`photogallery_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------
--
-- Структура таблицы `photogallery`
--

CREATE TABLE IF NOT EXISTS `##_photogallery` (
  `photogallery_id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `cover` VARCHAR(255) NOT NULL DEFAULT '',
  `cnt` INT(11) NOT NULL DEFAULT '9',
  `vflag` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`photogallery_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------
--
-- Структура таблицы `siteinfo`
--
CREATE TABLE IF NOT EXISTS `##_siteinfo` (
  `siteinfo_id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` INT(11) NOT NULL DEFAULT '1',
  `title` VARCHAR(255) NOT NULL DEFAULT '',
  `admin_email` VARCHAR(255) NOT NULL DEFAULT '',
  `mtflag` tinyint(1) NOT NULL DEFAULT '0',
  `mt_text` text NOT NULL,
  PRIMARY KEY (`siteinfo_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `siteinfo`
--

INSERT INTO `##_siteinfo` (`siteinfo_id`, `site_id`, `title`, `admin_email`, `mtflag`, `mt_text`) VALUES
(1, 1, 'Plum CMF', 'admin@plumcmf.com', 0, '');
-- --------------------------------------------------------

--
-- Структура таблицы `prj_feedback`
--
CREATE TABLE IF NOT EXISTS `##_prj_feedback` (
  `prj_feedback_id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(255) NOT NULL DEFAULT '',
  `rate` INT(11) NOT NULL,
  `body` text NOT NULL,
  `date_send` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`prj_feedback_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


-- --------------------------------------------------------

--
-- Структура для представления `adm_cmp_v`
--
-- CREATE ALGORITHM=UNDEFINED DEFINER=CURRENT_USER SQL SECURITY DEFINER VIEW `##_adm_cmp_v` AS SELECT `ac`.`adm_cmp_id` AS `adm_cmp_id`,`ac`.`char_id` AS `cmp_char_id`,`ac`.`name` AS `cmp_name`,`acg`.`adm_cmp_grp_id` AS `adm_cmp_grp_id`,`acg`.`char_id` AS `grp_char_id`,`acg`.`name` AS `grp_name`,`auac`.`adm_user_id` AS `adm_user_id` FROM ((`##_adm_cmp` `ac` JOIN `##_adm_cmp_grp` `acg`) JOIN `##_adm_user_adm_cmp` `auac`) WHERE ((`ac`.`adm_cmp_grp_id` = `acg`.`adm_cmp_grp_id`) AND (`ac`.`adm_cmp_id` = `auac`.`adm_cmp_id`)) ORDER BY `ac`.`adm_cmp_grp_id`;

-- --------------------------------------------------------
--
-- Структура для представления `adv_banners_v`
--
-- CREATE ALGORITHM=UNDEFINED DEFINER=CURRENT_USER SQL SECURITY DEFINER VIEW `##_adv_banners_v` AS (SELECT `ab`.`adv_banners_id` AS `adv_banners_id`,`ab`.`adv_places_id` AS `adv_places_id`,`ab`.`adv_sections_id` AS `adv_sections_id`,`ab`.`name` AS `name`,`ab`.`picture` AS `picture`,`ab`.`code` AS `code`,`ab`.`date_start` AS `date_start`,`ab`.`date_end` AS `date_end`,`ab`.`date0` AS `date0`,`ab`.`title` AS `title`,`ab`.`link` AS `link`,`ab`.`vflag` AS `vflag`,`ap`.`char_id` AS `char_id`,`ab`.`vflag` AS `ab_vflag`,`ast`.`vflag` AS `as_vflag`,`ap`.`vflag` AS `ap_vflag` FROM ((`##_adv_banners` `ab` JOIN `##_adv_sections` `ast`) JOIN `##_adv_places` `ap`) WHERE ((`ab`.`adv_places_id` = `ap`.`adv_places_id`) AND (`ab`.`adv_sections_id` = `ast`.`adv_sections_id`)) ORDER BY `ab`.`adv_banners_id`);

--
-- Ограничения внешнего ключа таблицы `adm_cmp`
--
 ALTER TABLE `##_adm_cmp` ADD CONSTRAINT `##_adm_cmp_ibfk_1`
FOREIGN KEY (`adm_cmp_grp_id`)
REFERENCES `##_adm_cmp_grp` (`adm_cmp_grp_id`)
  ON DELETE RESTRICT;
--
-- Ограничения внешнего ключа таблицы `adm_user_adm_cmp`
--
 ALTER TABLE `##_adm_user_adm_cmp` ADD CONSTRAINT `##_adm_user_adm_cmp_ibfk_1`
  FOREIGN KEY (`adm_user_id`)
    REFERENCES `##_adm_user` (`adm_user_id`)
      ON DELETE RESTRICT;

ALTER TABLE `##_adm_user_adm_cmp` ADD CONSTRAINT `##_adm_user_adm_cmp_ibfk_2`
  FOREIGN KEY (`adm_cmp_id`)
    REFERENCES `##_adm_cmp` (`adm_cmp_id`)
      ON DELETE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `page`
--
 ALTER TABLE `##_page` ADD CONSTRAINT `##_page_ibfk_1`
  FOREIGN KEY (`page_module_id`)
    REFERENCES `##_page_module` (`page_module_id`)
      ON DELETE RESTRICT;

ALTER TABLE `##_page` ADD CONSTRAINT `##_page_ibfk_2`
  FOREIGN KEY (`adv_sections_id`)
    REFERENCES `##_adv_sections` (`adv_sections_id`)
      ON DELETE RESTRICT;

ALTER TABLE `##_page` ADD CONSTRAINT `##_page_ibfk_3`
    FOREIGN KEY (`parent_id`)
      REFERENCES `##_page` (`page_id`)
        ON DELETE RESTRICT;
--
-- Ограничения внешнего ключа таблицы `newsfeed`
--
 ALTER TABLE `##_newsfeed` ADD CONSTRAINT `##_newsfeed_ibfk_1`
  FOREIGN KEY (`newscat_id`)
    REFERENCES `##_newscat` (`newscat_id`)
      ON DELETE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `newsitem`
--
 ALTER TABLE `##_newsitem` ADD CONSTRAINT `##_newsitem_ibfk_1`
  FOREIGN KEY (`newsfeed_id`)
    REFERENCES `##_newsfeed` (`newsfeed_id`)
      ON DELETE RESTRICT;
--
-- Ограничения внешнего ключа таблицы `photo`
--
 ALTER TABLE `##_photo` ADD CONSTRAINT `##_photo_ibfk_1`
  FOREIGN KEY (`photogallery_id`)
    REFERENCES `##_photogallery` (`photogallery_id`)
      ON DELETE RESTRICT;
--
-- Ограничения внешнего ключа таблицы `photo`
--
 ALTER TABLE `##_adv_banners` ADD CONSTRAINT `##_adv_banners_ibfk_1`
  FOREIGN KEY (`adv_places_id`)
    REFERENCES `##_adv_places` (`adv_places_id`)
      ON DELETE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `adv_banners`
--
 ALTER TABLE `##_adv_banners` ADD CONSTRAINT `##_adv_banners_ibfk_2`
  FOREIGN KEY (`adv_sections_id`)
    REFERENCES `##_adv_sections` (`adv_sections_id`)
      ON DELETE RESTRICT;

SET foreign_key_checks = 1;