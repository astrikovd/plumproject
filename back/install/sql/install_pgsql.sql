SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

--
-- name: plpgsql; Type: PROCEDURAL LANGUAGE; Schema: -; Owner: plumcmf
--
ALTER PROCEDURAL LANGUAGE plpgsql OWNER TO plumcmf;
SET search_path = public, pg_catalog;

-- --------------------------------------------------------
-- <REINSTALL_BLOCK>
-- DROP SEQUENCE IF EXISTS ##_adm_cmp_adm_cmp_id_seq                  CASCADE;
-- DROP SEQUENCE IF EXISTS ##_adm_cmp_grp_adm_cmp_grp_id_seq           CASCADE;
-- DROP SEQUENCE IF EXISTS ##_adm_user_adm_cmp_adm_user_adm_cmp_id_seq CASCADE;
-- DROP SEQUENCE IF EXISTS ##_adm_user_adm_user_id_seq                 CASCADE;
-- DROP SEQUENCE IF EXISTS ##_adv_banners_adv_banners_id_seq           CASCADE;
-- DROP SEQUENCE IF EXISTS ##_adv_places_adv_places_id_seq             CASCADE;
-- DROP SEQUENCE IF EXISTS ##_adv_sections_adv_sections_id_seq         CASCADE;
-- DROP SEQUENCE IF EXISTS ##_feedback_feedback_id_seq                 CASCADE;
-- DROP SEQUENCE IF EXISTS ##_newsfeed_newsfeed_id_seq                 CASCADE;
-- DROP SEQUENCE IF EXISTS ##_newsitem_newsitem_id_seq                 CASCADE;
-- DROP SEQUENCE IF EXISTS ##_page_page_id_seq                         CASCADE;
-- DROP SEQUENCE IF EXISTS ##_photo_photo_id_seq                       CASCADE;
-- DROP SEQUENCE IF EXISTS ##_photogallery_photogallery_id_seq         CASCADE;
-- DROP SEQUENCE IF EXISTS ##_prj_feedback_prj_feedback_id_seq         CASCADE;
-- DROP SEQUENCE IF EXISTS ##_siteinfo_siteinfo_id_seq                 CASCADE;
-- DROP TABLE IF EXISTS ##_adm_cmp          CASCADE;
-- DROP TABLE IF EXISTS ##_adm_cmp_grp      CASCADE;
-- DROP TABLE IF EXISTS ##_adm_user_adm_cmp CASCADE;
-- DROP TABLE IF EXISTS ##_adm_user         CASCADE;
-- DROP TABLE IF EXISTS ##_adv_banners      CASCADE;
-- DROP TABLE IF EXISTS ##_adv_places       CASCADE;
-- DROP TABLE IF EXISTS ##_adv_sections     CASCADE;
-- DROP TABLE IF EXISTS ##_feedback         CASCADE;
-- DROP TABLE IF EXISTS ##_newscat          CASCADE;
-- DROP TABLE IF EXISTS ##_newsfeed         CASCADE;
-- DROP TABLE IF EXISTS ##_newsitem         CASCADE;
-- DROP TABLE IF EXISTS ##_page             CASCADE;
-- DROP TABLE IF EXISTS ##_page_module      CASCADE;
-- DROP TABLE IF EXISTS ##_photo            CASCADE;
-- DROP TABLE IF EXISTS ##_photogallery     CASCADE;
-- DROP TABLE IF EXISTS ##_siteinfo         CASCADE;
-- DROP TABLE IF EXISTS ##_prj_feedback     CASCADE;
-- DROP VIEW IF EXISTS  ##_adv_banners_v;
-- DROP VIEW IF EXISTS  ##_adm_cmp_v;
-- </REINSTALL_BLOCK>
-- --------------------------------------------------------



--
-- name: adm_cmp; Type: TABLE; Schema: public; Owner: plumcmf; Tablespace: 
--
CREATE TABLE IF NOT EXISTS ##_adm_cmp (
    adm_cmp_id integer NOT null,
    char_id character varying(32) NOT null,
    name character varying(32) NOT null,
    adm_cmp_grp_id integer NOT null,
    description character varying(255) DEFAULT ''::character varying NOT null
);
ALTER TABLE public.##_adm_cmp OWNER TO plumcmf;
--
-- name: adm_cmp_adm_cmp_id_seq; Type: SEQUENCE; Schema: public; Owner: plumcmf
--
CREATE SEQUENCE  ##_adm_cmp_adm_cmp_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER TABLE public.##_adm_cmp_adm_cmp_id_seq OWNER TO plumcmf;
--
-- name: adm_cmp_adm_cmp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: plumcmf
--
ALTER SEQUENCE ##_adm_cmp_adm_cmp_id_seq OWNED BY ##_adm_cmp.adm_cmp_id;
--
-- name: adm_cmp_adm_cmp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: plumcmf
--
SELECT pg_catalog.setval('##_adm_cmp_adm_cmp_id_seq', 1, false);


--
-- name: adm_cmp_grp; Type: TABLE; Schema: public; Owner: plumcmf; Tablespace: 
--
CREATE TABLE IF NOT EXISTS ##_adm_cmp_grp (
    adm_cmp_grp_id integer NOT null,
    char_id character varying(32) NOT null,
    name character varying(32) NOT null
);
ALTER TABLE public.##_adm_cmp_grp OWNER TO plumcmf;
--
-- name: adm_cmp_grp_adm_cmp_grp_id_seq; Type: SEQUENCE; Schema: public; Owner: plumcmf
--
CREATE SEQUENCE  ##_adm_cmp_grp_adm_cmp_grp_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER TABLE public.##_adm_cmp_grp_adm_cmp_grp_id_seq OWNER TO plumcmf;
--
-- name: adm_cmp_grp_adm_cmp_grp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: plumcmf
--

ALTER SEQUENCE ##_adm_cmp_grp_adm_cmp_grp_id_seq OWNED BY ##_adm_cmp_grp.adm_cmp_grp_id;
--
-- name: adm_cmp_grp_adm_cmp_grp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: plumcmf
--
SELECT pg_catalog.setval('##_adm_cmp_grp_adm_cmp_grp_id_seq', 1, false);

--
-- name: adm_user_adm_cmp; Type: TABLE; Schema: public; Owner: plumcmf; Tablespace: 
--
CREATE TABLE IF NOT EXISTS ##_adm_user_adm_cmp (
    adm_user_adm_cmp_id integer NOT null,
    adm_user_id integer NOT null,
    adm_cmp_id integer NOT null
);
ALTER TABLE public.##_adm_user_adm_cmp OWNER TO plumcmf;

--
-- name: adm_cmp_v; Type: VIEW; Schema: public; Owner: plumcmf
--
CREATE VIEW  ##_adm_cmp_v AS
    SELECT ac.adm_cmp_id, ac.char_id AS cmp_char_id, ac.name AS cmp_name, acg.adm_cmp_grp_id, acg.char_id AS grp_char_id, acg.name AS grp_name, auac.adm_user_id FROM ##_adm_cmp ac, ##_adm_cmp_grp acg, ##_adm_user_adm_cmp auac WHERE ((ac.adm_cmp_grp_id = acg.adm_cmp_grp_id) AND (ac.adm_cmp_id = auac.adm_cmp_id)) ORDER BY ac.adm_cmp_grp_id;
ALTER TABLE public.##_adm_cmp_v OWNER TO plumcmf;

--
-- name: adm_user; Type: TABLE; Schema: public; Owner: plumcmf; Tablespace: 
--
CREATE TABLE IF NOT EXISTS ##_adm_user (
    adm_user_id integer NOT null,
    username character varying(32) NOT null,
    passwordmd5 character varying(32) NOT null,
    email character varying(64) NOT null,
    last_login timestamp without time zone DEFAULT now() NOT null,
    last_ip character varying(32) DEFAULT '0.0.0.0'::character varying NOT null,
    date_create timestamp without time zone DEFAULT now() NOT null
);
ALTER TABLE public.##_adm_user OWNER TO plumcmf;
--
-- name: adm_user_adm_cmp_adm_user_adm_cmp_id_seq; Type: SEQUENCE; Schema: public; Owner: plumcmf
--
CREATE SEQUENCE  ##_adm_user_adm_cmp_adm_user_adm_cmp_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER TABLE public.##_adm_user_adm_cmp_adm_user_adm_cmp_id_seq OWNER TO plumcmf;
--
-- name: adm_user_adm_cmp_adm_user_adm_cmp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: plumcmf
--
ALTER SEQUENCE ##_adm_user_adm_cmp_adm_user_adm_cmp_id_seq OWNED BY ##_adm_user_adm_cmp.adm_user_adm_cmp_id;
--
-- name: adm_user_adm_cmp_adm_user_adm_cmp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: plumcmf
--
SELECT pg_catalog.setval('##_adm_user_adm_cmp_adm_user_adm_cmp_id_seq', 1, false);


--
-- name: adm_user_adm_user_id_seq; Type: SEQUENCE; Schema: public; Owner: plumcmf
--
CREATE SEQUENCE  ##_adm_user_adm_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER TABLE public.##_adm_user_adm_user_id_seq OWNER TO plumcmf;
--
-- name: adm_user_adm_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: plumcmf
--
ALTER SEQUENCE ##_adm_user_adm_user_id_seq OWNED BY ##_adm_user.adm_user_id;

--
-- name: adm_user_adm_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: plumcmf
--
SELECT pg_catalog.setval('##_adm_user_adm_user_id_seq', 1, false);


--
-- name: adv_banners; Type: TABLE; Schema: public; Owner: plumcmf; Tablespace: 
--
CREATE TABLE IF NOT EXISTS ##_adv_banners (
    adv_banners_id integer NOT null,
    adv_places_id integer,
    adv_sections_id integer,
    name character varying(255) DEFAULT ''::character varying NOT null,
    picture character varying(255) DEFAULT ''::character varying NOT null,
    code text DEFAULT ''::text NOT null,
    date_start timestamp without time zone DEFAULT now() NOT null,
    date_end timestamp without time zone DEFAULT now() NOT null,
    date0 timestamp without time zone DEFAULT now() NOT null,
    title character varying(255) DEFAULT ''::character varying NOT null,
    link character varying(255) DEFAULT ''::character varying NOT null,
    vflag boolean DEFAULT true NOT null
);

ALTER TABLE public.##_adv_banners OWNER TO plumcmf;
--
-- name: adv_banners_adv_banners_id_seq; Type: SEQUENCE; Schema: public; Owner: plumcmf
--
CREATE SEQUENCE  ##_adv_banners_adv_banners_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER TABLE public.##_adv_banners_adv_banners_id_seq OWNER TO plumcmf;
--
-- name: adv_banners_adv_banners_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: plumcmf
--
ALTER SEQUENCE ##_adv_banners_adv_banners_id_seq OWNED BY ##_adv_banners.adv_banners_id;
--
-- name: adv_banners_adv_banners_id_seq; Type: SEQUENCE SET; Schema: public; Owner: plumcmf
--
SELECT pg_catalog.setval('##_adv_banners_adv_banners_id_seq', 1, false);


--
-- name: adv_places; Type: TABLE; Schema: public; Owner: plumcmf; Tablespace: 
--
CREATE TABLE IF NOT EXISTS ##_adv_places (
    adv_places_id integer NOT null,
    name character varying(255) DEFAULT ''::character varying NOT null,
    char_id character varying(64) NOT null,
    date0 timestamp without time zone DEFAULT now() NOT null,
    vflag boolean DEFAULT true NOT null
);
ALTER TABLE public.##_adv_places OWNER TO plumcmf;
--
-- name: adv_sections; Type: TABLE; Schema: public; Owner: plumcmf; Tablespace: 
--
CREATE TABLE IF NOT EXISTS ##_adv_sections (
    adv_sections_id integer NOT null,
    name character varying(255) DEFAULT ''::character varying NOT null,
    vflag boolean DEFAULT true NOT null,
    date0 timestamp without time zone DEFAULT now() NOT null
);
ALTER TABLE public.##_adv_sections OWNER TO plumcmf;
--
-- name: adv_banners_v; Type: VIEW; Schema: public; Owner: plumcmf
--
-- CREATE VIEW  ##_adv_banners_v AS
--     SELECT ab.adv_banners_id, ab.adv_places_id, ab.adv_sections_id, ab.name, ab.picture, ab.code, ab.date_start, ab.date_end, ab.date0, ab.title, ab.link, ab.vflag, ap.char_id, ab.vflag AS ab_vflag, ast.vflag AS as_vflag, ap.vflag AS ap_vflag FROM ##_adv_banners ab, ##_adv_sections ast, ##_adv_places ap WHERE ((ab.adv_places_id = ap.adv_places_id) AND (ab.adv_sections_id = ast.adv_sections_id)) ORDER BY ab.adv_banners_id;
-- ALTER TABLE public.##_adv_banners_v OWNER TO plumcmf;

--
-- name: adv_places_adv_places_id_seq; Type: SEQUENCE; Schema: public; Owner: plumcmf
--
CREATE SEQUENCE  ##_adv_places_adv_places_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER TABLE public.##_adv_places_adv_places_id_seq OWNER TO plumcmf;
--
-- name: adv_places_adv_places_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: plumcmf
--
ALTER SEQUENCE ##_adv_places_adv_places_id_seq OWNED BY ##_adv_places.adv_places_id;
--
-- name: adv_places_adv_places_id_seq; Type: SEQUENCE SET; Schema: public; Owner: plumcmf
--
SELECT pg_catalog.setval('##_adv_places_adv_places_id_seq', 2, true);
--
-- name: adv_sections_adv_sections_id_seq; Type: SEQUENCE; Schema: public; Owner: plumcmf
--
CREATE SEQUENCE  ##_adv_sections_adv_sections_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER TABLE public.##_adv_sections_adv_sections_id_seq OWNER TO plumcmf;
--
-- name: adv_sections_adv_sections_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: plumcmf
--
ALTER SEQUENCE ##_adv_sections_adv_sections_id_seq OWNED BY ##_adv_sections.adv_sections_id;
--
-- name: adv_sections_adv_sections_id_seq; Type: SEQUENCE SET; Schema: public; Owner: plumcmf
--
SELECT pg_catalog.setval('##_adv_sections_adv_sections_id_seq', 2, true);

--
-- name: feedback; Type: TABLE; Schema: public; Owner: plumcmf; Tablespace: 
--
CREATE TABLE IF NOT EXISTS ##_feedback (
    feedback_id integer NOT null,
    fio character varying(255) DEFAULT ''::character varying NOT null,
    phone character varying(255) DEFAULT ''::character varying NOT null,
    question text DEFAULT ''::text NOT null,
    answer text DEFAULT ''::text NOT null,
    date_send timestamp without time zone DEFAULT now(),
    date_ans timestamp without time zone DEFAULT now(),
    vflag boolean DEFAULT false NOT null
);
ALTER TABLE public.##_feedback OWNER TO plumcmf;
--
-- name: feedback_feedback_id_seq; Type: SEQUENCE; Schema: public; Owner: plumcmf
--
CREATE SEQUENCE  ##_feedback_feedback_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER TABLE public.##_feedback_feedback_id_seq OWNER TO plumcmf;
--
-- name: feedback_feedback_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: plumcmf
--
ALTER SEQUENCE ##_feedback_feedback_id_seq OWNED BY ##_feedback.feedback_id;
--
-- name: feedback_feedback_id_seq; Type: SEQUENCE SET; Schema: public; Owner: plumcmf
--
SELECT pg_catalog.setval('##_feedback_feedback_id_seq', 1, false);


--
-- name: newscat; Type: TABLE; Schema: public; Owner: plumcmf; Tablespace: 
--
CREATE TABLE IF NOT EXISTS ##_newscat (
    newscat_id character varying(32) NOT null,
    vflag boolean DEFAULT true NOT null,
    date0 timestamp without time zone DEFAULT now() NOT null,
    name character varying(64) DEFAULT ''::character varying NOT null
);
ALTER TABLE public.##_newscat OWNER TO plumcmf;
--
-- name: newsfeed; Type: TABLE; Schema: public; Owner: plumcmf; Tablespace: 
--
CREATE TABLE IF NOT EXISTS ##_newsfeed (
    newsfeed_id integer NOT null,
    newscat_id character varying(32),
    name character varying(128) DEFAULT ''::character varying NOT null,
    cnt integer DEFAULT 10 NOT null,
    vflag boolean DEFAULT true NOT null,
    date0 timestamp without time zone DEFAULT now() NOT null
);
ALTER TABLE public.##_newsfeed OWNER TO plumcmf;
--
-- name: newsfeed_newsfeed_id_seq; Type: SEQUENCE; Schema: public; Owner: plumcmf
--
CREATE SEQUENCE  ##_newsfeed_newsfeed_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER TABLE public.##_newsfeed_newsfeed_id_seq OWNER TO plumcmf;
--
-- name: newsfeed_newsfeed_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: plumcmf
--
ALTER SEQUENCE ##_newsfeed_newsfeed_id_seq OWNED BY ##_newsfeed.newsfeed_id;
--
-- name: newsfeed_newsfeed_id_seq; Type: SEQUENCE SET; Schema: public; Owner: plumcmf
--
SELECT pg_catalog.setval('##_newsfeed_newsfeed_id_seq', 1, false);

--
-- name: newsitem; Type: TABLE; Schema: public; Owner: plumcmf; Tablespace: 
--
CREATE TABLE IF NOT EXISTS ##_newsitem (
    newsitem_id integer NOT null,
    newsfeed_id integer,
    name character varying(255) DEFAULT ''::character varying NOT null,
    anons text DEFAULT ''::text NOT null,
    body text DEFAULT ''::text NOT null,
    picture character varying(255) DEFAULT ''::character varying NOT null,
    vflag boolean DEFAULT true NOT null,
    date0 timestamp without time zone DEFAULT now() NOT null,
    keywords character varying(255) DEFAULT ''::character varying NOT null,
    description character varying(1023) DEFAULT ''::character varying NOT null
);
ALTER TABLE public.##_newsitem OWNER TO plumcmf;

--
-- name: newsitem_newsitem_id_seq; Type: SEQUENCE; Schema: public; Owner: plumcmf
--
CREATE SEQUENCE  ##_newsitem_newsitem_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER TABLE public.##_newsitem_newsitem_id_seq OWNER TO plumcmf;
--
-- name: newsitem_newsitem_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: plumcmf
--
ALTER SEQUENCE ##_newsitem_newsitem_id_seq OWNED BY ##_newsitem.newsitem_id;
--
-- name: newsitem_newsitem_id_seq; Type: SEQUENCE SET; Schema: public; Owner: plumcmf
--
SELECT pg_catalog.setval('##_newsitem_newsitem_id_seq', 1, false);

--
-- name: page; Type: TABLE; Schema: public; Owner: plumcmf; Tablespace: 
--
CREATE TABLE IF NOT EXISTS ##_page (
    page_id integer NOT null,
    parent_id integer,
    page_module_id character varying(16) NOT null,
    page_module_param character varying(32) DEFAULT ''::character varying NOT null,
    url_path character varying(512) DEFAULT ''::character varying NOT null,
    url_name character varying(512) DEFAULT ''::character varying NOT null,
    redirect_url character varying(512) DEFAULT ''::character varying NOT null,
    name character varying(128) DEFAULT ''::character varying NOT null,
    header character varying(128) DEFAULT ''::character varying NOT null,
    before_content text DEFAULT ''::text NOT null,
    after_content text DEFAULT ''::text NOT null,
    sort integer NOT null,
    vflag boolean DEFAULT true NOT null,
    aflag boolean DEFAULT false NOT null,
    hflag boolean DEFAULT true NOT null,
    eflag boolean DEFAULT true NOT null,
    date0 timestamp without time zone DEFAULT now() NOT null,
    keywords character varying(255) DEFAULT ''::character varying NOT null,
    description character varying(1023) DEFAULT ''::character varying NOT null,
    adv_sections_id integer
);
ALTER TABLE public.##_page OWNER TO plumcmf;
--
-- name: page_module; Type: TABLE; Schema: public; Owner: plumcmf; Tablespace: 
--
CREATE TABLE IF NOT EXISTS ##_page_module (
    page_module_id character varying(16) NOT null,
    name character varying(64) NOT null,
    description text DEFAULT ''::text NOT null,
    vflag boolean DEFAULT true NOT null
);
ALTER TABLE public.##_page_module OWNER TO plumcmf;
--
-- name: page_page_id_seq; Type: SEQUENCE; Schema: public; Owner: plumcmf
--
CREATE SEQUENCE  ##_page_page_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER TABLE public.##_page_page_id_seq OWNER TO plumcmf;
--
-- name: page_page_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: plumcmf
--
ALTER SEQUENCE ##_page_page_id_seq OWNED BY ##_page.page_id;
--
-- name: page_page_id_seq; Type: SEQUENCE SET; Schema: public; Owner: plumcmf
--
SELECT pg_catalog.setval('##_page_page_id_seq', 1, false);
--
-- name: photo; Type: TABLE; Schema: public; Owner: plumcmf; Tablespace: 
--
CREATE TABLE IF NOT EXISTS ##_photo (
    photo_id integer NOT null,
    photogallery_id integer,
    name character varying(255) NOT null,
    author character varying(255) DEFAULT ''::character varying NOT null,
    description text DEFAULT ''::text NOT null,
    picture character varying(255) DEFAULT ''::character varying NOT null,
    date_create timestamp without time zone DEFAULT now() NOT null,
    vflag boolean DEFAULT true NOT null
);
ALTER TABLE public.##_photo OWNER TO plumcmf;
--
-- name: photo_photo_id_seq; Type: SEQUENCE; Schema: public; Owner: plumcmf
--
CREATE SEQUENCE  ##_photo_photo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER TABLE public.##_photo_photo_id_seq OWNER TO plumcmf;
--
-- name: photo_photo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: plumcmf
--
ALTER SEQUENCE ##_photo_photo_id_seq OWNED BY ##_photo.photo_id;
--
-- name: photo_photo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: plumcmf
--
SELECT pg_catalog.setval('##_photo_photo_id_seq', 1, false);

--
-- name: photogallery; Type: TABLE; Schema: public; Owner: plumcmf; Tablespace: 
--
CREATE TABLE IF NOT EXISTS ##_photogallery (
    photogallery_id integer NOT null,
    name character varying(255) NOT null,
    cover character varying(255) DEFAULT ''::character varying NOT null,
    cnt integer DEFAULT 9 NOT null,
    vflag boolean DEFAULT true NOT null
);
ALTER TABLE public.##_photogallery OWNER TO plumcmf;
--
-- name: photogallery_photogallery_id_seq; Type: SEQUENCE; Schema: public; Owner: plumcmf
--
CREATE SEQUENCE  ##_photogallery_photogallery_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER TABLE public.##_photogallery_photogallery_id_seq OWNER TO plumcmf;
--
-- name: photogallery_photogallery_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: plumcmf
--
ALTER SEQUENCE ##_photogallery_photogallery_id_seq OWNED BY ##_photogallery.photogallery_id;
--
-- name: photogallery_photogallery_id_seq; Type: SEQUENCE SET; Schema: public; Owner: plumcmf
--
SELECT pg_catalog.setval('##_photogallery_photogallery_id_seq', 1, false);

--
-- name: prj_feedback; Type: TABLE; Schema: public; Owner: plumcmf; Tablespace: 
--
CREATE TABLE IF NOT EXISTS ##_prj_feedback (
    prj_feedback_id integer NOT null,
    email character varying(255) DEFAULT ''::character varying NOT null,
    rate integer NOT null,
    body text DEFAULT ''::text NOT null,
    date_send timestamp without time zone DEFAULT now() NOT null
);
ALTER TABLE public.##_prj_feedback OWNER TO plumcmf;
--
-- name: prj_feedback_prj_feedback_id_seq; Type: SEQUENCE; Schema: public; Owner: plumcmf
--
CREATE SEQUENCE  ##_prj_feedback_prj_feedback_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER TABLE public.##_prj_feedback_prj_feedback_id_seq OWNER TO plumcmf;
--
-- name: prj_feedback_prj_feedback_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: plumcmf
--
ALTER SEQUENCE ##_prj_feedback_prj_feedback_id_seq OWNED BY ##_prj_feedback.prj_feedback_id;
--
-- name: prj_feedback_prj_feedback_id_seq; Type: SEQUENCE SET; Schema: public; Owner: plumcmf
--
SELECT pg_catalog.setval('##_prj_feedback_prj_feedback_id_seq', 1, false);

--
-- name: siteinfo; Type: TABLE; Schema: public; Owner: plumcmf; Tablespace: 
--
CREATE TABLE IF NOT EXISTS ##_siteinfo (
    siteinfo_id integer NOT null,
    site_id integer DEFAULT 1 NOT null,
    title character varying(255) DEFAULT ''::character varying NOT null,
    admin_email character varying(255) DEFAULT ''::character varying NOT null,
    mtflag boolean DEFAULT false NOT null,
    mt_text text DEFAULT ''::text NOT null
);
ALTER TABLE public.##_siteinfo OWNER TO plumcmf;
--
-- name: siteinfo_siteinfo_id_seq; Type: SEQUENCE; Schema: public; Owner: plumcmf
--
CREATE SEQUENCE  ##_siteinfo_siteinfo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER TABLE public.##_siteinfo_siteinfo_id_seq OWNER TO plumcmf;
--
-- name: siteinfo_siteinfo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: plumcmf
--
ALTER SEQUENCE ##_siteinfo_siteinfo_id_seq OWNED BY ##_siteinfo.siteinfo_id;
--
-- name: siteinfo_siteinfo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: plumcmf
--
SELECT pg_catalog.setval('##_siteinfo_siteinfo_id_seq', 1, true);


--
-- name: adm_cmp_id; Type: DEFAULT; Schema: public; Owner: plumcmf
--
ALTER TABLE ##_adm_cmp ALTER COLUMN adm_cmp_id SET DEFAULT nextval('##_adm_cmp_adm_cmp_id_seq'::regclass);
--
-- name: adm_cmp_grp_id; Type: DEFAULT; Schema: public; Owner: plumcmf
--
ALTER TABLE ##_adm_cmp_grp ALTER COLUMN adm_cmp_grp_id SET DEFAULT nextval('##_adm_cmp_grp_adm_cmp_grp_id_seq'::regclass);
--
-- name: adm_user_id; Type: DEFAULT; Schema: public; Owner: plumcmf
--
ALTER TABLE ##_adm_user ALTER COLUMN adm_user_id SET DEFAULT nextval('##_adm_user_adm_user_id_seq'::regclass);
--
-- name: adm_user_adm_cmp_id; Type: DEFAULT; Schema: public; Owner: plumcmf
--
ALTER TABLE ##_adm_user_adm_cmp ALTER COLUMN adm_user_adm_cmp_id SET DEFAULT nextval('##_adm_user_adm_cmp_adm_user_adm_cmp_id_seq'::regclass);
--
-- name: adv_banners_id; Type: DEFAULT; Schema: public; Owner: plumcmf
--
ALTER TABLE ##_adv_banners ALTER COLUMN adv_banners_id SET DEFAULT nextval('##_adv_banners_adv_banners_id_seq'::regclass);
--
-- name: adv_places_id; Type: DEFAULT; Schema: public; Owner: plumcmf
--
ALTER TABLE ##_adv_places ALTER COLUMN adv_places_id SET DEFAULT nextval('##_adv_places_adv_places_id_seq'::regclass);
--
-- name: adv_sections_id; Type: DEFAULT; Schema: public; Owner: plumcmf
--
ALTER TABLE ##_adv_sections ALTER COLUMN adv_sections_id SET DEFAULT nextval('##_adv_sections_adv_sections_id_seq'::regclass);
--
-- name: feedback_id; Type: DEFAULT; Schema: public; Owner: plumcmf
--
ALTER TABLE ##_feedback ALTER COLUMN feedback_id SET DEFAULT nextval('##_feedback_feedback_id_seq'::regclass);
--
-- name: newsfeed_id; Type: DEFAULT; Schema: public; Owner: plumcmf
--
ALTER TABLE ##_newsfeed ALTER COLUMN newsfeed_id SET DEFAULT nextval('##_newsfeed_newsfeed_id_seq'::regclass);
--
-- name: newsitem_id; Type: DEFAULT; Schema: public; Owner: plumcmf
--
ALTER TABLE ##_newsitem ALTER COLUMN newsitem_id SET DEFAULT nextval('##_newsitem_newsitem_id_seq'::regclass);
--
-- name: page_id; Type: DEFAULT; Schema: public; Owner: plumcmf
--
ALTER TABLE ##_page ALTER COLUMN page_id SET DEFAULT nextval('##_page_page_id_seq'::regclass);
--
-- name: photo_id; Type: DEFAULT; Schema: public; Owner: plumcmf
--
ALTER TABLE ##_photo ALTER COLUMN photo_id SET DEFAULT nextval('##_photo_photo_id_seq'::regclass);
--
-- name: photogallery_id; Type: DEFAULT; Schema: public; Owner: plumcmf
--
ALTER TABLE ##_photogallery ALTER COLUMN photogallery_id SET DEFAULT nextval('##_photogallery_photogallery_id_seq'::regclass);
--
-- name: prj_feedback_id; Type: DEFAULT; Schema: public; Owner: plumcmf
--
ALTER TABLE ##_prj_feedback ALTER COLUMN prj_feedback_id SET DEFAULT nextval('##_prj_feedback_prj_feedback_id_seq'::regclass);
--
-- name: siteinfo_id; Type: DEFAULT; Schema: public; Owner: plumcmf
--
ALTER TABLE ##_siteinfo ALTER COLUMN siteinfo_id SET DEFAULT nextval('##_siteinfo_siteinfo_id_seq'::regclass);

--
-- Data Type: TABLE DATA; Schema: public; Owner: plumcmf
--

COPY ##_adm_cmp (adm_cmp_id, char_id, name, adm_cmp_grp_id, description) FROM stdin;
1 users Пользователи Plum 1 Управление учетными записями и разграничением прав.
2 installer Модули системы  1 Управление модулями системы- загрузка  установка и удаление дополнительных модулей.
3 siteinfo Информация о сайте  1 Изменение информации о сайте такой как название сайта e-mail администратора. Управление режимом техобслуживания.
4 filemanager Файловый менеджер  1 Загрузка файлов на сервер для их последующего использования на сайте.
5 page Структура сайта  2 Управление структурой сайта добавление новых страниц управление их содержимым.
6 newsfeed Новостные ленты  2 Управление новостными лентами.
7 news Новости  2 Управление новостями статьями и другой информацией которая может быть представлена в виде списка элементов
8 photogallery Фотогалереи  2 Управление фотогалереями.
9 photo Фотографии  2 Управление фотографиями.
10 feedback Обратная связь  3 Управление сообщениями поступающими через форму обратной связи на сайте.
11 advplaces Рекламные места  5 Управление рекламными местами для размещения баннеров.
12 advsections Рекламные разделы  5 Управление разделами предназначенными для размещения рекламы.
13 advbanners Рекламные баннеры  5 Управление рекламными баннерами - загрузка изображений изменение кодов и т.д.
14 chpasswd Изменить пароль  6 Изменение пароля текущего пользователя.
15 auth Авторизация  7 Авторизация текущего пользователя. Модуль ядра.
16 content Контент  7 Контент. Модуль ядра.
17 site Сайт 7 Сайт. Модуль ядра.
18 upload Загрузка файлов  7 Загрузка файлов. Модуль ядра.
\.




--
-- Data for Name: adm_cmp_grp; Type: TABLE DATA; Schema: public; Owner: plumcmf
--

COPY ##_adm_cmp_grp (adm_cmp_grp_id, char_id, name) FROM stdin;
1	adm	Администрирование
2	info Информация
3	serv	Сервисы
4	shop	Интернет-магазин
5	adv	Реклама
6	other	Разное
7	core	Ядро
8	front	Фронтэнд
\.


--
-- Data for Name: adm_user; Type: TABLE DATA; Schema: public; Owner: plumcmf
--

COPY ##_adm_user (adm_user_id, username, passwordmd5, email, last_login, last_ip, date_create) FROM stdin;
1	admin	e10adc3949ba59abbe56e057f20f883e	astrikov.d@gmail.com	2012-11-09 11:13:57.684	127.0.0.1	2012-11-09 11:13:57.684
\.


--
-- Data for Name: adm_user_adm_cmp; Type: TABLE DATA; Schema: public; Owner: plumcmf
--

COPY ##_adm_user_adm_cmp (adm_user_adm_cmp_id, adm_user_id, adm_cmp_id) FROM stdin;
1	1	1
2	1	2
3	1	3
\.


--
-- Data for Name: adv_banners; Type: TABLE DATA; Schema: public; Owner: plumcmf
--

COPY ##_adv_banners (adv_banners_id, adv_places_id, adv_sections_id, name, picture, code, date_start, date_end, date0, title, link, vflag) FROM stdin;
\.


--
-- Data for Name: adv_places; Type: TABLE DATA; Schema: public; Owner: plumcmf
--

COPY ##_adv_places (adv_places_id, name, char_id, date0, vflag) FROM stdin;
1	Рекламное место вверху страницы	adv_place_top	2012-11-09 11:13:57.684	t
2	Рекламное место внизу страницы	adv_place_bottom	2012-11-09 11:13:57.684	t
\.


--
-- Data for Name: adv_sections; Type: TABLE DATA; Schema: public; Owner: plumcmf
--

COPY ##_adv_sections (adv_sections_id, name, vflag, date0) FROM stdin;
1	Не показывать баннеры	t	2012-11-09 11:13:57.684
2	Главная страница	t	2012-11-09 11:13:57.684
\.


--
-- Data for Name: feedback; Type: TABLE DATA; Schema: public; Owner: plumcmf
--

COPY ##_feedback (feedback_id, fio, phone, question, answer, date_send, date_ans, vflag) FROM stdin;
\.


--
-- Data for Name: newscat; Type: TABLE DATA; Schema: public; Owner: plumcmf
--

COPY ##_newscat (newscat_id, vflag, date0, name) FROM stdin;
news	t	2012-11-09 11:13:57.684	Новости
articles	t	2012-11-09 11:13:57.684	Статьи
\.


--
-- Data for Name: newsfeed; Type: TABLE DATA; Schema: public; Owner: plumcmf
--

COPY ##_newsfeed (newsfeed_id, newscat_id, name, cnt, vflag, date0) FROM stdin;
\.


--
-- Data for Name: newsitem; Type: TABLE DATA; Schema: public; Owner: plumcmf
--

COPY ##_newsitem (newsitem_id, newsfeed_id, name, anons, body, picture, vflag, date0, keywords, description) FROM stdin;
\.


--
-- Data for Name: page; Type: TABLE DATA; Schema: public; Owner: plumcmf
--

COPY ##_page (page_id, parent_id, page_module_id, page_module_param, url_path, url_name, redirect_url, name, header, before_content, after_content, sort, vflag, aflag, hflag, eflag, date0, keywords, description, adv_sections_id) FROM stdin;
1	\N	homepage		/			Главная				1	t	f	t	t	2012-11-09 11:13:57.684			2
\.


--
-- Data for Name: page_module; Type: TABLE DATA; Schema: public; Owner: plumcmf
--

COPY ##_page_module (page_module_id, name, description, vflag) FROM stdin;
homepage	Главная страница		f
static	Статическая страница		t
news	Новости		f
photo	Фотографии		f
feedback	Обратная связь		t
\.


--
-- Data for Name: photo; Type: TABLE DATA; Schema: public; Owner: plumcmf
--

COPY ##_photo (photo_id, photogallery_id, name, author, description, picture, date_create, vflag) FROM stdin;
\.


--
-- Data for Name: photogallery; Type: TABLE DATA; Schema: public; Owner: plumcmf
--

COPY ##_photogallery (photogallery_id, name, cover, cnt, vflag) FROM stdin;
\.


--
-- Data for Name: prj_feedback; Type: TABLE DATA; Schema: public; Owner: plumcmf
--

COPY ##_prj_feedback (prj_feedback_id, email, rate, body, date_send) FROM stdin;
\.


--
-- Data for Name: siteinfo; Type: TABLE DATA; Schema: public; Owner: plumcmf
--

COPY ##_siteinfo (siteinfo_id, site_id, title, admin_email, mtflag, mt_text) FROM stdin;
1	1	Plum CMF	admin@plumcmf.com	f
\.


--
-- Name: adm_cmp_grp_pkey; Type: CONSTRAINT; Schema: public; Owner: plumcmf; Tablespace:
--

ALTER TABLE ONLY ##_adm_cmp_grp
    ADD CONSTRAINT ##_adm_cmp_grp_pkey PRIMARY KEY (adm_cmp_grp_id);


--
-- name: adm_cmp_pkey; Type: CONSTRAINT; Schema: public; Owner: plumcmf; Tablespace: 
--

ALTER TABLE ONLY ##_adm_cmp
    ADD CONSTRAINT ##_adm_cmp_pkey PRIMARY KEY (adm_cmp_id);


--
-- name: adm_user_adm_cmp_pkey; Type: CONSTRAINT; Schema: public; Owner: plumcmf; Tablespace: 
--

ALTER TABLE ONLY ##_adm_user_adm_cmp
    ADD CONSTRAINT ##_adm_user_adm_cmp_pkey PRIMARY KEY (adm_user_adm_cmp_id);


--
-- name: adm_user_pkey; Type: CONSTRAINT; Schema: public; Owner: plumcmf; Tablespace: 
--

ALTER TABLE ONLY ##_adm_user
    ADD CONSTRAINT ##_adm_user_pkey PRIMARY KEY (adm_user_id);


--
-- name: adv_banners_pkey; Type: CONSTRAINT; Schema: public; Owner: plumcmf; Tablespace: 
--

ALTER TABLE ONLY ##_adv_banners
    ADD CONSTRAINT ##_adv_banners_pkey PRIMARY KEY (adv_banners_id);


--
-- name: adv_places_pkey; Type: CONSTRAINT; Schema: public; Owner: plumcmf; Tablespace: 
--

ALTER TABLE ONLY ##_adv_places
    ADD CONSTRAINT ##_adv_places_pkey PRIMARY KEY (adv_places_id);


--
-- name: adv_sections_pkey; Type: CONSTRAINT; Schema: public; Owner: plumcmf; Tablespace: 
--

ALTER TABLE ONLY ##_adv_sections
    ADD CONSTRAINT ##_adv_sections_pkey PRIMARY KEY (adv_sections_id);


--
-- name: feedback_pkey; Type: CONSTRAINT; Schema: public; Owner: plumcmf; Tablespace: 
--

ALTER TABLE ONLY ##_feedback
    ADD CONSTRAINT ##_feedback_pkey PRIMARY KEY (feedback_id);


--
-- name: newscat_pkey; Type: CONSTRAINT; Schema: public; Owner: plumcmf; Tablespace: 
--

ALTER TABLE ONLY ##_newscat
    ADD CONSTRAINT ##_newscat_pkey PRIMARY KEY (newscat_id);


--
-- name: newsfeed_pkey; Type: CONSTRAINT; Schema: public; Owner: plumcmf; Tablespace: 
--

ALTER TABLE ONLY ##_newsfeed
    ADD CONSTRAINT ##_newsfeed_pkey PRIMARY KEY (newsfeed_id);


--
-- name: newsitem_pkey; Type: CONSTRAINT; Schema: public; Owner: plumcmf; Tablespace: 
--

ALTER TABLE ONLY ##_newsitem
    ADD CONSTRAINT ##_newsitem_pkey PRIMARY KEY (newsitem_id);


--
-- name: page_module_pkey; Type: CONSTRAINT; Schema: public; Owner: plumcmf; Tablespace: 
--

ALTER TABLE ONLY ##_page_module
    ADD CONSTRAINT ##_page_module_pkey PRIMARY KEY (page_module_id);


--
-- name: page_pkey; Type: CONSTRAINT; Schema: public; Owner: plumcmf; Tablespace: 
--

ALTER TABLE ONLY ##_page
    ADD CONSTRAINT ##_page_pkey PRIMARY KEY (page_id);


--
-- name: photo_pkey; Type: CONSTRAINT; Schema: public; Owner: plumcmf; Tablespace: 
--

ALTER TABLE ONLY ##_photo
    ADD CONSTRAINT ##_photo_pkey PRIMARY KEY (photo_id);


--
-- name: photogallery_pkey; Type: CONSTRAINT; Schema: public; Owner: plumcmf; Tablespace: 
--

ALTER TABLE ONLY ##_photogallery
    ADD CONSTRAINT ##_photogallery_pkey PRIMARY KEY (photogallery_id);


--
-- name: prj_feedback_pkey; Type: CONSTRAINT; Schema: public; Owner: plumcmf; Tablespace: 
--

ALTER TABLE ONLY ##_prj_feedback
    ADD CONSTRAINT ##_prj_feedback_pkey PRIMARY KEY (prj_feedback_id);


--
-- name: siteinfo_pkey; Type: CONSTRAINT; Schema: public; Owner: plumcmf; Tablespace: 
--

ALTER TABLE ONLY ##_siteinfo
    ADD CONSTRAINT ##_siteinfo_pkey PRIMARY KEY (siteinfo_id);



--
-- name: adm_cmp_adm_cmp_grp_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: plumcmf
--

ALTER TABLE ONLY ##_adm_cmp
    ADD CONSTRAINT ##_adm_cmp_adm_cmp_grp_id_fkey FOREIGN KEY (adm_cmp_grp_id) REFERENCES ##_adm_cmp_grp(adm_cmp_grp_id);


--
-- name: adm_user_adm_cmp_adm_cmp_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: plumcmf
--

ALTER TABLE ONLY ##_adm_user_adm_cmp
    ADD CONSTRAINT ##_adm_user_adm_cmp_adm_cmp_id_fkey FOREIGN KEY (adm_cmp_id) REFERENCES ##_adm_cmp(adm_cmp_id);


--
-- name: adm_user_adm_cmp_adm_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: plumcmf
--

ALTER TABLE ONLY ##_adm_user_adm_cmp
    ADD CONSTRAINT ##_adm_user_adm_cmp_adm_user_id_fkey FOREIGN KEY (adm_user_id) REFERENCES ##_adm_user(adm_user_id);


--
-- name: adv_banners_adv_places_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: plumcmf
--

ALTER TABLE ONLY ##_adv_banners
    ADD CONSTRAINT ##_adv_banners_adv_places_id_fkey FOREIGN KEY (adv_places_id) REFERENCES ##_adv_places(adv_places_id);


--
-- name: adv_banners_adv_sections_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: plumcmf
--

ALTER TABLE ONLY ##_adv_banners
    ADD CONSTRAINT ##_adv_banners_adv_sections_id_fkey FOREIGN KEY (adv_sections_id) REFERENCES ##_adv_sections(adv_sections_id);


--
-- name: newsfeed_newscat_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: plumcmf
--

ALTER TABLE ONLY ##_newsfeed
    ADD CONSTRAINT ##_newsfeed_newscat_id_fkey FOREIGN KEY (newscat_id) REFERENCES ##_newscat(newscat_id);


--
-- name: newsitem_newsfeed_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: plumcmf
--

ALTER TABLE ONLY ##_newsitem
    ADD CONSTRAINT ##_newsitem_newsfeed_id_fkey FOREIGN KEY (newsfeed_id) REFERENCES ##_newsfeed(newsfeed_id);


--
-- name: page_adv_sections_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: plumcmf
--

ALTER TABLE ONLY ##_page
    ADD CONSTRAINT ##_page_adv_sections_id_fkey FOREIGN KEY (adv_sections_id) REFERENCES ##_adv_sections(adv_sections_id);


--
-- name: page_page_module_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: plumcmf
--

ALTER TABLE ONLY ##_page
    ADD CONSTRAINT ##_page_page_module_id_fkey FOREIGN KEY (page_module_id) REFERENCES ##_page_module(page_module_id);


--
-- name: page_parent_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: plumcmf
--

ALTER TABLE ONLY ##_page
    ADD CONSTRAINT ##_page_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES ##_page(page_id);


--
-- name: photo_photogallery_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: plumcmf
--

ALTER TABLE ONLY ##_photo
    ADD CONSTRAINT ##_photo_photogallery_id_fkey FOREIGN KEY (photogallery_id) REFERENCES ##_photogallery(photogallery_id);


--
-- name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

