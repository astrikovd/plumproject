<!DOCTYPE html>
<html>
<head>
  <title>PlumCMF - Установка</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="language" content="ru" />
  <link rel="stylesheet" type="text/css" href="/install/css/style.css" media="screen, projection" />
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
  <script type="text/javascript" src="/install/js/jquery.form.js"></script>

  <script>
    var options = {
      success: function(result) {
        if(result.success) {
          $('#id-form-result').removeClass('alert-error');
          $('#id-form-result').addClass('alert alert-success');
          $('#id-cfg').html(result.cfg);
          $('#id-cfg').slideDown('fast');

          if(window.location.search.match('debug=1')){
            $('#debug').show();
            $('#id-out').html(result.out);
            $('#id-out').slideDown('fast');
          }
        }
        else {
          $('#id-form-result').removeClass('alert-success');
          $('#id-form-result').addClass('alert alert-error');
        }
        $('#id-form-result').html(result.msg);
        $('#id-form-result').show();
      },
      dataType: 'json'
    };

    $(function(){
      $('#id-db-form').ajaxForm(options);
      $('#id-dbdriver').change(function(){
        if($(this).val() == 'pgsql') {
          $('#id-dbport').val('5432');
        } else {
          $('#id-dbport').val('3306');
        }
      });
      $('#id-mode').change(function(){
        if($(this).val() == 'install') {
          $('#wrapper-oldtableprefix').hide();
        } else {
          $('#wrapper-oldtableprefix').show();
        }
      });
    });
  </script>
</head>
<body>

<?php
/**
 * Copyright (c) 2012 by Dmitry Astrikov / www.astrikov.ru
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
?>

<div id="wrapper">
  <div id="content">
    <div class="page-header">
      <h1>PlumCMF</h1>
      <p>Мастер установки</p>
    </div>
    <div class="alert alert-error">
      <strong>Внимание!</strong> После установки обязательно удалите директорию <strong><?=dirname(__FILE__)?></strong>
    </div>
    <form method="POST" id="id-db-form" action="/install/db_install.php">
      <legend>Информация о сайте:</legend>
      <div>
        <label for="id-site-url">Адрес сайта:</label>
        <input type="text" name="site_url" id="id-site-url" value="http://" required="required"/>
      </div>
      <legend>Режим установки</legend>
      <div>
        <label for="id-mode">Режим установки:</label>
        <select name="mode" id="id-mode">
          <option value="install">Установить PlumCMF</option>
          <option value="reinstall">Переустановить PlumCMF</option>
        </select>
        <div id="wrapper-oldtableprefix" style="display: none;">
          <label for="id-oldtableprefix">Старые префиксы таблиц:</label>
          <input type="text" name="oldtableprefix" id="id-oldtableprefix"/>
        </div>
      </div>
      <legend>Настройки соединения с базой данных:</legend>
      <div>
        <label for="id-dbdriver">СУБД:</label>
        <select name="dbdriver" id="id-dbdriver">
          <option value="pgsql">PostgreSQL</option>
          <option value="mysql">MySQL</option>
        </select>
      </div>
      <div>
        <label for="id-dbname">Название БД:</label>
        <input type="text" name="dbname" id="id-dbname" required="required"/>
      </div>
      <div>
        <label for="id-dbhost">Хост:</label>
        <input type="text" name="dbhost" id="id-dbhost" value="localhost"/>
      </div>
      <div>
        <label for="id-dbport">Порт:</label>
        <input type="text" name="dbport" id="id-dbport" required="required" value="5432"/>
      </div>
      <div>
        <label for="id-dbuser">Имя пользователя:</label>
        <input type="text" name="dbuser" id="id-dbuser" required="required"/>
      </div>
      <div>
        <label for="id-dbpassword">Пароль:</label>
        <input type="password" name="dbpassword" id="id-dbpassword" required="required"/>
      </div>
      <div>
        <label for="id-tableprefix">Использовать префиксы для таблиц:</label>
        <input type="text" name="tableprefix" id="id-tableprefix" required="required"/>
      </div>
      <div>
        <input type="submit" name="check_db" id="id-submit" value="Применить настройки"/>
      </div>
      <div id="id-form-result"></div>
    </form>
    <textarea id="id-cfg"></textarea>
    <div id="footer">

    </div>
  </div>
</div>
</body>
</html>