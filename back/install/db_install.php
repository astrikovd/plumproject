<?php
if(isset($_POST["check_db"]))
{
  $consolePath= realpath( dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'var'.DIRECTORY_SEPARATOR.'cli'.DIRECTORY_SEPARATOR.'dbmigrate').DIRECTORY_SEPARATOR;

  $consoleTpl = file_get_contents($consolePath.'config'.DIRECTORY_SEPARATOR.'console.php.tpl');


  list($username, $password, $dbname, $dbport, $dbhost) = array($_POST["dbuser"], $_POST["dbpassword"], $_POST["dbname"], $_POST["dbport"], $_POST["dbhost"]);
  $adm_url          = $_SERVER["HTTP_HOST"];
  $site_url         = $_POST["site_url"];

  $mode             = $_POST["mode"];
  $dbdriver         = $_POST["dbdriver"];
  $table_prefix     = $_POST['tableprefix'];
  $table_prefix     = empty($table_prefix) ? '' : $table_prefix;

  if($table_prefix!='' && strpos($table_prefix, '_')===false)
  {
    $table_prefix  .='_';
    $table_prefix_config = "'tablePrefix'=>'{$table_prefix}'";
  }


  $consoleTpl = str_replace(
    array('##type##','##host##','##database##','##username##','##password##','##tablePrefix##',),
    array($dbdriver,$dbhost,$dbname,$username,$password,$table_prefix),
    $consoleTpl
  );

  file_put_contents( $consolePath.'config'.DIRECTORY_SEPARATOR.'console.php',$consoleTpl );

  $baseCmd = 'php '.$consolePath.'yiic.php contextmigrate ';
  if( $mode == 'reinstall' )
  {
    exec( $baseCmd.' autodown '.$dbdriver.' 100', $ret);
  }
  exec( $baseCmd.' autoup '.$dbdriver.' 100',$ret);

  $okCnt = 0;
  foreach($ret AS $outStr)
  {
    if($dbdriver=='mysql' && preg_match('/^.*execute SQL\: SET foreign_key_checks = 1; \.\.\. done.*$/',$outStr))
    {
      $okCnt++;
    }
    elseif($dbdriver=='pgsql' && preg_match('/^.*GRANT ALL ON SCHEMA public TO PUBLIC.*$/',$outStr))
    {
      $okCnt++;
    }
  }
  if($dbdriver=='mysql'){
    $out = $mode == 'reinstall' ? $okCnt==2 : $okCnt==1;
  }
  else
  {
    $out = $okCnt==1;
  }

  $cfg =
      "<?php
    /**
     * Main configuration
     * @package config
     */
    return CMap::mergeArray(
        require(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'etc'. DIRECTORY_SEPARATOR . 'sys.php'),
        array(
            'components'=>array(
                'user'=>array(
                    'allowAutoLogin'=>true,
                    'loginUrl'=>array('auth/user/login'),
                ),
                'db'=>array(
                    'connectionString' => '$dbdriver:host=$dbhost;dbname=$dbname',
                    'emulatePrepare' => true,
                    'username' => '$username',
                    'password' => '$password',
                    'charset' => 'utf8',
                    $table_prefix_config
                ),
            ),
            'params'=>array(
                'back_url' => 'http://$adm_url',
                'front_url' => '$site_url',
            ),
            'modules' => array(
                'site',
                'auth' => array(
                    'defaultController' => 'user'
                ),
                'content',
                'user',
                'page',
                'newsfeed',
                'news',
                'feedback',
                'photo',
                'photogallery',
                'advsections',
                'advplaces',
                'advbanners',
                'siteinfo',
                'installer'
            ),
        )
    );";

  $result = array('success' => empty($out)  ? false : true,
    'msg' => empty($out) ? 'Невозможно создать таблицы БД. Проверьте правильность указанных настроек' : 'Таблицы успешно созданы. Поместите эту конфигурацию в файл /app/config/main.php.',
    'cfg' => $cfg
  );
  echo json_encode($result);
  die();
}